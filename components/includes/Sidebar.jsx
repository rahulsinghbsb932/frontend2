import React, { useState } from "react";
import { AdminList } from "./AdminList";
import Link from 'next/link';
import Image from 'next/image';

const Sidebar = () => {
  const [menu, setMenu] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const toggleMenu = () => {
    setIsOpen(!isOpen);
  };

  console.log(AdminList);

  return (
    <React.Fragment>
      <div className="sidebar">
        <div className="menu">
          <nav className={isOpen ? "open" : ""}>
          <span onClick={toggleMenu} id="toggle" className="toggle">
          <div className="hamburger">
            <i></i>
            <i></i>
            <i></i>
          </div>
          </span>
            {
              <div className="side-nav">
                {AdminList.map((value) => {
                  return (
                    <>
                      <div className="admin-flex">
                        {/* <img className="admin-icon" src={value.img} alt="" /> */}
                        <Image
                          src={value.img}
                          width={30}
                          height={30}
                          alt="img"
                        />
                        <Link href={`${value.url}`}><h2 className="admin-name">{value.name}</h2></Link>
                      </div>
                    </>
                  );
                })}
              </div>
            }
          </nav>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Sidebar;
