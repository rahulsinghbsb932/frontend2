
import Dashboard from "public/images/admin-menu-icon/dashboard.svg";
import Wallet from "public/images/admin-menu-icon/Wallet.svg";
import Accademic from "public/images/admin-menu-icon/Accademic.svg";
import Certificates from "public/images/admin-menu-icon/Certificates.svg";
import Profiles from "public/images/admin-menu-icon/Profiles.svg";
import Resume from "public/images/admin-menu-icon/Resume Builder.svg";
import Higher from "public/images/admin-menu-icon/Higher Education.svg";
import Competition from "public/images/admin-menu-icon/Competition.svg";
import Scholarship from "public/images/admin-menu-icon/Scholarship.svg";
import Job from "public/images/admin-menu-icon/Job Marketplace.svg";
import Social from "public/images/admin-menu-icon/Social Media Sharing.svg";
import Financial from "public/images/admin-menu-icon/Financial.svg";

export const AdminList = [
    {
        "name": "Dashboard",
        "img": Dashboard,
        "url":'/dashboard'
    },
    {
        "name": "Wallet",
        "img": Wallet,
         "url": '/Admin/Wallet/DocumentList'
    },
    {
        "name": "Accademic",
        "img": Accademic,
        "url": '/Admin/Academic/Academic'
    },
    {
        "name": "Certificates",
        "img": Certificates,
        "url" : '/certificate'
    },
    {
        "name": "Profiles",
        "img": Profiles,
         "url": '/Admin/Profile'
    },
    {
        "name": "School Registration",
        "img": Profiles,
         "url": '/register'
    },
    {
        "name": "Resume Builder",
        "img": Resume
    },
    {
        "name": "Higher Education",
        "img": Higher
    },
    {
        "name": "Competition",
        "img": Competition
    },
    {
        "name": "Scholarship",
        "img": Scholarship
    },
    {
        "name": " Job Marketplace",
        "img": Job
    },
    {
        "name": "Social Media Sharing",
        "img": Social
    },
    {
        "name": "Financial",
        "img": Financial
    }
]