import React from "react";
import Link from "next/link";
import { Container, Navbar, Nav, NavDropdown } from "react-bootstrap";
import { HomeFilled, LoginOutlined } from "@ant-design/icons";
import { LogowIcon, LogobIcon } from "../Icons";
import { useLocation } from "react-router-dom";

export default function Header() {
  return (
    <>
      <Navbar className="customBgNav mb-3" expand="lg">
        <Container fluid>
          <div className="logo_White logo_White-m">
            <Link href="/educational-institutions">
              <LogowIcon />
            </Link>
          </div>
          <Navbar.Toggle aria-controls="offcanvasNavbar-expand-lg" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className=" mr-auto navBarRight">
              <div className="MenuItems">
                <Link href="/">
                  <HomeFilled style={{ fontSize: "150%" }} />
                </Link>
              </div>
              <NavDropdown className="MenuItems" title="Solution">
                <NavDropdown.Item>
                  <div>
                    <Link href="/educational-institutions">
                      Educational Institutions
                    </Link>
                  </div>
                  <NavDropdown className="MenuItems" title="Solution">
                    <NavDropdown.Item>
                      <Link href="/educational-institutions">
                        Educational Institutions
                      </Link>
                    </NavDropdown.Item>
                    <NavDropdown.Item>
                      <Link href="/school-course-details">
                        School Course Details
                      </Link>
                    </NavDropdown.Item>
                    <NavDropdown.Item>
                      <Link href="/certified-corporate">Corporate</Link>
                    </NavDropdown.Item>
                    <NavDropdown.Item>
                      <Link href="/event-management-certifications">
                        Events & Competitions
                      </Link>
                    </NavDropdown.Item>
                    <NavDropdown.Item>
                      <Link href="/certified-academy">
                        Accademy & Coaching Institutes
                      </Link>
                    </NavDropdown.Item>
                    <NavDropdown.Item>
                      <Link href="/student-document-certification">
                        Student
                      </Link>
                    </NavDropdown.Item>
                    <NavDropdown.Item>
                      <Link href="/employee-document-certification">
                        Employee
                      </Link>
                    </NavDropdown.Item>
                  </NavDropdown>
                  <NavDropdown className="MenuItems" title="Services">
                    <NavDropdown.Item>
                      <Link href="/digital-certification">
                        Digital Certificates
                      </Link>
                    </NavDropdown.Item>
                    <NavDropdown.Item>
                      <Link href="/digital-wallet">Digital Wallet</Link>
                    </NavDropdown.Item>
                    <NavDropdown.Item>
                      <Link href="/job-marketplace">Job Marketplace</Link>
                    </NavDropdown.Item>
                    <NavDropdown.Item>
                      <Link href="/scholarship">Scholarship</Link>
                    </NavDropdown.Item>

                    <NavDropdown.Item>
                      <Link href="/background-checks-degree-certificate-verification">
                        Background Verfication
                      </Link>
                    </NavDropdown.Item>
                    <NavDropdown.Item>
                      <Link href="/admissions">Admissions</Link>
                    </NavDropdown.Item>
                    <NavDropdown.Item>
                      <Link href="/white-label-solution">
                        White Label Solution
                      </Link>
                    </NavDropdown.Item>
                    <NavDropdown.Item>
                      <Link href="/automated-resume-builder">
                        Automated Resume Builder
                      </Link>
                    </NavDropdown.Item>
                    <NavDropdown.Item>
                      <Link href="/template-database">Templates Database</Link>
                    </NavDropdown.Item>
                    <NavDropdown.Item>
                      <Link href="/talent-pool">Talent Pool</Link>
                    </NavDropdown.Item>
                  </NavDropdown>
                  <NavDropdown className="MenuItems" title="Resources">
                    <NavDropdown.Item>
                      Scholarship <span class=" blink_me"> Coming Soon</span>
                    </NavDropdown.Item>
                    <NavDropdown.Item>
                      Jobs <span class=" blink_me"> Coming Soon</span>
                    </NavDropdown.Item>
                    <NavDropdown.Item>
                      Result <span class=" blink_me"> Coming Soon</span>
                    </NavDropdown.Item>
                    <NavDropdown.Item>
                      Study Abroad <span class=" blink_me"> Coming Soon</span>
                    </NavDropdown.Item>
                    <NavDropdown.Item>
                      Admissions <span class=" blink_me"> Coming Soon</span>
                    </NavDropdown.Item>
                    <NavDropdown.Item>
                      <Link href="/courses">Courses</Link>
                      {/* Courses <span class=" blink_me"> Coming Soon</span> */}
                    </NavDropdown.Item>
                    <NavDropdown.Item>
                      Competitive exams
                      <span class=" blink_me"> Coming Soon</span>
                    </NavDropdown.Item>
                  </NavDropdown>
                </NavDropdown.Item>
                <NavDropdown.Item>
                  <Link href="/certified-corporate">Corporate</Link>
                </NavDropdown.Item>
                <NavDropdown.Item>
                  <Link href="/event-management-certifications">
                    Events & Competitions
                  </Link>
                </NavDropdown.Item>
                <NavDropdown.Item>
                  <Link href="/certified-academy">
                    Accademy & Coaching Institutes
                  </Link>
                </NavDropdown.Item>
                <NavDropdown.Item>
                  <Link href="/student-document-certification">Student</Link>
                </NavDropdown.Item>
                <NavDropdown.Item>
                  <Link href="/employee-document-certification">Employee</Link>
                </NavDropdown.Item>
              </NavDropdown>
              <NavDropdown className="MenuItems" title="Services">
                <NavDropdown.Item>
                  <Link href="/digital-certification">
                    Digital Certificates
                  </Link>
                </NavDropdown.Item>
                <NavDropdown.Item>
                  <Link href="/digital-wallet">Digital Wallet</Link>
                </NavDropdown.Item>
                <NavDropdown.Item>
                  <Link href="/job-marketplace">Job Marketplace</Link>
                </NavDropdown.Item>
                <NavDropdown.Item>
                  <Link href="/scholarship">Scholarship</Link>
                </NavDropdown.Item>

                <NavDropdown.Item>
                  <Link href="/background-checks-degree-certificate-verification">
                    Background Verfication
                  </Link>
                </NavDropdown.Item>
                <NavDropdown.Item>
                  <Link href="/admissions">Admissions</Link>
                </NavDropdown.Item>
                <NavDropdown.Item>
                  <Link href="/white-label-solution">White Label Solution</Link>
                </NavDropdown.Item>
                <NavDropdown.Item>
                  <Link href="/automated-resume-builder">
                    Automated Resume Builder
                  </Link>
                </NavDropdown.Item>
                <NavDropdown.Item>
                  <Link href="/template-database">Templates Database</Link>
                </NavDropdown.Item>
                <NavDropdown.Item>
                  <Link href="/talent-pool">Talent Pool</Link>
                </NavDropdown.Item>
              </NavDropdown>
              <NavDropdown className="MenuItems" title="Resources">
                <NavDropdown.Item>
                  <Link href="/Scholarships">Scholarship</Link>
                </NavDropdown.Item>
                <NavDropdown.Item>
                  <Link href="/JobList">Jobs</Link>
                </NavDropdown.Item>
                <NavDropdown.Item>
                  <Link href="/Result">Result</Link>
                </NavDropdown.Item>
                <NavDropdown.Item>
                  <Link href="study-abroad">Study Abroad </Link>
                </NavDropdown.Item>
                <NavDropdown.Item>
                  Admissions <span class=" blink_me"> Coming Soon</span>
                </NavDropdown.Item>
                <NavDropdown.Item>
                  <Link href="/courses">Courses</Link>
                </NavDropdown.Item>
                <NavDropdown.Item>
                  <Link href="/CompetitiveExam">Competitive exams</Link>
                </NavDropdown.Item>
              </NavDropdown>

              <NavDropdown className="MenuItems" title="Company">
                <NavDropdown.Item>
                  <Link href="/about-us">About Us</Link>
                </NavDropdown.Item>
                <NavDropdown.Item>
                  <Link href="/contact-us">Contact Us</Link>
                </NavDropdown.Item>
                <NavDropdown.Item>
                  We are Hiring <span class=" blink_me"> Coming Soon</span>
                </NavDropdown.Item>
              </NavDropdown>
              <div className="MenuItems">
                <a
                  href="https://www.verifykiya.com/blog/"
                  target="_blank"
                  className="MenuItems "
                >
                  Blog
                </a>
              </div>
              <LoginOutlined
                style={{ fontSize: "150%", color: "#000" }}
                className="MenuItems login"
              />
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
}
