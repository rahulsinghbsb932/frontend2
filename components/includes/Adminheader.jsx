import React, { useState } from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";

const Adminheader = () => {
  const [active, setActive] = useState(false);
  const router = useRouter();

  const handleLogout = () => {
    localStorage.removeItem("token"); 
    router.push("/login");
  };

  return (
    <React.Fragment>
      <div className="main-header">
        <>
          <Row className="align-items-center">
            <Col lg={6} xs={4}>
              <div className="logo">
                <>
                  <Image
                    src="/images/logo_B.png"
                    width={70}
                    height={60}
                    alt="logo"
                  />
                </>
              </div>
            </Col>
            <Col lg={6} className="d-none">
              <div className="search">
                <Form>
                  {/* <InputGroup size="sm">
                    <InputGroup.Text id="inputGroup-sizing-sm">
                      <div>
                        <img
                          src={require("../../assets/img/search.png")}
                          className="img-fluid"
                        />
                      </div>
                    </InputGroup.Text>
                    <Form.Control
                      aria-label="Small"
                      aria-describedby="inputGroup-sizing-sm"
                      placeholder="Search here..."
                    />
                  </InputGroup> */}
                </Form>
              </div>
            </Col>
            <Col lg={6} xs={8}>
              <div className="d-flex align-items-center justify-content-end">
                <div className="notifi">
                  <i></i>
                  <i></i>
                  <i></i>
                  <i></i>
                </div>

                <div
                  className="profile-img"
                  onClick={() => setActive(!active)}
                >
                  <img
                    className="img-fluid"
                    src="https://images.unsplash.com/photo-1602233158242-3ba0ac4d2167?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8Z2lybHxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60"
                    alt="img"
                  />
                  <div className={active ? "drop" : "d-none"}>
                    <ul>
                      <li onClick={handleLogout}>Logout</li>
                    </ul>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </>
      </div>
    </React.Fragment>
  );
};

export default Adminheader;