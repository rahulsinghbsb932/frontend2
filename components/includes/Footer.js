import Link from "next/link";
import React from "react";
import {
  LocationIcon,
  MailIcon,
  FacebookIcon,
  TwitterIcon,
  LinkedInIcon,
  InstagramIcon,
  LogowIcon,
} from "../Icons";

export default function PageFooter() {
  return (
    <div>
      <footer>
        <div className="container-fluid">
          <div className="row">
            <div className="col-lg-4 order-lg-1 order-2">
              <div className="FooterBox1">
                <div className="logo_White">
                  <a href="/">
                    <LogowIcon />
                  </a>
                </div>
                <div className="aboutaddressfooter">
                  <p>
                    World’s only Blockchain-Based NFT enabled solution for
                    Certification!
                  </p>
                  <ul className="footerContectDetailsList">
                    <li>
                      <div>
                        <LocationIcon />
                      </div>{" "}
                      UTC Tower, S1 Floor,Sector 132 Noida, UP 201301
                    </li>
                    <li className="mt-2 text-decoration-underline">
                      <div>
                        <MailIcon />
                      </div>
                      <Link href="mailTo:info@verifykiya.com">
                        info@verifykiya.com
                      </Link>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-lg-3 col-4 order-lg-2 order-1">
              <div className="QuickLinks">
                <h3>Quick Links</h3>
                <ul className="QuickLinksLits">
                  <li>
                    {" "}
                    <Link href="/certified-corporate">Corporate</Link>
                  </li>
                  <li>
                    {" "}
                    <Link href="/student-document-certification">Student</Link>
                  </li>
                  <li>
                    <Link href="/employee-document-certification">
                      Employee
                    </Link>{" "}
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-lg-3 col-4 order-lg-2 order-1">
              <div className="QuickLinks">
                <h3>Company</h3>
                <ul className="QuickLinksLits">
                  <li>
                    {" "}
                    <Link href="/about-us">About Us</Link>
                  </li>
                  <li>
                    {" "}
                    <Link href="/contact-us">Contact Us</Link>
                  </li>
                  <li>
                    <Link
                      href="https://www.verifykiya.com/blog/"
                      target="_blank"
                    >
                      Blog
                    </Link>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-lg-2 col-4 order-lg-2 order-1">
              <div className="QuickLinks">
                <h3>Links</h3>
                <ul className="QuickLinksLits">
                  <li>
                    <Link href="/disclaimer">Disclaimer</Link>
                  </li>
                  <li>
                    <Link href="/privacy-policy">Privacy Policy</Link>
                  </li>
                  <li>
                    {" "}
                    <Link href="/terms-of-use">Terms of Use</Link>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div className="CopyRightsWrapper">
          <div className="container">
            <div className="row">
              <div className="col-lg-7 col-4">
                <div className="SocialIcons footerSocialicons">
                  <ul>
                    <li>
                      <a
                        href="https://www.facebook.com/verifykiya"
                        target="_blank"
                      >
                        {" "}
                        <FacebookIcon />
                      </a>
                    </li>
                    <li>
                      <a href="https://twitter.com/verfykiya" target="_blank">
                        {" "}
                        <TwitterIcon />
                      </a>
                    </li>
                    <li>
                      <a
                        href="https://www.linkedin.com/company/verifykiya"
                        target="_blank"
                      >
                        {" "}
                        <LinkedInIcon />
                      </a>
                    </li>
                    <li>
                      <a
                        href="https://www.instagram.com/verifykiya/"
                        target="_blank"
                      >
                        {" "}
                        <InstagramIcon />
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="col-lg-5 col-8 d-flex align-items-center justify-content-end">
                <div className="copyrightContent">
                  <p className="bottom">
                    © 2023 Created by Blockwise Solution Pvt. Ltd.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </div>
  );
}
