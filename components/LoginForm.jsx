import React, { useState } from "react";
import { useRouter } from "next/router";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import Button from "react-bootstrap/Button";
import Link from "next/link";
import axios from "axios"; // Import Axios

const LoginForm = () => {
  const [email, setEmail] = useState("");
  const [otp, setOTP] = useState("");
  const [otpSent, setOTPSent] = useState(false);
  const router = useRouter();

  const handleSendOTP = async (e) => {
    e.preventDefault();

    try {
      const response = await axios.post(
        "https://verifykiya.com/api/v1/login",
        { email },
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      console.log("response----", response);
      if (response.status === 200) {
        setOTPSent(true);
        console.log("OTP sent successfully!");
      } else {
        console.log("Failed to send OTP. Please try again.");
      }
    } catch (error) {
      console.log(error);
      console.log("An error occurred. Please try again later.");
    }
  };

  const handleVerifyOTP = async (e) => {
    e.preventDefault();

    try {
      const response = await axios.post(
        "https://verifykiya.com/api/v1/login/verify",
        { email, otp },
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      console.log("response", response);
      console.log("response token", response.data.token);
      if (response.status === 200) {
        const token = response.data.token;
        localStorage.setItem("token", token);
        const getToken = localStorage.getItem("token");
        console.log("display store token", getToken);
        console.log("OTP verified successfully!");
        router.push("/Admin/Dashboard/UserDashboard"); // Redirect to the dashboard page
      } else {
        console.log("Failed to verify OTP. Please try again.");
      }
    } catch (error) {
      console.log(error);
      console.log("An error occurred. Please try again later.");
    }
  };

  const handleEmailChange = (e) => {
    setEmail(e.target.value);
  };

  const handleOTPChange = (e) => {
    setOTP(e.target.value);
  };

  return (
    <div>
      <div className="top-blue-frame"></div>
      <div className="top-frame"></div>
      <div className="ms-5">
        <h1>Sign Up</h1>
        <p>Create a new account for One-stop solution</p>
      </div>
      <Form>
        <div className="box my-5">
          <div className="google-icon"></div>
          <div> Sign in with Google</div>
        </div>
        <div className="line">
          <div className="h">or</div>
        </div>
        <InputGroup size="sm" className="my-3">
          <InputGroup.Text id="inputGroup-sizing-sm">
            <div>
              <img
                alt="   "
                src="/images/Login/msg.svg"
                className="img-fluid me-2"
              />
            </div>
          </InputGroup.Text>
          <Form.Control
            aria-label="Small"
            aria-describedby="inputGroup-sizing-sm"
            placeholder="Sign in with Phone or Email"
            value={email}
            onChange={handleEmailChange}
          />
        </InputGroup>
        {otpSent && (
          <InputGroup size="sm" className="mb-3">
            <InputGroup.Text id="inputGroup-sizing-sm">
              <div>
                <img
                  alt="   "
                  src="/images/Login/lock.svg"
                  className="img-fluid me-2"
                />
              </div>
            </InputGroup.Text>
            <Form.Control
              aria-label="Small"
              aria-describedby="inputGroup-sizing-sm"
              placeholder="OTP"
              value={otp}
              onChange={handleOTPChange}
            />
          </InputGroup>
        )}
        <Form.Group className="mb-3">
          <Form.Check
            required
            label={
              <span>
                I agree to the
                <Link
                  href={"/terms-of-use"}
                  target="_blank"
                  rel="noopener noreferrer"
                  style={{ color: "blue" }}
                >
                  &nbsp;Terms and Conditions
                </Link>
              </span>
            }
            feedback="You must agree before submitting."
            feedbackType="invalid"
          />
        </Form.Group>
        <div className="text-center mt-3">
          {!otpSent ? (
            <Button type="Send" onClick={handleSendOTP}>
              Send
            </Button>
          ) : (
            <Button type="Verify" onClick={handleVerifyOTP}>
              Verify
            </Button>
          )}
        </div>
      </Form>
    </div>
  );
};

export default LoginForm;