import { loggedInUserType } from "helpers/Template";
import Cookies from "js-cookie";
import jwt from "jsonwebtoken";

// set access token and refresh token
export function setToken(token) {
  Cookies.set("access_token", token);
}

// get access token
export function getAccessToken() {
  return Cookies.get("access_token");
}

// get refresh token
export function getRefreshToken() {
  return Cookies.get("refresh_token");
}

// clear access and refresh token
export function clearToken() {
  Cookies.remove("access_token");
  Cookies.remove("refresh_token");
  localStorage.removeItem("homeUrl");
  localStorage.removeItem("sideNav");
}

// get logged in user data
export function getUserData() {
  let userToken = getAccessToken();
  let data;
  if (userToken) {
    data = jwt.decode(userToken);
  }
  return data;
}

// get logged in user data
export function getUserID() {
  let data = getUserData();
  let id;
  if (data && data?.companyUserId) {
    id = data.companyUserId;
  } else {
    id = data?.userId || "";
  }
  return id;
}

export function getUserSlug() {
  let uData = getUserData();
  return uData?.userSlug;
}

export function loggedInAsCode() {
  let data = getUserData();
  let type = "";
  if (data?.userType && data?.userType != "admin") {
    type = data?.userType || "";
  } else {
    type = data?.loginAs || "";
  }
  return type;
}

export function loggedInAs() {
  let data = getUserData();

  let type = "";
  if (data?.userType && data?.userType != "admin") {
    type = loggedInUserType[parseInt(data?.userType)];
  } else {
    type = loggedInUserType[parseInt(data?.loginAs)];
  }

  return type;
}

export function isUserPermission() {
  let uData = getUserData();

  if (uData?.permissionTypes == "2" || uData?.permissions == "all") {
    return true;
  } else {
    return false;
  }
}
