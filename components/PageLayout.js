import React, { useEffect, useState } from "react";
import PageFooter from "./includes/Footer";
import PageHeader from "./includes/HeaderInner";


function PageLayout(props) {
  let { children } = props;

  return (
    <div className="websiteLayout">
      {/* <PageHeader /> */}
      {children}
      {/* <PageFooter /> */}
    </div>
  );
}

export default PageLayout;
