import Image from "next/image";
import { Card } from "react-bootstrap";

const CustomCard = (props) => {
    return (
        <div>
            <Card className="CardSol">
                    <Image
                      src={props.src}
                      layout="fill"
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">{props.title}</Card.Title>
                      <Card.Text>
                        {props.text}
                      </Card.Text>
                    </Card.Body>
            </Card>
        </div>
    );
}

export default CustomCard;