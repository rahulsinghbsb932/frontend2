import axios from "axios";

export const devInstance = axios.create({ baseURL: "https://verifykiya.com" });

export default {
  install: (app) => {
    app.config.globalProperties.$https = devInstance;
    app.provide('axios', devInstance);
  }
};
