/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/_app";
exports.ids = ["pages/_app"];
exports.modules = {

/***/ "./components/TokenServices.js":
/*!*************************************!*\
  !*** ./components/TokenServices.js ***!
  \*************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {\n__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"clearToken\": () => (/* binding */ clearToken),\n/* harmony export */   \"getAccessToken\": () => (/* binding */ getAccessToken),\n/* harmony export */   \"getRefreshToken\": () => (/* binding */ getRefreshToken),\n/* harmony export */   \"getUserData\": () => (/* binding */ getUserData),\n/* harmony export */   \"getUserID\": () => (/* binding */ getUserID),\n/* harmony export */   \"getUserSlug\": () => (/* binding */ getUserSlug),\n/* harmony export */   \"isUserPermission\": () => (/* binding */ isUserPermission),\n/* harmony export */   \"loggedInAs\": () => (/* binding */ loggedInAs),\n/* harmony export */   \"loggedInAsCode\": () => (/* binding */ loggedInAsCode),\n/* harmony export */   \"setToken\": () => (/* binding */ setToken)\n/* harmony export */ });\n/* harmony import */ var helpers_Template__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! helpers/Template */ \"./helpers/Template.js\");\n/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! js-cookie */ \"js-cookie\");\n/* harmony import */ var jsonwebtoken__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jsonwebtoken */ \"jsonwebtoken\");\n/* harmony import */ var jsonwebtoken__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jsonwebtoken__WEBPACK_IMPORTED_MODULE_2__);\nvar __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([js_cookie__WEBPACK_IMPORTED_MODULE_1__]);\njs_cookie__WEBPACK_IMPORTED_MODULE_1__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];\n\n\n\n// set access token and refresh token\nfunction setToken(token) {\n    js_cookie__WEBPACK_IMPORTED_MODULE_1__[\"default\"].set(\"access_token\", token);\n}\n// get access token\nfunction getAccessToken() {\n    return js_cookie__WEBPACK_IMPORTED_MODULE_1__[\"default\"].get(\"access_token\");\n}\n// get refresh token\nfunction getRefreshToken() {\n    return js_cookie__WEBPACK_IMPORTED_MODULE_1__[\"default\"].get(\"refresh_token\");\n}\n// clear access and refresh token\nfunction clearToken() {\n    js_cookie__WEBPACK_IMPORTED_MODULE_1__[\"default\"].remove(\"access_token\");\n    js_cookie__WEBPACK_IMPORTED_MODULE_1__[\"default\"].remove(\"refresh_token\");\n    localStorage.removeItem(\"homeUrl\");\n    localStorage.removeItem(\"sideNav\");\n}\n// get logged in user data\nfunction getUserData() {\n    let userToken = getAccessToken();\n    let data;\n    if (userToken) {\n        data = jsonwebtoken__WEBPACK_IMPORTED_MODULE_2___default().decode(userToken);\n    }\n    return data;\n}\n// get logged in user data\nfunction getUserID() {\n    let data = getUserData();\n    let id;\n    if (data && data?.companyUserId) {\n        id = data.companyUserId;\n    } else {\n        id = data?.userId || \"\";\n    }\n    return id;\n}\nfunction getUserSlug() {\n    let uData = getUserData();\n    return uData?.userSlug;\n}\nfunction loggedInAsCode() {\n    let data = getUserData();\n    let type = \"\";\n    if (data?.userType && data?.userType != \"admin\") {\n        type = data?.userType || \"\";\n    } else {\n        type = data?.loginAs || \"\";\n    }\n    return type;\n}\nfunction loggedInAs() {\n    let data = getUserData();\n    let type = \"\";\n    if (data?.userType && data?.userType != \"admin\") {\n        type = helpers_Template__WEBPACK_IMPORTED_MODULE_0__.loggedInUserType[parseInt(data?.userType)];\n    } else {\n        type = helpers_Template__WEBPACK_IMPORTED_MODULE_0__.loggedInUserType[parseInt(data?.loginAs)];\n    }\n    return type;\n}\nfunction isUserPermission() {\n    let uData = getUserData();\n    if (uData?.permissionTypes == \"2\" || uData?.permissions == \"all\") {\n        return true;\n    } else {\n        return false;\n    }\n}\n\n__webpack_async_result__();\n} catch(e) { __webpack_async_result__(e); } });//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9jb21wb25lbnRzL1Rva2VuU2VydmljZXMuanMuanMiLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBb0Q7QUFDcEI7QUFDRDtBQUUvQixxQ0FBcUM7QUFDOUIsU0FBU0csUUFBUSxDQUFDQyxLQUFLLEVBQUU7SUFDOUJILHFEQUFXLENBQUMsY0FBYyxFQUFFRyxLQUFLLENBQUMsQ0FBQztDQUNwQztBQUVELG1CQUFtQjtBQUNaLFNBQVNFLGNBQWMsR0FBRztJQUMvQixPQUFPTCxxREFBVyxDQUFDLGNBQWMsQ0FBQyxDQUFDO0NBQ3BDO0FBRUQsb0JBQW9CO0FBQ2IsU0FBU08sZUFBZSxHQUFHO0lBQ2hDLE9BQU9QLHFEQUFXLENBQUMsZUFBZSxDQUFDLENBQUM7Q0FDckM7QUFFRCxpQ0FBaUM7QUFDMUIsU0FBU1EsVUFBVSxHQUFHO0lBQzNCUix3REFBYyxDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBQy9CQSx3REFBYyxDQUFDLGVBQWUsQ0FBQyxDQUFDO0lBQ2hDVSxZQUFZLENBQUNDLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUNuQ0QsWUFBWSxDQUFDQyxVQUFVLENBQUMsU0FBUyxDQUFDLENBQUM7Q0FDcEM7QUFFRCwwQkFBMEI7QUFDbkIsU0FBU0MsV0FBVyxHQUFHO0lBQzVCLElBQUlDLFNBQVMsR0FBR1IsY0FBYyxFQUFFO0lBQ2hDLElBQUlTLElBQUk7SUFDUixJQUFJRCxTQUFTLEVBQUU7UUFDYkMsSUFBSSxHQUFHYiwwREFBVSxDQUFDWSxTQUFTLENBQUMsQ0FBQztLQUM5QjtJQUNELE9BQU9DLElBQUksQ0FBQztDQUNiO0FBRUQsMEJBQTBCO0FBQ25CLFNBQVNFLFNBQVMsR0FBRztJQUMxQixJQUFJRixJQUFJLEdBQUdGLFdBQVcsRUFBRTtJQUN4QixJQUFJSyxFQUFFO0lBQ04sSUFBSUgsSUFBSSxJQUFJQSxJQUFJLEVBQUVJLGFBQWEsRUFBRTtRQUMvQkQsRUFBRSxHQUFHSCxJQUFJLENBQUNJLGFBQWEsQ0FBQztLQUN6QixNQUFNO1FBQ0xELEVBQUUsR0FBR0gsSUFBSSxFQUFFSyxNQUFNLElBQUksRUFBRSxDQUFDO0tBQ3pCO0lBQ0QsT0FBT0YsRUFBRSxDQUFDO0NBQ1g7QUFFTSxTQUFTRyxXQUFXLEdBQUc7SUFDNUIsSUFBSUMsS0FBSyxHQUFHVCxXQUFXLEVBQUU7SUFDekIsT0FBT1MsS0FBSyxFQUFFQyxRQUFRLENBQUM7Q0FDeEI7QUFFTSxTQUFTQyxjQUFjLEdBQUc7SUFDL0IsSUFBSVQsSUFBSSxHQUFHRixXQUFXLEVBQUU7SUFDeEIsSUFBSVksSUFBSSxHQUFHLEVBQUU7SUFDYixJQUFJVixJQUFJLEVBQUVXLFFBQVEsSUFBSVgsSUFBSSxFQUFFVyxRQUFRLElBQUksT0FBTyxFQUFFO1FBQy9DRCxJQUFJLEdBQUdWLElBQUksRUFBRVcsUUFBUSxJQUFJLEVBQUUsQ0FBQztLQUM3QixNQUFNO1FBQ0xELElBQUksR0FBR1YsSUFBSSxFQUFFWSxPQUFPLElBQUksRUFBRSxDQUFDO0tBQzVCO0lBQ0QsT0FBT0YsSUFBSSxDQUFDO0NBQ2I7QUFFTSxTQUFTRyxVQUFVLEdBQUc7SUFDM0IsSUFBSWIsSUFBSSxHQUFHRixXQUFXLEVBQUU7SUFFeEIsSUFBSVksSUFBSSxHQUFHLEVBQUU7SUFDYixJQUFJVixJQUFJLEVBQUVXLFFBQVEsSUFBSVgsSUFBSSxFQUFFVyxRQUFRLElBQUksT0FBTyxFQUFFO1FBQy9DRCxJQUFJLEdBQUd6Qiw4REFBZ0IsQ0FBQzZCLFFBQVEsQ0FBQ2QsSUFBSSxFQUFFVyxRQUFRLENBQUMsQ0FBQyxDQUFDO0tBQ25ELE1BQU07UUFDTEQsSUFBSSxHQUFHekIsOERBQWdCLENBQUM2QixRQUFRLENBQUNkLElBQUksRUFBRVksT0FBTyxDQUFDLENBQUMsQ0FBQztLQUNsRDtJQUVELE9BQU9GLElBQUksQ0FBQztDQUNiO0FBRU0sU0FBU0ssZ0JBQWdCLEdBQUc7SUFDakMsSUFBSVIsS0FBSyxHQUFHVCxXQUFXLEVBQUU7SUFFekIsSUFBSVMsS0FBSyxFQUFFUyxlQUFlLElBQUksR0FBRyxJQUFJVCxLQUFLLEVBQUVVLFdBQVcsSUFBSSxLQUFLLEVBQUU7UUFDaEUsT0FBTyxJQUFJLENBQUM7S0FDYixNQUFNO1FBQ0wsT0FBTyxLQUFLLENBQUM7S0FDZDtDQUNGIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vdmVpZnlraXlhLy4vY29tcG9uZW50cy9Ub2tlblNlcnZpY2VzLmpzP2M1NWMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgbG9nZ2VkSW5Vc2VyVHlwZSB9IGZyb20gXCJoZWxwZXJzL1RlbXBsYXRlXCI7XHJcbmltcG9ydCBDb29raWVzIGZyb20gXCJqcy1jb29raWVcIjtcclxuaW1wb3J0IGp3dCBmcm9tIFwianNvbndlYnRva2VuXCI7XHJcblxyXG4vLyBzZXQgYWNjZXNzIHRva2VuIGFuZCByZWZyZXNoIHRva2VuXHJcbmV4cG9ydCBmdW5jdGlvbiBzZXRUb2tlbih0b2tlbikge1xyXG4gIENvb2tpZXMuc2V0KFwiYWNjZXNzX3Rva2VuXCIsIHRva2VuKTtcclxufVxyXG5cclxuLy8gZ2V0IGFjY2VzcyB0b2tlblxyXG5leHBvcnQgZnVuY3Rpb24gZ2V0QWNjZXNzVG9rZW4oKSB7XHJcbiAgcmV0dXJuIENvb2tpZXMuZ2V0KFwiYWNjZXNzX3Rva2VuXCIpO1xyXG59XHJcblxyXG4vLyBnZXQgcmVmcmVzaCB0b2tlblxyXG5leHBvcnQgZnVuY3Rpb24gZ2V0UmVmcmVzaFRva2VuKCkge1xyXG4gIHJldHVybiBDb29raWVzLmdldChcInJlZnJlc2hfdG9rZW5cIik7XHJcbn1cclxuXHJcbi8vIGNsZWFyIGFjY2VzcyBhbmQgcmVmcmVzaCB0b2tlblxyXG5leHBvcnQgZnVuY3Rpb24gY2xlYXJUb2tlbigpIHtcclxuICBDb29raWVzLnJlbW92ZShcImFjY2Vzc190b2tlblwiKTtcclxuICBDb29raWVzLnJlbW92ZShcInJlZnJlc2hfdG9rZW5cIik7XHJcbiAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oXCJob21lVXJsXCIpO1xyXG4gIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKFwic2lkZU5hdlwiKTtcclxufVxyXG5cclxuLy8gZ2V0IGxvZ2dlZCBpbiB1c2VyIGRhdGFcclxuZXhwb3J0IGZ1bmN0aW9uIGdldFVzZXJEYXRhKCkge1xyXG4gIGxldCB1c2VyVG9rZW4gPSBnZXRBY2Nlc3NUb2tlbigpO1xyXG4gIGxldCBkYXRhO1xyXG4gIGlmICh1c2VyVG9rZW4pIHtcclxuICAgIGRhdGEgPSBqd3QuZGVjb2RlKHVzZXJUb2tlbik7XHJcbiAgfVxyXG4gIHJldHVybiBkYXRhO1xyXG59XHJcblxyXG4vLyBnZXQgbG9nZ2VkIGluIHVzZXIgZGF0YVxyXG5leHBvcnQgZnVuY3Rpb24gZ2V0VXNlcklEKCkge1xyXG4gIGxldCBkYXRhID0gZ2V0VXNlckRhdGEoKTtcclxuICBsZXQgaWQ7XHJcbiAgaWYgKGRhdGEgJiYgZGF0YT8uY29tcGFueVVzZXJJZCkge1xyXG4gICAgaWQgPSBkYXRhLmNvbXBhbnlVc2VySWQ7XHJcbiAgfSBlbHNlIHtcclxuICAgIGlkID0gZGF0YT8udXNlcklkIHx8IFwiXCI7XHJcbiAgfVxyXG4gIHJldHVybiBpZDtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIGdldFVzZXJTbHVnKCkge1xyXG4gIGxldCB1RGF0YSA9IGdldFVzZXJEYXRhKCk7XHJcbiAgcmV0dXJuIHVEYXRhPy51c2VyU2x1ZztcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIGxvZ2dlZEluQXNDb2RlKCkge1xyXG4gIGxldCBkYXRhID0gZ2V0VXNlckRhdGEoKTtcclxuICBsZXQgdHlwZSA9IFwiXCI7XHJcbiAgaWYgKGRhdGE/LnVzZXJUeXBlICYmIGRhdGE/LnVzZXJUeXBlICE9IFwiYWRtaW5cIikge1xyXG4gICAgdHlwZSA9IGRhdGE/LnVzZXJUeXBlIHx8IFwiXCI7XHJcbiAgfSBlbHNlIHtcclxuICAgIHR5cGUgPSBkYXRhPy5sb2dpbkFzIHx8IFwiXCI7XHJcbiAgfVxyXG4gIHJldHVybiB0eXBlO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gbG9nZ2VkSW5BcygpIHtcclxuICBsZXQgZGF0YSA9IGdldFVzZXJEYXRhKCk7XHJcblxyXG4gIGxldCB0eXBlID0gXCJcIjtcclxuICBpZiAoZGF0YT8udXNlclR5cGUgJiYgZGF0YT8udXNlclR5cGUgIT0gXCJhZG1pblwiKSB7XHJcbiAgICB0eXBlID0gbG9nZ2VkSW5Vc2VyVHlwZVtwYXJzZUludChkYXRhPy51c2VyVHlwZSldO1xyXG4gIH0gZWxzZSB7XHJcbiAgICB0eXBlID0gbG9nZ2VkSW5Vc2VyVHlwZVtwYXJzZUludChkYXRhPy5sb2dpbkFzKV07XHJcbiAgfVxyXG5cclxuICByZXR1cm4gdHlwZTtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIGlzVXNlclBlcm1pc3Npb24oKSB7XHJcbiAgbGV0IHVEYXRhID0gZ2V0VXNlckRhdGEoKTtcclxuXHJcbiAgaWYgKHVEYXRhPy5wZXJtaXNzaW9uVHlwZXMgPT0gXCIyXCIgfHwgdURhdGE/LnBlcm1pc3Npb25zID09IFwiYWxsXCIpIHtcclxuICAgIHJldHVybiB0cnVlO1xyXG4gIH0gZWxzZSB7XHJcbiAgICByZXR1cm4gZmFsc2U7XHJcbiAgfVxyXG59XHJcbiJdLCJuYW1lcyI6WyJsb2dnZWRJblVzZXJUeXBlIiwiQ29va2llcyIsImp3dCIsInNldFRva2VuIiwidG9rZW4iLCJzZXQiLCJnZXRBY2Nlc3NUb2tlbiIsImdldCIsImdldFJlZnJlc2hUb2tlbiIsImNsZWFyVG9rZW4iLCJyZW1vdmUiLCJsb2NhbFN0b3JhZ2UiLCJyZW1vdmVJdGVtIiwiZ2V0VXNlckRhdGEiLCJ1c2VyVG9rZW4iLCJkYXRhIiwiZGVjb2RlIiwiZ2V0VXNlcklEIiwiaWQiLCJjb21wYW55VXNlcklkIiwidXNlcklkIiwiZ2V0VXNlclNsdWciLCJ1RGF0YSIsInVzZXJTbHVnIiwibG9nZ2VkSW5Bc0NvZGUiLCJ0eXBlIiwidXNlclR5cGUiLCJsb2dpbkFzIiwibG9nZ2VkSW5BcyIsInBhcnNlSW50IiwiaXNVc2VyUGVybWlzc2lvbiIsInBlcm1pc3Npb25UeXBlcyIsInBlcm1pc3Npb25zIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./components/TokenServices.js\n");

/***/ }),

/***/ "./helpers/Template.js":
/*!*****************************!*\
  !*** ./helpers/Template.js ***!
  \*****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"loggedInDashboardUrlFun\": () => (/* binding */ loggedInDashboardUrlFun),\n/* harmony export */   \"loggedInUserType\": () => (/* binding */ loggedInUserType),\n/* harmony export */   \"loggedInUserTypeName\": () => (/* binding */ loggedInUserTypeName),\n/* harmony export */   \"profileUrl\": () => (/* binding */ profileUrl),\n/* harmony export */   \"scanQrCodeUrl\": () => (/* binding */ scanQrCodeUrl)\n/* harmony export */ });\nconst loggedInUserType = {\n    1: \"company\",\n    2: \"manufacturer\",\n    3: \"warehouse\",\n    4: \"distributor\",\n    5: \"user\",\n    11: \"importer\",\n    12: \"dealer\"\n};\nfunction loggedInDashboardUrlFun(slug, id) {\n    let urls = {\n        1: \"/company/dashboard\",\n        2: \"/manufacturer/dashboard\",\n        3: \"/warehouse/dashboard\",\n        4: \"/distributor/dashboard\",\n        5: \"/user/dashboard\",\n        11: \"/importer/dashboard\",\n        12: \"/dealer/dashboard\"\n    };\n    let tempUrl = urls[id];\n    let finalUrl = `/${slug}${tempUrl}`;\n    return finalUrl;\n}\nfunction scanQrCodeUrl(slug, id) {\n    let urls = {\n        1: \"/medsure/company/dashboard\",\n        2: \"/medsure/manufacturer/dashboard\",\n        3: \"/warehouse/authenticate\",\n        4: \"/distributor/authenticate\",\n        5: \"/user/scan-qr-code\",\n        11: \"/importer/authenticate\",\n        12: \"/dealer/authenticate\"\n    };\n    let tempUrl = urls[id];\n    let finalUrl = `/${slug}${tempUrl}`;\n    return finalUrl;\n}\nfunction profileUrl(slug, id) {\n    let urls = {\n        1: \"/company/profile\",\n        2: \"/manufacturer/profile\",\n        3: \"/warehouse/profile\",\n        4: \"/distributor/profile\",\n        5: \"/user/profile\",\n        11: \"/importer/profile\",\n        12: \"/dealer/profile\"\n    };\n    let tempUrl = urls[id];\n    let finalUrl = `/${slug}${tempUrl}`;\n    return finalUrl;\n}\nconst loggedInUserTypeName = {\n    1: \"Company\",\n    2: \"Manufacturer\",\n    3: \"Warehouse\",\n    4: \"Distributor\",\n    5: \"User\",\n    11: \"Importer\",\n    12: \"Dealer\"\n};\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9oZWxwZXJzL1RlbXBsYXRlLmpzLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQU8sTUFBTUEsZ0JBQWdCLEdBQUc7QUFDOUIsS0FBQyxFQUFFLFNBQVM7QUFDWixLQUFDLEVBQUUsY0FBYztBQUNqQixLQUFDLEVBQUUsV0FBVztBQUNkLEtBQUMsRUFBRSxhQUFhO0FBQ2hCLEtBQUMsRUFBRSxNQUFNO0FBQ1QsTUFBRSxFQUFFLFVBQVU7QUFDZCxNQUFFLEVBQUUsUUFBUTtDQUNiLENBQUM7QUFFSyxTQUFTQyx1QkFBdUIsQ0FBQ0MsSUFBSSxFQUFFQyxFQUFFLEVBQUU7SUFDaEQsSUFBSUMsSUFBSSxHQUFHO0FBQ1QsU0FBQyxFQUFFLG9CQUFvQjtBQUN2QixTQUFDLEVBQUUseUJBQXlCO0FBQzVCLFNBQUMsRUFBRSxzQkFBc0I7QUFDekIsU0FBQyxFQUFFLHdCQUF3QjtBQUMzQixTQUFDLEVBQUUsaUJBQWlCO0FBQ3BCLFVBQUUsRUFBRSxxQkFBcUI7QUFDekIsVUFBRSxFQUFFLG1CQUFtQjtLQUN4QjtJQUVELElBQUlDLE9BQU8sR0FBR0QsSUFBSSxDQUFDRCxFQUFFLENBQUM7SUFDdEIsSUFBSUcsUUFBUSxHQUFHLENBQUMsQ0FBQyxFQUFFSixJQUFJLENBQUMsRUFBRUcsT0FBTyxDQUFDLENBQUM7SUFDbkMsT0FBT0MsUUFBUSxDQUFDO0NBQ2pCO0FBRU0sU0FBU0MsYUFBYSxDQUFDTCxJQUFJLEVBQUVDLEVBQUUsRUFBRTtJQUN0QyxJQUFJQyxJQUFJLEdBQUc7QUFDVCxTQUFDLEVBQUUsNEJBQTRCO0FBQy9CLFNBQUMsRUFBRSxpQ0FBaUM7QUFDcEMsU0FBQyxFQUFFLHlCQUF5QjtBQUM1QixTQUFDLEVBQUUsMkJBQTJCO0FBQzlCLFNBQUMsRUFBRSxvQkFBb0I7QUFDdkIsVUFBRSxFQUFFLHdCQUF3QjtBQUM1QixVQUFFLEVBQUUsc0JBQXNCO0tBQzNCO0lBQ0QsSUFBSUMsT0FBTyxHQUFHRCxJQUFJLENBQUNELEVBQUUsQ0FBQztJQUN0QixJQUFJRyxRQUFRLEdBQUcsQ0FBQyxDQUFDLEVBQUVKLElBQUksQ0FBQyxFQUFFRyxPQUFPLENBQUMsQ0FBQztJQUNuQyxPQUFPQyxRQUFRLENBQUM7Q0FDakI7QUFFTSxTQUFTRSxVQUFVLENBQUNOLElBQUksRUFBRUMsRUFBRSxFQUFFO0lBQ25DLElBQUlDLElBQUksR0FBRztBQUNULFNBQUMsRUFBRSxrQkFBa0I7QUFDckIsU0FBQyxFQUFFLHVCQUF1QjtBQUMxQixTQUFDLEVBQUUsb0JBQW9CO0FBQ3ZCLFNBQUMsRUFBRSxzQkFBc0I7QUFDekIsU0FBQyxFQUFFLGVBQWU7QUFDbEIsVUFBRSxFQUFFLG1CQUFtQjtBQUN2QixVQUFFLEVBQUUsaUJBQWlCO0tBQ3RCO0lBQ0QsSUFBSUMsT0FBTyxHQUFHRCxJQUFJLENBQUNELEVBQUUsQ0FBQztJQUN0QixJQUFJRyxRQUFRLEdBQUcsQ0FBQyxDQUFDLEVBQUVKLElBQUksQ0FBQyxFQUFFRyxPQUFPLENBQUMsQ0FBQztJQUNuQyxPQUFPQyxRQUFRLENBQUM7Q0FDakI7QUFFTSxNQUFNRyxvQkFBb0IsR0FBRztBQUNsQyxLQUFDLEVBQUUsU0FBUztBQUNaLEtBQUMsRUFBRSxjQUFjO0FBQ2pCLEtBQUMsRUFBRSxXQUFXO0FBQ2QsS0FBQyxFQUFFLGFBQWE7QUFDaEIsS0FBQyxFQUFFLE1BQU07QUFDVCxNQUFFLEVBQUUsVUFBVTtBQUNkLE1BQUUsRUFBRSxRQUFRO0NBQ2IsQ0FBQyIsInNvdXJjZXMiOlsid2VicGFjazovL3ZlaWZ5a2l5YS8uL2hlbHBlcnMvVGVtcGxhdGUuanM/ZmM2NSJdLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY29uc3QgbG9nZ2VkSW5Vc2VyVHlwZSA9IHtcclxuICAxOiBcImNvbXBhbnlcIixcclxuICAyOiBcIm1hbnVmYWN0dXJlclwiLFxyXG4gIDM6IFwid2FyZWhvdXNlXCIsXHJcbiAgNDogXCJkaXN0cmlidXRvclwiLFxyXG4gIDU6IFwidXNlclwiLFxyXG4gIDExOiBcImltcG9ydGVyXCIsXHJcbiAgMTI6IFwiZGVhbGVyXCIsXHJcbn07XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gbG9nZ2VkSW5EYXNoYm9hcmRVcmxGdW4oc2x1ZywgaWQpIHtcclxuICBsZXQgdXJscyA9IHtcclxuICAgIDE6IFwiL2NvbXBhbnkvZGFzaGJvYXJkXCIsXHJcbiAgICAyOiBcIi9tYW51ZmFjdHVyZXIvZGFzaGJvYXJkXCIsXHJcbiAgICAzOiBcIi93YXJlaG91c2UvZGFzaGJvYXJkXCIsXHJcbiAgICA0OiBcIi9kaXN0cmlidXRvci9kYXNoYm9hcmRcIixcclxuICAgIDU6IFwiL3VzZXIvZGFzaGJvYXJkXCIsXHJcbiAgICAxMTogXCIvaW1wb3J0ZXIvZGFzaGJvYXJkXCIsXHJcbiAgICAxMjogXCIvZGVhbGVyL2Rhc2hib2FyZFwiLFxyXG4gIH07XHJcblxyXG4gIGxldCB0ZW1wVXJsID0gdXJsc1tpZF07XHJcbiAgbGV0IGZpbmFsVXJsID0gYC8ke3NsdWd9JHt0ZW1wVXJsfWA7XHJcbiAgcmV0dXJuIGZpbmFsVXJsO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gc2NhblFyQ29kZVVybChzbHVnLCBpZCkge1xyXG4gIGxldCB1cmxzID0ge1xyXG4gICAgMTogXCIvbWVkc3VyZS9jb21wYW55L2Rhc2hib2FyZFwiLFxyXG4gICAgMjogXCIvbWVkc3VyZS9tYW51ZmFjdHVyZXIvZGFzaGJvYXJkXCIsXHJcbiAgICAzOiBcIi93YXJlaG91c2UvYXV0aGVudGljYXRlXCIsXHJcbiAgICA0OiBcIi9kaXN0cmlidXRvci9hdXRoZW50aWNhdGVcIixcclxuICAgIDU6IFwiL3VzZXIvc2Nhbi1xci1jb2RlXCIsXHJcbiAgICAxMTogXCIvaW1wb3J0ZXIvYXV0aGVudGljYXRlXCIsXHJcbiAgICAxMjogXCIvZGVhbGVyL2F1dGhlbnRpY2F0ZVwiLFxyXG4gIH07XHJcbiAgbGV0IHRlbXBVcmwgPSB1cmxzW2lkXTtcclxuICBsZXQgZmluYWxVcmwgPSBgLyR7c2x1Z30ke3RlbXBVcmx9YDtcclxuICByZXR1cm4gZmluYWxVcmw7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBwcm9maWxlVXJsKHNsdWcsIGlkKSB7XHJcbiAgbGV0IHVybHMgPSB7XHJcbiAgICAxOiBcIi9jb21wYW55L3Byb2ZpbGVcIixcclxuICAgIDI6IFwiL21hbnVmYWN0dXJlci9wcm9maWxlXCIsXHJcbiAgICAzOiBcIi93YXJlaG91c2UvcHJvZmlsZVwiLFxyXG4gICAgNDogXCIvZGlzdHJpYnV0b3IvcHJvZmlsZVwiLFxyXG4gICAgNTogXCIvdXNlci9wcm9maWxlXCIsXHJcbiAgICAxMTogXCIvaW1wb3J0ZXIvcHJvZmlsZVwiLFxyXG4gICAgMTI6IFwiL2RlYWxlci9wcm9maWxlXCIsXHJcbiAgfTtcclxuICBsZXQgdGVtcFVybCA9IHVybHNbaWRdO1xyXG4gIGxldCBmaW5hbFVybCA9IGAvJHtzbHVnfSR7dGVtcFVybH1gO1xyXG4gIHJldHVybiBmaW5hbFVybDtcclxufVxyXG5cclxuZXhwb3J0IGNvbnN0IGxvZ2dlZEluVXNlclR5cGVOYW1lID0ge1xyXG4gIDE6IFwiQ29tcGFueVwiLFxyXG4gIDI6IFwiTWFudWZhY3R1cmVyXCIsXHJcbiAgMzogXCJXYXJlaG91c2VcIixcclxuICA0OiBcIkRpc3RyaWJ1dG9yXCIsXHJcbiAgNTogXCJVc2VyXCIsXHJcbiAgMTE6IFwiSW1wb3J0ZXJcIixcclxuICAxMjogXCJEZWFsZXJcIixcclxufTtcclxuIl0sIm5hbWVzIjpbImxvZ2dlZEluVXNlclR5cGUiLCJsb2dnZWRJbkRhc2hib2FyZFVybEZ1biIsInNsdWciLCJpZCIsInVybHMiLCJ0ZW1wVXJsIiwiZmluYWxVcmwiLCJzY2FuUXJDb2RlVXJsIiwicHJvZmlsZVVybCIsImxvZ2dlZEluVXNlclR5cGVOYW1lIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./helpers/Template.js\n");

/***/ }),

/***/ "./pages/_app.js":
/*!***********************!*\
  !*** ./pages/_app.js ***!
  \***********************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {\n__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var antd_dist_antd_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! antd/dist/antd.css */ \"./node_modules/antd/dist/antd.css\");\n/* harmony import */ var antd_dist_antd_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(antd_dist_antd_css__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var font_awesome_css_font_awesome_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! font-awesome/css/font-awesome.css */ \"./node_modules/font-awesome/css/font-awesome.css\");\n/* harmony import */ var font_awesome_css_font_awesome_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(font_awesome_css_font_awesome_css__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var react_intl_tel_input_dist_main_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-intl-tel-input/dist/main.css */ \"./node_modules/react-intl-tel-input/dist/main.css\");\n/* harmony import */ var react_intl_tel_input_dist_main_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_intl_tel_input_dist_main_css__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var nprogress_nprogress_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! nprogress/nprogress.css */ \"./node_modules/nprogress/nprogress.css\");\n/* harmony import */ var nprogress_nprogress_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(nprogress_nprogress_css__WEBPACK_IMPORTED_MODULE_4__);\n/* harmony import */ var _styles_home_scss__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @styles/home.scss */ \"./styles/home.scss\");\n/* harmony import */ var _styles_home_scss__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_styles_home_scss__WEBPACK_IMPORTED_MODULE_5__);\n/* harmony import */ var _styles_header_scss__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @styles/header.scss */ \"./styles/header.scss\");\n/* harmony import */ var _styles_header_scss__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_styles_header_scss__WEBPACK_IMPORTED_MODULE_6__);\n/* harmony import */ var bootstrap_dist_css_bootstrap_min_css__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! bootstrap/dist/css/bootstrap.min.css */ \"./node_modules/bootstrap/dist/css/bootstrap.min.css\");\n/* harmony import */ var bootstrap_dist_css_bootstrap_min_css__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(bootstrap_dist_css_bootstrap_min_css__WEBPACK_IMPORTED_MODULE_7__);\n/* harmony import */ var _styles_educational_institutions_scss__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @styles/educational-institutions.scss */ \"./styles/educational-institutions.scss\");\n/* harmony import */ var _styles_educational_institutions_scss__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_styles_educational_institutions_scss__WEBPACK_IMPORTED_MODULE_8__);\n/* harmony import */ var _styles_corporate_scss__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @styles/corporate.scss */ \"./styles/corporate.scss\");\n/* harmony import */ var _styles_corporate_scss__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_styles_corporate_scss__WEBPACK_IMPORTED_MODULE_9__);\n/* harmony import */ var _styles_event_scss__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @styles/event.scss */ \"./styles/event.scss\");\n/* harmony import */ var _styles_event_scss__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(_styles_event_scss__WEBPACK_IMPORTED_MODULE_10__);\n/* harmony import */ var _styles_courses_scss__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @styles/courses.scss */ \"./styles/courses.scss\");\n/* harmony import */ var _styles_courses_scss__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(_styles_courses_scss__WEBPACK_IMPORTED_MODULE_11__);\n/* harmony import */ var _styles_resources_scss__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @styles/resources.scss */ \"./styles/resources.scss\");\n/* harmony import */ var _styles_resources_scss__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(_styles_resources_scss__WEBPACK_IMPORTED_MODULE_12__);\n/* harmony import */ var _styles_contact_scss__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @styles/contact.scss */ \"./styles/contact.scss\");\n/* harmony import */ var _styles_contact_scss__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(_styles_contact_scss__WEBPACK_IMPORTED_MODULE_13__);\n/* harmony import */ var _styles_sidebar_scss__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @styles/sidebar.scss */ \"./styles/sidebar.scss\");\n/* harmony import */ var _styles_sidebar_scss__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(_styles_sidebar_scss__WEBPACK_IMPORTED_MODULE_14__);\n/* harmony import */ var _styles_admin_scss__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @styles/admin.scss */ \"./styles/admin.scss\");\n/* harmony import */ var _styles_admin_scss__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(_styles_admin_scss__WEBPACK_IMPORTED_MODULE_15__);\n/* harmony import */ var _styles_profile_scss__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @styles/profile.scss */ \"./styles/profile.scss\");\n/* harmony import */ var _styles_profile_scss__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(_styles_profile_scss__WEBPACK_IMPORTED_MODULE_16__);\n/* harmony import */ var _styles_school_course_details_scss__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @styles/school-course-details.scss */ \"./styles/school-course-details.scss\");\n/* harmony import */ var _styles_school_course_details_scss__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(_styles_school_course_details_scss__WEBPACK_IMPORTED_MODULE_17__);\n/* harmony import */ var _styles_registration_scss__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @styles/registration.scss */ \"./styles/registration.scss\");\n/* harmony import */ var _styles_registration_scss__WEBPACK_IMPORTED_MODULE_18___default = /*#__PURE__*/__webpack_require__.n(_styles_registration_scss__WEBPACK_IMPORTED_MODULE_18__);\n/* harmony import */ var _styles_wallet_scss__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @styles/wallet.scss */ \"./styles/wallet.scss\");\n/* harmony import */ var _styles_wallet_scss__WEBPACK_IMPORTED_MODULE_19___default = /*#__PURE__*/__webpack_require__.n(_styles_wallet_scss__WEBPACK_IMPORTED_MODULE_19__);\n/* harmony import */ var _styles_AdminElSchool_scss__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @styles/AdminElSchool.scss */ \"./styles/AdminElSchool.scss\");\n/* harmony import */ var _styles_AdminElSchool_scss__WEBPACK_IMPORTED_MODULE_20___default = /*#__PURE__*/__webpack_require__.n(_styles_AdminElSchool_scss__WEBPACK_IMPORTED_MODULE_20__);\n/* harmony import */ var _styles_AdminProfile_scss__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @styles/AdminProfile.scss */ \"./styles/AdminProfile.scss\");\n/* harmony import */ var _styles_AdminProfile_scss__WEBPACK_IMPORTED_MODULE_21___default = /*#__PURE__*/__webpack_require__.n(_styles_AdminProfile_scss__WEBPACK_IMPORTED_MODULE_21__);\n/* harmony import */ var _styles_about_us_scss__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @styles/about-us.scss */ \"./styles/about-us.scss\");\n/* harmony import */ var _styles_about_us_scss__WEBPACK_IMPORTED_MODULE_22___default = /*#__PURE__*/__webpack_require__.n(_styles_about_us_scss__WEBPACK_IMPORTED_MODULE_22__);\n/* harmony import */ var _styles_style_scss__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @styles/style.scss */ \"./styles/style.scss\");\n/* harmony import */ var _styles_style_scss__WEBPACK_IMPORTED_MODULE_23___default = /*#__PURE__*/__webpack_require__.n(_styles_style_scss__WEBPACK_IMPORTED_MODULE_23__);\n/* harmony import */ var _styles_PartnerWithUs_scss__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @styles/PartnerWithUs.scss */ \"./styles/PartnerWithUs.scss\");\n/* harmony import */ var _styles_PartnerWithUs_scss__WEBPACK_IMPORTED_MODULE_24___default = /*#__PURE__*/__webpack_require__.n(_styles_PartnerWithUs_scss__WEBPACK_IMPORTED_MODULE_24__);\n/* harmony import */ var _styles_Login_scss__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @styles/Login.scss */ \"./styles/Login.scss\");\n/* harmony import */ var _styles_Login_scss__WEBPACK_IMPORTED_MODULE_25___default = /*#__PURE__*/__webpack_require__.n(_styles_Login_scss__WEBPACK_IMPORTED_MODULE_25__);\n/* harmony import */ var _styles_CoursesDetails_scss__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! @styles/CoursesDetails.scss */ \"./styles/CoursesDetails.scss\");\n/* harmony import */ var _styles_CoursesDetails_scss__WEBPACK_IMPORTED_MODULE_26___default = /*#__PURE__*/__webpack_require__.n(_styles_CoursesDetails_scss__WEBPACK_IMPORTED_MODULE_26__);\n/* harmony import */ var _styles_affiliate_scss__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! @styles/affiliate.scss */ \"./styles/affiliate.scss\");\n/* harmony import */ var _styles_affiliate_scss__WEBPACK_IMPORTED_MODULE_27___default = /*#__PURE__*/__webpack_require__.n(_styles_affiliate_scss__WEBPACK_IMPORTED_MODULE_27__);\n/* harmony import */ var _styles_jobs_details_scss__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! @styles/jobs-details.scss */ \"./styles/jobs-details.scss\");\n/* harmony import */ var _styles_jobs_details_scss__WEBPACK_IMPORTED_MODULE_28___default = /*#__PURE__*/__webpack_require__.n(_styles_jobs_details_scss__WEBPACK_IMPORTED_MODULE_28__);\n/* harmony import */ var _styles_pricing_scss__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! @styles/pricing.scss */ \"./styles/pricing.scss\");\n/* harmony import */ var _styles_pricing_scss__WEBPACK_IMPORTED_MODULE_29___default = /*#__PURE__*/__webpack_require__.n(_styles_pricing_scss__WEBPACK_IMPORTED_MODULE_29__);\n/* harmony import */ var _styles_admin_referral_level_1_scss__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! @styles/admin-referral-level-1.scss */ \"./styles/admin-referral-level-1.scss\");\n/* harmony import */ var _styles_admin_referral_level_1_scss__WEBPACK_IMPORTED_MODULE_30___default = /*#__PURE__*/__webpack_require__.n(_styles_admin_referral_level_1_scss__WEBPACK_IMPORTED_MODULE_30__);\n/* harmony import */ var _styles_explore_school_scss__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! @styles/explore-school.scss */ \"./styles/explore-school.scss\");\n/* harmony import */ var _styles_explore_school_scss__WEBPACK_IMPORTED_MODULE_31___default = /*#__PURE__*/__webpack_require__.n(_styles_explore_school_scss__WEBPACK_IMPORTED_MODULE_31__);\n/* harmony import */ var _styles_AdmissionTracker_scss__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! @styles/AdmissionTracker.scss */ \"./styles/AdmissionTracker.scss\");\n/* harmony import */ var _styles_AdmissionTracker_scss__WEBPACK_IMPORTED_MODULE_32___default = /*#__PURE__*/__webpack_require__.n(_styles_AdmissionTracker_scss__WEBPACK_IMPORTED_MODULE_32__);\n/* harmony import */ var _redux_store__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! @redux/store */ \"./redux/store.js\");\n/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! react-redux */ \"react-redux\");\n/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_34___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_34__);\nvar __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_redux_store__WEBPACK_IMPORTED_MODULE_33__]);\n_redux_store__WEBPACK_IMPORTED_MODULE_33__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n// import \"@styles/AdminAcademic.scss\"\n// ----------------\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nfunction MyApp({ Component , pageProps  }) {\n    const getLayout = Component.getLayout || ((page)=>page);\n    return /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_redux__WEBPACK_IMPORTED_MODULE_34__.Provider, {\n        store: _redux_store__WEBPACK_IMPORTED_MODULE_33__[\"default\"],\n        children: getLayout(/*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(Component, {\n            ...pageProps\n        }, void 0, false, {\n            fileName: \"D:\\\\frontend2\\\\pages\\\\_app.js\",\n            lineNumber: 45,\n            columnNumber: 40\n        }, this))\n    }, void 0, false, {\n        fileName: \"D:\\\\frontend2\\\\pages\\\\_app.js\",\n        lineNumber: 45,\n        columnNumber: 5\n    }, this);\n}\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (MyApp);\n\n__webpack_async_result__();\n} catch(e) { __webpack_async_result__(e); } });//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy9fYXBwLmpzLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUE0QjtBQUNlO0FBQ0M7QUFDWDtBQUNOO0FBQ0U7QUFDaUI7QUFDQztBQUNmO0FBQ0o7QUFDRTtBQUNFO0FBQ0Y7QUFDQTtBQUNGO0FBQ0U7QUFDYztBQUNUO0FBQ047QUFDTztBQUNEO0FBQ25DLHNDQUFzQztBQUN0QyxtQkFBbUI7QUFDWTtBQUNhO0FBQ1o7QUFDSjtBQUNRO0FBQ1I7QUFDUztBQUNMO0FBQ0c7QUFDTDtBQUNlO0FBRVI7QUFDRTtBQUVOO0FBQ007QUFFdkMsU0FBU0UsS0FBSyxDQUFDLEVBQUVDLFNBQVMsR0FBRUMsU0FBUyxHQUFFLEVBQUU7SUFDdkMsTUFBTUMsU0FBUyxHQUFHRixTQUFTLENBQUNFLFNBQVMsSUFBSSxDQUFDLENBQUNDLElBQUksR0FBS0EsSUFBSSxDQUFDO0lBQ3pELHFCQUNFLDhEQUFDTCxrREFBUTtRQUFDRCxLQUFLLEVBQUVBLHFEQUFLO2tCQUFHSyxTQUFTLGVBQUMsOERBQUNGLFNBQVM7WUFBRSxHQUFHQyxTQUFTOzs7OztnQkFBSSxDQUFDOzs7OztZQUFZLENBQzVFO0NBQ0g7QUFFRCxpRUFBZUYsS0FBSyxFQUFDIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vdmVpZnlraXlhLy4vcGFnZXMvX2FwcC5qcz9lMGFkIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBcImFudGQvZGlzdC9hbnRkLmNzc1wiO1xyXG5pbXBvcnQgXCJmb250LWF3ZXNvbWUvY3NzL2ZvbnQtYXdlc29tZS5jc3NcIjtcclxuaW1wb3J0IFwicmVhY3QtaW50bC10ZWwtaW5wdXQvZGlzdC9tYWluLmNzc1wiO1xyXG5pbXBvcnQgXCJucHJvZ3Jlc3MvbnByb2dyZXNzLmNzc1wiO1xyXG5pbXBvcnQgXCJAc3R5bGVzL2hvbWUuc2Nzc1wiO1xyXG5pbXBvcnQgXCJAc3R5bGVzL2hlYWRlci5zY3NzXCI7XHJcbmltcG9ydCBcImJvb3RzdHJhcC9kaXN0L2Nzcy9ib290c3RyYXAubWluLmNzc1wiO1xyXG5pbXBvcnQgXCJAc3R5bGVzL2VkdWNhdGlvbmFsLWluc3RpdHV0aW9ucy5zY3NzXCI7XHJcbmltcG9ydCBcIkBzdHlsZXMvY29ycG9yYXRlLnNjc3NcIjtcclxuaW1wb3J0IFwiQHN0eWxlcy9ldmVudC5zY3NzXCI7XHJcbmltcG9ydCBcIkBzdHlsZXMvY291cnNlcy5zY3NzXCI7XHJcbmltcG9ydCBcIkBzdHlsZXMvcmVzb3VyY2VzLnNjc3NcIjtcclxuaW1wb3J0IFwiQHN0eWxlcy9jb250YWN0LnNjc3NcIjtcclxuaW1wb3J0IFwiQHN0eWxlcy9zaWRlYmFyLnNjc3NcIjtcclxuaW1wb3J0IFwiQHN0eWxlcy9hZG1pbi5zY3NzXCI7XHJcbmltcG9ydCBcIkBzdHlsZXMvcHJvZmlsZS5zY3NzXCI7XHJcbmltcG9ydCBcIkBzdHlsZXMvc2Nob29sLWNvdXJzZS1kZXRhaWxzLnNjc3NcIjtcclxuaW1wb3J0IFwiQHN0eWxlcy9yZWdpc3RyYXRpb24uc2Nzc1wiO1xyXG5pbXBvcnQgXCJAc3R5bGVzL3dhbGxldC5zY3NzXCI7XHJcbmltcG9ydCBcIkBzdHlsZXMvQWRtaW5FbFNjaG9vbC5zY3NzXCI7XHJcbmltcG9ydCBcIkBzdHlsZXMvQWRtaW5Qcm9maWxlLnNjc3NcIjtcclxuLy8gaW1wb3J0IFwiQHN0eWxlcy9BZG1pbkFjYWRlbWljLnNjc3NcIlxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tXHJcbmltcG9ydCBcIkBzdHlsZXMvYWJvdXQtdXMuc2Nzc1wiO1xyXG5pbXBvcnQgXCJAc3R5bGVzL3NjaG9vbC1jb3Vyc2UtZGV0YWlscy5zY3NzXCI7XHJcbmltcG9ydCBcIkBzdHlsZXMvcmVzb3VyY2VzLnNjc3NcIjtcclxuaW1wb3J0IFwiQHN0eWxlcy9zdHlsZS5zY3NzXCI7XHJcbmltcG9ydCBcIkBzdHlsZXMvUGFydG5lcldpdGhVcy5zY3NzXCI7XHJcbmltcG9ydCBcIkBzdHlsZXMvTG9naW4uc2Nzc1wiO1xyXG5pbXBvcnQgXCJAc3R5bGVzL0NvdXJzZXNEZXRhaWxzLnNjc3NcIjtcclxuaW1wb3J0IFwiQHN0eWxlcy9hZmZpbGlhdGUuc2Nzc1wiO1xyXG5pbXBvcnQgXCJAc3R5bGVzL2pvYnMtZGV0YWlscy5zY3NzXCI7XHJcbmltcG9ydCBcIkBzdHlsZXMvcHJpY2luZy5zY3NzXCI7XHJcbmltcG9ydCBcIkBzdHlsZXMvYWRtaW4tcmVmZXJyYWwtbGV2ZWwtMS5zY3NzXCI7XHJcblxyXG5pbXBvcnQgXCJAc3R5bGVzL2V4cGxvcmUtc2Nob29sLnNjc3NcIjtcclxuaW1wb3J0IFwiQHN0eWxlcy9BZG1pc3Npb25UcmFja2VyLnNjc3NcIjtcclxuXHJcbmltcG9ydCBzdG9yZSBmcm9tIFwiQHJlZHV4L3N0b3JlXCI7XHJcbmltcG9ydCB7IFByb3ZpZGVyIH0gZnJvbSBcInJlYWN0LXJlZHV4XCI7XHJcblxyXG5mdW5jdGlvbiBNeUFwcCh7IENvbXBvbmVudCwgcGFnZVByb3BzIH0pIHtcclxuICBjb25zdCBnZXRMYXlvdXQgPSBDb21wb25lbnQuZ2V0TGF5b3V0IHx8ICgocGFnZSkgPT4gcGFnZSk7XHJcbiAgcmV0dXJuIChcclxuICAgIDxQcm92aWRlciBzdG9yZT17c3RvcmV9PntnZXRMYXlvdXQoPENvbXBvbmVudCB7Li4ucGFnZVByb3BzfSAvPil9PC9Qcm92aWRlcj5cclxuICApO1xyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBNeUFwcDtcclxuIl0sIm5hbWVzIjpbInN0b3JlIiwiUHJvdmlkZXIiLCJNeUFwcCIsIkNvbXBvbmVudCIsInBhZ2VQcm9wcyIsImdldExheW91dCIsInBhZ2UiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./pages/_app.js\n");

/***/ }),

/***/ "./redux/reducer/modalReducer.js":
/*!***************************************!*\
  !*** ./redux/reducer/modalReducer.js ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"REGISTER_MODAL\": () => (/* binding */ REGISTER_MODAL),\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\nconst initialState = {\n    isRegisterModal: false\n};\nconst REGISTER_MODAL = \"REGISTER_MODAL\";\nconst modalReducer = (state = initialState, action)=>{\n    switch(action.type){\n        case REGISTER_MODAL:\n            return {\n                ...state,\n                isRegisterModal: action.status\n            };\n        default:\n            return {\n                ...state\n            };\n    }\n};\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (modalReducer);\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9yZWR1eC9yZWR1Y2VyL21vZGFsUmVkdWNlci5qcy5qcyIsIm1hcHBpbmdzIjoiOzs7OztBQUFBLE1BQU1BLFlBQVksR0FBRztJQUNuQkMsZUFBZSxFQUFFLEtBQUs7Q0FDdkI7QUFFTSxNQUFNQyxjQUFjLEdBQUcsZ0JBQWdCLENBQUM7QUFFL0MsTUFBTUMsWUFBWSxHQUFHLENBQUNDLEtBQUssR0FBR0osWUFBWSxFQUFFSyxNQUFNLEdBQUs7SUFDckQsT0FBUUEsTUFBTSxDQUFDQyxJQUFJO1FBQ2pCLEtBQUtKLGNBQWM7WUFDakIsT0FBTztnQkFBRSxHQUFHRSxLQUFLO2dCQUFFSCxlQUFlLEVBQUVJLE1BQU0sQ0FBQ0UsTUFBTTthQUFFLENBQUM7UUFFdEQ7WUFDRSxPQUFPO2dCQUFFLEdBQUdILEtBQUs7YUFBRSxDQUFDO0tBQ3ZCO0NBQ0Y7QUFFRCxpRUFBZUQsWUFBWSxFQUFDIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vdmVpZnlraXlhLy4vcmVkdXgvcmVkdWNlci9tb2RhbFJlZHVjZXIuanM/MWIxMCJdLCJzb3VyY2VzQ29udGVudCI6WyJjb25zdCBpbml0aWFsU3RhdGUgPSB7XHJcbiAgaXNSZWdpc3Rlck1vZGFsOiBmYWxzZSxcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBSRUdJU1RFUl9NT0RBTCA9IFwiUkVHSVNURVJfTU9EQUxcIjtcclxuXHJcbmNvbnN0IG1vZGFsUmVkdWNlciA9IChzdGF0ZSA9IGluaXRpYWxTdGF0ZSwgYWN0aW9uKSA9PiB7XHJcbiAgc3dpdGNoIChhY3Rpb24udHlwZSkge1xyXG4gICAgY2FzZSBSRUdJU1RFUl9NT0RBTDpcclxuICAgICAgcmV0dXJuIHsgLi4uc3RhdGUsIGlzUmVnaXN0ZXJNb2RhbDogYWN0aW9uLnN0YXR1cyB9O1xyXG5cclxuICAgIGRlZmF1bHQ6XHJcbiAgICAgIHJldHVybiB7IC4uLnN0YXRlIH07XHJcbiAgfVxyXG59O1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgbW9kYWxSZWR1Y2VyO1xyXG4iXSwibmFtZXMiOlsiaW5pdGlhbFN0YXRlIiwiaXNSZWdpc3Rlck1vZGFsIiwiUkVHSVNURVJfTU9EQUwiLCJtb2RhbFJlZHVjZXIiLCJzdGF0ZSIsImFjdGlvbiIsInR5cGUiLCJzdGF0dXMiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./redux/reducer/modalReducer.js\n");

/***/ }),

/***/ "./redux/reducer/refreshDataReducer.js":
/*!*********************************************!*\
  !*** ./redux/reducer/refreshDataReducer.js ***!
  \*********************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {\n__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"ADD_USER_DATA\": () => (/* binding */ ADD_USER_DATA),\n/* harmony export */   \"COMPANY_DATA\": () => (/* binding */ COMPANY_DATA),\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var _components_TokenServices__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @components/TokenServices */ \"./components/TokenServices.js\");\nvar __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_components_TokenServices__WEBPACK_IMPORTED_MODULE_0__]);\n_components_TokenServices__WEBPACK_IMPORTED_MODULE_0__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];\n\nconst initialState = {\n    userData: {},\n    companyProfile: {}\n};\nconst ADD_USER_DATA = \"ADD_USER_DATA\";\nconst COMPANY_DATA = \"COMPANY_DATA\";\nconst refreshDataReducer = (state = initialState, action)=>{\n    let user = (0,_components_TokenServices__WEBPACK_IMPORTED_MODULE_0__.getUserData)();\n    switch(action.type){\n        case ADD_USER_DATA:\n            let data = action.item;\n            return {\n                ...state,\n                userData: {\n                    ...data\n                }\n            };\n        case COMPANY_DATA:\n            let data1 = action.item;\n            return {\n                ...state,\n                companyProfile: {\n                    ...data1\n                }\n            };\n        default:\n            return {\n                ...state\n            };\n    }\n};\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (refreshDataReducer);\n\n__webpack_async_result__();\n} catch(e) { __webpack_async_result__(e); } });//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9yZWR1eC9yZWR1Y2VyL3JlZnJlc2hEYXRhUmVkdWNlci5qcy5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQXdEO0FBRXhELE1BQU1DLFlBQVksR0FBRztJQUNuQkMsUUFBUSxFQUFFLEVBQUU7SUFDWkMsY0FBYyxFQUFFLEVBQUU7Q0FDbkI7QUFFTSxNQUFNQyxhQUFhLEdBQUcsZUFBZSxDQUFDO0FBQ3RDLE1BQU1DLFlBQVksR0FBRyxjQUFjLENBQUM7QUFFM0MsTUFBTUMsa0JBQWtCLEdBQUcsQ0FBQ0MsS0FBSyxHQUFHTixZQUFZLEVBQUVPLE1BQU0sR0FBSztJQUMzRCxJQUFJQyxJQUFJLEdBQUdULHNFQUFXLEVBQUU7SUFDeEIsT0FBUVEsTUFBTSxDQUFDRSxJQUFJO1FBQ2pCLEtBQUtOLGFBQWE7WUFDaEIsSUFBSU8sSUFBSSxHQUFHSCxNQUFNLENBQUNJLElBQUk7WUFDdEIsT0FBTztnQkFBRSxHQUFHTCxLQUFLO2dCQUFFTCxRQUFRLEVBQUU7b0JBQUUsR0FBR1MsSUFBSTtpQkFBRTthQUFFLENBQUM7UUFFN0MsS0FBS04sWUFBWTtZQUNmLElBQUlRLEtBQUssR0FBR0wsTUFBTSxDQUFDSSxJQUFJO1lBQ3ZCLE9BQU87Z0JBQUUsR0FBR0wsS0FBSztnQkFBRUosY0FBYyxFQUFFO29CQUFFLEdBQUdVLEtBQUs7aUJBQUU7YUFBRSxDQUFDO1FBRXBEO1lBQ0UsT0FBTztnQkFBRSxHQUFHTixLQUFLO2FBQUUsQ0FBQztLQUN2QjtDQUNGO0FBRUQsaUVBQWVELGtCQUFrQixFQUFDIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vdmVpZnlraXlhLy4vcmVkdXgvcmVkdWNlci9yZWZyZXNoRGF0YVJlZHVjZXIuanM/NTA4ZSJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBnZXRVc2VyRGF0YSB9IGZyb20gXCJAY29tcG9uZW50cy9Ub2tlblNlcnZpY2VzXCI7XHJcblxyXG5jb25zdCBpbml0aWFsU3RhdGUgPSB7XHJcbiAgdXNlckRhdGE6IHt9LFxyXG4gIGNvbXBhbnlQcm9maWxlOiB7fSxcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBBRERfVVNFUl9EQVRBID0gXCJBRERfVVNFUl9EQVRBXCI7XHJcbmV4cG9ydCBjb25zdCBDT01QQU5ZX0RBVEEgPSBcIkNPTVBBTllfREFUQVwiO1xyXG5cclxuY29uc3QgcmVmcmVzaERhdGFSZWR1Y2VyID0gKHN0YXRlID0gaW5pdGlhbFN0YXRlLCBhY3Rpb24pID0+IHtcclxuICBsZXQgdXNlciA9IGdldFVzZXJEYXRhKCk7XHJcbiAgc3dpdGNoIChhY3Rpb24udHlwZSkge1xyXG4gICAgY2FzZSBBRERfVVNFUl9EQVRBOlxyXG4gICAgICBsZXQgZGF0YSA9IGFjdGlvbi5pdGVtO1xyXG4gICAgICByZXR1cm4geyAuLi5zdGF0ZSwgdXNlckRhdGE6IHsgLi4uZGF0YSB9IH07XHJcblxyXG4gICAgY2FzZSBDT01QQU5ZX0RBVEE6XHJcbiAgICAgIGxldCBkYXRhMSA9IGFjdGlvbi5pdGVtO1xyXG4gICAgICByZXR1cm4geyAuLi5zdGF0ZSwgY29tcGFueVByb2ZpbGU6IHsgLi4uZGF0YTEgfSB9O1xyXG5cclxuICAgIGRlZmF1bHQ6XHJcbiAgICAgIHJldHVybiB7IC4uLnN0YXRlIH07XHJcbiAgfVxyXG59O1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgcmVmcmVzaERhdGFSZWR1Y2VyO1xyXG4iXSwibmFtZXMiOlsiZ2V0VXNlckRhdGEiLCJpbml0aWFsU3RhdGUiLCJ1c2VyRGF0YSIsImNvbXBhbnlQcm9maWxlIiwiQUREX1VTRVJfREFUQSIsIkNPTVBBTllfREFUQSIsInJlZnJlc2hEYXRhUmVkdWNlciIsInN0YXRlIiwiYWN0aW9uIiwidXNlciIsInR5cGUiLCJkYXRhIiwiaXRlbSIsImRhdGExIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./redux/reducer/refreshDataReducer.js\n");

/***/ }),

/***/ "./redux/reducer/userReducer.js":
/*!**************************************!*\
  !*** ./redux/reducer/userReducer.js ***!
  \**************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {\n__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"USER_DETAIL\": () => (/* binding */ USER_DETAIL),\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var _components_TokenServices__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @components/TokenServices */ \"./components/TokenServices.js\");\nvar __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_components_TokenServices__WEBPACK_IMPORTED_MODULE_0__]);\n_components_TokenServices__WEBPACK_IMPORTED_MODULE_0__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];\n\nconst initialState = {\n    userDetail: {}\n};\nconst USER_DETAIL = \"USER_DETAIL\";\nconst userReducer = (state = initialState, action)=>{\n    let user = (0,_components_TokenServices__WEBPACK_IMPORTED_MODULE_0__.getUserData)();\n    switch(action.type){\n        case USER_DETAIL:\n            return {\n                ...state,\n                ...user\n            };\n        default:\n            return {\n                ...state\n            };\n    }\n};\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (userReducer);\n\n__webpack_async_result__();\n} catch(e) { __webpack_async_result__(e); } });//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9yZWR1eC9yZWR1Y2VyL3VzZXJSZWR1Y2VyLmpzLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUF3RDtBQUV4RCxNQUFNQyxZQUFZLEdBQUc7SUFDbkJDLFVBQVUsRUFBRSxFQUFFO0NBQ2Y7QUFFTSxNQUFNQyxXQUFXLEdBQUcsYUFBYSxDQUFDO0FBRXpDLE1BQU1DLFdBQVcsR0FBRyxDQUFDQyxLQUFLLEdBQUdKLFlBQVksRUFBRUssTUFBTSxHQUFLO0lBQ3BELElBQUlDLElBQUksR0FBR1Asc0VBQVcsRUFBRTtJQUN4QixPQUFRTSxNQUFNLENBQUNFLElBQUk7UUFDakIsS0FBS0wsV0FBVztZQUNkLE9BQU87Z0JBQUUsR0FBR0UsS0FBSztnQkFBRSxHQUFHRSxJQUFJO2FBQUUsQ0FBQztRQUUvQjtZQUNFLE9BQU87Z0JBQUUsR0FBR0YsS0FBSzthQUFFLENBQUM7S0FDdkI7Q0FDRjtBQUVELGlFQUFlRCxXQUFXLEVBQUMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly92ZWlmeWtpeWEvLi9yZWR1eC9yZWR1Y2VyL3VzZXJSZWR1Y2VyLmpzP2U2NTUiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgZ2V0VXNlckRhdGEgfSBmcm9tIFwiQGNvbXBvbmVudHMvVG9rZW5TZXJ2aWNlc1wiO1xyXG5cclxuY29uc3QgaW5pdGlhbFN0YXRlID0ge1xyXG4gIHVzZXJEZXRhaWw6IHt9LFxyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IFVTRVJfREVUQUlMID0gXCJVU0VSX0RFVEFJTFwiO1xyXG5cclxuY29uc3QgdXNlclJlZHVjZXIgPSAoc3RhdGUgPSBpbml0aWFsU3RhdGUsIGFjdGlvbikgPT4ge1xyXG4gIGxldCB1c2VyID0gZ2V0VXNlckRhdGEoKTtcclxuICBzd2l0Y2ggKGFjdGlvbi50eXBlKSB7XHJcbiAgICBjYXNlIFVTRVJfREVUQUlMOlxyXG4gICAgICByZXR1cm4geyAuLi5zdGF0ZSwgLi4udXNlciB9O1xyXG5cclxuICAgIGRlZmF1bHQ6XHJcbiAgICAgIHJldHVybiB7IC4uLnN0YXRlIH07XHJcbiAgfVxyXG59O1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgdXNlclJlZHVjZXI7XHJcbiJdLCJuYW1lcyI6WyJnZXRVc2VyRGF0YSIsImluaXRpYWxTdGF0ZSIsInVzZXJEZXRhaWwiLCJVU0VSX0RFVEFJTCIsInVzZXJSZWR1Y2VyIiwic3RhdGUiLCJhY3Rpb24iLCJ1c2VyIiwidHlwZSJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./redux/reducer/userReducer.js\n");

/***/ }),

/***/ "./redux/rootReducer.js":
/*!******************************!*\
  !*** ./redux/rootReducer.js ***!
  \******************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {\n__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux */ \"redux\");\n/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _reducer_refreshDataReducer__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./reducer/refreshDataReducer */ \"./redux/reducer/refreshDataReducer.js\");\n/* harmony import */ var _reducer_userReducer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./reducer/userReducer */ \"./redux/reducer/userReducer.js\");\n/* harmony import */ var _reducer_modalReducer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./reducer/modalReducer */ \"./redux/reducer/modalReducer.js\");\nvar __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_reducer_refreshDataReducer__WEBPACK_IMPORTED_MODULE_1__, _reducer_userReducer__WEBPACK_IMPORTED_MODULE_2__]);\n([_reducer_refreshDataReducer__WEBPACK_IMPORTED_MODULE_1__, _reducer_userReducer__WEBPACK_IMPORTED_MODULE_2__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);\n\n\n\n\nconst rootReducer = (0,redux__WEBPACK_IMPORTED_MODULE_0__.combineReducers)({\n    userData: _reducer_userReducer__WEBPACK_IMPORTED_MODULE_2__[\"default\"],\n    refreshDataReducer: _reducer_refreshDataReducer__WEBPACK_IMPORTED_MODULE_1__[\"default\"],\n    modalReducer: _reducer_modalReducer__WEBPACK_IMPORTED_MODULE_3__[\"default\"]\n});\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (rootReducer);\n\n__webpack_async_result__();\n} catch(e) { __webpack_async_result__(e); } });//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9yZWR1eC9yb290UmVkdWNlci5qcy5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFBd0M7QUFDc0I7QUFDZDtBQUNFO0FBRWxELE1BQU1JLFdBQVcsR0FBR0osc0RBQWUsQ0FBQztJQUNsQ0ssUUFBUSxFQUFFSCw0REFBVztJQUNyQkQsa0JBQWtCLEVBQUVBLG1FQUFrQjtJQUN0Q0UsWUFBWSxFQUFFQSw2REFBWTtDQUMzQixDQUFDO0FBRUYsaUVBQWVDLFdBQVcsRUFBQyIsInNvdXJjZXMiOlsid2VicGFjazovL3ZlaWZ5a2l5YS8uL3JlZHV4L3Jvb3RSZWR1Y2VyLmpzP2YzMGEiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgY29tYmluZVJlZHVjZXJzIH0gZnJvbSBcInJlZHV4XCI7XHJcbmltcG9ydCByZWZyZXNoRGF0YVJlZHVjZXIgZnJvbSBcIi4vcmVkdWNlci9yZWZyZXNoRGF0YVJlZHVjZXJcIjtcclxuaW1wb3J0IHVzZXJSZWR1Y2VyIGZyb20gXCIuL3JlZHVjZXIvdXNlclJlZHVjZXJcIjtcclxuaW1wb3J0IG1vZGFsUmVkdWNlciBmcm9tIFwiLi9yZWR1Y2VyL21vZGFsUmVkdWNlclwiO1xyXG5cclxuY29uc3Qgcm9vdFJlZHVjZXIgPSBjb21iaW5lUmVkdWNlcnMoe1xyXG4gIHVzZXJEYXRhOiB1c2VyUmVkdWNlcixcclxuICByZWZyZXNoRGF0YVJlZHVjZXI6IHJlZnJlc2hEYXRhUmVkdWNlcixcclxuICBtb2RhbFJlZHVjZXI6IG1vZGFsUmVkdWNlcixcclxufSk7XHJcblxyXG5leHBvcnQgZGVmYXVsdCByb290UmVkdWNlcjtcclxuIl0sIm5hbWVzIjpbImNvbWJpbmVSZWR1Y2VycyIsInJlZnJlc2hEYXRhUmVkdWNlciIsInVzZXJSZWR1Y2VyIiwibW9kYWxSZWR1Y2VyIiwicm9vdFJlZHVjZXIiLCJ1c2VyRGF0YSJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./redux/rootReducer.js\n");

/***/ }),

/***/ "./redux/store.js":
/*!************************!*\
  !*** ./redux/store.js ***!
  \************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {\n__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux */ \"redux\");\n/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _rootReducer__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./rootReducer */ \"./redux/rootReducer.js\");\nvar __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_rootReducer__WEBPACK_IMPORTED_MODULE_1__]);\n_rootReducer__WEBPACK_IMPORTED_MODULE_1__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];\n\n\nconst store = (0,redux__WEBPACK_IMPORTED_MODULE_0__.createStore)(_rootReducer__WEBPACK_IMPORTED_MODULE_1__[\"default\"]);\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (store);\n\n__webpack_async_result__();\n} catch(e) { __webpack_async_result__(e); } });//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9yZWR1eC9zdG9yZS5qcy5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQW9DO0FBQ0k7QUFFeEMsTUFBTUUsS0FBSyxHQUFHRixrREFBVyxDQUFDQyxvREFBVyxDQUFDO0FBRXRDLGlFQUFlQyxLQUFLLEVBQUMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly92ZWlmeWtpeWEvLi9yZWR1eC9zdG9yZS5qcz8zNTQ5Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IGNyZWF0ZVN0b3JlIH0gZnJvbSBcInJlZHV4XCI7XHJcbmltcG9ydCByb290UmVkdWNlciBmcm9tIFwiLi9yb290UmVkdWNlclwiO1xyXG5cclxuY29uc3Qgc3RvcmUgPSBjcmVhdGVTdG9yZShyb290UmVkdWNlcik7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBzdG9yZTtcclxuIl0sIm5hbWVzIjpbImNyZWF0ZVN0b3JlIiwicm9vdFJlZHVjZXIiLCJzdG9yZSJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./redux/store.js\n");

/***/ }),

/***/ "./node_modules/antd/dist/antd.css":
/*!*****************************************!*\
  !*** ./node_modules/antd/dist/antd.css ***!
  \*****************************************/
/***/ (() => {



/***/ }),

/***/ "./node_modules/bootstrap/dist/css/bootstrap.min.css":
/*!***********************************************************!*\
  !*** ./node_modules/bootstrap/dist/css/bootstrap.min.css ***!
  \***********************************************************/
/***/ (() => {



/***/ }),

/***/ "./node_modules/font-awesome/css/font-awesome.css":
/*!********************************************************!*\
  !*** ./node_modules/font-awesome/css/font-awesome.css ***!
  \********************************************************/
/***/ (() => {



/***/ }),

/***/ "./node_modules/nprogress/nprogress.css":
/*!**********************************************!*\
  !*** ./node_modules/nprogress/nprogress.css ***!
  \**********************************************/
/***/ (() => {



/***/ }),

/***/ "./node_modules/react-intl-tel-input/dist/main.css":
/*!*********************************************************!*\
  !*** ./node_modules/react-intl-tel-input/dist/main.css ***!
  \*********************************************************/
/***/ (() => {



/***/ }),

/***/ "./styles/AdminElSchool.scss":
/*!***********************************!*\
  !*** ./styles/AdminElSchool.scss ***!
  \***********************************/
/***/ (() => {



/***/ }),

/***/ "./styles/AdminProfile.scss":
/*!**********************************!*\
  !*** ./styles/AdminProfile.scss ***!
  \**********************************/
/***/ (() => {



/***/ }),

/***/ "./styles/AdmissionTracker.scss":
/*!**************************************!*\
  !*** ./styles/AdmissionTracker.scss ***!
  \**************************************/
/***/ (() => {



/***/ }),

/***/ "./styles/CoursesDetails.scss":
/*!************************************!*\
  !*** ./styles/CoursesDetails.scss ***!
  \************************************/
/***/ (() => {



/***/ }),

/***/ "./styles/Login.scss":
/*!***************************!*\
  !*** ./styles/Login.scss ***!
  \***************************/
/***/ (() => {



/***/ }),

/***/ "./styles/PartnerWithUs.scss":
/*!***********************************!*\
  !*** ./styles/PartnerWithUs.scss ***!
  \***********************************/
/***/ (() => {



/***/ }),

/***/ "./styles/about-us.scss":
/*!******************************!*\
  !*** ./styles/about-us.scss ***!
  \******************************/
/***/ (() => {



/***/ }),

/***/ "./styles/admin-referral-level-1.scss":
/*!********************************************!*\
  !*** ./styles/admin-referral-level-1.scss ***!
  \********************************************/
/***/ (() => {



/***/ }),

/***/ "./styles/admin.scss":
/*!***************************!*\
  !*** ./styles/admin.scss ***!
  \***************************/
/***/ (() => {



/***/ }),

/***/ "./styles/affiliate.scss":
/*!*******************************!*\
  !*** ./styles/affiliate.scss ***!
  \*******************************/
/***/ (() => {



/***/ }),

/***/ "./styles/contact.scss":
/*!*****************************!*\
  !*** ./styles/contact.scss ***!
  \*****************************/
/***/ (() => {



/***/ }),

/***/ "./styles/corporate.scss":
/*!*******************************!*\
  !*** ./styles/corporate.scss ***!
  \*******************************/
/***/ (() => {



/***/ }),

/***/ "./styles/courses.scss":
/*!*****************************!*\
  !*** ./styles/courses.scss ***!
  \*****************************/
/***/ (() => {



/***/ }),

/***/ "./styles/educational-institutions.scss":
/*!**********************************************!*\
  !*** ./styles/educational-institutions.scss ***!
  \**********************************************/
/***/ (() => {



/***/ }),

/***/ "./styles/event.scss":
/*!***************************!*\
  !*** ./styles/event.scss ***!
  \***************************/
/***/ (() => {



/***/ }),

/***/ "./styles/explore-school.scss":
/*!************************************!*\
  !*** ./styles/explore-school.scss ***!
  \************************************/
/***/ (() => {



/***/ }),

/***/ "./styles/header.scss":
/*!****************************!*\
  !*** ./styles/header.scss ***!
  \****************************/
/***/ (() => {



/***/ }),

/***/ "./styles/home.scss":
/*!**************************!*\
  !*** ./styles/home.scss ***!
  \**************************/
/***/ (() => {



/***/ }),

/***/ "./styles/jobs-details.scss":
/*!**********************************!*\
  !*** ./styles/jobs-details.scss ***!
  \**********************************/
/***/ (() => {



/***/ }),

/***/ "./styles/pricing.scss":
/*!*****************************!*\
  !*** ./styles/pricing.scss ***!
  \*****************************/
/***/ (() => {



/***/ }),

/***/ "./styles/profile.scss":
/*!*****************************!*\
  !*** ./styles/profile.scss ***!
  \*****************************/
/***/ (() => {



/***/ }),

/***/ "./styles/registration.scss":
/*!**********************************!*\
  !*** ./styles/registration.scss ***!
  \**********************************/
/***/ (() => {



/***/ }),

/***/ "./styles/resources.scss":
/*!*******************************!*\
  !*** ./styles/resources.scss ***!
  \*******************************/
/***/ (() => {



/***/ }),

/***/ "./styles/school-course-details.scss":
/*!*******************************************!*\
  !*** ./styles/school-course-details.scss ***!
  \*******************************************/
/***/ (() => {



/***/ }),

/***/ "./styles/sidebar.scss":
/*!*****************************!*\
  !*** ./styles/sidebar.scss ***!
  \*****************************/
/***/ (() => {



/***/ }),

/***/ "./styles/style.scss":
/*!***************************!*\
  !*** ./styles/style.scss ***!
  \***************************/
/***/ (() => {



/***/ }),

/***/ "./styles/wallet.scss":
/*!****************************!*\
  !*** ./styles/wallet.scss ***!
  \****************************/
/***/ (() => {



/***/ }),

/***/ "jsonwebtoken":
/*!*******************************!*\
  !*** external "jsonwebtoken" ***!
  \*******************************/
/***/ ((module) => {

"use strict";
module.exports = require("jsonwebtoken");

/***/ }),

/***/ "react-redux":
/*!******************************!*\
  !*** external "react-redux" ***!
  \******************************/
/***/ ((module) => {

"use strict";
module.exports = require("react-redux");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ ((module) => {

"use strict";
module.exports = require("react/jsx-dev-runtime");

/***/ }),

/***/ "redux":
/*!************************!*\
  !*** external "redux" ***!
  \************************/
/***/ ((module) => {

"use strict";
module.exports = require("redux");

/***/ }),

/***/ "js-cookie":
/*!****************************!*\
  !*** external "js-cookie" ***!
  \****************************/
/***/ ((module) => {

"use strict";
module.exports = import("js-cookie");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("./pages/_app.js"));
module.exports = __webpack_exports__;

})();