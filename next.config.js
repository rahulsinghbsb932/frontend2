module.exports = {
  images: {
    domains: ["localhost"],
    deviceSizes: [640, 750, 828, 1080, 1200, 1440, 1600, 1920, 2048, 3840],
  },
  eslint: { ignoreDuringBuilds: true },
  env: {
    basePath: "http://192.168.10.81:4000/",
    baseUrl: "http://localhost:3000/",
    GOOGLE_ANALYTICS_ID: "UA-263947855-1",
  },
  webpack: (config, options) => {
    config.resolve.fallback = { fs: false };
    return config;
  },
};
