export const loggedInUserType = {
  1: "company",
  2: "manufacturer",
  3: "warehouse",
  4: "distributor",
  5: "user",
  11: "importer",
  12: "dealer",
};

export function loggedInDashboardUrlFun(slug, id) {
  let urls = {
    1: "/company/dashboard",
    2: "/manufacturer/dashboard",
    3: "/warehouse/dashboard",
    4: "/distributor/dashboard",
    5: "/user/dashboard",
    11: "/importer/dashboard",
    12: "/dealer/dashboard",
  };

  let tempUrl = urls[id];
  let finalUrl = `/${slug}${tempUrl}`;
  return finalUrl;
}

export function scanQrCodeUrl(slug, id) {
  let urls = {
    1: "/medsure/company/dashboard",
    2: "/medsure/manufacturer/dashboard",
    3: "/warehouse/authenticate",
    4: "/distributor/authenticate",
    5: "/user/scan-qr-code",
    11: "/importer/authenticate",
    12: "/dealer/authenticate",
  };
  let tempUrl = urls[id];
  let finalUrl = `/${slug}${tempUrl}`;
  return finalUrl;
}

export function profileUrl(slug, id) {
  let urls = {
    1: "/company/profile",
    2: "/manufacturer/profile",
    3: "/warehouse/profile",
    4: "/distributor/profile",
    5: "/user/profile",
    11: "/importer/profile",
    12: "/dealer/profile",
  };
  let tempUrl = urls[id];
  let finalUrl = `/${slug}${tempUrl}`;
  return finalUrl;
}

export const loggedInUserTypeName = {
  1: "Company",
  2: "Manufacturer",
  3: "Warehouse",
  4: "Distributor",
  5: "User",
  11: "Importer",
  12: "Dealer",
};
