import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import Head from "next/head";
import Image from "next/image";
import PageLayout from "@components/PageLayout";
import Card from "react-bootstrap/Card";

function About() {
  return (
    <div>
      <Head>
        <title>About VerifyKiya - Our Mission, Vision, and Values</title>
        <meta
          id="meta-description"
          name="description"
          content="Learn about VerifyKiya's mission, vision, and values. Discover how we're changing the game when it comes to certificate verification, admissions, and results."
        />
        <meta
          property="og:title"
          content="About VerifyKiya - Our Mission, Vision, and Values"
        />
        <meta
          property="og:description"
          content="Learn about VerifyKiya's mission, vision, and values. Discover how we're changing the game when it comes to certificate verification, admissions, and results."
        />
        <meta
          property="twitter:title"
          content="About VerifyKiya - Our Mission, Vision, and Values"
        />
        <meta
          property="twitter:description"
          content="Learn about VerifyKiya's mission, vision, and values. Discover how we're changing the game when it comes to certificate verification, admissions, and results."
        />
      </Head>
      <PageLayout>
        <div className="HeroSectionWrapper">
          <div className="headingAboutUs Montserrat">
            <h1>Our Story</h1>
            <div className="aboutuspera Montserrat">
              <p>
                VerifyKiya’s journey began with the coming together of four
                like-minded individuals, each of whom believed that India’s
                education landscape and job market needed a major overhaul.
                Tormented by long admission and job onboarding processes, which
                often resulted in lesser-qualified individuals making the final
                cut, we decided to come up with a go-to solution that every
                stakeholder from the education industry and job market could
                benefit from.
              </p>
            </div>
          </div>
          <Container className=" pt-5">
            <Row>
              <Col md={4}>
                <div className="aboutbox1">
                  <Image
                    src="/images/AboutImages/IconBox1.png"
                    width={266}
                    height={266}
                    className="img-fluid"
                    alt="Our Vision"
                  />
                  <h4>Our Vision</h4>
                  <p>
                    Creating an egalitarian and merit-based world with equal
                    opportunities for one and all.
                  </p>
                </div>
              </Col>
              <Col md={4}>
                <div className="aboutbox1">
                  <Image
                    src="/images/AboutImages/IconBox2.png"
                    width={266}
                    height={266}
                    className="img-fluid"
                    alt="Our Mission"
                  />
                  <h4>Our Mission</h4>
                  <p>
                    Making access to education and job opportunities super-easy
                    for everyone.
                  </p>
                </div>
              </Col>
              <Col md={4}>
                <div className="aboutbox1">
                  <Image
                    src="/images/AboutImages/IconBox3.png"
                    width={266}
                    height={266}
                    className="img-fluid"
                    alt="Our Values"
                  />
                  <h4>Our Values</h4>
                  <p>
                    We’re guided by strong virtues of transparency, equity, and
                    accountability.
                  </p>
                </div>
              </Col>
            </Row>
          </Container>
        </div>
        <Container fluid className="">
          <Row>
            <Col md={4}>
              <div className="FutureImageBox">
                <Image
                  src="/images/OurSolution/Group_187blockchan.svg"
                  width={427}
                  height={635}
                  className="img-fluid"
                  alt="Blockchain"
                />
              </div>
            </Col>
            <Col md={8} className="">
              <div className="AboutHeadingblack Montserrat">
                <h1 className="AboutHeadingOne">What Sets Us</h1>
                <h2 className="AboutheadingTwo">Apart?</h2>
                <p className="AboutPera">
                  VerifyKiya isn’t merely a profit-making medium but the gateway
                  to India’s ascendence across the global academic echelons. We
                  help you pursue excellence; we brighten up the future!
                </p>
                <p className="AboutPera">
                  While problem-solving lies at the core of our principles, we
                  are also guided by a relentless quest to see Indians make
                  their mark at every stage. Thus, we do everything within our
                  reach to give them the required impetus to unlock academic and
                  professional pathways.
                </p>
              </div>
            </Col>
          </Row>
        </Container>
        <Container fluid className="">
          <Row className="justify-content-between Montserrat ">
            <Col md={8} className="instituteCustompadding">
              <div>
                <h1 className="AboutHeadingOne">
                  We Help Every Stakeholder At Every Step of
                </h1>
                <h2 className="AboutheadingTwo">the Way!</h2>
                <p className="AboutPera mt-4 mb-3">
                  We truly believe in taking everyone along, which is why
                  VerifyKiya seeks to onboard every last stakeholder associated
                  with the education industry and job market. Through a
                  collaborative approach, we create new avenues and incentives
                  that can benefit every participant.{" "}
                </p>
                <p className="AboutPera mt-4 mb-3">
                  Thus, while we ensure that students get easy access to the
                  best education and job opportunities, we also see to it that
                  educational institutions and corporates have the best talent
                  pool at their disposal along with a heightened visibility
                  across multiple channels.
                </p>
              </div>
            </Col>
            <Col md={4}>
              <div className="FutureImageBox">
                <Image
                  src="/images/OurSolution/Group_188brand.svg"
                  width={427}
                  height={635}
                  className="img-fluid"
                  alt="Brand"
                />
              </div>
            </Col>
          </Row>
        </Container>

        <Container fluid className="">
          <Row>
            <Col md={4}>
              <div className="FutureImageBox">
                <Image
                  src="/images/OurSolution/Group_187unified.svg"
                  width={427}
                  height={635}
                  className="img-fluid"
                  alt="Unified platform for learner acquisition, retention, and
                      upsell"
                />
              </div>
            </Col>
            <Col md={8}>
              <div className="AboutHeadingblack">
                <h1 className="AboutHeadingOne">
                  VerifyKiya: Bridging The Gap Between
                </h1>
                <h2 className="AboutheadingTwo">Talent & Opportunities!</h2>
                <p className="AboutPera">
                  VerifyKiya is a one-of-its-kind next-generation platform that
                  follows the adage- exposure escalates experience. As an
                  all-in-one blockchain-powered platform, VerifyKiya has
                  established itself as a reliable avenue for students and job
                  seekers who wish to maximize the potential of blockchain
                  technology and ride the digital wave.{" "}
                </p>
                <p className="AboutPera">
                  From offering access to admission forms and scholarships to
                  dishing out real-time updates on job openings that match your
                  skills and educational background, we, at VerifyKiya, strive
                  hard to help you stand out of the crowd and shine like the
                  morning star!
                </p>
              </div>
            </Col>
          </Row>
        </Container>
        <Container fluid className="">
          <Row>
            <Col md={12}>
              <div className="benifitsHeadingblack Montserrat">
                <h1 className="AboutHeadingOne">
                  Meet Our
                  <span className="AboutheadingTwo"> Team</span>
                </h1>

                <p className="AboutPera">
                  The VerifyKiya founding team comprises four individuals with
                  wholly different tastes, who have nothing in common except a
                  never-ending penchant for blockchain technology and an
                  unwavering faith in its potential to disrupt the world as it
                  stands. The VerifyKiya team consists of-
                </p>
              </div>
            </Col>
          </Row>
          <Row>
            <Col md={3}>
              <Card className="CardAbout">
                <Image
                  src="/images/team/raghav.svg"
                  width={315}
                  height={360}
                  className="img-fluid"
                  alt="Our Vision"
                />
                <Card.Body>
                  <Card.Title>Raghav Agarwal</Card.Title>
                  <Card.Text className="institutesubpera">
                    Masters in Business Financial Management,University of
                    Wollongong, Oxford Certified Blockchain Analyst
                  </Card.Text>
                </Card.Body>
              </Card>
            </Col>
            <Col md={3}>
              <Card className="CardAbout">
                <Image
                  src="/images/team/devika.svg"
                  width={315}
                  height={360}
                  className="img-fluid"
                  alt="Our Vision"
                />

                <Card.Body>
                  <Card.Title>Devika Raaj Gupta</Card.Title>
                  <Card.Text className="institutesubpera">
                    B.Sc (Hons), Mathematics, Jesus & Mary College, Delhi
                    University
                  </Card.Text>
                </Card.Body>
              </Card>
            </Col>
            <Col md={3}>
              <Card className="CardAbout">
                <Image
                  src="/images/team/keshav.svg"
                  width={315}
                  height={360}
                  className="img-fluid"
                  alt="Our Vision"
                />

                <Card.Body>
                  <Card.Title>Keshav Agarwal</Card.Title>
                  <Card.Text className="institutesubpera">
                    Masters in Management, Warwick Business School, United
                    Kingdom
                  </Card.Text>
                </Card.Body>
              </Card>
            </Col>
            <Col md={3}>
              <Card className="CardAbout">
                <Image
                  src="/images/team/radhika.svg"
                  width={315}
                  height={360}
                  className="img-fluid"
                  alt="Our Vision"
                />
                <Card.Body>
                  <Card.Title>Radhika Agarwal</Card.Title>
                  <Card.Text className="institutesubpera">
                    B.Com (Hons), Lady Shri Ram College for Women, Delhi
                    University
                  </Card.Text>
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Container>
      </PageLayout>
    </div>
  );
}

export default About;
