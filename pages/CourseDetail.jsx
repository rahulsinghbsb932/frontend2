import React from "react";
import { Col, Container, Row, Form, InputGroup } from "react-bootstrap";

import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import Typography from "@mui/material/Typography";
import PageLayout from "@components/PageLayout";
import Head from "next/head";
import Signup from "./form/Signup";
import RegisterRecieve from "./form/RegisterRecieve";
import Subscribe from "./form/Subscribe";
// import ExpandMoreIcon from "@mui/icons-material/ExpandMore";

const DiplomaCourses = [
  {
    name: "Diploma in Engineering",
  },
  {
    name: "Diploma in Hotel Management",
  },
  {
    name: "Diploma in Journalism",
  },
  {
    name: "Diploma in Education",
  },
  {
    name: "Diploma in Photography",
  },
  {
    name: "Diploma in Peychulagy",
  },
  {
    name: "Diploma in Elementary Education",
  },
  {
    name: "Diploma in Digital Marketing",
  },
  {
    name: "Diploma in Fine Arts",
  },
  {
    name: "Diploma in English",
  },
];

const DiplomaBranch = [
  {
    profile: "Course Overview",
    name: "Diploma in Automobile Engineering",
  },
  {
    profile: "Eligibility Criteria",
    name: "Diploma in Electrical Engineering",
  },
  {
    profile: "Subjects",
    name: " Diploma in Mechanical Engineering",
  },
  {
    profile: "Popular Courses",
    name: "Diploma in Automobile Engineering",
  },
];

const PopularCourses = [
  {
    img: "/images/checkIcon.svg",
    degree: "Bachelor/Undergradute",
    courses: "Arts, Humanities and Social Science",
  },
  {
    img: "/images/checkIcon.svg",
    degree: "Master with Pre-Master Programme",
    courses: "Medicine, Pharmacy , Health and Life Sciences",
  },

  {
    img: "/images/checkIcon.svg",
    degree: "Bachelor with Foundation Year",
    courses: "Computer Science, Data Sciene & IT",
  },
  {
    img: "/images/checkIcon.svg",
    degree: "PhD / Doctorate / Master",
    courses: "Science",
  },
];

const UniversityDetails = [
  {
    logo: "/images/CoursesDetails/Stanford.svg",
    name: "Stanford Graduate School of Business",
    place: "Stanford (CA), United States",
  },
  {
    logo: "/images/CoursesDetails/Stanford.svg",
    name: "Stanford Graduate School of Business",
    place: "Stanford (CA), United States",
  },
  {
    logo: "/images/CoursesDetails/Stanford.svg",
    name: "Stanford Graduate School of Business",
    place: "Stanford (CA), United States",
  },
  {
    logo: "/images/CoursesDetails/Stanford.svg",
    name: "Stanford Graduate School of Business",
    place: "Stanford (CA), United States",
  },
];

const CourseDetail = () => {
  return (
    <>
      <Head>
        <title>Courses Details | Business Verification Services</title>
        <meta
          id="meta-description"
          name="description"
          content=" This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
        <meta
          property="og:title"
          content="Courses Details | Business Verification Services"
        />
      </Head>
      <PageLayout>
        <div className="HeroSectionWrapper CoursesDetails">
          <Container fluid>
            <Row>
              <Col lg={12}>
                <div className="HeroSection">
                  <h1 className="HeroSection-Heading">
                    Courses After 10th Standard
                  </h1>
                  <p className="HeroSection-Para">
                    If you aim to pursue a course after the 10th, there is a
                    wide range of courses on offer across various fields of
                    study. You can explore a long list of courses after the 10th
                    which are available in Designing, Engineering, Technology,
                    Business, Management, Computer Science, Arts, Media
                    Journalism and so on.
                  </p>

                  <div className="Sub-HeroSection">
                    <Row>
                      <Col lg={4}>
                        <Row>
                          <Col lg={2}>
                            <div className="Sub-HeroSection-Img">
                              <img
                                src="/images/checkIcon.svg"
                                className="Sub-HeroSection-Img-Icn"
                                alt=""
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <div className="Sub-HeroSection-Text">
                              <p className="Sub-HeroSection-Text-Para">
                                Class XI-Class XII
                              </p>
                            </div>
                          </Col>
                        </Row>
                      </Col>
                      <Col lg={4}>
                        <Row>
                          <Col lg={2}>
                            <div className="Sub-HeroSection-Img">
                              <img
                                src="/images/checkIcon.svg"
                                className="Sub-HeroSection-Img-Icn"
                                alt=""
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <div className="Sub-HeroSection-Text">
                              <p className="Sub-HeroSection-Text-Para">
                                Diploma Courses
                              </p>
                            </div>
                          </Col>
                        </Row>
                      </Col>
                      <Col lg={4}>
                        <Row>
                          <Col lg={2}>
                            <div className="Sub-HeroSection-Img">
                              <img
                                src="/images/checkIcon.svg"
                                className="Sub-HeroSection-Img-Icn"
                                alt=""
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <div className="Sub-HeroSection-Text">
                              <p className="Sub-HeroSection-Text-Para">
                                Polytechnic Courses
                              </p>
                            </div>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                    <Row>
                      <Col lg={4}>
                        <Row>
                          <Col lg={2}>
                            <div className="Sub-HeroSection-Img">
                              <img
                                src="/images/checkIcon.svg"
                                className="Sub-HeroSection-Img-Icn"
                                alt=""
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <div className="Sub-HeroSection-Text">
                              <p className="Sub-HeroSection-Text-Para">
                                Paramedical Courses
                              </p>
                            </div>
                          </Col>
                        </Row>
                      </Col>
                      <Col lg={4}>
                        <Row>
                          <Col lg={2}>
                            <div className="Sub-HeroSection-Img">
                              <img
                                src="/images/checkIcon.svg"
                                className="Sub-HeroSection-Img-Icn"
                                alt=""
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <div className="Sub-HeroSection-Text">
                              <p className="Sub-HeroSection-Text-Para">
                                ITI Courses
                              </p>
                            </div>
                          </Col>
                        </Row>
                      </Col>
                      <Col lg={4}>
                        <Row>
                          <Col lg={2}>
                            <div className="Sub-HeroSection-Img">
                              <img
                                src="/images/checkIcon.svg"
                                className="Sub-HeroSection-Img-Icn"
                                alt=""
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <div className="Sub-HeroSection-Text">
                              <p className="Sub-HeroSection-Text-Para">
                                Vocational Courses
                              </p>
                            </div>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                  </div>
                </div>
              </Col>
            </Row>
          </Container>
        </div>

        <div className="DiplomaCourses">
          <h1 className="DiplomaCourses-inner-heading">Diploma Courses</h1>
          <Container fluid>
            <Row>
              <Col lg={12}>
                <div className="DiplomaCourses-inner">
                  <ul className="DiplomaCourses-branch">
                    {DiplomaCourses.map((val) => {
                      return (
                        <li className="DiplomaCourses-branch-width">
                          {val.name}
                        </li>
                      );
                    })}
                  </ul>
                </div>
              </Col>
            </Row>
          </Container>
        </div>
        <Container fluid>
          <div className="d-flex flex-wrap flex-lg-nowrap ">
            <div className="mx-auto">
              <div className="frame-width">
                <iframe
                  src="/images/1.png"
                  scrolling="no"
                  width="100%"
                  height="100%"
                  class="responsive-iframe"
                ></iframe>
              </div>
            </div>
          </div>
        </Container>

        <div className="MidSection ">
          <Container fluid>
            <Row>
              <Col lg={9}>
                <div className="MidSection-component">
                  <div className="MidSection-component-inner">
                    <div className="align-items-end d-flex flex-wrap">
                      <div className="me-3">
                        <div className="MidSection-component-img">
                          <img
                            src="/images/CoursesDetails/midicon.svg"
                            alt=""
                            className="img-fluid"
                          />
                        </div>
                      </div>
                      <div>
                        <h1 className="MidSection-Conponent-heading">
                          Diploma in Engineering
                        </h1>
                      </div>
                    </div>
                  </div>
                  <div className="MidSection-Conponent-para">
                    <p className="MidSection-Conponent-para-inner">
                      Diploma in Engineering is a great option for those who
                      want to pursue technical education as well as start
                      working early. The course concentrated on skill
                      development and one is likely to master advanced
                      computing, scientific skills, and mathematical techniques
                      in the course duration.
                    </p>
                  </div>
                  <hr />

                  <div className="MidSection-Accordian">
                    {DiplomaBranch.map((val, index) => {
                      return (
                        <div key={index}>
                          <Accordion
                            style={{
                              backgroundColor: "unset",
                              boxShadow: "unset",
                              zIndex: 50,
                            }}
                          >
                            <AccordionSummary
                              sx={{
                                transform: "unset",
                                color: "white",
                              }}
                              expandIcon="Know more"
                              aria-controls="panel1a-content"
                              id="panel1a-header"
                            >
                              <Typography className="accor-heading">
                                {val.profile}
                              </Typography>
                            </AccordionSummary>
                            <AccordionDetails>
                              <Typography>{val.name}</Typography>
                            </AccordionDetails>
                          </Accordion>
                        </div>
                      );
                    })}
                  </div>
                </div>

                <div className="i-frame ">
                  <iframe
                    src="/images/1.png"
                    title="description"
                    height="100"
                    width="100%"
                  ></iframe>
                </div>

                <div className="MidSection-secondComponent">
                  <div className="MidSection-secondConmponent-img">
                    <img
                      src="/images/CoursesDetails/mssci1.svg"
                      alt=""
                      className="img-fluid"
                    />
                  </div>

                  <div className="MidSection-secondConmponent-hero">
                    <h1 className="MidSection-secondConmponent-hero-heading">
                      Popular Courses
                    </h1>

                    <p className="MidSection-secondConmponent-hero-para">
                      Though there is no perfect time to study abroad, you’ll
                      find certain school years and semesters to be most common
                      for students seeking an international academic experience.
                      While it isn’t unusual to hear of high school graduates
                      going overseas before their freshman year in college,
                      sophomore and junior years are often popular for study
                      abroad. By this time students have realized why everyone
                      should study abroad, and are more likely to have narrowed
                      down the direction of their major.
                    </p>
                  </div>

                  <div className="MidSection-secondConmponent-hero-courses">
                    {PopularCourses.map((val) => {
                      return (
                        <>
                          <div className="MidSection-secondConmponent-hero-courses-inner">
                            <div className="MidSection-secondConmponent-hero-courses-inner-img">
                              <img
                                src={val.img}
                                className="Sub-HeroSection-Img-Icn"
                                alt=""
                              />
                            </div>
                            <div className="MidSection-secondConmponent-hero-courses-inner-text">
                              <h1 className="MidSection-secondConmponent-hero-courses-inner-text-head">
                                {val.degree}
                              </h1>
                              <h1 className="MidSection-secondConmponent-hero-courses-inner-text-para">
                                {val.courses}
                              </h1>
                            </div>
                          </div>
                        </>
                      );
                    })}
                  </div>

                  <div className="more">
                    <div className="circle opacity-50"></div>
                    <div className="circle opacity-75"></div>
                    <div className="circle"></div>
                    <div className="arrow"></div>
                    <div>
                      <strong>Know more</strong>
                    </div>
                  </div>
                </div>

                <div className="i-frame ">
                  <iframe
                    src="/images/CoursesDetails/if1.svg"
                    title="description"
                    height="100"
                    width="100%"
                  ></iframe>
                </div>

                <div className="MidSection-secondComponent">
                  <div className="MidSection-secondConmponent-imgg">
                    <img
                      src="/images/CoursesDetails/mssci1.svg"
                      alt=""
                      className="img-fluid"
                    />
                  </div>

                  <div className="MidSection-secondConmponent-hero">
                    <h1 className="MidSection-secondConmponent-hero-heading">
                      Top Universities
                    </h1>

                    <p className="MidSection-secondConmponent-hero-para">
                      Though there is no perfect time to study abroad, you’ll
                      find certain school years and semesters to be most common
                      for students seeking an international academic experience.
                      While it isn’t unusual to hear of high school graduates
                      going overseas before their freshman year in college,
                      sophomore and junior years are often popular for study
                      abroad. By this time students have realized why everyone
                      should study abroad, and are more likely to have narrowed
                      down the direction of their major.
                    </p>
                  </div>

                  <div className="MidSection-secondConmponent-hero-courses">
                    {UniversityDetails.map((val) => {
                      return (
                        <>
                          <div className="MidSection-secondConmponent-hero-courses-inner">
                            <div className="MidSection-secondConmponent-hero-courses-inner-img">
                              <img
                                src={val.logo}
                                className="Sub-HeroSection-Img-Icn"
                                alt=""
                              />
                            </div>
                            <div className="MidSection-secondConmponent-hero-courses-inner-text">
                              <h1 className="MidSection-secondConmponent-hero-courses-inner-text-head">
                                {val.name}
                              </h1>
                              <h1 className="MidSection-secondConmponent-hero-courses-inner-text-para">
                                {val.place}
                              </h1>
                            </div>
                          </div>
                        </>
                      );
                    })}
                  </div>

                  <div className="more">
                    <div className="circle opacity-50"></div>
                    <div className="circle opacity-75"></div>
                    <div className="circle"></div>
                    <div className="arrow"></div>
                    <div>
                      <strong>Know more</strong>
                    </div>
                  </div>
                </div>

                {/* <div className="box">
                  <Row>
                    <Col lg={12}>
                      <div className="box-upper">
                        <div className="img">
                          <img
                            src="/images/CoursesDetails/cimg1.svg"
                            alt=""
                            className="img-fluid"
                          />
                        </div>
                        <div className="text">
                          <h1 className="head">Diploma in Engineering</h1>
                          <p className="para">
                            Diploma in Engineering is a great option for those
                            who want to pursue technical education as well as
                            start working early. The course concentrated on
                            skill development and one is likely to master
                            advanced computing, scientific skills, and
                            mathematical techniques in the course duration.
                          </p>
                        </div>
                      </div>
                      <hr />

                      <div className="box-lower">
                        <h3 className="head">Course Overview</h3>
                        <p className="para">
                          A Diploma can directly be done after completing your
                          school education and is also referred to as a back
                          door entry to full-fledged and hardcore Engineering
                          programmes. It is a professional course designed for
                          students who are want to gain some extra knowledge
                          before going for a Bachelor of Engineering (B.E.) or
                          Bachelor of Technology (B.Tech.) courses. These
                          courses are available in varied disciplines for a
                          duration of 2- 3 years, depending on the university.
                          Some popular ones are given below:
                        </p>
                        <ul className="list-flex">
                          <li className="list">
                            Diploma in Automobile Engineering
                          </li>
                          <li className="list">
                            Diploma in Automobile Engineering
                          </li>
                          <li className="list">
                            Diploma in Electrical Engineering
                          </li>
                          <li className="list">
                            Diploma in Electrical Engineering
                          </li>
                          <li className="list">
                            Diploma in Mechanical Engineering
                          </li>
                          <li className="list">
                            Diploma in Mechanical Engineering
                          </li>
                        </ul>

                        <h3 className="head">Eligibility Criteria</h3>
                        <p className="para">
                          Now if you have decided to pursue this diploma course
                          you need to ensure the basic eligibility criteria set
                          for different courses and check the one specific to
                          your desired course. Described below is a general set
                          of requirements for enrolling into Diploma in
                          Engineering course:
                        </p>
                        <h4 className="head1">Diploma</h4>
                        <ul>
                          <li className="list1">
                            Passed High school / SSC examination.
                          </li>
                          <li className="list1">
                            Obtained at least 70% marks at the qualification
                            examination (subjective to the university/college
                            you are applying for.)
                          </li>
                          <li className="list1">
                            Language proficiency exams such as IELTS, TOEFL,
                            PTE, etc.
                          </li>
                        </ul>

                        <h4 className="head1">Diploma (Lateral Entry):</h4>
                        <ul>
                          <li className="list1">
                            Passed 12th standard / ITI – 2 Year Course
                          </li>
                          <li className="list1">
                            Obtained at least 70% marks at the qualification
                            examination. (subjective to the university/college
                            you are applying for.)
                          </li>
                          <li className="list1">
                            Language proficiency exams such as IELTS, TOEFL,
                            PTE, etc.
                          </li>
                        </ul>

                        <h3 className="head">Eligibility Criteria</h3>
                        <p className="para">
                          The course structure mainly consists of some basic
                          engineering subjects, given below we have compiled
                          some of them for you:
                        </p>
                        <ul className="list-flex">
                          <li className="list">Engineering Mathematics</li>
                          <li className="list">
                            Diploma in Automobile Engineering
                          </li>
                          <li className="list">
                            Diploma in Electrical Engineering
                          </li>
                          <li className="list">
                            Diploma in Electrical Engineering
                          </li>
                          <li className="list">
                            Diploma in Mechanical Engineering
                          </li>
                          <li className="list">Fluid Mechanics</li>
                        </ul>

                        <h3 className="head">Popular Courses</h3>
                        <p className="para">
                          Finding a perfect Diploma course can be a daunting
                          process. One needs to pick the right one in order to
                          acquire the desired skills. Given below is the list of
                          some popular programmes:
                        </p>
                        <ul className="list-flex">
                          <li className="list">Diploma in Automobiles</li>
                          <li className="list">
                            Diploma in Automobile Engineering
                          </li>
                          <li className="list">
                            Diploma in Electrical Engineering
                          </li>
                          <li className="list">
                            Diploma in Electrical Engineering
                          </li>
                          <li className="list">
                            Diploma in Mechanical Engineering
                          </li>
                          <li className="list">Diploma in Aeronautics</li>
                        </ul>
                      </div>
                    </Col>
                  </Row>
                </div> */}
              </Col>
              <Col lg={3}>
                <div className="resources">
                  <div className="contact-box">
                    <div className="w-300 ">
                      <Signup />
                      <RegisterRecieve />
                      <Subscribe />
                      <iframe
                        src="/images/host.png"
                        scrolling="no"
                        width="300"
                        height="250"
                      ></iframe>
                      <iframe
                        src="/images/ad.png"
                        scrolling="no"
                        width="300"
                        height="600"
                      ></iframe>
                    </div>
                  </div>
                </div>
              </Col>
            </Row>
          </Container>
        </div>
      </PageLayout>
    </>
  );
};

export default CourseDetail;
