import React from "react";

import Head from "next/head";
import Image from "next/image";
import PageLayout from "@components/PageLayout";
import { Container, Row, Col } from "react-bootstrap";
import Card from "react-bootstrap/Card";

const backgroundChecksDegreeCertificateVerification = () => {
  return (
    <div>
      <Head>
        <title>
          Degree Certificate Verification | Trusted Background Checker
        </title>
        <meta
          id="meta-description"
          name="description"
          content="Ensure the authenticity of degree certificates with our trusted background checker. Verify degrees quickly and accurately. Explore our services today."
        />
        <meta
          property="og:title"
          content=" Degree Certificate Verification | Trusted Background Checker"
        />
        <meta
          property="og:description"
          content="Ensure the authenticity of degree certificates with our trusted background checker. Verify degrees quickly and accurately. Explore our services today."
        />
        <meta
          property="twitter:title"
          content=" Degree Certificate Verification | Trusted Background Checker"
        />
        <meta
          property="twitter:description"
          content="Ensure the authenticity of degree certificates with our trusted background checker. Verify degrees quickly and accurately. Explore our services today."
        />
      </Head>

      <PageLayout>
        <div className="HeroSectionWrapper">
          <Container fluid>
            <Row>
              <Col md={12}>
                <div className="headingWhileLabel Montserrat">
                  <h1>Background & Degree / Certificate Verification</h1>
                  <p>
                    At VerifyKiya, we help institutions and employers make solid
                    admission and hiring decisions by screening your potential
                    students and employees respectively, to help you feel
                    assured about your new students and employees.
                  </p>
                </div>
              </Col>
            </Row>
          </Container>

          <Container fluid className="mb-5">
            <Row>
              <Col md={4} className="rowCenter mt-5">
                <a className="cardanchor" href="#Instant-Verification">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Background/Instant Verification.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Background/Instant Verification.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Instant Verification
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={4} className="rowCenter mt-5">
                <a className="cardanchor" href="#Brand-Protection">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Background/Brand Protection.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Background/Brand Protection.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Brand Protection
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={4} className="rowCenter mt-5">
                <a
                  className="cardanchor"
                  href="#Reduced-Administrative-Workload"
                >
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Background/Reduced_Administrative_Workload_1_.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Background/Reduced_Administrative_Workload_1_.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Reduced Administrative Workload
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
            </Row>
            <Row>
              <Col md={4} className="rowCenter mt-5">
                <a className="cardanchor" href="#Verified-Resumes">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Background/Verified_resumes_1_.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Background/Verified_resumes_1_.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Verified resumes
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={4} className="rowCenter mt-5">
                <a className="cardanchor" href="#Trustless-Immutable">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Background/Trustless__x26__Immutable_2_.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Background/Trustless__x26__Immutable_2_.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Trustless & Immutable
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={4} className="rowCenter mt-5">
                <a className="cardanchor" href="#Verified-Seal">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Background/Verified Seal with QR code.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Background/Verified Seal with QR code.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Verified Seal with QR code
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
            </Row>
          </Container>
        </div>

        <section id="Instant-Verification">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Background Verfication Degree Certificate Verification/Instant Verification.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Background Verfication Degree Certificate Verification/Instant Verification.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Background/Instant Verification.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Background/Instant Verification.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Instant Verification
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      With instant verification of profiles, wasting time and
                      money on running an extensive background check will become
                      a thing of the past. The immutability property of our
                      technology-base is a testament to the authenticity of
                      these profiles. It is virtually impossible to fake a
                      credential with Verifykiya!
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section id="Brand-Protection">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Background/Brand Protection.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Background/Brand Protection.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Brand Protection
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Brand building for any institution is a continuous and
                      painful exercise. Any unauthorised or fake credential can
                      adversely affect the brand of the institution and has a
                      direct effect on its business. Verifykia’s promise of
                      legitimacy and security is an instant protection for
                      institutions. It increases brand visibility not only
                      within the student communities but also within the
                      corporate sector. Usage of blockchain and NFT for
                      credentials makes your institution stand apart from the
                      rest and also increases the credibility.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Background Verfication Degree Certificate Verification/Brand Protection.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Background Verfication Degree Certificate Verification/Brand Protection.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section id="Reduced-Administrative-Workload">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Background Verfication Degree Certificate Verification/Reduced Administrative Workload.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Background Verfication Degree Certificate Verification/Reduced Administrative Workload.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Background/Reduced_Administrative_Workload_1_.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Background/Reduced_Administrative_Workload_1_.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Reduced Administrative Workload
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      The process of hiring, selecting and verifying the
                      employees gets really simplified with Verifykiya! Our
                      platform not only provides fantastic automation mechanisms
                      for the administrative work associated with hiring and
                      verifying but also extends precise analytical support in
                      terms of workforce and workload analysis and
                      identification of the ideal and trustworthy candidate.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section id="Verified-Resumes">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Background/Verified_resumes_1_.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Background/Verified_resumes_1_.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Verified Resumes
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      The resumes of the candidates on Verifykiya come with a
                      Seal of Verification. This seal ensures that all
                      information within the resume is completely genuine and
                      authenticated. There is no scope for fakes and our robust
                      technology makes sure of this particular fact.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Background Verfication Degree Certificate Verification/Verified resumes.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Background Verfication Degree Certificate Verification/Verified resumes.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section id="Trustless-Immutable">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Background Verfication Degree Certificate Verification/Trustless & Immutable.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Background Verfication Degree Certificate Verification/Trustless & Immutable.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Background/Trustless__x26__Immutable_2_.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Background/Trustless__x26__Immutable_2_.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Trustless & Immutable
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      With the entire process powered by blockchain technology,
                      VerifyKiya’s blockchain-based digital certificates are
                      immutable and can never be tampered with. Also, with our
                      trustless network, you can always rely on our
                      blockchain-based certificate and no longer have to place
                      your trust in third parties.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section id="Verified-Seal" className="mb-5">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Background/Verified Seal with QR code.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Background/Verified Seal with QR code.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Verified Seal with QR code
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      The certificates and degrees of the candidates on
                      Verifykiya come with a Seal of Verification. This seal
                      ensures that these documents are completely genuine,
                      authenticated and the NFT-enabled QR code ensures that it
                      can not be duplicated. There is no scope for fakes and our
                      robust technology makes sure of this particular fact.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Background Verfication Degree Certificate Verification/Verified Seal with QR code.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Background Verfication Degree Certificate Verification/Verified Seal with QR code.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
      </PageLayout>
    </div>
  );
};

export default backgroundChecksDegreeCertificateVerification;
