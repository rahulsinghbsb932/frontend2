import React from "react";
import Head from "next/head";
import Image from "next/image";
import PageLayout from "@components/PageLayout";
import { Container, Row, Col } from "react-bootstrap";
import Card from "react-bootstrap/Card";

const automatedResumeBuilder = () => {
  return (
    <div>
      <Head>
        <title>Create CV Online for Free | Automated Resume Builder</title>
        <meta
          id="meta-description"
          name="description"
          content="Build your perfect CV with our automated resume builder. Create CVs online for free in minutes. Get started now and land your dream job."
        />
        <meta
          property="og:title"
          content="Create CV Online for Free | Automated Resume Builder"
        />
        <meta
          property="og:description"
          content="Build your perfect CV with our automated resume builder. Create CVs online for free in minutes. Get started now and land your dream job."
        />
        <meta
          property="twitter:title"
          content="Create CV Online for Free | Automated Resume Builder"
        />
        <meta
          property="twitter:description"
          content="Build your perfect CV with our automated resume builder. Create CVs online for free in minutes. Get started now and land your dream job."
        />
      </Head>

      <PageLayout>
        <div className="HeroSectionWrapper">
          <Container fluid>
            <Row>
              <Col md={12}>
                <div className="headingWhileLabel Montserrat">
                  <h1>Automated Resume Builder</h1>
                  <p>
                    Build A Professional Resume Within Minutes With the Best
                    Resume Templates!
                  </p>
                </div>
              </Col>
            </Row>
          </Container>

          <Container fluid className="mb-5">
            <Row>
              <Col md={3} className="rowCenter mt-5">
                <a className="cardanchor" href="#Automated-Trust-Score">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Automated-Resume-Builder/Automated Trust Score.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Automated-Resume-Builder/Automated Trust Score.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Automated Trust Score
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={3} className="rowCenter mt-5">
                <a className="cardanchor" href="#Automated-Resume-Builder">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Automated-Resume-Builder/Automated Resume Builder.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Automated-Resume-Builder/Automated Resume Builder.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Automated Resume Builder
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={3} className="rowCenter mt-5">
                <a className="cardanchor" href="#Unlimited-Modifications">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Automated-Resume-Builder/Unlimited Modifications.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Automated-Resume-Builder/Unlimited Modifications.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Unlimited Modifications
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={3} className="rowCenter mt-5">
                <a className="cardanchor" href="#Multiple-Languages">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Automated-Resume-Builder/Multiple Languages.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Automated-Resume-Builder/Multiple Languages.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Multiple Languages
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={3} className="rowCenter mt-5">
                {" "}
                <a className="cardanchor" href="#Verified-Seal-with-QR-code">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Automated-Resume-Builder/Verified Seal with QR Code.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Automated-Resume-Builder/Verified Seal with QR Code.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Verified Seal with QR code
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={3} className="rowCenter mt-5">
                {" "}
                <a className="cardanchor" href="#Grammatical-Accuracy">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Automated-Resume-Builder/Grammatical Accuracy.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Automated-Resume-Builder/Grammatical Accuracy.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Grammatical Accuracy
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={3} className="rowCenter mt-5">
                <a
                  className="cardanchor"
                  href="#Multiple-Sample-Resume-Formats"
                >
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Automated-Resume-Builder/Multiple Sample Resume Formats.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Automated-Resume-Builder/Multiple Sample Resume Formats.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Multiple Sample Resume Formats
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={3} className="rowCenter mt-5">
                <a
                  className="cardanchor"
                  href="#Customization-and-Professional-Help"
                >
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Automated-Resume-Builder/Customization and Professional Help.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Automated-Resume-Builder/Customization and Professional Help.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Customization and Professional Help
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
            </Row>
          </Container>
        </div>

        <section id="Automated-Trust-Score">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Automated Resume Builder/Automated Trust Score.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Automated Resume Builder/Automated Trust Score.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Automated-Resume-Builder/Automated Trust Score.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Automated-Resume-Builder/Automated Trust Score.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Automated Trust Score
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Resumes on VerifyKiya comes with a trust score which
                      safeguards the recruiter from fraud and potential cheats.
                      Verification of the information provided on the resumes
                      are done at the platforms backend and then they are
                      provided a trust score accordingly. Higher the trust score
                      of the candidate means information on the resume is
                      genuine and verified. By using this feature recruiter can
                      focus on the hiring instead of worrying about the
                      authenticity of information provided.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section id="Automated-Resume-Builder">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Automated-Resume-Builder/Automated Resume Builder.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Automated-Resume-Builder/Automated Resume Builder.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Automated Resume Builder
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Sometimes even the best candidates miss out on amazing
                      opportunities because they are clueless about how to build
                      an appropriate resume. Writing and designing your resume
                      is an art and one of the most important steps in your
                      journey as a applicant. To make your life simpler and to
                      save your precious time, Verifykiya provides the user of
                      automated resume builder which automatically captures the
                      credentials / certificates whenever they are issued or
                      uploaded to add on the applicant’s resume.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
                <Col xl={3} lg={12} className="order-xl-2 order-1">
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Automated Resume Builder/Automated Resume Builder.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid "
                    /> */}

                    <Image
                      src="/images/Services/images/Automated Resume Builder/Automated Resume Builder.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section id="Unlimited-Modifications">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Automated Resume Builder/Unlimited Modifications.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Automated Resume Builder/Unlimited Modifications.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Automated-Resume-Builder/Unlimited Modifications.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Automated-Resume-Builder/Unlimited Modifications.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Unlimited Modifications
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      All your information will be automatically placed on a
                      template of your choice. Though the resumes are
                      automatically created on verifykiya whenever you upload
                      any information but we allow unlimited modifications to
                      content part only, once the resumes are verified no
                      changes are allowed on the platform.We provide a verified
                      seal on the resume which means that all the information
                      has been checked by us and are found to be authentic.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section id="Multiple-Languages">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Automated-Resume-Builder/Multiple Languages.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}
                      <Image
                        src="/images/Services/icon/Automated-Resume-Builder/Multiple Languages.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Multiple Languages
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Verifykiya provides multilingual support on its platform
                      for resume creation and distribution. You can create a
                      resume in a language of your choice and the one you are
                      comfortable with and distribute it in the language asked
                      by the recruiter.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Automated Resume Builder/Multiple Languages.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Automated Resume Builder/Multiple Languages.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section id="Verified-Seal-with-QR-code">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Automated Resume Builder/Verified Seal with QR code.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Automated Resume Builder/Verified Seal with QR code.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Automated-Resume-Builder/Verified Seal with QR Code.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Automated-Resume-Builder/Verified Seal with QR Code.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Verified Seal with QR code
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      The resumes of the candidates on Verifykiya come with a
                      Seal of Verification. This seal ensures that all
                      information within the resume is completely genuine and
                      authenticated. There is no scope for fakes and our robust
                      technology makes sure of this particular fact.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section id="Grammatical-Accuracy">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Automated-Resume-Builder/Grammatical Accuracy.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Automated-Resume-Builder/Grammatical Accuracy.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Grammatical Accuracy
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Your resume is your best chance to present yourself in
                      writing as the ideal candidate for your dream job,
                      VerifyKiya checks it for accuracy before sending it in. It
                      is nearly impossible to recover from spelling errors on
                      your resume. Spelling and grammar are important indicators
                      of your attention to detail and we have it covered for
                      you.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Automated Resume Builder/Grammatical Accuracy.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Automated Resume Builder/Grammatical Accuracy.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section id="Multiple-Sample-Resume-Formats">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Automated Resume Builder/Multiple Sample Resume Formats.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Automated Resume Builder/Multiple Sample Resume Formats.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Automated-Resume-Builder/Multiple Sample Resume Formats.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Automated-Resume-Builder/Multiple Sample Resume Formats.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Multiple Sample Resume Formats
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Stand out from the competition with VerifyKiya’s wide
                      range of multiple resume templates. Choose from a variety
                      of resume templates to create a professional looking
                      resume that highlights your right skills and relevant
                      experience for the job. Whether you're a recent graduate
                      or an experienced professional, our templates can help you
                      make a lasting impression and land your dream job.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section id="Customization-and-Professional-Help" className="mb-5">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Automated-Resume-Builder/Customization and Professional Help.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Automated-Resume-Builder/Customization and Professional Help.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Customization and Professional Help
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Verifykiya provides you with the option to get resume
                      custom built, which is not covered by us in any of our
                      templates. Our professionals can customizable the resume
                      as per your needs and requirements.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Automated Resume Builder/Customization and Professional Help.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Automated Resume Builder/Customization and Professional Help.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
      </PageLayout>
    </div>
  );
};

export default automatedResumeBuilder;
