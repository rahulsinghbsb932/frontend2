import React from "react";
import { Col, Container, Row, Form, InputGroup } from "react-bootstrap";

import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import Typography from "@mui/material/Typography";
import PageLayout from "@components/PageLayout";
import Head from "next/head";
import Signup from "./form/Signup";
import RegisterRecieve from "./form/RegisterRecieve";
import Subscribe from "./form/Subscribe";
// import ExpandMoreIcon from "@mui/icons-material/ExpandMore";

const DiplomaCourses = [
  {
    name: "Diploma in Engineering",
  },
  {
    name: "Diploma in Hotel Management",
  },
  {
    name: "Diploma in Journalism",
  },
  {
    name: "Diploma in Education",
  },
  {
    name: "Diploma in Photography",
  },
  {
    name: "Diploma in Peychulagy",
  },
  {
    name: "Diploma in Elementary Education",
  },
  {
    name: "Diploma in Digital Marketing",
  },
  {
    name: "Diploma in Fine Arts",
  },
  {
    name: "Diploma in English",
  },
];

const DiplomaBranch = [
  {
    profile: "Course Overview",
    name: "Diploma in Automobile Engineering",
  },
  {
    profile: "Eligibility Criteria",
    name: "Diploma in Electrical Engineering",
  },
  {
    profile: "Subjects",
    name: " Diploma in Mechanical Engineering",
  },
  {
    profile: "Popular Courses",
    name: "Diploma in Automobile Engineering",
  },
];

const PopularCourses = [
  {
    img: "/images/checkIcon.svg",
    degree: "Bachelor/Undergradute",
    courses: "Arts, Humanities and Social Science",
  },
  {
    img: "/images/checkIcon.svg",
    degree: "Master with Pre-Master Programme",
    courses: "Medicine, Pharmacy , Health and Life Sciences",
  },

  {
    img: "/images/checkIcon.svg",
    degree: "Bachelor with Foundation Year",
    courses: "Computer Science, Data Sciene & IT",
  },
  {
    img: "/images/checkIcon.svg",
    degree: "PhD / Doctorate / Master",
    courses: "Science",
  },
];

const UniversityDetails = [
  {
    logo: "/images/CoursesDetails/Stanford.svg",
    name: "Stanford Graduate School of Business",
    place: "Stanford (CA), United States",
  },
  {
    logo: "/images/CoursesDetails/Stanford.svg",
    name: "Stanford Graduate School of Business",
    place: "Stanford (CA), United States",
  },
  {
    logo: "/images/CoursesDetails/Stanford.svg",
    name: "Stanford Graduate School of Business",
    place: "Stanford (CA), United States",
  },
  {
    logo: "/images/CoursesDetails/Stanford.svg",
    name: "Stanford Graduate School of Business",
    place: "Stanford (CA), United States",
  },
];

const CoursesDetailsList = () => {
  return (
    <>
      <Head>
        <title>Courses Details | Business Verification Services</title>
        <meta
          id="meta-description"
          name="description"
          content=" This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
        <meta
          property="og:title"
          content="Courses Details | Business Verification Services"
        />
      </Head>
      <PageLayout>
        <div className="HeroSectionWrapper CoursesDetails">
          <Container fluid>
            <Row>
              <Col lg={12}>
                <div className="HeroSection">
                  <h1 className="HeroSection-Heading">
                    Courses After 10th Standard
                  </h1>
                  <p className="HeroSection-Para">
                    If you aim to pursue a course after the 10th, there is a
                    wide range of courses on offer across various fields of
                    study. You can explore a long list of courses after the 10th
                    which are available in Designing, Engineering, Technology,
                    Business, Management, Computer Science, Arts, Media
                    Journalism and so on.
                  </p>

                  <div className="Sub-HeroSection">
                    <Row>
                      <Col lg={4}>
                        <Row>
                          <Col lg={2}>
                            <div className="Sub-HeroSection-Img">
                              <img
                                src="/images/checkIcon.svg"
                                className="Sub-HeroSection-Img-Icn"
                                alt=""
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <div className="Sub-HeroSection-Text">
                              <p className="Sub-HeroSection-Text-Para">
                                Class XI-Class XII
                              </p>
                            </div>
                          </Col>
                        </Row>
                      </Col>
                      <Col lg={4}>
                        <Row>
                          <Col lg={2}>
                            <div className="Sub-HeroSection-Img">
                              <img
                                src="/images/checkIcon.svg"
                                className="Sub-HeroSection-Img-Icn"
                                alt=""
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <div className="Sub-HeroSection-Text">
                              <p className="Sub-HeroSection-Text-Para">
                                Diploma Courses
                              </p>
                            </div>
                          </Col>
                        </Row>
                      </Col>
                      <Col lg={4}>
                        <Row>
                          <Col lg={2}>
                            <div className="Sub-HeroSection-Img">
                              <img
                                src="/images/checkIcon.svg"
                                className="Sub-HeroSection-Img-Icn"
                                alt=""
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <div className="Sub-HeroSection-Text">
                              <p className="Sub-HeroSection-Text-Para">
                                Polytechnic Courses
                              </p>
                            </div>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                    <Row>
                      <Col lg={4}>
                        <Row>
                          <Col lg={2}>
                            <div className="Sub-HeroSection-Img">
                              <img
                                src="/images/checkIcon.svg"
                                className="Sub-HeroSection-Img-Icn"
                                alt=""
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <div className="Sub-HeroSection-Text">
                              <p className="Sub-HeroSection-Text-Para">
                                Paramedical Courses
                              </p>
                            </div>
                          </Col>
                        </Row>
                      </Col>
                      <Col lg={4}>
                        <Row>
                          <Col lg={2}>
                            <div className="Sub-HeroSection-Img">
                              <img
                                src="/images/checkIcon.svg"
                                className="Sub-HeroSection-Img-Icn"
                                alt=""
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <div className="Sub-HeroSection-Text">
                              <p className="Sub-HeroSection-Text-Para">
                                ITI Courses
                              </p>
                            </div>
                          </Col>
                        </Row>
                      </Col>
                      <Col lg={4}>
                        <Row>
                          <Col lg={2}>
                            <div className="Sub-HeroSection-Img">
                              <img
                                src="/images/checkIcon.svg"
                                className="Sub-HeroSection-Img-Icn"
                                alt=""
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <div className="Sub-HeroSection-Text">
                              <p className="Sub-HeroSection-Text-Para">
                                Vocational Courses
                              </p>
                            </div>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                  </div>
                </div>
              </Col>
            </Row>
          </Container>
        </div>

        <div className="DiplomaCourses">
          <h1 className="DiplomaCourses-inner-heading">Diploma Courses</h1>
          <Container fluid>
            <Row>
              <Col lg={12}>
                <div className="DiplomaCourses-inner">
                  <ul className="DiplomaCourses-branch">
                    {DiplomaCourses.map((val) => {
                      return (
                        <li className="DiplomaCourses-branch-width">
                          {val.name}
                        </li>
                      );
                    })}
                  </ul>
                </div>
              </Col>
            </Row>
          </Container>
        </div>
        <div className="d-flex">
          <iframe
            src="/images/1.png"
            scrolling="no"
            width="100%"
            height="100%"
            class="responsive-iframe"
          ></iframe>
          <iframe
            src="/images/2.png"
            scrolling="no"
            width="100%"
            height="100%"
            class="responsive-iframe"
          ></iframe>
        </div>

        <div className="MidSection ">
          <Container fluid>
            <Row>
              <Col lg={9}>
                <div className="box">
                  <Row>
                    <Col lg={12}>
                      <div className="box-upper">
                        <div className="img">
                          <img
                            src="/images/CoursesDetails/cimg1.svg"
                            alt=""
                            className="img-fluid"
                          />
                        </div>
                        <div className="text">
                          <h1 className="head">Diploma in Engineering</h1>
                          <p className="para">
                            Diploma in Engineering is a great option for those
                            who want to pursue technical education as well as
                            start working early. The course concentrated on
                            skill development and one is likely to master
                            advanced computing, scientific skills, and
                            mathematical techniques in the course duration.
                          </p>
                        </div>
                      </div>
                      <hr />

                      <div className="box-lower">
                        <h3 className="head">Course Overview</h3>
                        <p className="para">
                          A Diploma can directly be done after completing your
                          school education and is also referred to as a back
                          door entry to full-fledged and hardcore Engineering
                          programmes. It is a professional course designed for
                          students who are want to gain some extra knowledge
                          before going for a Bachelor of Engineering (B.E.) or
                          Bachelor of Technology (B.Tech.) courses. These
                          courses are available in varied disciplines for a
                          duration of 2- 3 years, depending on the university.
                          Some popular ones are given below:
                        </p>
                        <ul className="list-flex">
                          <li className="list">
                            Diploma in Automobile Engineering
                          </li>
                          <li className="list">
                            Diploma in Automobile Engineering
                          </li>
                          <li className="list">
                            Diploma in Electrical Engineering
                          </li>
                          <li className="list">
                            Diploma in Electrical Engineering
                          </li>
                          <li className="list">
                            Diploma in Mechanical Engineering
                          </li>
                          <li className="list">
                            Diploma in Mechanical Engineering
                          </li>
                        </ul>

                        <h3 className="head">Eligibility Criteria</h3>
                        <p className="para">
                          Now if you have decided to pursue this diploma course
                          you need to ensure the basic eligibility criteria set
                          for different courses and check the one specific to
                          your desired course. Described below is a general set
                          of requirements for enrolling into Diploma in
                          Engineering course:
                        </p>
                        <h4 className="head1">Diploma</h4>
                        <ul>
                          <li className="list1">
                            Passed High school / SSC examination.
                          </li>
                          <li className="list1">
                            Obtained at least 70% marks at the qualification
                            examination (subjective to the university/college
                            you are applying for.)
                          </li>
                          <li className="list1">
                            Language proficiency exams such as IELTS, TOEFL,
                            PTE, etc.
                          </li>
                        </ul>

                        <h4 className="head1">Diploma (Lateral Entry):</h4>
                        <ul>
                          <li className="list1">
                            Passed 12th standard / ITI – 2 Year Course
                          </li>
                          <li className="list1">
                            Obtained at least 70% marks at the qualification
                            examination. (subjective to the university/college
                            you are applying for.)
                          </li>
                          <li className="list1">
                            Language proficiency exams such as IELTS, TOEFL,
                            PTE, etc.
                          </li>
                        </ul>

                        <h3 className="head">Eligibility Criteria</h3>
                        <p className="para">
                          The course structure mainly consists of some basic
                          engineering subjects, given below we have compiled
                          some of them for you:
                        </p>
                        <ul className="list-flex">
                          <li className="list">Engineering Mathematics</li>
                          <li className="list">
                            Diploma in Automobile Engineering
                          </li>
                          <li className="list">
                            Diploma in Electrical Engineering
                          </li>
                          <li className="list">
                            Diploma in Electrical Engineering
                          </li>
                          <li className="list">
                            Diploma in Mechanical Engineering
                          </li>
                          <li className="list">Fluid Mechanics</li>
                        </ul>

                        <h3 className="head">Popular Courses</h3>
                        <p className="para">
                          Finding a perfect Diploma course can be a daunting
                          process. One needs to pick the right one in order to
                          acquire the desired skills. Given below is the list of
                          some popular programmes:
                        </p>
                        <ul className="list-flex">
                          <li className="list">Diploma in Automobiles</li>
                          <li className="list">
                            Diploma in Automobile Engineering
                          </li>
                          <li className="list">
                            Diploma in Electrical Engineering
                          </li>
                          <li className="list">
                            Diploma in Electrical Engineering
                          </li>
                          <li className="list">
                            Diploma in Mechanical Engineering
                          </li>
                          <li className="list">Diploma in Aeronautics</li>
                        </ul>
                      </div>
                    </Col>
                  </Row>
                </div>
              </Col>
              <Col lg={3}>
                <iframe
                  src="/images/host.png"
                  scrolling="no"
                  width="100%"
                  height="250"
                  class="responsive-iframe"
                ></iframe>
                <div className="resources">
                  <div className="contact-box">
                    <div className="w-300 ">
                      <Signup />
                      <RegisterRecieve />
                      <Subscribe />
                    </div>
                  </div>
                </div>
              </Col>
            </Row>
          </Container>
        </div>
      </PageLayout>
    </>
  );
};

export default CoursesDetailsList;
