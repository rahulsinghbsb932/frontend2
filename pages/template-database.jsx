import React from "react";

import Head from "next/head";
import Image from "next/image";
import PageLayout from "@components/PageLayout";
import { Container, Row, Col } from "react-bootstrap";
import Card from "react-bootstrap/Card";

const templateDatabase = () => {
  return (
    <div>
      <Head>
        <title>Create CV Online for Free | Degree</title>
        <meta
          id="meta-description"
          name="description"
          content="Build your perfect CV with our Degree. Create CVs online for free in minutes. Get started now and land your dream job."
        />
        <meta
          property="og:title"
          content="Create CV Online for Free | Degree"
        />
        <meta
          property="og:description"
          content="Build your perfect CV with our Degree. Create CVs online for free in minutes. Get started now and land your dream job."
        />
        <meta
          property="twitter:title"
          content="Create CV Online for Free | Degree"
        />
        <meta
          property="twitter:description"
          content="Build your perfect CV with our Degree. Create CVs online for free in minutes. Get started now and land your dream job."
        />
      </Head>

      <PageLayout>
        <div className="HeroSectionWrapper">
          <Container fluid>
            <Row>
              <Col md={12}>
                <div className="headingWhileLabel Montserrat">
                  <h1>Templates Database</h1>
                  <p>
                    Build A Professional Resume Within Minutes With the Best
                    Resume Templates!
                  </p>
                </div>
              </Col>
            </Row>
          </Container>

          <Container fluid className="mb-5">
            <Row>
              <Col md={3} className="rowCenter mt-5">
                <a className="cardanchor" href="#Certificates">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Templates_Database/Certificates.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Templates_Database/Certificates.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Certificates
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={3} className="rowCenter mt-5">
                <a className="cardanchor" href="#Degree">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Templates_Database/Degree.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Templates_Database/Degree.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">Degree</Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={3} className="rowCenter mt-5">
                <a className="cardanchor" href="#Badges">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Templates_Database/Badges.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Templates_Database/Badges.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">Badges</Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={3} className="rowCenter mt-5">
                <a className="cardanchor" href="#Experience-letters">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Templates_Database/Experience letters.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Templates_Database/Experience letters.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Experience letters
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={3} className="rowCenter mt-5">
                {" "}
                <a className="cardanchor" href="#Invitations">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Templates_Database/Invitations.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Templates_Database/Invitations.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">Invitations</Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={3} className="rowCenter mt-5">
                {" "}
                <a className="cardanchor" href="#Entry-Pass">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Templates_Database/Entry Pass.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Templates_Database/Entry Pass.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">Entry Pass</Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={3} className="rowCenter mt-5">
                <a className="cardanchor" href="#Id-Cards">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Templates_Database/Id Cards.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Templates_Database/Id Cards.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">Id Cards</Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>

              <Col md={3} className="rowCenter mt-5">
                <a className="cardanchor" href="#Option-for-Custom-Documents">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Templates_Database/Option for Custom Documents.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Templates_Database/Option for Custom Documents.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Option for Custom Documents
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
            </Row>
          </Container>
        </div>

        <section id="Certificates">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Templates Database/Certificates.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Templates Database/Certificates.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Templates_Database/Certificates.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Templates_Database/Certificates.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">Certificates</h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Take your digital certification creation process to the
                      next level. VerifyKiya's digital certificates are created
                      using industry standards on blockchain. By using
                      blockchain and NFT, we provide you with a trustless and
                      immutable platform for your certificates which not only
                      safeguards your institution from any unauthorised access
                      or tampering but takes your brand to a new height. We
                      provide you with a database of templates to choose from or
                      we can customize them as per requirements or standards.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section id="Degree">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Templates_Database/Degree.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Templates_Database/Degree.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">Degree</h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Institutions security over brand or learner’s control over
                      their credentials is of utmost importance, by using
                      Verifykiya,s platform to issue digital degrees, you get
                      security and transparency of blockchain and immutability
                      of NFT’s, which enhances the values and legitimacy of the
                      degree. With Version and access control security of the
                      data is assured. Verifykiya allows for bulk issuance of
                      digital degrees which saves time , reduces workoad and
                      also makes your institution environment freindly. Choose
                      from multiple options or we can simply customise according
                      to your needs and requirements.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
                <Col xl={3} lg={12} className="order-xl-2 order-1">
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Templates Database/Degree.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid "
                    /> */}

                    <Image
                      src="/images/Services/images/Templates Database/Degree.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section id="Badges">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Templates Database/Badges.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Templates Database/Badges.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Templates_Database/Badges.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Templates_Database/Badges.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">Badges</h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Badges are an effective way to reward and recognise
                      achievements, skills accomplishments, etc.of your students
                      and employees. Badges plays a important part in boosting
                      moral and creates a trust between both. Reciever of the
                      digital badges can use it on their resumes and share it
                      across sector and industry. Receiver can also share it
                      across social media platform directly through our platform
                      which in turn gives your brand a boost. Verifykiya has
                      multiple templates to choose from, designed as per
                      industry standards, We can also customize as per design
                      and specifications.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section id="Experience-letters">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Templates_Database/Experience letters.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Templates_Database/Experience letters.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Experience letters
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Experience letter provides credibility and proof of
                      employee’s position and function in his previous
                      employment, Companies use experience letters to validate
                      job candidate’s resume and claims about their skills and
                      experience. Experience lettershould be professionally
                      designed and secured as it represents your brand in front
                      of your previous employees potential employer. Verifykiya
                      allows the institution to use the professionally designed
                      templates to craft an experience letter that will stand
                      out. Choose from host of designs to suit your
                      organisations statement and requirements from our template
                      database or get it customised according to your needs.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Templates Database/Experience letters.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}
                    <Image
                      src="/images/Services/images/Templates Database/Experience letters.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section id="Invitations">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Templates Database/Invitations.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Templates Database/Invitations.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Templates_Database/Invitations.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Templates_Database/Invitations.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">Invitations</h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Invitations are key to any function as they help in
                      building excitement and sets overall tone of the event.
                      Verifykiya helps you creates asthetic and beautiful
                      invitations, which can stand out and give your event all
                      the excitement and attention that it needs. Get started in
                      seconds with the help of professionally designed database
                      of templates for every occasion such as for Conferences,
                      trade shows, seminars, family functions, charity events,
                      and many more.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section id="Entry-Pass">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Templates_Database/Entry Pass.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Templates_Database/Entry Pass.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">Entry Pass</h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Security required for any entry pass is paramount,
                      Verifykiya brings to you blockchain based NFT enabled
                      entry passes which are secured cryptographically and are
                      unhackable / immutable. Digital entry pass comes with a
                      host of features like time based access, area restriction,
                      on the fly updation, revocation, QR based verification
                      etc. Browse professionally designed entry pass templates
                      from our database and choose as per your requirement and
                      needs.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Templates Database/Entry Pass.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Templates Database/Entry Pass.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section id="Id-Cards">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12} className="">
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Templates Database/Id Cards.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Templates Database/Id Cards.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Templates_Database/Id Cards.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Templates_Database/Id Cards.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">Id Cards</h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Create identity cards on blockchain using the security of
                      NFT and convenience of Verifykiya’s proprietary QR codes.
                      Browse through our library of identification card
                      templates and find a professional and creative design that
                      can be personalized to match the brand or business. Our
                      curated selection of ID cards include a variety of
                      layouts, styles and themes that can be used as school ID
                      cards, club membership cards, gym cards, company ID cards
                      and more. Select a template and easily customize it with
                      our intuitive editing tools.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section id="Option-for-Custom-Documents" className="mb-5">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Templates_Database/Option for Custom Documents.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Templates_Database/Option for Custom Documents.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Option for Custom Documents
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Verifykiya provides you an option to get any document
                      custom built, which is not covered by us in any of our
                      templates. Our fully customizable templates give you a
                      quick and easy starting point to create what you need or
                      you can use our team of graphic designers to meet your
                      requirements.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Templates Database/Option for Custom Documents.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Templates Database/Option for Custom Documents.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
      </PageLayout>
    </div>
  );
};

export default templateDatabase;
