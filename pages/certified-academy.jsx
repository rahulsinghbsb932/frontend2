import React from "react";
import Head from "next/head";
import Image from "next/image";
import PageLayout from "@components/PageLayout";
import { Container, Row, Col } from "react-bootstrap";
import Card from "react-bootstrap/Card";

const certifiedAcademy = () => {
  return (
    <div>
      <Head>
        <title>Certified Academy | Academy Verification | VerifyKiya</title>
        <meta
          id="meta-description"
          name="description"
          content=" Certified Academy is a prestigious educational institution that offers specialized training and certification programs in various fields, designed to validate the knowledge, skills, and expertise of professionals."
        />
        <meta
          property="og:title"
          content="Certified Academy | Academy Verification | VerifyKiya"
        />
        <meta
          property="og:description"
          content="Certified Academy is a prestigious educational institution that offers specialized training and certification programs in various fields, designed to validate the knowledge, skills, and expertise of professionals."
        />
        <meta
          property="twitter:title"
          content="Certified Academy | Academy Verification | VerifyKiya"
        />
        <meta
          property="twitter:description"
          content="Certified Academy is a prestigious educational institution that offers specialized training and certification programs in various fields, designed to validate the knowledge, skills, and expertise of professionals."
        />
      </Head>

      <PageLayout>
        <div className="HeroSectionWrapper">
          <Container fluid>
            <Row>
              <Col md={12}>
                <div className="headingWhileLabel Montserrat">
                  <h1>Accademy & Coaching Institutes</h1>
                  <p>
                    Academies are the backbone of the sports and fitness
                    industry. From a student’s primary education / training to
                    specialized training, the topmost academies are sought after
                    for maximum growth opportunities. Verifykiya offers the best
                    for the best.  Our engaging and precisely curated feature
                    set perfectly coalesces to formulate a tailor-made solution
                    for your brand that promises progress, relevancy and
                    quality.  Keeping in mind the core values of an Academy,
                    Verifykiya keeps the students at the centre of it all. This
                    enhances brand image and also helps in the recruitment and
                    retention of future learners.
                  </p>
                </div>
              </Col>
            </Row>
          </Container>

          <Container fluid>
            <Row>
              <Col md={4} className="rowCenter mt-5">
                <a className="cardanchor" href="/">
                  <Card className="CardSol">
                    <Image
                      src="/images/academy/training.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Training institutes
                      </Card.Title>
                      <Card.Text>
                        Institutions are humanly devised structures of rules and
                        norms that shape & constrain.
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={4} className="rowCenter mt-5">
                <a className="cardanchor" href="/">
                  <Card className="CardSol">
                    <Image
                      src="/images/academy/home.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">Home Tutor</Card.Title>
                      <Card.Text>
                        Institutions are humanly devised structures of rules and
                        norms that shape & constrain.
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={4} className="rowCenter mt-5">
                <a className="cardanchor" href="/">
                  <Card className="CardSol">
                    <Image
                      src="/images/academy/tution.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Tuition Center
                      </Card.Title>
                      <Card.Text>
                        A university is an institution of higher education and
                        research which awards academic.
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
            </Row>
            <Row>
              <Col md={4} className="rowCenter mt-5">
                <a className="cardanchor" href="/">
                  <Card className="CardSol">
                    <Image
                      src="/images/academy/coaching.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Coaching Center
                      </Card.Title>
                      <Card.Text>
                        A school is an educational institution designed to
                        provide learning spaces and learning.
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={4} className="rowCenter mt-5">
                <a className="cardanchor" href="/">
                  <Card className="CardSol">
                    <Image
                      src="/images/academy/sport.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">Sports</Card.Title>
                      <Card.Text>
                        Institutions are humanly devised structures of rules and
                        norms that shape & constrain.
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={4} className="rowCenter mt-5"></Col>
            </Row>
          </Container>
        </div>

        <div className="sectionRelative m-5">
          <Container fluid>
            <Col md={12}>
              <div class="ourSolutionWrapper">
                <h1 class="ourSolutionSectionheading">
                  <span style={{ fontWeight: 300 }}>How does </span>
                  Verifykiya Help You?
                </h1>
                <p className="ourSolutionPera">
                  Companies have a lot on their plate. To ensure that you focus
                  on your fundamental and key areas of your organization,
                  Verifykiya extends its assistance. A myriad of tasks become a
                  single-click phenomenon with Verifykiya’s cutting edge
                  platform.
                </p>
              </div>
            </Col>
          </Container>
        </div>

        <section>
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    <Image
                      src="/images/academy/badges.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="badge-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Digital Certification and Badges
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Our certification process is powered by blockchain
                      technology and NFTs, which ensures seamless creation and
                      verification of authentic credentials, such as badges and
                      certificates. Thanks to the trustless algorithm and
                      immutable properties of NFTs, any unauthorized changes are
                      impossible, which guarantees the highest level of security
                      and authenticity for our clients
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section>
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="unified-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Unified Platform for Acquisition and Retention
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Our platform can be easily integrated in any company
                      without much disruption. There is no requirement of
                      changing your process, our software can integrate with
                      your existing SAP or other software solutionsl. We make
                      tailor-made solutions so that all your needs and wants can
                      be incorporated.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
                <Col xl={3} lg={12} className="order-xl-2 order-1">
                  <div className="img">
                    <Image
                      src="/images/academy/unified.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section>
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    <Image
                      src="/images/academy/option.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="option-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Option to Enroll in Nation and International Competition
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Take your skills to the global stage and compete against
                      the best with our option to enroll in national and
                      international competitions. Challenge yourself and
                      showcase your talents in front of a worldwide audience,
                      and gain valuable experience and recognition that can help
                      propel your career to new heights.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section>
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    <Image
                      src="/images/academy/skill.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="skill-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Access to Scholarships, Placements, Skill & Personality
                        Development
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Unlock your full potential with our comprehensive program
                      that offers access to scholarships, placements, skill and
                      personality development. From financial assistance to
                      career opportunities, we're committed to empowering you
                      with the tools you need to succeed and helping you achieve
                      your goals.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section>
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="wide-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Access to Wide Consumer Base
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Reduced workload and time saving - The assimilation of
                      records of different students/ participants under one
                      table is unnecessary and time consuming, but with
                      Verifykiya this task becomes easy and quick.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
                <Col xl={3} lg={12} className="order-xl-2 order-1">
                  <div className="img">
                    <Image
                      src="/images/academy/wide.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section>
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    <Image
                      src="/images/academy/ambasdor.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="ambas-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Social Ambassadors
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Join our community of social ambassadors and become a
                      voice for change. As a social ambassador, you'll have the
                      opportunity to spread awareness and promote important
                      causes through your social media channels. Join us in
                      making a positive impact and inspire others to do the
                      same.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section className="mb-5">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="tem-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Template Database
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Create professional-looking resumes, certificates, and
                      academic documents with ease using our comprehensive
                      template database. With a wide range of customizable
                      options to choose from, you can easily build your own
                      templates and showcase your achievements in the best
                      possible light. Get started today and make a lasting
                      impression.
                    </p>
                  </div>
                </Col>
                <Col xl={3} lg={12} className="order-xl-2 order-1">
                  <div className="img">
                    <Image
                      src="/images/academy/template.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
      </PageLayout>
    </div>
  );
};

export default certifiedAcademy;
