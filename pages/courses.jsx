import React from "react";
import Card from "react-bootstrap/Card";
import { Container, Row, Col } from "react-bootstrap";
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import Head from "next/head";
import Image from "next/image";
import CustomCard from "@components/CustomCard"
import PageLayout from "@components/PageLayout";


const Courses = () => {
    return (
      <div>
        <Head>
          <title>Certified Corporate | Business Verification Services</title>
          <meta
            id="meta-description"
            name="description"
            content=" This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
          />
          <meta
            property="og:title"
            content="Certified Corporate | Business Verification Services"
          />
        </Head>
        <PageLayout>
          <div className="courses">
            <div className="HeroSectionWrapper">
              <div className="frame1 d-xl-block d-none"></div>
              <div className="frame2 d-xl-block d-none"></div>
              <Container fluid>
                <Row>
                  <Col md={12}>
                    <div className="headingWhileLabel text-center">
                      <h1>All Courses</h1>
                      <p>
                        <strong>
                          Don't Know What to Choose? Choose by Your Level
                        </strong>
                      </p>
                      <p>
                        e-Validated offers a white label solutions refer to
                        products or services that are created by one company for
                        the purpose of being sold by another company under that
                        company's own unique branding. one-stop
                        blockchain-powered solution to Students, Institutions,
                        Employees, and Corporate.
                      </p>
                    </div>
                  </Col>
                </Row>
              </Container>

              <Container fluid>
                <Row>
                  <Col lg={4} md={6} className="rowCenter mt-5">
                    <a href="/" className="cardanchor">
                      <Card className="CardSol">
                        <Image
                          src="/images/courses/after-tenth.svg"
                          height={125}
                          width={125}
                        />
                        <Card.Body className="CardBody">
                          <Card.Title className="CardTitle">
                            After 10th Courses
                          </Card.Title>
                          <Card.Text>
                            Applicable for Diploma Course, Degree Courses &
                            Certification Courses
                          </Card.Text>
                        </Card.Body>
                      </Card>
                    </a>
                  </Col>
                  <Col lg={4} md={6} className="rowCenter mt-5">
                    <a href="/" className="cardanchor">
                      <Card className="CardSol">
                        <Image
                          src="/images/courses/bachelor.svg"
                          height={125}
                          width={125}
                        />
                        <Card.Body className="CardBody">
                          <Card.Title className="CardTitle">
                            Bachelor Courses
                          </Card.Title>
                          <Card.Text>
                            Applicable for Diploma Course, Degree Courses &
                            Certification Courses
                          </Card.Text>
                        </Card.Body>
                      </Card>
                    </a>
                  </Col>
                  <Col lg={4} md={6} className="rowCenter mt-5">
                    <a href="/" className="cardanchor">
                      <Card className="CardSol">
                        <Image
                          src="/images/courses/diploma.svg"
                          height={125}
                          width={125}
                        />
                        <Card.Body className="CardBody">
                          <Card.Title className="CardTitle">
                            Diploma Courses
                          </Card.Title>
                          <Card.Text>Applicable for Diploma Courses</Card.Text>
                        </Card.Body>
                      </Card>
                    </a>
                  </Col>
                  <Col lg={4} md={6} className="rowCenter mt-5">
                    <a href="/" className="cardanchor">
                      <Card className="CardSol">
                        <Image
                          src="/images/courses/certification.svg"
                          height={125}
                          width={125}
                        />
                        <Card.Body className="CardBody">
                          <Card.Title className="CardTitle">
                            Certification Courses
                          </Card.Title>
                          <Card.Text>
                            Applicable for Certification Courses
                          </Card.Text>
                        </Card.Body>
                      </Card>
                    </a>
                  </Col>
                  <Col lg={4} md={6} className="rowCenter mt-5">
                    <a href="/" className="cardanchor">
                      <Card className="CardSol">
                        <Image
                          src="/images/courses/md-pg.svg"
                          height={125}
                          width={125}
                        />
                        <Card.Body className="CardBody">
                          <Card.Title className="CardTitle">
                            MD or PG Courses
                          </Card.Title>
                          <Card.Text>
                            Applicable for Degree Courses & Diploma Courses
                          </Card.Text>
                        </Card.Body>
                      </Card>
                    </a>
                  </Col>
                  <Col lg={4} md={6} className="rowCenter mt-5">
                    <a href="/" className="cardanchor">
                      <Card className="CardSol">
                        <Image
                          src="/images/courses/phd.svg"
                          height={125}
                          width={125}
                        />
                        <Card.Body className="CardBody">
                          <Card.Title className="CardTitle">
                            Ph.D Research Courses
                          </Card.Title>
                          <Card.Text>Applicable for Degree Courses</Card.Text>
                        </Card.Body>
                      </Card>
                    </a>
                  </Col>
                </Row>
              </Container>
              <div className="frame3"></div>
              <div className="frame4"></div>
            </div>
            {/* First Wrapper */}
            <div className="wrapper">
              <Container fluid>
                <Row>
                  <Col xl={4} lg={12}>
                    <div className="img-mask">
                      <Image
                        src="/images/courses/image.png"
                        alt=""
                        width={617}
                        height={1123}
                      />
                    </div>
                    <div className="frame-4"></div>
                  </Col>
                  <Col xl={8} lg={12}>
                    <Row>
                      <Col lg={2}>
                        <div className="interest-icon">
                          <Image
                            src="/images/courses/interest.svg"
                            alt=""
                            width={178}
                            height={178}
                            className="icon-image"
                          />
                        </div>
                      </Col>
                      <Col lg={10}>
                        <h5 className="heading-first">
                          The Future is Secure with us
                        </h5>
                        <h2 className="custom-heading">Select by Interests</h2>
                        <p className="para">
                          Verifykiya is an extensive search engine for the
                          students, parents, and education industry players who
                          are seeking information
                        </p>
                      </Col>
                    </Row>
                    <Row>
                      <Col lg={6}>
                        <Row>
                          <Col lg={2}>
                            <div className="icon">
                              <Image
                                src="/images/courses/Frame.svg"
                                alt=""
                                width={32}
                                height={35}
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <h2 className="heading-second">
                              <strong>Engineering</strong>
                            </h2>
                            <p className="para text-underline">
                              BE/B.Tech, ME/M.Tech, Polytechnic
                            </p>
                          </Col>
                        </Row>
                      </Col>
                      <Col lg={6}>
                        <Row>
                          <Col lg={2}>
                            <div className="icon">
                              <Image
                                src="/images/courses/Frame.svg"
                                alt=""
                                width={32}
                                height={35}
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <h2 className="heading-second">Science</h2>
                            <p className="para text-underline">
                              M.Sc, B.Sc, B.F.Sc, M.F.Sc
                            </p>
                          </Col>
                        </Row>
                      </Col>
                      <Col lg={6}>
                        <Row>
                          <Col lg={2}>
                            <div className="icon">
                              <Image
                                src="/images/courses/Frame.svg"
                                alt=""
                                width={32}
                                height={35}
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <h2 className="heading-second">Management</h2>
                            <p className="para text-underline">
                              BBA/BBM, MBA/PGDM, BHM (Hospital), Executive MBA
                            </p>
                          </Col>
                        </Row>
                      </Col>
                      <Col lg={6}>
                        <Row>
                          <Col lg={2}>
                            <div className="icon">
                              <Image
                                src="/images/courses/Frame.svg"
                                alt=""
                                width={32}
                                height={35}
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <h2 className="heading-second">Commerce</h2>
                            <p className="para text-underline">B.Com & M.Com</p>
                          </Col>
                        </Row>
                      </Col>
                      <Col lg={6}>
                        <Row>
                          <Col lg={2}>
                            <div className="icon">
                              <Image
                                src="/images/courses/Frame.svg"
                                alt=""
                                width={32}
                                height={35}
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <h2 className="heading-second">Management</h2>
                            <p className="para text-underline">
                              BBA/BBM, MBA/PGDM, BHM (Hospital), Executive MBA
                            </p>
                          </Col>
                        </Row>
                      </Col>
                      <Col lg={6}>
                        <Row>
                          <Col lg={2}>
                            <div className="icon">
                              <Image
                                src="/images/courses/Frame.svg"
                                alt=""
                                width={32}
                                height={35}
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <h2 className="heading-second">Arts</h2>
                            <p className="para">BA, BFA, BSW, MA</p>
                          </Col>
                        </Row>
                      </Col>
                      <Col lg={6}>
                        <Row>
                          <Col lg={2}>
                            <div className="icon">
                              <Image
                                src="/images/courses/Frame.svg"
                                alt=""
                                width={32}
                                height={35}
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <h2 className="heading-second">
                              Computer Applications
                            </h2>
                            <p className="para text-underline">BCA & MCA</p>
                          </Col>
                        </Row>
                      </Col>
                      <Col lg={6}>
                        <Row>
                          <Col lg={2}>
                            <div className="icon">
                              <Image
                                src="/images/courses/Frame.svg"
                                alt=""
                                width={32}
                                height={35}
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <h2 className="heading-second">Education</h2>
                            <p className="para text-underline">
                              B.Ed, B.P.Ed, M.Ed, M.P.Ed
                            </p>
                          </Col>
                        </Row>
                      </Col>
                      <Col lg={6}>
                        <Row>
                          <Col lg={2}>
                            <div className="icon">
                              <Image
                                src="/images/courses/Frame.svg"
                                alt=""
                                width={32}
                                height={35}
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <h2 className="heading-second">Law</h2>
                            <p className="para text-underline">
                              LLB, LLM, BA LLB
                            </p>
                          </Col>
                        </Row>
                      </Col>
                      <Col lg={6}>
                        <Row>
                          <Col lg={2}>
                            <div className="icon">
                              <Image
                                src="/images/courses/Frame.svg"
                                alt=""
                                width={32}
                                height={35}
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <h2 className="heading-second">Pharmacy</h2>
                            <p className="para text-underline">
                              B.Pharm, M.Pharm
                            </p>
                          </Col>
                        </Row>
                      </Col>
                      <Col lg={6}>
                        <Row>
                          <Col lg={2}>
                            <div className="icon">
                              <Image
                                src="/images/courses/Frame.svg"
                                alt=""
                                width={32}
                                height={35}
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <h2 className="heading-second">Agriculture</h2>
                            <p className="para text-underline">B.Sc, M.Sc</p>
                          </Col>
                        </Row>
                      </Col>
                      <Col lg={6}>
                        <Row>
                          <Col lg={2}>
                            <div className="icon">
                              <Image
                                src="/images/courses/Frame.svg"
                                alt=""
                                width={32}
                                height={35}
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <h2 className="heading-second">Dental</h2>
                            <p className="para text-underline">BDS, MDS</p>
                          </Col>
                        </Row>
                      </Col>
                      <Col lg={6}>
                        <Row>
                          <Col lg={2}>
                            <div className="icon">
                              <Image
                                src="/images/courses/Frame.svg"
                                alt=""
                                width={32}
                                height={35}
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <h2 className="heading-second">Paramedical</h2>
                            <p className="para text-underline">
                              B.Sc(Nursing), M.Sc (Nursing)
                            </p>
                          </Col>
                        </Row>
                      </Col>
                      <Col lg={6}>
                        <Row>
                          <Col lg={2}>
                            <div className="icon">
                              <Image
                                src="/images/courses/Frame.svg"
                                alt=""
                                width={32}
                                height={35}
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <h2 className="heading-second">Architecture</h2>
                            <p className="para text-underline">
                              B.Arch, B.Planning, M.Arch, M.Planning
                            </p>
                          </Col>
                        </Row>
                      </Col>
                      <Col lg={6}>
                        <Row>
                          <Col lg={2}>
                            <div className="icon">
                              <Image
                                src="/images/courses/Frame.svg"
                                alt=""
                                width={32}
                                height={35}
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <h2 className="heading-second">
                              Veterinary Sciences
                            </h2>
                            <p className="para text-underline">
                              BVSc, MVSc, Ph.D
                            </p>
                          </Col>
                        </Row>
                      </Col>
                      <Col lg={6}>
                        <Row>
                          <Col lg={2}>
                            <div className="icon">
                              <Image
                                src="/images/courses/Frame.svg"
                                alt=""
                                width={32}
                                height={35}
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <h2 className="heading-second">Hotel Management</h2>
                            <p className="para text-underline">BHM, MHM</p>
                          </Col>
                        </Row>
                      </Col>
                      <Col lg={6}>
                        <Row>
                          <Col lg={2}>
                            <div className="icon">
                              <Image
                                src="/images/courses/Frame.svg"
                                alt=""
                                width={32}
                                height={35}
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <h2 className="heading-second">Aviation</h2>
                            <p className="para text-underline">
                              BBA (Aviation), M.Sc (Aviation), AME
                            </p>
                          </Col>
                        </Row>
                      </Col>
                      <Col lg={6}>
                        <Row>
                          <Col lg={2}>
                            <div className="icon">
                              <Image
                                src="/images/courses/Frame.svg"
                                alt=""
                                width={32}
                                height={35}
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <h2 className="heading-second">
                              Mass Communications
                            </h2>
                            <p className="para text-underline">BMM, MMC</p>
                          </Col>
                        </Row>
                      </Col>
                      <Col lg={6}>
                        <Row>
                          <Col lg={2}>
                            <div className="icon">
                              <Image
                                src="/images/courses/Frame.svg"
                                alt=""
                                width={32}
                                height={35}
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <h2 className="heading-second">
                              Vocational Courses
                            </h2>
                            <p className="para text-underline">Bachelors</p>
                          </Col>
                        </Row>
                      </Col>
                      <Col lg={6}>
                        <Row>
                          <Col lg={2}>
                            <div className="icon">
                              <Image
                                src="/images/courses/Frame.svg"
                                alt=""
                                width={32}
                                height={35}
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <h2 className="heading-second">Design</h2>
                            <p className="para text-underline">B.Des, M.Des</p>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </Container>
            </div>
            {/* Second Wrapper */}
            <div className="second-wrapper">
              <Container fluid className="bg">
                <Row>
                  <Col lg={3}>
                    <div className="form_icon">
                      <Image
                        src="/images/courses/form-icon.svg"
                        alt=""
                        width={116}
                        height={153}
                      />
                    </div>
                  </Col>
                  <Col lg={9} className="headings my-4">
                    <h1 className="heading-first">
                      Are you looking to study abroad ?
                    </h1>
                    <h1 className="heading-second">Talk to Our Counsellor</h1>
                  </Col>
                </Row>
                <Row>
                  <form>
                    <Row>
                      <Col lg={6} >
                      <div className="m-4">
                        <InputGroup className="mb-3">
                          <InputGroup.Text id="basic-addon1"><Image
                              src="/images/Wallet/penicon.svg"
                              alt=""
                              width={25}
                              height={25}
                            /></InputGroup.Text>
                          <Form.Control
                            placeholder="Full Name"
                            
                          />
                        </InputGroup>
                        </div>
                      <div className="m-4">
                        <InputGroup className="mb-3">
                          <InputGroup.Text id="basic-addon1"><Image
                              src="/images/Wallet/penicon.svg"
                              alt=""
                              width={25}
                              height={25}
                            /></InputGroup.Text>
                          <Form.Control
                            placeholder="Email"
                            type="email"
                          />
                        </InputGroup>
                        </div>
                      <div className="m-4">
                        <InputGroup className="mb-3">
                          <InputGroup.Text id="basic-addon1"><Image
                              src="/images/Wallet/penicon.svg"
                              alt=""
                              width={25}
                              height={25}
                            /></InputGroup.Text>
                          <Form.Control
                            placeholder="Have you given english proficiency test?"
                            
                          />
                        </InputGroup>
                        </div>
                      <div className="m-4">
                        <InputGroup className="mb-3">
                          <InputGroup.Text id="basic-addon1"><Image
                              src="/images/Wallet/penicon.svg"
                              alt=""
                              width={25}
                              height={25}
                            /></InputGroup.Text>
                          <Form.Control
                            placeholder="Or Studyabroad Countries"
                            
                          />
                        </InputGroup>
                        </div>
                      </Col>
                      <Col lg={6}>
                      <div className="m-4">
                        <InputGroup className="mb-3">
                          <InputGroup.Text id="basic-addon1"><Image
                              src="/images/Wallet/penicon.svg"
                              alt=""
                              width={25}
                              height={25}
                            /></InputGroup.Text>
                          <Form.Control
                            placeholder="Mobile No. "
                            
                          />
                        </InputGroup>
                        </div>
                      <div className="m-4">
                        <InputGroup className="mb-3">
                          <InputGroup.Text id="basic-addon1"><Image
                              src="/images/Wallet/penicon.svg"
                              alt=""
                              width={25}
                              height={25}
                            /></InputGroup.Text>
                          <Form.Control
                            placeholder="When do you want to join?"
                            
                          />
                        </InputGroup>
                        </div>
                      <div className="m-4">
                        <InputGroup className="mb-3">
                          <InputGroup.Text id="basic-addon1"><Image
                              src="/images/Wallet/penicon.svg"
                              alt=""
                              width={25}
                              height={25}
                            /></InputGroup.Text>
                          <Form.Control
                            placeholder="Where do you want to study?"
                            
                          />
                        </InputGroup>
                        </div>
                      </Col>
                     
                    </Row>
                    <div className="d-flex justify-content-center m-4">
                      <button className="btn btn-success" >Request Free Counselling</button>
                    </div>
                    <div>
                      <p className="para">
                        Dont want to miss out on Board updates? Fill in and
                        register to receive exclusive and reliable board details
                        such as dates, time-table, results, exam pattern, sample
                        papers and much more!
                      </p>
                    </div>
                  </form>
                </Row>
              </Container>
            </div>
            {/* Third Wrapper */}
            <div className="third-wrapper">
              <Container fluid className="bg">
                <Row>
                  <Col xl={6}>
                    <Row>
                      <Col>
                        <h1 className="custom-heading">Why Study Abroad</h1>
                        <h3 className="heading-first">
                          we provide to Institutions
                        </h3>
                        <p className="para">
                          Based on the Ministry of External Affairs, Government
                          of India data, more than 7 Lakh Indian students are
                          studying abroad in 2019.
                        </p>
                      </Col>
                    </Row>
                    <p className="paragraph">
                      Some of the key reasons why students prefer to study
                      abroad:
                    </p>
                    <Row className="my-4">
                      <Col lg={2} className="my-2">
                        <div className="icon">
                          <Image
                            src="/images/courses/Group234.svg"
                            alt=""
                            width={43}
                            height={37}
                          />
                        </div>
                      </Col>
                      <Col lg={10} className="my-3">
                        <p className="icon-para">
                          Excellent career opportunities
                        </p>
                      </Col>

                      <Col lg={2} className="my-2">
                        <div className="icon">
                          <Image
                            src="/images/courses/Group234.svg"
                            alt=""
                            width={43}
                            height={37}
                          />
                        </div>
                      </Col>
                      <Col lg={10} className="my-3">
                        <p className="icon-para">Enhanced language skills</p>
                      </Col>

                      <Col lg={2} className="my-2">
                        <div className="icon">
                          <Image
                            src="/images/courses/Group234.svg"
                            alt=""
                            width={43}
                            height={37}
                          />
                        </div>
                      </Col>
                      <Col lg={10} className="my-3">
                        <p className="icon-para">
                          Exposure to a new culture and education system
                        </p>
                      </Col>

                      <Col lg={2} className="my-2">
                        <div className="icon">
                          <Image
                            src="/images/courses/Group234.svg"
                            alt=""
                            width={43}
                            height={37}
                          />
                        </div>
                      </Col>
                      <Col lg={10} className="my-3">
                        <p className="icon-para">
                          Overall personality development
                        </p>
                      </Col>
                    </Row>
                  </Col>
                  <Col xl={6} lg={12}>
                    <div className="img-mask">
                      <div className="study-icon">
                        <Image
                          src="/images/courses/studyAbroadIcon.svg"
                          alt=""
                          width={144}
                          height={125}
                        />
                      </div>
                      <div className="img-Study">
                      <Image
                        src="/images/courses/image-2.png"
                        alt=""
                        width={590}
                        height={881}
                        
                      />
                      </div>
                    </div>
                    <div className="frame-4"></div>
                  </Col>
                </Row>
              </Container>
            </div>

            {/* Fourth Wrapper */}
            <div className="second-wrapper">
              <Container fluid className="bg">
                <Row>
                  <Col lg={3}>
                    <div className="form_icon">
                      <Image
                        src="/images/courses/form-icon.svg"
                        alt=""
                        width={116}
                        height={153}
                      />
                    </div>
                  </Col>
                  <Col lg={9} className="headings my-4">
                    <h1 className="heading-first">
                    Are you ready to explore? If yes, talk to our experts.
                    </h1>
                    <h1 className="heading-second">Register Now to Consult a Counsellor</h1>
                  </Col>
                </Row>
                <Row>
                  <form>
                    <Row>
                      <Col lg={6} >
                      <div className="m-4">
                        <InputGroup className="mb-3">
                          <InputGroup.Text id="basic-addon1"><Image
                              src="/images/Wallet/penicon.svg"
                              alt=""
                              width={25}
                              height={25}
                            /></InputGroup.Text>
                          <Form.Control
                            placeholder="Full Name"
                            
                          />
                        </InputGroup>
                        </div>
                      <div className="m-4">
                        <InputGroup className="mb-3">
                          <InputGroup.Text id="basic-addon1"><Image
                              src="/images/Wallet/penicon.svg"
                              alt=""
                              width={25}
                              height={25}
                            /></InputGroup.Text>
                          <Form.Control
                            placeholder="E-mail"
                            type="email"
                          />
                        </InputGroup>
                        </div>
                      <div className="m-4">
                        <InputGroup className="mb-3">
                          <InputGroup.Text id="basic-addon1"><Image
                              src="/images/Wallet/penicon.svg"
                              alt=""
                              width={25}
                              height={25}
                            /></InputGroup.Text>
                          <Form.Control
                            placeholder="Have you taken an English exam?"
                            
                          />
                        </InputGroup>
                        </div>
                      <div className="m-4">
                        <InputGroup className="mb-3">
                          <InputGroup.Text id="basic-addon1"><Image
                              src="/images/Wallet/penicon.svg"
                              alt=""
                              width={25}
                              height={25}
                            /></InputGroup.Text>
                          <Form.Control
                            placeholder="Intake date"
                            
                          />
                        </InputGroup>
                        </div>
                      </Col>
                      <Col lg={6}>
                      <div className="m-4">
                        <InputGroup className="mb-3">
                          <InputGroup.Text id="basic-addon1"><Image
                              src="/images/Wallet/penicon.svg"
                              alt=""
                              width={25}
                              height={25}
                            /></InputGroup.Text>
                          <Form.Control
                            placeholder="Mobile No. "
                            
                          />
                        </InputGroup>
                        </div>
                      <div className="m-4">
                        <InputGroup className="mb-3">
                          <InputGroup.Text id="basic-addon1"><Image
                              src="/images/Wallet/penicon.svg"
                              alt=""
                              width={25}
                              height={25}
                            /></InputGroup.Text>
                          <Form.Control
                            placeholder="Country of residence"
                            
                          />
                        </InputGroup>
                        </div>
                      <div className="m-4">
                        <InputGroup className="mb-3">
                          <InputGroup.Text id="basic-addon1"><Image
                              src="/images/Wallet/penicon.svg"
                              alt=""
                              width={25}
                              height={25}
                            /></InputGroup.Text>
                          <Form.Control
                            placeholder="Where do you want to study?"
                            
                          />
                        </InputGroup>
                        </div>
                      </Col>
                     
                    </Row>
                    <div>
                      <p className="para">
                      Once you submit your details, our student counselling team 
                      will get in touch with you to follow-up.
                      </p>
                    </div>
                    <div className="d-flex justify-content-center m-4">
                      <button className="btn btn-success" >Request Free Counselling</button>
                    </div>
                  </form>
                </Row>
              </Container>
            </div>
          </div>
        </PageLayout>
      </div>
    );
}

export default Courses;