import "antd/dist/antd.css";
import "font-awesome/css/font-awesome.css";
import "react-intl-tel-input/dist/main.css";
import "nprogress/nprogress.css";
import "@styles/home.scss";
import "@styles/header.scss";
import "bootstrap/dist/css/bootstrap.min.css";
import "@styles/educational-institutions.scss";
import "@styles/corporate.scss";
import "@styles/event.scss";
import "@styles/courses.scss";
import "@styles/resources.scss";
import "@styles/contact.scss";
import "@styles/sidebar.scss";
import "@styles/admin.scss";
import "@styles/profile.scss";
import "@styles/school-course-details.scss";
import "@styles/registration.scss";
import "@styles/wallet.scss";
import "@styles/AdminElSchool.scss";
import "@styles/AdminProfile.scss";
// import "@styles/AdminAcademic.scss"
// ----------------
import "@styles/about-us.scss";
import "@styles/school-course-details.scss";
import "@styles/resources.scss";
import "@styles/style.scss";
import "@styles/PartnerWithUs.scss";
import "@styles/Login.scss";
import "@styles/CoursesDetails.scss";
import "@styles/affiliate.scss";
import "@styles/jobs-details.scss";
import "@styles/pricing.scss";
import "@styles/admin-referral-level-1.scss";

import "@styles/explore-school.scss";
import "@styles/AdmissionTracker.scss";

import store from "@redux/store";
import { Provider } from "react-redux";

function MyApp({ Component, pageProps }) {
  const getLayout = Component.getLayout || ((page) => page);
  return (
    <Provider store={store}>{getLayout(<Component {...pageProps} />)}</Provider>
  );
}

export default MyApp;
