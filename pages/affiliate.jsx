import LoginForm from "@components/LoginForm";
import PageLayout from "@components/PageLayout";
import Head from "next/head";
import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import Signup from "./form/Signup";
import RegisterRecieve from "./form/RegisterRecieve";
import Subscribe from "./form/Subscribe";

const Affiliate = () => {
  return (
    <>
      <Head>
        <title>Affiliate | Credentials Wallet</title>
        <meta
          id="meta-description"
          name="description"
          content="Securely store and verify your documents with our blockchain-based credentials wallet. Get started with document verification today."
        />
        <meta property="og:title" content="  Affiliate | Credentials Wallet" />
        <meta
          property="og:description"
          content="Securely store and verify your documents with our blockchain-based credentials wallet. Get started with document verification today."
        />
        <meta
          property="twitter:title"
          content="  Affiliate | Credentials Wallet"
        />
        <meta
          property="twitter:description"
          content="Securely store and verify your documents with our blockchain-based credentials wallet. Get started with document verification today."
        />
      </Head>
      <PageLayout>
        <div className="affiliate-wrapper1 HeroSectionWrapper">
          <Container fluid>
            <Row>
              <Col lg={12}>
                <div className="affiliate-wrapper1-inner">
                  <div className="affiliate-wrapper1-inner-hero">
                    <h1 className="affiliate-wrapper1-inner-hero-heading">
                      Become A VerifyKiya Associate
                    </h1>
                    <p className="affiliate-wrapper1-inner-hero-para">
                      VerifyKiya offers an incredibly high number of educational
                      opportunities to students. And now, you can earn a
                      significant commission by promoting VerifyKiya’s unique
                      solutions. All you have to do is advertise and share our
                      solutions to benefit from our 3-layered affiliate program.
                    </p>
                  </div>

                  <div className="affiliate-wrapper1-inner-sub-hero">
                    <Row>
                      <Col lg={4}>
                        <div className="sub-hero">
                          <img
                            src="/images/affiliate/signup.svg"
                            alt=""
                            className="sub-hero-img"
                          />
                          <h2 className="sub-hero-heading">Sign Up</h2>
                          <p className="sub-hero-para">
                            Sign up for VerifyKiya’s Affiliate Program to
                            generate a unique affiliate link that will help you
                            earn rewards.
                          </p>
                        </div>
                      </Col>
                      <Col lg={4}>
                        <div className="sub-hero">
                          <img
                            src="/images/affiliate/share.svg"
                            alt=""
                            className="sub-hero-img"
                          />
                          <h2 className="sub-hero-heading">Share</h2>
                          <p className="sub-hero-para">
                            Share your affiliate link with other people and urge
                            them to click upon your link to receive your
                            commission.
                          </p>
                        </div>
                      </Col>
                      <Col lg={4}>
                        <div className="sub-hero">
                          <img
                            src="/images/affiliate/earn.svg"
                            alt=""
                            className="sub-hero-img"
                          />
                          <h2 className="sub-hero-heading">Earn</h2>
                          <p className="sub-hero-para">
                            Sign up for VerifyKiya’s Affiliate Program to
                            generate a unique affiliate link that will help you
                            earn rewards.
                          </p>
                        </div>
                      </Col>
                    </Row>
                  </div>
                </div>
              </Col>
            </Row>
          </Container>
        </div>
        <Container fluid>
          <div className="d-flex flex-wrap flex-lg-nowrap ">
            <div className="mx-auto">
              <div className="frame-width">
                <iframe
                  src="/images/1.png"
                  scrolling="no"
                  width="100%"
                  height="100%"
                  class="responsive-iframe"
                ></iframe>
              </div>
            </div>
          </div>
        </Container>
        <div className="p-5">
          <Container fluid>
            <Row>
              <Col lg={9}>
                <Row>
                  <Col md={4}>
                    <div className="affiliate-Component-img">
                      <div className="affiliate-Component-img1-main">
                        <img
                          src="/images/affiliate/man.svg"
                          alt=""
                          className="affiliate-Component-img-main"
                        />
                      </div>
                      <img
                        src="/images/affiliate/main1.svg"
                        alt=""
                        className="affiliate-Component-img-main1"
                      />
                      <img
                        src="/images/affiliate/main2.svg"
                        alt=""
                        className="affiliate-Component-img-main2"
                      />
                    </div>
                  </Col>
                  <Col md={1}></Col>
                  <Col md={7}>
                    <div className="affiliate-Component">
                      <div className="affiliate-Component-text">
                        <h1 className="affiliate-Component-heading">
                          VerfiyKiya’s Affiliate Program
                        </h1>
                        <p className="affiliate-Component-para">
                          Redefining The Concept of Passive Income
                        </p>
                      </div>

                      <div className="affiliate-Component-icn">
                        <Row>
                          <Col lg={2}>
                            <div className="affiliate-Component-icn-img">
                              <img
                                src="/images/affiliate/icn1.svg"
                                alt=""
                                className="affiliate-Component-icn-imgg"
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <h1 className="affiliate-Component-icn-heading">
                              Up to 20% Commission
                            </h1>
                            <p className="affiliate-Component-icn-para">
                              Earn a 10% commission on every conversion and a 5%
                              commission each on the next 2 subsequent
                              conversions.
                            </p>
                          </Col>
                        </Row>

                        <Row>
                          <Col lg={2}>
                            <div className="affiliate-Component-icn-img">
                              <img
                                src="/images/affiliate/icn2.svg"
                                alt=""
                                className="affiliate-Component-icn-imgg"
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <h1 className="affiliate-Component-icn-heading">
                              Dedicated Team Support
                            </h1>
                            <p className="affiliate-Component-icn-para">
                              Get real-time assistance from VerifyKiya’s team of
                              experts to turn prospects into lifelong
                              stakeholders of VerifyKiya’s affiliate program.
                            </p>
                          </Col>
                        </Row>
                        <Row>
                          <Col lg={2}>
                            <div className="affiliate-Component-icn-img">
                              <img
                                src="/images/affiliate/icn3.svg"
                                alt=""
                                className="affiliate-Component-icn-imgg"
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <h1 className="affiliate-Component-icn-heading">
                              Reading Material
                            </h1>
                            <p className="affiliate-Component-icn-para">
                              Help your people realize the potential of our
                              affiliate program through our detailed and
                              well-curated reading material.
                            </p>
                          </Col>
                        </Row>
                        <Row>
                          <Col lg={2}>
                            <div className="affiliate-Component-icn-img">
                              <img
                                src="/images/affiliate/icn4.svg"
                                alt=""
                                className="affiliate-Component-icn-imgg"
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <h1 className="affiliate-Component-icn-heading">
                              Conversion Tips
                            </h1>
                            <p className="affiliate-Component-icn-para">
                              Get valuable tips on how to turn your propsects
                              into esteemed pillars of our affiliate program and
                              earn massive rewards effortlessly.
                            </p>
                          </Col>
                        </Row>
                        <Row>
                          <Col lg={2}>
                            <div className="affiliate-Component-icn-img">
                              <img
                                src="/images/affiliate/icn5.svg"
                                alt=""
                                className="affiliate-Component-icn-imgg"
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <h1 className="affiliate-Component-icn-heading">
                              Early Beta Access
                            </h1>
                            <p className="affiliate-Component-icn-para">
                              Be the first one to get access to VerfiyKiya’s
                              beta version apps and take your earnings from our
                              affiliate program to a whole new level.
                            </p>
                          </Col>
                        </Row>
                      </div>
                    </div>
                  </Col>
                </Row>

                <div className="affi">
                  <div className="sign-up">
                    <div className="main-box">
                      <Col lg={8} className="mx-auto">
                        <LoginForm />
                        <p className="main-box-para">
                          Have already an account ? <span>Login here</span>
                        </p>
                      </Col>
                    </div>
                  </div>
                </div>
              </Col>
              <Col lg={3}>
                <div className="resources">
                  <Signup />
                  <RegisterRecieve />
                  <Subscribe />
                </div>
              </Col>
            </Row>
          </Container>
        </div>
      </PageLayout>
    </>
  );
};

export default Affiliate;
