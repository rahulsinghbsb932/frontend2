import React, { useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  InputGroup,
  Button,
  Accordion,
} from "react-bootstrap";
import Head from "next/head";
import PageLayout from "@components/PageLayout";
import Link from "next/link";
import Signup from "./form/Signup";
import RegisterRecieve from "./form/RegisterRecieve";
import Subscribe from "./form/Subscribe";

const ResultDetails = () => {
  return (
    <div>
      <Head>
        <title>Result Details | Business Verification Services</title>
        <meta
          id="meta-description"
          name="description"
          content=" This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
        <meta
          property="og:title"
          content="Result Details | Business Verification Services"
        />
        <meta
          property="og:description"
          content="This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
        <meta
          property="twitter:title"
          content="Certified Academy | Academy Verification | VerifyKiya"
        />
        <meta
          property="twitter:description"
          content="This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
      </Head>
      <PageLayout>
        <div className="resources">
          <div className="HeroSectionWrapper">
            <Container fluid>
              <Row>
                <Col md={12}>
                  <div className="headingWhileLabel Montserrat text-center mb-5">
                    <h1>Board of Intermediate Education</h1>
                    <h3>Vijayawada, Andhra Pradesh, India</h3>
                  </div>
                </Col>
              </Row>
              <Row>
                <Col md={12}>
                  <div className="headingWhileLabel Montserrat text-center mb-5">
                    <h4>
                      <strong>
                        Board of Intermediate Education of Andhra Pradesh
                        (BIEAP) - Inter 1st & 2nd Year Results 2023
                      </strong>
                    </h4>
                    <p>
                      The Board of Intermediate Education of Andhra Pradesh
                      (BIEAP) was established in 1971 with its headquarters at
                      Vijayawada to regulate and supervise the system of
                      Intermediate education in the state of Andhra Pradesh. The
                      important functions of the board include prescribing
                      syllabus and text books, publishing textbooks, granting
                      affiliation to the institutions offering Intermediate
                      courses, conducting examinations, publishing results and
                      issuing certificates.
                    </p>
                    <p>
                      The Andhra Pradesh Inter 1st Year Exams 2023 began on 15th
                      March 2023 and concluded on 3rd April 2023. The AP
                      Intermediate 2nd Year Exam 2023 have been conducted from
                      16th March 2023 till 4th April 2023. BIEAP shall announce
                      the AP Inter Results 2023 in the month of May 2023, which
                      comprises of the Andhra Inter 1st Year Results 2023 and AP
                      Inter 2nd Year Result 2023. Over 10 lakh students appear
                      for the AP Inter Examinations each year.
                    </p>
                  </div>
                  <div className="headingWhileLabel Montserrat text-center">
                    <h4>
                      <strong>
                        Board of Secondary Education of Andhra Pradesh (BSEAP) -
                        SSC & OSSC Result 2023
                      </strong>
                    </h4>
                    <p>
                      Board of Secondary Education Andhra Pradesh (BSEAP), also
                      known as the Directorate of Government Examinations, was
                      established in 1953 to regulate and supervise the system
                      of secondary education in Andhra Pradesh. BSEAP conducts
                      the Andhra SSC & OSSC Examinations in the state in which
                      over 14 lakh students appear ever year. With the Andhra
                      Pradesh SSC Exams 2023 scheduled in April 2023, the AP SSC
                      Result 2023 is expected to be announced in the month of
                      May 2023.
                    </p>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
          <div className="contact wrapper">
            <div className="contact-box text-white">
              <Container fluid>
                <Row>
                  <Col lg={9}>
                    <div className="bg ">
                      <div className="z-index">
                        <Row>
                          <Row>
                            <Col lg={1}>
                              <img
                                src="/images/result/1.svg"
                                className="img-fluid"
                              />
                            </Col>
                            <Col lg={11}>
                              <h2>Intermediate Public 1st Year General</h2>
                              <p>Exam Result 2023</p>
                            </Col>
                          </Row>
                          <Row>
                            <Col lg={12} className="mt-3">
                              <p>
                                As of yet, declaration schedule is not fixed. We
                                will update this page as soon as the date / time
                                is decided.However, In 2022, BIEAP announced
                                Intermediate Public I Year General Exam Result
                                2023 on 22/06/2022 . Please check back this page
                                often for 2023 results.
                              </p>
                            </Col>
                          </Row>
                          <Col lg={12}>
                            <Form>
                              <Row>
                                <Col lg={6}>
                                  <Form>
                                    <InputGroup size="sm" className="mb-3">
                                      <InputGroup.Text id="inputGroup-sizing-sm">
                                        <img
                                          src="/images/contact/email.svg"
                                          className="img-fluid me-2"
                                        />
                                      </InputGroup.Text>
                                      <Form.Control
                                        aria-label="Small"
                                        aria-describedby="inputGroup-sizing-sm"
                                        placeholder="RollNo for e.g 1901110001*"
                                      />
                                    </InputGroup>
                                  </Form>
                                </Col>
                                <Col lg={6}>
                                  <Form>
                                    <InputGroup size="sm" className="mb-3">
                                      <InputGroup.Text id="inputGroup-sizing-sm">
                                        <img
                                          src="/images/contact/name.svg"
                                          className="img-fluid me-2"
                                        />
                                      </InputGroup.Text>
                                      <Form.Control
                                        aria-label="Small"
                                        aria-describedby="inputGroup-sizing-sm"
                                        placeholder="Name"
                                      />
                                    </InputGroup>
                                  </Form>
                                </Col>
                              </Row>
                              {/* <Row>
                              <Col lg={6}>
                                <Form>
                                  <InputGroup size="sm" className="mb-3">
                                    <InputGroup.Text id="inputGroup-sizing-sm">
                                      <div>
                                        <img
                                          src="/images/contact/email.svg"
                                          className="img-fluid me-2"
                                        />
                                      </div>
                                    </InputGroup.Text>
                                    <Form.Control
                                      aria-label="Small"
                                      aria-describedby="inputGroup-sizing-sm"
                                      placeholder="Date of Birth (dd-mm-yyyy)*"
                                    />
                                  </InputGroup>
                                </Form>
                              </Col>
                              <Col lg={6}></Col>
                            </Row> */}
                              <p className="mt-3">
                                This site is developed and hosted by BIEAP. This
                                information is brought to you by the Board of
                                Secondary Education Andhra Pradesh. Links to
                                other Internet sites and posting of news
                                headlines should not be construed as an
                                endorsement of the views contained therein.
                              </p>
                              <div className="text-center">
                                <Button variant="success" size="sm">
                                  View Result
                                </Button>
                              </div>
                            </Form>
                          </Col>
                        </Row>
                      </div>
                    </div>
                    <Row className="mt-5">
                      <Col lg={12}>
                        <div className="text">
                          <h6 className="mb-5 text-white">
                            Steps to check your AP Inter I Year Results 2023
                          </h6>
                          <ul>
                            <li>
                              <div>
                                <strong>Step 1</strong>
                              </div>
                              <div>
                                Visit the Official Website for BIEAP Inter 1st
                                Year Result 2023:
                                https://www.results.shiksha/andhra-pradesh/inter-first-year.htm.
                              </div>
                            </li>
                            <li>
                              <div>
                                <strong>Step 2</strong>
                              </div>
                              <div>
                                Enter your Inter First Year 2023 Registration /
                                Hall Ticket Number and Date of Birth (in
                                DD/MM/YYYY format) and click on 'GET INTER
                                RESULT 2023'.
                              </div>
                            </li>
                            <li>
                              <div>
                                <strong>Step 3</strong>
                              </div>
                              <div>
                                Your AP Inter 1st Year Result & Marksheet 2023
                                will be displayed instantly.
                              </div>
                            </li>
                            <li>
                              <div>
                                <strong>Step 4:</strong>
                              </div>
                              <div>
                                Take a printout of your AP Inter Result 2023 for
                                future reference. This will also be helpful for
                                admission in the Inter Senior Colleges across
                                AP.
                              </div>
                            </li>
                          </ul>
                        </div>

                        <div className="mt-5">
                          <Accordion defaultActiveKey="0">
                            <Accordion.Item eventKey="0">
                              <Accordion.Header>
                                <span>
                                  Other Exam Result of Board of Intermediate
                                  Education, Andhra Pradesh
                                </span>
                                <span>13 April 2023</span>
                              </Accordion.Header>
                              <Accordion.Body>
                                <ul>
                                  <li>
                                    <div className="flex">
                                      <Link href="">
                                        <div>
                                          <a>
                                            SSC Exam Result 2023 (Regular &
                                            Private)
                                          </a>
                                        </div>
                                      </Link>
                                      <span>Register</span>
                                    </div>
                                  </li>
                                  <li>
                                    <div className="flex">
                                      <Link href="">
                                        <div>
                                          <a>
                                            Intermediate Public 1st Year General
                                          </a>
                                          -<strong> Exam Result 2023 </strong>
                                        </div>
                                      </Link>
                                      <span>Register</span>
                                    </div>
                                  </li>
                                  <li>
                                    <div className="flex">
                                      <Link href="">
                                        <div>
                                          <a>
                                            Intermediate 1st Year Vocational
                                          </a>
                                          -<strong> Exam Result 2023 </strong>
                                        </div>
                                      </Link>
                                    </div>
                                  </li>
                                  <li>
                                    <div className="flex">
                                      <Link href="">
                                        <div>
                                          <a>
                                            Intermediate 2nd Year General Public
                                          </a>
                                          -<strong> Exam Result 2023 </strong>
                                        </div>
                                      </Link>
                                    </div>
                                  </li>
                                  <li>
                                    <div className="flex">
                                      <Link href="">
                                        <div>
                                          <a>
                                            Intermediate 1st Year Vocational
                                          </a>
                                          -<strong> Exam Result 2023 </strong>
                                        </div>
                                      </Link>
                                    </div>
                                  </li>
                                </ul>
                              </Accordion.Body>
                            </Accordion.Item>
                          </Accordion>
                        </div>
                      </Col>
                    </Row>
                  </Col>
                  <Col lg={3}>
                    <div className="w-300">
                      <iframe
                        src="/images/host.png"
                        scrolling="no"
                        width='100%'
                        height="250"
                        class="responsive-iframe"
                      ></iframe>
                      <Signup />
                      <RegisterRecieve />
                      <Subscribe />
                    </div>
                  </Col>
                </Row>
              </Container>
            </div>
          </div>
        </div>
      </PageLayout>
    </div>
  );
};

export default ResultDetails;
