import React, { useState } from "react";
import { Container, Row, Col } from "react-bootstrap";
import Table from "react-bootstrap/Table";
import Figure from "react-bootstrap/Figure";
import Head from "next/head";
import PageLayout from "@components/PageLayout";
import Signup from "./form/Signup";
import RegisterRecieve from "./form/RegisterRecieve";
import Subscribe from "./form/Subscribe";

const SchoolCourseDetails = () => {
  const [items, setItems] = useState([
    {
      title: "Apeejay School International",
      para: "Surajpur-Kasna Road, Greater Noida",
      form: "Online Admission Form",
      class: "Nursery",
      session: "2023-2024",
      date: "31-08-2023 23:59:00",
      fee: 500,
      status: "Ongoing",
      classes: ["AC Classes", "Smart Classes", "WiFi"],
      boarding: ["Boys Hostel", "Girls Hostel"],
      infrastructure: [
        "Auditorium/Media Room",
        "Cafeteria/Canteen",
        "Library/Reading Room ",
        "Playground",
      ],
      safetyandsecurity: [
        "CCTV",
        "GPS Bus Tracking App",
        "Student Tracking App",
      ],
      advancedfacilities: [
        "Science Lab",
        "Computer Lab",
        "Meals",
        "Medical Room",
        "Transportation",
      ],
      indooesports: [
        "Game",
        "Gym",
        "Yoga",
        "Bowling",
        "Do not drop the ball",
        "Four Corners",
        "Balloon or foam ball",
        "Hand tricks",
        "Chair-less musical chairs",
        "The Hot/Cold game",
      ],
      outdoorsports: [
        "Skating",
        "Horse Riding ",
        "Gym",
        "Swimming Pool",
        "Karate",
        "Taekwondo",
        "Kho-Kho",
        "Kabaddi",
        "Cricket",
        "Volleyball",
        "Basketball",
        "Football",
      ],
    },
  ]);
  return (
    <div>
      <Head>
        <title>School Course Details | Business Verification Services</title>
        <meta
          id="meta-description"
          name="description"
          content=" This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
        <meta
          property="og:title"
          content="School Course Details | Business Verification Services"
        />
        <meta
          property="og:description"
          content="This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
        <meta
          property="twitter:title"
          content="Certified Academy | Academy Verification | VerifyKiya"
        />
        <meta
          property="twitter:description"
          content="This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
      </Head>
      <PageLayout>
        <div className="school-course-details coursedetails">
          <div className="HeroSectionWrapper">
            <Container fluid>
              <Row>
                <Col>
                  <div className="headingWhileLabel Montserrat">
                    <h1>Apeejay International School </h1>
                    <p>
                      <strong>Surajpur-Kasna Road, Greater Noida</strong>
                    </p>
                    <p>
                      Counted among the best schools in Greater Noida, Apeejay
                      International School is a part of the Apeejay Education
                      Society which was founded in 1967 by Dr Stya Paul to
                      provide quality education right from the pre-nursery to
                      the doctorate level. Spread over a lush-green 15-acre
                      complex, the School is conveniently located in Greater
                      Noida, which is one of the youngest, best planned,
                      pollution-free cities of Uttar Pradesh. It is a
                      CBSE-affiliated, co-ed school that has been consistently
                      ranked among top international schools in Greater Noida by
                      several renowned publications.
                    </p>
                    <div className="list-center">
                      <ul>
                        <li>Ranked no 1 for overall quality of education</li>
                        <li>Numerous Study Choices</li>
                        <li>Internationally recognised</li>
                        <li>Flexible Education system</li>
                        <li>Merit based university assistance provided</li>
                        <li>Scholarships and fellowships awarded</li>
                        <li>Tuition fee waivers</li>
                        <li>On campus jobs with minimal wages</li>
                        <li>Leader in latest technologies and advancement</li>
                        <li>Industry based training and research</li>
                        <li>Graduate, Teaching & research assistantship</li>
                        <li>Vibrant Campus life</li>
                      </ul>
                    </div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>

          <div className="school-course-details-box pb-5">
            {items.map((item, index) => (
              <div key={index}>
                <Container fluid>
                  <Row>
                    <Col lg={9}>
                      <div className="bg">
                        <Row className="mb-5">
                          <Col xl={1} lg={1}>
                            <div className="icons">
                              <img
                                src="/images/result/AP-education.svg"
                                className="img-fluid"
                              />
                            </div>
                          </Col>
                          <Col xl={7} lg={7}>
                            <div className="end">
                              <div>
                                <h2>{item.title}</h2>
                              </div>
                              <div>
                                <p>{item.para}</p>
                              </div>
                            </div>
                          </Col>
                          <Col xl={4} lg={4}>
                            <div className="btn p-0">
                              <div className="d-flex justify-content-center my-2">
                                <button
                                  className="btn btn-primary btn-sm green"
                                  type="button"
                                >
                                  Apply Now
                                </button>
                              </div>
                              {/* <Link href="" className="green">
                                    Apply Now
                                </Link> */}
                            </div>
                          </Col>
                        </Row>
                        <Row>
                          <Col>
                            <div className="price ">
                              <div className="d-flex justify-content-between align-items-center header flex-wrap">
                                <div className="text d-flex flex-wrap align-items-center gap-2">
                                  <strong>{item.form}</strong>
                                </div>
                              </div>
                              <Table responsive className="text-center">
                                <tbody>
                                  <tr>
                                    <td>
                                      <strong>Classes</strong>
                                    </td>
                                    <td>
                                      <strong>Course Type</strong>
                                    </td>
                                    <td>
                                      <strong>Duration</strong>
                                    </td>
                                    <td>
                                      <strong>Batches Dates</strong>
                                    </td>
                                    <td>
                                      <strong>Total Fees ₹</strong>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>{item.class}</td>
                                    <td>{item.session}</td>
                                    <td>{item.date}</td>
                                    <td>{item.fee}</td>
                                    <td>{item.status}</td>
                                  </tr>
                                  <tr>
                                    <td>{item.class}</td>
                                    <td>{item.session}</td>
                                    <td>{item.date}</td>
                                    <td>{item.fee}</td>
                                    <td>{item.status}</td>
                                  </tr>
                                  <tr>
                                    <td>{item.class}</td>
                                    <td>{item.session}</td>
                                    <td>{item.date}</td>
                                    <td>{item.fee}</td>
                                    <td>{item.status}</td>
                                  </tr>
                                  <tr>
                                    <td>{item.class}</td>
                                    <td>{item.session}</td>
                                    <td>{item.date}</td>
                                    <td>{item.fee}</td>
                                    <td>{item.status}</td>
                                  </tr>
                                  <tr>
                                    <td>{item.class}</td>
                                    <td>{item.session}</td>
                                    <td>{item.date}</td>
                                    <td>{item.fee}</td>
                                    <td>{item.status}</td>
                                  </tr>
                                </tbody>
                              </Table>
                              <div className="d-flex justify-content-center my-2">
                                <button
                                  className="btn btn-primary btn-sm dropdown-toggle view-more"
                                  type="button"
                                >
                                  View More
                                </button>
                              </div>
                            </div>
                            {/* Fee Structure */}
                            <div className="price my-5 feestructure">
                              <div className="d-flex justify-content-between align-items-center header flex-wrap">
                                <div className="text d-flex flex-wrap align-items-center gap-2">
                                  <strong>Fee Structure</strong>
                                </div>
                                <div className="d-flex">
                                  <button
                                    className="btn btn-primary btn-sm dropdown-toggle view-more mx-3"
                                    type="button"
                                  >
                                    2023-2024
                                  </button>
                                  <button
                                    className="btn btn-primary btn-sm dropdown-toggle view-more"
                                    type="button"
                                  >
                                    Pre-Nursery
                                  </button>
                                </div>
                              </div>
                              <Row>
                                <Col lg={8}>
                                  <p className="px-5 mt-3">
                                    Total Cost for a new admission <br />
                                    <strong>70,730</strong> (for first year)
                                  </p>
                                  <p className="px-5">
                                    Fee Structure for Pre-Nursery:
                                  </p>
                                  <div className="px-5 my-3">
                                    <Table responsive>
                                      <tbody>
                                        <tr>
                                          <td>
                                            <strong>Type</strong>
                                          </td>
                                          <td>
                                            <strong>Amount</strong>
                                          </td>
                                          <td>
                                            <strong>Frequency</strong>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td>Registration Fees</td>
                                          <td>₹ 1,000</td>
                                          <td>Onetime</td>
                                        </tr>
                                        <tr>
                                          <td>Admission Fees</td>
                                          <td>₹ 10,000</td>
                                          <td>Onetime</td>
                                        </tr>
                                        <tr>
                                          <td>Tuition Fees</td>
                                          <td>₹ 3,520</td>
                                          <td>Monthly</td>
                                        </tr>
                                        <tr>
                                          <td>Security Fees</td>
                                          <td>₹ 10,000</td>
                                          <td>Onetime</td>
                                        </tr>
                                        <tr>
                                          <td>Annual Fees</td>
                                          <td>₹ 7,500</td>
                                          <td>Annually</td>
                                        </tr>
                                        <tr>
                                          <td>Transportation Fees</td>
                                          <td>₹ 1,000 - ₹ 3,000</td>
                                          <td>Monthly</td>
                                        </tr>
                                      </tbody>
                                    </Table>
                                  </div>
                                </Col>
                                <Col lg={4} className="my-2 second-col">
                                  <div className="my-4">
                                    <p>
                                      Form Availability <br />
                                      <strong>Online</strong> (for first year)
                                    </p>
                                    <hr />
                                  </div>
                                  <div className="my-4">
                                    <p>
                                      Form Payment <br />
                                      <strong>Online</strong>
                                    </p>
                                    <hr />
                                  </div>
                                  <div className="my-4">
                                    <p>
                                      School Timing <br />
                                      <strong>08:00 AM TO 01:40 PM</strong>
                                    </p>
                                    <hr />
                                  </div>
                                  <div className="my-4">
                                    <p>
                                      Office Timing <br />
                                      <strong>08:00 AM TO 03:00 PM</strong>
                                    </p>
                                    <hr />
                                  </div>
                                </Col>
                              </Row>
                            </div>

                            {/* Fee Structure */}

                            <div className="price mt-5">
                              <div className="d-flex justify-content-between align-items-center header flex-wrap">
                                <div className="text d-flex flex-wrap align-items-center gap-2">
                                  <strong>
                                    Admission Criteria & Eligibility
                                  </strong>
                                </div>
                                <div className="d-flex justify-content-end my-2">
                                  <div className="d-flex justify-content-center ">
                                    <button
                                      className="btn btn-primary btn-sm dropdown-toggle view-more"
                                      type="button"
                                    >
                                      Pre-Nursery
                                    </button>
                                  </div>
                                  {/* <button className="btn btn-primary btn-sm dropdown-toggle" type="button" >
                                        Pre-Nursery
                                        </button> */}
                                </div>
                              </div>
                              <Table responsive borderless className="px-3">
                                <tbody>
                                  <tr>
                                    <td>Eligibility (Age Qualification)</td>
                                    <td>Student Interaction</td>
                                    <td>Parents Interaction</td>
                                    <td>Total Seats</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      <strong>
                                        3 - 4 Years as on 31 March, 2023
                                      </strong>
                                    </td>
                                    <td>
                                      <strong>Yes</strong>
                                    </td>
                                    <td>
                                      <strong>Yes</strong>
                                    </td>
                                    <td>
                                      <strong>55</strong>
                                    </td>
                                  </tr>
                                </tbody>
                              </Table>
                            </div>
                            <div className="price mt-5">
                              <div className="d-flex justify-content-between align-items-center header flex-wrap">
                                <div className="text d-flex flex-wrap align-items-center gap-2">
                                  <strong>Academic Stats</strong>
                                </div>
                              </div>
                              <Table responsive borderless>
                                <tbody>
                                  <tr>
                                    <td>Classes Offered</td>
                                    <td>Languages</td>
                                    <td>Academic Session</td>
                                    <td>Student Faculty Ratio</td>
                                    <td>School Format</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      <strong>Nursery -Class 12</strong>
                                    </td>
                                    <td>
                                      <strong>English</strong>
                                    </td>
                                    <td>
                                      <strong>April-March</strong>
                                    </td>
                                    <td>
                                      <strong>20:1</strong>
                                    </td>
                                    <td>
                                      <strong>Day School</strong>
                                    </td>
                                  </tr>
                                </tbody>
                              </Table>
                            </div>
                            <div className="price mt-5">
                              <div className="d-flex justify-content-between align-items-center header flex-wrap">
                                <div className="text d-flex flex-wrap align-items-center gap-2">
                                  <strong>Key School Stats</strong>
                                </div>
                              </div>
                              <Table responsive borderless>
                                <tbody>
                                  <tr>
                                    <td>Classes Offered</td>
                                    <td>Languages</td>
                                    <td>Academic Session</td>
                                    <td>Student Faculty Ratio</td>
                                    <td>School Format</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      <strong>Nursery -Class 12</strong>
                                    </td>
                                    <td>
                                      <strong>English</strong>
                                    </td>
                                    <td>
                                      <strong>April-March</strong>
                                    </td>
                                    <td>
                                      <strong>20:1</strong>
                                    </td>
                                    <td>
                                      <strong>Day School</strong>
                                    </td>
                                  </tr>
                                </tbody>
                              </Table>
                            </div>
                            <div className="price mt-5">
                              <div className="d-flex justify-content-between align-items-center header flex-wrap">
                                <div className="text d-flex flex-wrap align-items-center gap-2">
                                  <strong>Facilities</strong>
                                </div>
                              </div>
                              <Row>
                                <Col className="mt-3">
                                  <ul>
                                    Class
                                    {item.classes.map((ele, i) => {
                                      return (
                                        <li className="fw-bold" key={i}>
                                          {ele}
                                        </li>
                                      );
                                    })}
                                  </ul>
                                </Col>
                                <Col className="mt-3">
                                  <ul>
                                    Boarding
                                    {item.boarding.map((ele, i) => {
                                      return (
                                        <li className="fw-bold" key={i}>
                                          {ele}
                                        </li>
                                      );
                                    })}
                                  </ul>
                                </Col>
                                <Col className="mt-3">
                                  <ul>
                                    Infrastructure
                                    {item.infrastructure.map((ele, i) => {
                                      return (
                                        <li className="fw-bold" key={i}>
                                          {ele}
                                        </li>
                                      );
                                    })}
                                  </ul>
                                </Col>
                                <Col className="mt-3">
                                  <ul>
                                    Safety and Security
                                    {item.safetyandsecurity.map((ele, i) => {
                                      return (
                                        <li className="fw-bold" key={i}>
                                          {ele}
                                        </li>
                                      );
                                    })}
                                  </ul>
                                </Col>
                                <Col className="mt-3">
                                  <ul>
                                    Advance Facilities
                                    {item.advancedfacilities.map((ele, i) => {
                                      return (
                                        <li className="fw-bold" key={i}>
                                          {ele}
                                        </li>
                                      );
                                    })}
                                  </ul>
                                </Col>
                              </Row>
                            </div>
                            <div className="price mt-5">
                              <div className="d-flex justify-content-between align-items-center header flex-wrap">
                                <div className="text d-flex flex-wrap align-items-center gap-2">
                                  <strong>Sports and Fittness</strong>
                                </div>
                              </div>
                              <Row>
                                <Col lg={6} className="mt-3">
                                  <ul>
                                    Indoor Sports
                                    {item.indooesports.map((ele, i) => {
                                      return (
                                        <li className="fw-bold" key={i}>
                                          {ele}
                                        </li>
                                      );
                                    })}
                                  </ul>
                                </Col>
                                <Col lg={6} className="mt-3">
                                  <ul>
                                    Outdoor Sports
                                    {item.outdoorsports.map((ele, i) => {
                                      return (
                                        <li className="fw-bold" key={i}>
                                          {ele}
                                        </li>
                                      );
                                    })}
                                  </ul>
                                </Col>
                              </Row>
                            </div>
                            <div className="price mt-5">
                              <div className="d-flex justify-content-between align-items-center header flex-wrap">
                                <div className="text d-flex flex-wrap align-items-center gap-2">
                                  <strong>Gallery</strong>
                                </div>
                              </div>

                              <section className="mx-3">
                                <Figure className="m-3 image-fluid">
                                  <Figure.Image
                                    width={171}
                                    height={180}
                                    alt="171x180"
                                    src="/images/result/image 6.png"
                                  />
                                </Figure>
                                <Figure className="m-3 image-fluid">
                                  <Figure.Image
                                    width={171}
                                    height={180}
                                    alt="171x180"
                                    src="/images/result/image 7.png"
                                  />
                                </Figure>
                                <Figure className="m-3 image-fluid">
                                  <Figure.Image
                                    width={171}
                                    height={180}
                                    alt="171x180"
                                    src="/images/result/image 9.png"
                                  />
                                </Figure>
                                <Figure className="m-3 image-fluid">
                                  <Figure.Image
                                    width={171}
                                    height={180}
                                    alt="171x180"
                                    src="/images/result/image 10.png"
                                  />
                                </Figure>
                                <Figure className="m-3 image-fluid">
                                  <Figure.Image
                                    width={171}
                                    height={180}
                                    alt="171x180"
                                    src="/images/result/image 11.png"
                                  />
                                </Figure>
                                <Figure className="m-3 image-fluid">
                                  <Figure.Image
                                    width={171}
                                    height={180}
                                    alt="171x180"
                                    src="/images/result/image 12.png"
                                  />
                                </Figure>
                                <Figure className="m-3 image-fluid">
                                  <Figure.Image
                                    width={171}
                                    height={180}
                                    alt="171x180"
                                    src="/images/result/image 13.png"
                                  />
                                </Figure>
                                <Figure className="m-3 image-fluid">
                                  <Figure.Image
                                    width={171}
                                    height={180}
                                    alt="171x180"
                                    src="/images/result/image 14.png"
                                  />
                                </Figure>
                                <Figure className="m-3 image-fluid">
                                  <Figure.Image
                                    width={171}
                                    height={180}
                                    alt="171x180"
                                    src="/images/result/image 15.png"
                                  />
                                </Figure>
                                <Figure className="m-3 image-fluid">
                                  <Figure.Image
                                    width={171}
                                    height={180}
                                    alt="171x180"
                                    src="/images/result/image 21.png"
                                  />
                                </Figure>
                                <Figure className="m-3 image-fluid">
                                  <Figure.Image
                                    width={171}
                                    height={180}
                                    alt="171x180"
                                    src="/images/SchoolCourseDetails/image 23.png"
                                  />
                                </Figure>
                                <Figure className="m-3 image-fluid">
                                  <Figure.Image
                                    width={171}
                                    height={180}
                                    alt="171x180"
                                    src="/images/SchoolCourseDetails/image 24.png"
                                  />
                                </Figure>
                                <Figure className="m-3 image-fluid">
                                  <Figure.Image
                                    width={171}
                                    height={180}
                                    alt="171x180"
                                    src="/images/SchoolCourseDetails/image 25.png"
                                  />
                                </Figure>
                                <Figure className="m-3 image-fluid">
                                  <Figure.Image
                                    width={171}
                                    height={180}
                                    alt="171x180"
                                    src="/images/SchoolCourseDetails/image 26.png"
                                  />
                                </Figure>
                                <Figure className="m-3 image-fluid">
                                  <Figure.Image
                                    width={171}
                                    height={180}
                                    alt="171x180"
                                    src="/images/SchoolCourseDetails/image 27.png"
                                  />
                                </Figure>
                              </section>
                            </div>
                          </Col>
                        </Row>
                      </div>
                    </Col>

                    <Col lg={3}>
                      <div className="w-300">
                        <iframe
                          src="/images/host.png"
                          scrolling="no"
                          width="400"
                          height="250"
                          class="responsive-iframe"
                        ></iframe>
                        <Signup />
                        <RegisterRecieve />
                        <Subscribe />
                      </div>
                    </Col>
                  </Row>
                </Container>
              </div>
            ))}
          </div>
        </div>
      </PageLayout>
    </div>
  );
};

export default SchoolCourseDetails;
