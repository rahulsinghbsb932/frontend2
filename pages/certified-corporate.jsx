import React from "react";
import Card from "react-bootstrap/Card";
import { Container, Row, Col } from "react-bootstrap";
import Head from "next/head";
import Image from "next/image";
import PageLayout from "@components/PageLayout";

const Solution = () => {
  return (
    <div>
      <Head>
        <title>Certified Corporate | Business Verification Services</title>
        <meta
          id="meta-description"
          name="description"
          content=" This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
        <meta
          property="og:title"
          content="Certified Corporate | Business Verification Services"
        />
        <meta
          property="og:description"
          content="This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
        <meta
          property="twitter:title"
          content="Certified Academy | Academy Verification | VerifyKiya"
        />
        <meta
          property="twitter:description"
          content="This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
      </Head>
      <PageLayout>
        <div className="HeroSectionWrapper">
          <Container fluid>
            <Row>
              <Col md={12}>
                <div className="headingWhileLabel Montserrat">
                  <h1>For Corporate</h1>
                  <p>
                    Sieving through hundreds of profiles but none match the
                    exact requirements? Confused about which institutes to
                    target to get the brightest talent the market has to offer?
                    Or concerned about falsified resumes and hiring the wrong
                    talent? All these concerns are a daily constant at any
                    functioning corporation these days. 
                  </p>
                  <p>
                    {" "}
                    Employers are looking for a talented and reliable workforce
                    but have no idea where to look. This is where Verifykiya
                    comes into the picture! With the help of our platform,
                    recruiters can get access to thousands and thousands of
                    verified profiles. You can improve your hiring efficiency
                    and bid farewell to exhausting worries.
                  </p>
                </div>
              </Col>
            </Row>
          </Container>

          <Container fluid>
            <Row>
              <Col lg={3} md={6} className="rowCenter mt-5">
                <a href="/" className="cardanchor">
                  <Card className="CardSol">
                    <Image
                      src="/images/OurSolution/school.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">Private</Card.Title>
                      <Card.Text>
                        A Private is an educational institution designed to
                        provide learning spaces and learning.
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col lg={3} md={6} className="rowCenter mt-5">
                <a href="/" className="cardanchor">
                  <Card className="CardSol">
                    <Image
                      src="/images/OurSolution/colledge.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">Government</Card.Title>
                      <Card.Text>
                        A Government is an educational institution designed to
                        provide learning spaces and learning.
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col lg={3} md={6} className="rowCenter mt-5">
                <a href="/" className="cardanchor">
                  <Card className="CardSol">
                    <Image
                      src="/images/OurSolution/university.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Multinational
                      </Card.Title>
                      <Card.Text>
                        A Multinational is an educational institution designed
                        to provide learning spaces and learning.
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col lg={3} md={6} className="rowCenter mt-5">
                <a href="/" className="cardanchor">
                  <Card className="CardSol">
                    <Image
                      src="/images/OurSolution/trainin.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">NGO’s</Card.Title>
                      <Card.Text>
                        A NGO l is an educational institution designed to
                        provide learning spaces and learning.
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
            </Row>
          </Container>
        </div>

        <div className="sectionRelative m-5">
          <Container fluid>
            <Col md={12}>
              <div class="ourSolutionWrapper">
                <h1 class="ourSolutionSectionheading">
                  <span style={{ fontWeight: 300 }}>How does </span>
                  Verifykiya Help You?
                </h1>
                <p className="ourSolutionPera">
                  Companies have a lot on their plate. To ensure that you focus
                  on your fundamental and key areas of your organization,
                  Verifykiya extends its assistance. A myriad of tasks become a
                  single-click phenomenon with Verifykiya’s cutting edge
                  platform.
                </p>
              </div>
            </Col>
          </Container>
        </div>

        <section>
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    <Image
                      src="/images/corporate/digital.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="digital-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Digital Certification and Documents
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Blockchain based NFT enabled credentials like appointment
                      letter, Pay Slips, certificates, badges, experience letter
                      etc can be given to the employees. These verifiable
                      credentials go a long way in easy authentication,
                      credibility and avoidance of disputes etc. Their immutable
                      property is an instant image booster and fosters faith
                      within all stakeholders, once uploaded cannot be tampered
                      with without proper authorization. All these documents can
                      be stored in your Verifykiya wallet for having everything
                      verified at one place.
                    </p>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section>
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="hr-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        HR Workload And Administrative Process
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      The process of hiring, selecting and training employees
                      gets really simplified with Verifykiya! Our platform not
                      only provides fantastic automation mechanisms for the
                      administrative work associated with hiring but also
                      extends precise analytical support in terms of workforce
                      and workload analysis and identification of the ideal
                      candidate.  With our verified resumes,certificates and
                      documents, you would be be able to find the best man for
                      the job requirement and because his verification is
                      already been done, it will cut the verification workload
                      and cost.
                    </p>
                  </div>
                </Col>
                <Col xl={3} lg={12} className="order-xl-2 order-1">
                  <div className="img">
                    <Image
                      src="/images/corporate/hr.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section>
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    <Image
                      src="/images/corporate/corporate.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="cor-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Access To Certified Corporate Trainers
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      It is as essential to train the fresh hires as it is to
                      enhance skills of the existing ones. Verifykiya has an
                      exclusive roster of seasoned professionals who are experts
                      in their fields. As and when required, these professionals
                      can lend their expertise in order to train employees to
                      become functional and efficient assets. All the corporate
                      trainers credentials are completely verified by our
                      blockchain based platform and by virtue of a robust review
                      system you can guage their performance as well
                    </p>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section>
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="putting-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Putting An End To ‘Two-Timing’ - Moonlighting
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Moonlighting, which means working two jobs at the same
                      time- often secretively, is a problem that companies have
                      been dealing with for quite some time. With VerifyKiya’s
                      blockchain-powered platform, companies finally have a way
                      of ensuring that their employees aren’t working two jobs
                      as it gives companies their complete employment history
                      and ensures maximum productivity.
                    </p>
                  </div>
                </Col>
                <Col xl={3} lg={12} className="order-xl-2 order-1">
                  <div className="img">
                    <Image
                      src="/images/corporate/putting.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section>
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    <Image
                      src="/images/corporate/seamless.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="seam-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Seamless Integration With Existing Systems
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Our platform can be easily integrated in any company
                      without much disruption. There is no requirement of
                      changing your process, our software can integrate with
                      your existing SAP or other software solutionsl. We make
                      tailor-made solutions so that all your needs and wants can
                      be incorporated.
                    </p>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section>
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="em-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Employees As Social Ambassadors
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Through Verifykiya, employees can even share their issued
                      accolades like certificates, badges etc. which can boost
                      your public image and put faces to your success stories.
                      Your employees can emerge as ambassadors and represent
                      your company. This will boost morale within the employees
                      and even attract new ones. 
                    </p>
                  </div>
                </Col>
                <Col xl={3} lg={12} className="order-xl-2 order-1">
                  <div className="img">
                    <Image
                      src="/images/corporate/employees.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section>
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    <Image
                      src="/images/corporate/templates_database.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="tem-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Templates Database
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      We offer a wide range of industry-approved and attractive
                      templates to choose from so that your work gets easier and
                      much quicker. These templates are set by top experts in
                      the respected fields to match the industry standard but
                      even if that doesn’t satisfy the requirements we also have
                      a customization feature which allows you to request for a
                      unique template personalized to your perfection. We also
                      give an option to institutions to utilise our platform as
                      a white label solution as well.
                    </p>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section>
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="ai-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        AI Curated Resumes
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Sometimes even deciphering which candidate would be best
                      for the job is taxing. It is time consuming and not an
                      efficient process. Verifykiya, with its technology base,
                      will be able to provide ideal results for resumes that are
                      completely apt for the job description mentioned. The
                      resumes will be curated by the AI and will do a thorough,
                      in depth check on suitable candidates, virtually
                      impossible to do by employees on time. These AI Checked
                      resumes will be optimally qualified for the jobs.
                    </p>
                  </div>
                </Col>
                <Col xl={3} lg={12} className="order-xl-2 order-1">
                  <div className="img">
                    <Image
                      src="/images/corporate/AI.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section>
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    <Image
                      src="/images/corporate/talent.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="tal-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Access to Talent pool
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Our diverse and verifiable talent pool will be available
                      to you with just a click! This talent pool will have
                      people with different qualifications, cohorts etc. so all
                      possible hiring needs can be taken care of! This talent
                      pool will save time and money as candidates through this
                      will already have verified credentials.
                    </p>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section>
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="qui-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Quick Background Checks
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      These pre-verified profiles save time and money because
                      running an extensive background check will become a thing
                      of the past. The immutability property of our
                      technology-base is a testament to the authenticity of
                      these profiles. It is virtually impossible to fake a
                      credential with Verifykiya!
                    </p>
                  </div>
                </Col>
                <Col xl={3} lg={12} className="order-xl-2 order-1">
                  <div className="img">
                    <Image
                      src="/images/corporate/quick.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section className="mb-5">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    <Image
                      src="/images/corporate/simplified.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="sim-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Simplified Hiring
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      You will have access to a diversified and authenticated
                      talent pool. Recruitment and selection of desired
                      candidates will become seamless. Simply specifying your
                      requirements and eligibility will even generate an even
                      more targeted list of potential candidates. 
                    </p>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
      </PageLayout>
    </div>
  );
};

export default Solution;
