import React from "react";
import Card from "react-bootstrap/Card";
import { Container, Row, Col } from "react-bootstrap";
import Head from "next/head";
import Image from "next/image";
import PageLayout from "@components/PageLayout";

const Solution = () => {
  return (
    <div>
      <Head>
        <title>
          Educational institutions | Schools | Colleges | Universities |
          VerifyKiya
        </title>
        <meta
          id="meta-description"
          name="description"
          content="We refer here to certification institutions, schools and colleges that issue certificates to individuals or institutions that meet certain standards or requirements, through the use of blockchain technology and NFTs."
        />
        <meta
          property="og:title"
          content=" Educational institutions | Schools | Colleges | Universities | VerifyKiya"
        />
        <meta
          property="og:description"
          content="We refer here to certification institutions, schools and colleges that issue certificates to individuals or institutions that meet certain standards or requirements, through the use of blockchain technology and NFTs."
        />
        <meta
          property="twitter:title"
          content=" Educational institutions | Schools | Colleges | Universities | VerifyKiya"
        />
        <meta
          property="twitter:description"
          content="We refer here to certification institutions, schools and colleges that issue certificates to individuals or institutions that meet certain standards or requirements, through the use of blockchain technology and NFTs."
        />
      </Head>
      <PageLayout>
        <div className="HeroSectionWrapper">
          <Container fluid>
            <Row>
              <Col md={12}>
                <div className="headingWhileLabel Montserrat">
                  <h1>Educational institutions & Universities</h1>
                  <p>
                    Institutions are the backbone of the education industry.
                    From a student’s primary education to specialized higher
                    studies, the topmost institutions are sought after for
                    maximum growth opportunities. Verifykiya offers the best for
                    the best. Our engaging and precisely curated feature set
                    perfectly coalesces to formulate a tailor-made solution for
                    your brand that promises progress, relevancy and quality.
                    Keeping in mind the core values of an educational
                    institution, Verifykiya keeps the students at the centre of
                    it all. This enhances brand image and also helps in the
                    recruitment and retention of future learners.
                  </p>
                </div>
              </Col>
            </Row>
          </Container>

          <Container fluid>
            <Row>
              <Col lg={3} md={6} className="rowCenter mt-5">
                <a href="/" className="cardanchor">
                  <Card className="CardSol">
                    <Image
                      src="/images/OurSolution/school.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">Schools</Card.Title>
                      <Card.Text>
                        Find the perfect fit for your child's education -
                        Explore our comprehensive list of schools in India
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col lg={3} md={6} className="rowCenter mt-5">
                <a href="/" className="cardanchor">
                  <Card className="CardSol">
                    <Image
                      src="/images/OurSolution/colledge.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">College</Card.Title>
                      <Card.Text>
                        Unlock your potential and embrace your future - Discover
                        the top colleges in India with our list
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col lg={3} md={6} className="rowCenter mt-5">
                <a href="/" className="cardanchor">
                  <Card className="CardSol">
                    <Image
                      src="/images/OurSolution/university.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Universities
                      </Card.Title>
                      <Card.Text>
                        Empowering the future leaders of India - Discover the
                        top universities with our comprehensive list
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col lg={3} md={6} className="rowCenter mt-5">
                <a href="/" className="cardanchor">
                  <Card className="CardSol">
                    <Image
                      src="/images/OurSolution/trainin.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Training institutes
                      </Card.Title>
                      <Card.Text>
                        Where skills meet success - Discover the best training
                        institutes in India with our comprehensive list
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
            </Row>

            <Row>
              <Col lg={3} md={6} className="rowCenter mt-5">
                <a href="/" className="cardanchor">
                  <Card className="CardSol">
                    <Image
                      src="/images/OurSolution/coaching.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Coaching Center
                      </Card.Title>
                      <Card.Text>
                        Unleashing the potential within you - Find the most
                        reliable coaching centers in India with us
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col lg={3} md={6} className="rowCenter mt-5">
                <a href="/" className="cardanchor">
                  <Card className="CardSol">
                    <Image
                      src="/images/OurSolution/tution.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Tuition Center
                      </Card.Title>
                      <Card.Text>
                        Elevating your grades to new heights - Discover the best
                        tuition centers in India with our list
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col lg={3} md={6} className="rowCenter mt-5">
                <a href="/" className="cardanchor">
                  <Card className="CardSol">
                    <Image
                      src="/images/OurSolution/home.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">Home Tutor</Card.Title>
                      <Card.Text>
                        One-on-one tutoring for a brighter future - Find the
                        most reliable home tutors in India with us
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col lg={3} md={6} className="rowCenter mt-5">
                <a href="/" className="cardanchor">
                  <Card className="CardSol">
                    <Image
                      src="/images/OurSolution/edthes.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">Edtechs</Card.Title>
                      <Card.Text>
                        Transforming education through technology - Discover the
                        best EdTech companies in India with our list
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
            </Row>
          </Container>
        </div>

        <div className="sectionRelative m-5">
          <Container fluid>
            <Col md={12}>
              <div class="ourSolutionWrapper">
                <h1 class="ourSolutionSectionheading">
                  <span>Here comes </span>
                  Verifykiya!
                </h1>
                <p className="ourSolutionPera">
                  Understanding the gravity of the situation, we created this
                  platform to meet the needs of industry-wide institutions. You
                  can finally say goodbye to all the deceit and the irrelevant
                  workload. It’s time to take your brand to greater heights,
                  with Verifykiya!
                </p>
              </div>
            </Col>
          </Container>
        </div>

        <div className="solution">
          <div className="">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    <Image
                      src="/images/OurSolution/blockchain.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="key"></div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Blockchain Based Digital Credentials
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Verifykiya uses the power of blockchain and security of
                      Non fungible tokens (NFT) for creation and verification of
                      genuine credentials like degrees, diplomas, mark sheet,
                      badges, certificates etc. The trustless algorithm of
                      blockchain and the immutable nature of NFTs make it
                      impossible for any unauthorized changes. The security and
                      immutability of NFT ensures the uniqueness and genuineness
                      of the certifications.Verifykiya also provides digital
                      wallets to store your digital certificates and you have an
                      option to upload your previous credentials and get them
                      verified on blockchain. 
                    </p>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </div>

        <section>
          <div className="solution">
            <div className="brand">
              <Container fluid>
                <Row className="bg">
                  <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                    <div className="d-flex position">
                      <div className="icon"></div>
                      <div className="solutioseciconTop ">
                        <h1 class="ourSolutionSectionheading">
                          Brand Building
                        </h1>
                      </div>
                    </div>

                    <div className="solutionSeciconText">
                      <p>
                        Brand building for any institutions is a continuous and
                        painfull exercise. Any unauthorized or spurious
                        credential adversely effects the brand of the
                        institution and has a direct effect on its business.
                        Verifykia’s promise of legitimacy and security is an
                        instant image booster for institutions. It increases
                        brand visibility not only within the student communities
                        but also within the corporate sector. Usage of
                        blockchain and NFT for credentials makes your
                        institution stand apart from the rest and also increases
                        the credibility.
                        <div className="space"></div>
                      </p>
                    </div>
                  </Col>
                  <Col xl={3} lg={12} className="order-xl-2 order-1">
                    <div className="img">
                      <Image
                        src="/images/OurSolution/book.svg"
                        height={480}
                        width={300}
                      />
                    </div>
                  </Col>
                </Row>
              </Container>
            </div>
          </div>
        </section>

        <div className="solution">
          <div className="">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    <Image
                      src="/images/OurSolution/lap.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="wi">
                      <div className="lap-icon key"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Unified Platform for Learner Acquisition, Retention &
                        Upsell
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Verifykiya uses the power of blockchain and security of
                      Non fungible tokens (NFT) for creation and verification of
                      genuine credentials like degrees, diplomas, mark sheet,
                      badges, certificates etc. The trustless algorithm of
                      blockchain and the immutable nature of NFTs make it
                      impossible for any unauthorized changes. The security and
                      immutability of NFT ensures the uniqueness and genuineness
                      of the certifications.Verifykiya also provides digital
                      wallets to store your digital certificates and you have an
                      option to upload your previous credentials and get them
                      verified on blockchain. 
                    </p>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </div>

        <section>
          <div className="solution">
            <div className="brand">
              <Container fluid>
                <Row className="bg">
                  <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                    <div className="d-flex position">
                      <div className="width">
                        <div className="icon acess-icon"></div>
                      </div>
                      <div className="solutioseciconTop ">
                        <h1 class="ourSolutionSectionheading">
                          Access to Scholarships, Placements, Skill &
                          Personality Development
                        </h1>
                      </div>
                    </div>

                    <div className="solutionSeciconText">
                      <p>
                        In this day and age, students need something much more
                        than their course material. They are expected to go
                        above and beyond. And if they have their institution’s
                        support in this journey, that is what sets apart the
                        best institutions. With Verifykiya, your students will
                        get exclusive and real-time updates about all
                        scholarships, placements etc that they may be eligible
                        for and will even offer registration support. To make
                        students more well-rounded, our platform even features
                        great gamified techniques to boost and hone their soft
                        and hard skills.
                        <div className="space"></div>
                      </p>
                    </div>
                  </Col>
                  <Col xl={3} lg={12} className="order-xl-2 order-1">
                    <div className="img">
                      <Image
                        src="/images/OurSolution/acess.svg"
                        height={480}
                        width={300}
                      />
                    </div>
                  </Col>
                </Row>
              </Container>
            </div>
          </div>
        </section>

        <div className="solution">
          <div className="">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    <div className="box"></div>
                    <Image
                      src="/images/OurSolution/option.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="wi">
                      <div className="op-icon key"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Option to Enroll in National & International
                        Competitions
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Verikykiya extends complete support for any data
                      maintenance or automation needs you may have. Our robust
                      platform reduces administrative burden from your
                      shoulders. We even step in and automate admission,
                      credential verification, fee handling, placement etc to
                      streamline everything without disrupting your existing
                      infrastructure and let you focus on your core areas
                      instead of administrative workloads. 
                    </p>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </div>

        <section>
          <div className="solution">
            <div className="brand">
              <Container fluid>
                <Row className="bg">
                  <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                    <div className="d-flex position">
                      <div className="icon depth-icon"></div>
                      <div className="solutioseciconTop ">
                        <h1 class="ourSolutionSectionheading">
                          In Depth Report and Analysis
                        </h1>
                      </div>
                    </div>

                    <div className="solutionSeciconText">
                      <p>
                        With Verifykia, Institutions get a detailed and in depth
                        report and analysis on various metrics that measures its
                        performance like acquisition, success rates, engagement,
                        preference etc. Institutions ca use these reports to
                        identify problem areas and take corrective measures. On
                        the basis of these reports institutions can get a clear
                        view of the progress and accordingly take informed
                        future decisions.
                        <div className="space"></div>
                      </p>
                    </div>
                  </Col>
                  <Col xl={3} lg={12} className="order-xl-2 order-1">
                    <div className="img">
                      <Image
                        src="/images/OurSolution/depth.svg"
                        height={480}
                        width={300}
                      />
                    </div>
                  </Col>
                </Row>
              </Container>
            </div>
          </div>
        </section>

        <div className="solution">
          <div className="">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    <div className="box"></div>
                    <Image
                      src="/images/OurSolution/administrativ.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div>
                      <div className="ad-icon key"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Administrative Workload and Admission Process Automation
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Verikykiya extends complete support for any data
                      maintenance or automation needs you may have. Our robust
                      platform reduces administrative burden from your
                      shoulders. We even step in and automate admission,
                      credential verification, fee handling, placement etc to
                      streamline everything without disrupting your existing
                      infrastructure and let you focus on your core areas
                      instead of administrative workloads. 
                    </p>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </div>

        <section>
          <div className="solution">
            <div className="brand">
              <Container fluid>
                <Row className="bg">
                  <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                    <div className="d-flex position">
                      <div className="icon eas-icon"></div>
                      <div className="solutioseciconTop ">
                        <h1 class="ourSolutionSectionheading">
                          Easy Integration
                        </h1>
                      </div>
                    </div>

                    <div className="solutionSeciconText">
                      <p>
                        Our platform can be easily integrated in any institution
                        without much disruption. Normal processes don’t need to
                        be changed at all. We make tailor-made solutions so that
                        all your needs and wants can be incorporated. We are
                        fully compatible with SAP, LMS or software solutions.
                        Our digital certifications solutions is fully integrated
                        with digilocker or seamless distribution
                        <div className="space"></div>
                      </p>
                    </div>
                  </Col>
                  <Col xl={3} lg={12} className="order-xl-2 order-1">
                    <div className="img">
                      <Image
                        src="/images/OurSolution/easy.svg"
                        height={480}
                        width={300}
                      />
                    </div>
                  </Col>
                </Row>
              </Container>
            </div>
          </div>
        </section>

        <div className="solution">
          <div className="">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    <div className="box"></div>
                    <Image
                      src="/images/OurSolution/make.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div>
                      <div className="make-icon key"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Make Your Students Your Social Ambassadors
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Through Verifykiya, students can even share their issued
                      accolades like certificates, diploma,  degrees, badges,
                      progress reports etc. which can boost your public image
                      and put faces to your success stories. Your students can
                      emerge as your ambassadors and represent your legacy and
                      in turn build your brand and reputation 
                    </p>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </div>

        <section>
          <div className="solution mb-5">
            <div className="brand">
              <Container fluid>
                <Row className="bg">
                  <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                    <div className="d-flex position">
                      <div className="icon dat-icon"></div>
                      <div className="solutioseciconTop ">
                        <h1 class="ourSolutionSectionheading">
                          Templates Database and white Label Solution
                        </h1>
                      </div>
                    </div>

                    <div className="solutionSeciconText">
                      <p>
                        We offer a wide range of industry-approved and
                        attractive templates to choose from so that your work
                        gets easier and much quicker. Along with this, we also
                        have a customization feature which allows you to request
                        for a unique template personalized to your perfection.
                        We also give an option to institutions to  utilise our
                        platform as a white label solution as well.
                        <div className="space"></div>
                      </p>
                    </div>
                  </Col>
                  <Col xl={3} lg={12} className="order-xl-2 order-1">
                    <div className="img">
                      <Image
                        src="/images/OurSolution/database.svg"
                        height={480}
                        width={300}
                      />
                    </div>
                  </Col>
                </Row>
              </Container>
            </div>
          </div>
        </section>
      </PageLayout>
    </div>
  );
};

export default Solution;
