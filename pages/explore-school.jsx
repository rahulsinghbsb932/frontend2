import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import SearchIcon from "@mui/icons-material/Search";
import Button from "@mui/material/Button";
import LocationOnIcon from "@mui/icons-material/LocationOn";
import PageLayout from "@components/PageLayout";
import Head from "next/head";
import Signup from "./form/Signup";
import RegisterRecieve from "./form/RegisterRecieve";
import Subscribe from "./form/Subscribe";
import { Grid } from "@mui/material";
import Image from "next/image";

const exploreSchool = () => {
  const items = [
    {
      logo: "/images/CoursesDetails/Stanford.svg",
      name: "Stanford Graduate School of Business",
      place: "Stanford (CA), United States",
    },
    {
      logo: "/images/CoursesDetails/Stanford.svg",
      name: "Stanford Graduate School of Business",
      place: "Stanford (CA), United States",
    },
    {
      logo: "/images/CoursesDetails/Stanford.svg",
      name: "Stanford Graduate School of Business",
      place: "Stanford (CA), United States",
    },
    {
      logo: "/images/CoursesDetails/Stanford.svg",
      name: "Stanford Graduate School of Business",
      place: "Stanford (CA), United States",
    },
  ];
  return (
    <>
      <Head>
        <title>Explore School | Business Verification Services</title>
        <meta
          id="meta-description"
          name="description"
          content=" This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
        <meta
          property="og:title"
          content="Explore School | Business Verification Services"
        />
        <meta
          property="og:description"
          content="This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
        <meta
          property="twitter:title"
          content="Certified Academy | Academy Verification | VerifyKiya"
        />
        <meta
          property="twitter:description"
          content="This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
      </Head>
      <PageLayout>
        <div className="explore-school">
          <div className="HeroSectionWrapper">
            <div className="frame1 d-xl-block d-none"></div>
            <div className="frame2 d-xl-block d-none"></div>
            <div className="wrapper-wrapper">
              <Container fluid>
                <Row>
                  <Col lg={12}>
                    <div className="wrapper">
                      <h1 className="head">Explore Schools</h1>
                      <p className="para">
                        Search the best schools from a list of 25,000 plus
                        schools located across India. Verifykiya offers
                        personalized counseling support to help you find exactly
                        what you're looking for.
                      </p>
                      <div className="bttn">
                        <span className="btn-inner">
                          <img
                            src="/images/explore-school/icn1.svg"
                            alt=""
                            className="img"
                          />
                          <span className="text">Preferred Location</span>
                        </span>
                        <span className="btn-inner">
                          <img
                            src="/images/explore-school/icn2.svg"
                            alt=""
                            className="img"
                          />
                          <span className="text">Affiliation (Board)</span>
                        </span>
                        <span className="btn-inner">
                          <img
                            src="/images/explore-school/icn3.svg"
                            alt=""
                            className="img"
                          />
                          <span className="text">Admission Tracker</span>
                        </span>
                        <span className="btn-inner">
                          <img
                            src="/images/explore-school/icn4.svg"
                            alt=""
                            className="img"
                          />
                          <span className="text">Compare Schools</span>
                        </span>
                      </div>

                      <div className="box">
                        <span className="list-box">
                          <span className="list">All School</span>
                          <span className="list">Play School</span>
                          <span className="list">Day School</span>
                          <span className="list">Boarding School</span>
                          <span className="list">Day Boarding School</span>
                        </span>

                        <div className="search">
                          <select name="" id="" className="select">
                            <option value="A" className="option">
                              Select District
                            </option>
                            <option value="B">B</option>
                            <option value="C">C</option>
                          </select>

                          <input
                            className="input"
                            type="text"
                            placeholder="Please type school Name or Location"
                          />

                          <div className="bttn">
                            <Button
                              variant="outlined"
                              endIcon={<SearchIcon />}
                              className="btn-ser"
                            >
                              Search
                            </Button>
                          </div>
                        </div>

                        <div className="map">
                          <Button
                            variant="none"
                            startIcon={<LocationOnIcon />}
                            className="btn-map"
                          >
                            FIND THE BEST SCHOOL NEAR YOU
                          </Button>
                        </div>
                      </div>

                      <p className="cen-text">
                        -- “Make the right decisions with choosing a school for
                        your child with Veriykiya tools” --
                      </p>
                    </div>
                  </Col>
                </Row>
              </Container>
            </div>
          </div>
          <Container fluid>
          <div className="d-flex flex-wrap flex-lg-nowrap ">
            <div className="mx-auto">
              <div className="frame-width">
                <iframe
                  src="/images/1.png"
                  scrolling="no"
                  width="100%"
                  height="100%"
                  class="responsive-iframe"
                ></iframe>
              </div>
            </div>
          </div>
        </Container>
          <div className="my-5">
            <Container fluid>
              <Row>
                <Col lg={9}>
                  <div className="MidSection">
                    <div className="MidSection-secondComponent">
                      <Grid container>
                        <Col xl={4} lg={12} md={12} sm={12}>
                          <div className="main-frame">

                            <div className="frame">
                              <div className="icons">
                                <Image
                                  src="/images/explore-school/icon.png"
                                  width={140}
                                  height={150}
                                  className="img-fluid"
                                />
                              </div>
                              <div className="img">
                                <Image
                                  src="/images/explore-school/explore-school.png"
                                  width={400}
                                  height={500}
                                  layout="fill"
                                />
                              </div>
                            </div>
                            <div className="frame-2 d-xl-block d-none">
                            <Image
                                  src="/images/explore-school/frame.svg"
                                  width={400}
                                  height={300}
                                  layout="responsive"
                                />
                            </div>
                          </div>
                        </Col>
                        <Col xl={8}>
                          <div className="MidSection-secondConmponent-hero first">
                            <h1 className="MidSection-secondConmponent-hero-heading ">
                              School in Your Locality
                            </h1>

                            <p className="MidSection-secondConmponent-hero-para">
                              Pick your choice for your child, and we’ll take
                              you through the rest. Check out these top
                              schooling in your locality.
                            </p>
                          </div>

                          <div className="MidSection-secondConmponent-hero-courses mt-5">
                            {items.map((val) => {
                              return (
                                <>
                                  <div className="MidSection-secondConmponent-hero-courses-inner">
                                    <div className="MidSection-secondConmponent-hero-courses-inner-img">
                                      <img
                                        src={val.logo}
                                        className="Sub-HeroSection-Img-Icn"
                                        alt=""
                                      />
                                    </div>
                                    <div className="MidSection-secondConmponent-hero-courses-inner-text">
                                      <h1 className="MidSection-secondConmponent-hero-courses-inner-text-head">
                                        {val.name}
                                      </h1>
                                      <h1 className="MidSection-secondConmponent-hero-courses-inner-text-para">
                                        {val.place}
                                      </h1>
                                    </div>
                                  </div>
                                </>
                              );
                            })}
                          </div>

                          <div className="more">
                            <div className="circle opacity-50"></div>
                            <div className="circle opacity-75"></div>
                            <div className="circle"></div>
                            <div className="arrow"></div>
                            <div>
                              <strong>Know more</strong>
                            </div>
                          </div>
                        </Col>
                      </Grid>
                    </div>

                    <div className="MidSection-secondComponent">
                      <div className="MidSection-secondConmponent-imgg">
                        <img
                          src="/images/CoursesDetails/mssci1.svg"
                          alt=""
                          className="img-fluid"
                        />
                      </div>

                      <div className="MidSection-secondConmponent-hero">
                        <h1 className="MidSection-secondConmponent-hero-heading">
                          Top Ranking Schools
                        </h1>

                        <p className="MidSection-secondConmponent-hero-para">
                          Though there is no perfect time to study abroad,
                          you’ll find certain school years and semesters to be
                          most common for students seeking an international
                          academic experience. While it isn’t unusual to hear of
                          high school graduates going overseas before their
                          freshman year in college, sophomore and junior years
                          are often popular for study abroad. By this time
                          students have realized why everyone should study
                          abroad, and are more likely to have narrowed down the
                          direction of their major.
                        </p>
                      </div>

                      <div className="MidSection-secondConmponent-hero-courses mt-5">
                        {items.map((val) => {
                          return (
                            <>
                              <div className="MidSection-secondConmponent-hero-courses-inner">
                                <div className="MidSection-secondConmponent-hero-courses-inner-img">
                                  <img
                                    src={val.logo}
                                    className="Sub-HeroSection-Img-Icn"
                                    alt=""
                                  />
                                </div>
                                <div className="MidSection-secondConmponent-hero-courses-inner-text">
                                  <h1 className="MidSection-secondConmponent-hero-courses-inner-text-head">
                                    {val.name}
                                  </h1>
                                  <h1 className="MidSection-secondConmponent-hero-courses-inner-text-para">
                                    {val.place}
                                  </h1>
                                </div>
                              </div>
                            </>
                          );
                        })}
                      </div>

                      <div className="more">
                        <div className="circle opacity-50"></div>
                        <div className="circle opacity-75"></div>
                        <div className="circle"></div>
                        <div className="arrow"></div>
                        <div>
                          <strong>Know more</strong>
                        </div>
                      </div>
                    </div>

                    <div className="MidSection-secondComponent">
                      <div className="MidSection-secondConmponent-imgg">
                        <img
                          src="/images/CoursesDetails/mssci1.svg"
                          alt=""
                          className="img-fluid"
                        />
                      </div>

                      <div className="MidSection-secondConmponent-hero">
                        <h1 className="MidSection-secondConmponent-hero-heading">
                          Popular Schools in India
                        </h1>

                        <p className="MidSection-secondConmponent-hero-para">
                          Though there is no perfect time to study abroad,
                          you’ll find certain school years and semesters to be
                          most common for students seeking an international
                          academic experience. While it isn’t unusual to hear of
                          high school graduates going overseas before their
                          freshman year in college, sophomore and junior years
                          are often popular for study abroad. By this time
                          students have realized why everyone should study
                          abroad, and are more likely to have narrowed down the
                          direction of their major.
                        </p>
                      </div>

                      <div className="MidSection-secondConmponent-hero-courses mt-5">
                        {items.map((val) => {
                          return (
                            <>
                              <div className="MidSection-secondConmponent-hero-courses-inner">
                                <div className="MidSection-secondConmponent-hero-courses-inner-img">
                                  <img
                                    src={val.logo}
                                    className="Sub-HeroSection-Img-Icn"
                                    alt=""
                                  />
                                </div>
                                <div className="MidSection-secondConmponent-hero-courses-inner-text">
                                  <h1 className="MidSection-secondConmponent-hero-courses-inner-text-head">
                                    {val.name}
                                  </h1>
                                  <h1 className="MidSection-secondConmponent-hero-courses-inner-text-para">
                                    {val.place}
                                  </h1>
                                </div>
                              </div>
                            </>
                          );
                        })}
                      </div>

                      <div className="more">
                        <div className="circle opacity-50"></div>
                        <div className="circle opacity-75"></div>
                        <div className="circle"></div>
                        <div className="arrow"></div>
                        <div>
                          <strong>Know more</strong>
                        </div>
                      </div>
                    </div>
                  </div>
                </Col>
                <Col lg={3}>
                  <div className="resources">
                    <Signup />
                    <RegisterRecieve />
                    <Subscribe />
                    <iframe src="/images/ad.png" scrolling="no" width='300' height='600'></iframe>
                    <div className="my-2">
                      <iframe src="/images/ad.png" scrolling="no" width='300' height='600'></iframe>
                    </div>
                    <iframe src="/images/ad.png" scrolling="no" width='300' height='600'></iframe>
                    <iframe src="/images/host.png" scrolling="no" width='300' height='600'></iframe>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </div>
      </PageLayout>
    </>
  );
};

export default exploreSchool;
