import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import LoginForm from "@components/LoginForm";
import PageLayout from "@components/PageLayout";
import Head from "next/head";

const Login = () => {
  return (
    <div>
      <Head>
        <title>Login</title>
        <meta
          id="meta-description"
          name="description"
          content=" This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
        <meta
          property="og:title"
          content="Login"
        />
        <meta
          property="og:description"
          content="This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
        <meta
          property="twitter:title"
          content="Certified Academy | Academy Verification | VerifyKiya"
        />
        <meta
          property="twitter:description"
          content="This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
      </Head>
      <PageLayout>
      <div className="sign-up">
          <Container fluid>
        <div className="main-box mx-auto">
            <Row>
              <Col lg={3}>
                <div className="relative">
                  <div className="s-icon">
                    <img alt="   " src="/images/Login/icon.svg" />
                  </div>
                  <div className="img-box">
                    <img alt="   " src="/images/Login/img.png" />
                  </div>
                  <div className="frame"></div>
                </div>
              </Col>
              <Col lg={9}>
                <LoginForm />
                <div className="frame"></div>
              </Col>
            </Row>
        </div>
          </Container>
      </div>
      </PageLayout>
    </div>
  );
};

export default Login;
