import React from "react";
import { Col, Container, Row } from "react-bootstrap";

const adminReferralLevel1 = () => {
  return (
    <>
      <div className="level-1">
        <Container fluid>
          <Row>
            <Col lg={12}>
              <div className="box">
                <Row>
                  <Col lg={1}>
                    <div className="img">
                      <img
                        src="/images/admin-referral-level-1/icon.svg"
                        alt=""
                        className="img-fluid"
                      />
                    </div>
                  </Col>
                  <Col lg={11}>
                    <div className="text">
                      <h1 className="head">Affiliate Program</h1>
                      <p className="para">
                        Sign up for VerifyKiya’s Affiliate Program to generate a
                        unique affiliate link that will help you earn rewards.
                        Share your affiliate link with other people and urge
                        them to click upon your link to receive your commission.
                        For every user that you convert, you’ll receive a 10%
                        commission, followed by a 5% commission each on the next
                        2 subsequent conversions.
                      </p>
                      <p className="point">
                        <span className="point-level">Level 1: </span>
                        Promote VerfifyKiya’s Affiliate Program and earn a 10%
                        commission on every successful conversion.
                      </p>
                      <p className="point">
                        <span className="point-level">Level 2: </span>
                        Earn a 5% commission by adding a second chain of
                        affiliates to our referral program.
                      </p>
                      <p className="point">
                        <span className="point-level">Level 3: </span>
                        Win an additional 5% by adding a third chain of
                        affiliate members to VerifyKiya’s referral program.
                      </p>
                    </div>
                  </Col>
                </Row>

                <div className="link">
                  <h4 className="head">Share the referral link</h4>
                  <p className="para">
                    Copy the referral link and share it on your social media
                    accounts to make the most out of our referral program.
                  </p>
                  <div className="referral">
                    <Row>
                      <Col lg={10}>
                        <div className="referral-link">
                          <div className="copy-l">
                            <img
                              src="/images/admin-referral-level-1/link.svg"
                              alt="p"
                              className="img-p"
                            />
                            <p className="r-link">
                              https://www.verifykiya.com/username130523/your_referral_link
                            </p>
                          </div>

                          <div className="copy">
                            <img
                              src="/images/admin-referral-level-1/Copy.svg"
                              alt="p"
                              className="c-img"
                            />
                            <p className="cl">Copy link</p>
                          </div>
                        </div>
                      </Col>
                      <Col lg={2}>
                        <span>Share</span>
                      </Col>
                    </Row>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
};

export default adminReferralLevel1;
