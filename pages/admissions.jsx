import React from "react";
import Head from "next/head";
import Image from "next/image";
import PageLayout from "@components/PageLayout";
import { Container, Row, Col } from "react-bootstrap";
import Card from "react-bootstrap/Card";

const admissions = () => {
  return (
    <div>
      <Head>
        <title>
          Blockchain Based Wallets For Documents storage | Credentials Wallet
        </title>
        <meta
          id="meta-description"
          name="description"
          content="Securely store and verify your documents with our blockchain-based credentials wallet. Get started with document verification today."
        />
        <meta
          property="og:title"
          content="  Blockchain Based Wallets For Documents storage | Credentials Wallet"
        />
        <meta
          property="og:description"
          content="Securely store and verify your documents with our blockchain-based credentials wallet. Get started with document verification today."
        />
        <meta
          property="twitter:title"
          content="  Blockchain Based Wallets For Documents storage | Credentials Wallet"
        />
        <meta
          property="twitter:description"
          content="Securely store and verify your documents with our blockchain-based credentials wallet. Get started with document verification today."
        />
      </Head>

      <PageLayout>
        <div className="HeroSectionWrapper">
          <Container fluid>
            <Row>
              <Col md={12}>
                <div className="headingWhileLabel Montserrat">
                  <h1>Admissions</h1>
                  <p>
                    Document management is becoming increasingly important as
                    the idea of a paperless office becomes a commonplace
                    reality. Hold up your documents on a single platform
                    effortlessly. Verifykiya enables you to store documents in a
                    blockchain protected Digital Wallet which provides
                    protection against vulnerable situations and hackers.
                  </p>
                </div>
              </Col>
            </Row>
          </Container>

          <Container fluid className="mb-5">
            <Row>
              <Col md={3} className="rowCenter mt-5">
                <a className="cardanchor" href="#Play-School">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Admissions/Play School.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Admissions/Play School.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">Play School</Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={3} className="rowCenter mt-5">
                <a className="cardanchor" href="#Schools">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Admissions/Schools.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Admissions/Schools.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">Schools</Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={3} className="rowCenter mt-5">
                <a className="cardanchor" href="#Colleges">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Admissions/Colleges.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Admissions/Colleges.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">Colleges</Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={3} className="rowCenter mt-5">
                <a className="cardanchor" href="#University">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Admissions/University.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Admissions/University.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">University</Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={3} className="rowCenter mt-5">
                {" "}
                <a className="cardanchor" href="#Study-Abroad">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Admissions/Study Abroad.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Admissions/Study Abroad.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Study Abroad
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={3} className="rowCenter mt-5">
                {" "}
                <a className="cardanchor" href="#Coaching-Centers">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Admissions/Coaching Centers.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Admissions/Coaching Centers.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Coaching Centers
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={3} className="rowCenter mt-5">
                {" "}
                <a className="cardanchor" href="#Tution-Centers">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Admissions/Tution Centers.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Admissions/Tution Centers.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Tution Centers
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>

              <Col md={3} className="rowCenter mt-5">
                <a className="cardanchor" href="#Training-Institutes">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Admissions/Training Institutes.svg")
                          .default
                      }
                      alt=""
                    /> */}
                    <Image
                      src="/images/Services/icon/Admissions/Training Institutes.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Training Institutes
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
            </Row>
          </Container>
        </div>

        <section id="Play-School">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Admissions/Play School.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Admissions/Play School.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Admissions/Play School.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Admissions/Play School.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">Play School</h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Verifykiya comes with a key to solve as to which play
                      school is apt for you and your child. Play School is a
                      vital part of your child's early learning. Browse through
                      our verified list of play schools that have a curriculum
                      that goes with your child's requirements and development
                      needs. You can choose the play school by proximity from
                      your premises, good reputation, infrastructure, trained
                      staff etc.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section id="Schools">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Admissions/Schools.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Admissions/Schools.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">Schools</h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      In dilemma for choosing the right school for your child?
                      Not to worry anymore… Blockchain based platform,
                      Verifykiya enables you to choose the best school for you
                      from exhaustive and verified listings of schools on our
                      platform. Select the schools on the basis of different
                      parameters such as academic success, curriculum,
                      infrastructure, facilities, faculty, etc. Find and compare
                      Information on the website between different schools and
                      choose the best suited for you.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
                <Col xl={3} lg={12} className="order-xl-2 order-1">
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Admissions/Schools.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid "
                    /> */}

                    <Image
                      src="/images/Services/images/Admissions/Schools.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section id="Colleges">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Admissions/Colleges.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Admissions/Colleges.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Admissions/Colleges.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Admissions/Colleges.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">Colleges</h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Verifykiya provides you with an exhaustive, reliable and
                      authentic search engine for educators, students and
                      parents to choose a college best suited based on courses
                      offered, faculty, infrastructure, extra curricular,
                      societies, placements, scholarships, grants, hostel etc.
                      Verifykiya uses advance data algorithms and data mining
                      techniques to bring to you exhaustive details about the
                      college to help you with your decisions.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section id="University">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Admissions/University.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Admissions/University.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">University</h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Verifkiya provides a comprehensive list of universities in
                      India based on rankings, courses offered, fees etc. that
                      will help you and align with your interest, future
                      prospects and goals. Verifykiya also provides the updated
                      status ( Recognised or deemed ) and ownership of the
                      university whether the university is Government (Central /
                      state ) or privately owned as per UGC.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Admissions/University.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}
                    <Image
                      src="/images/Services/images/Admissions/University.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section id="Study-Abroad">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Admissions/Study Abroad.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Admissions/Study Abroad.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Admissions/Study Abroad.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Admissions/Study Abroad.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">Study Abroad</h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Verifykiya is a platform designed to handhold students who
                      are looking for studying abroad. We provide authentic,
                      extensive, verified data on universities abroad based on
                      rankings, courses offered, infrastructure, eligibility
                      requirements, scholarships, placements, course duration,
                      fees etc to help you to take a informed decision. Using
                      advanced AI technology we allow students to apply to
                      multiple universities with a single click and also provide
                      verified partners to take care of your travel and visa
                      requirements. Get assured scholarship on successful
                      international admission with Verifykiya
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section id="Coaching-Centers">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Admissions/Coaching Centers.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Admissions/Coaching Centers.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Coaching Centers
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Verifykiya provides a verified and extensive list of
                      coaching centers which have become vital requirement in
                      higher education and securing rich careers in diverse
                      professional disciplines. Good coaching is considered to
                      be benficial for success in targeted competitive exam
                      therefore selecting the center becomes extremely important
                      and essential. Verifykiya helps students to Look for a
                      coaching center that has trained, experienced and
                      knowledgeable faculty, has infrastructure and resources to
                      fulfill their aim.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Admissions/Coaching Centers.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Admissions/Coaching Centers.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section id="Tution-Centers">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Admissions/Tution Centers.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Admissions/Tution Centers.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Admissions/Tution Centers.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Admissions/Tution Centers.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">Tution Centers</h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Verifykiya has a vast network of trained and experienced
                      tutors and tution centers for almost every subject and
                      level of your requirement. Any listing we provide is
                      extensively verified and genuine. Our AI driven search
                      engine helps you connect and provides the right solution
                      to your questions.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section id="Training-Institutes" className="mb-5">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Admissions/Training Institutes.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Admissions/Training Institutes.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Training Institutes
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Choose from verified list of training institutes and
                      trainers that has the required experienced trainers in the
                      skill set and field you are interested in. Verifykiya
                      provides you with an exhaustive, reliable and authentic
                      search engine for corporates, employees, students and
                      parents to choose a institute or trainer best suited based
                      on skills offered, faculty, infrastructure, fees, etc.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/academy/template.svg").default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/academy/template.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
      </PageLayout>
    </div>
  );
};

export default admissions;
