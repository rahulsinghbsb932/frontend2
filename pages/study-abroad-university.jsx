import React, { useState } from "react";
import {
  Container,
  Row,
  Col,
  Card,
  Table,
  FormLabel,
  Form,
} from "react-bootstrap";
import Head from "next/head";
import Link from "next/link";
import Image from "next/image";
import PageLayout from "@components/PageLayout";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import ListItemText from "@mui/material/ListItemText";
import Select from "@mui/material/Select";
import Checkbox from "@mui/material/Checkbox";

const StudyabroadUniversity = () => {
  // multiple select in mui
  const ITEM_HEIGHT = 48;
  const ITEM_PADDING_TOP = 4;
  const MenuProps = {
    PaperProps: {
      style: {
        maxHeight: ITEM_HEIGHT * 3.5 + ITEM_PADDING_TOP,
        maxWidth: "100%",
        backgroundColor: "#B1B2FF",
        borderColor: "#B1B2FF",
        borderRadius: 1,
      },
    },
  };

  const names = [
    "Boston (6)",
    "Chicago (6)",
    "Hawaii (6)",
    "Los Angeles (6)",
    "Miami (5)",
    "New York (14)",
    "San Diego (1)",
    "San Francisco (9)",
    "Washington D.C. (5)",
  ];
  const courses = [
    "Masters Degree",
    "Bachelors Degree",
    "PG Diploma",
    "UG Diploma",
    "Short program",
    "PhD",
    "Diploma",
    "MBA",
    "Short Term",
  ];
  const fees = [
    "Arts",
    "Engineering",
    "Medicine",
    "Biology",
    "Accounting",
    "Psychology",
    "Finance",
    "Humanities",
    "Education",
  ];
  const tution = [
    "No Budget Constraints",
    "Upto 5 Lakh INR",
    "Upto 10 Lakh INR",
    "Upto 15 Lakh INR",
    "Upto 25 Lakh INR",
    "Upto 35 Lakh INR",
    "Upto 45 Lakh INR",
    "Upto 55 Lakh INR",
    "More than 55Lakh INR",
  ];
  const [personName, setPersonName] = React.useState([]);
  const [coursesName, setCoursesName] = React.useState([]);
  const [feesName, setFeesName] = React.useState([]);
  const [tutionName, setTutionName] = React.useState([]);

  const handleChange = (event) => {
    const {
      target: { value },
    } = event;
    setPersonName(
      // On autofill we get a stringified value.
      typeof value === "string" ? value.split(",") : value
    );
  };
  const coursesHandleChange = (event) => {
    const {
      target: { value },
    } = event;
    setCoursesName(
      // On autofill we get a stringified value.
      typeof value === "string" ? value.split(",") : value
    );
  };
  const feesHandleChange = (event) => {
    const {
      target: { value },
    } = event;
    setFeesName(
      // On autofill we get a stringified value.
      typeof value === "string" ? value.split(",") : value
    );
  };
  const tutionHandleChange = (event) => {
    const {
      target: { value },
    } = event;
    setTutionName(
      // On autofill we get a stringified value.
      typeof value === "string" ? value.split(",") : value
    );
  };
  // multiple select in mui
  const [items, setItems] = useState([
    {
      title: "UPSC Course - Regular",
      // img: require(`../../../assets/img/top-institution/small-icon.svg`)
      //   .default,
      para: "Central Delhi Classroom Programs",
      courses: "UPSC,PCS",
      classes: "6 days",
      coursetype: "Regular, Online, Test Series",
      duration: "1 years",
      batch: "01 May 2023",
      total: "1.6 Lakh",
    },
  ]);
  return (
    <div>
      <Head>
        <title>Study Abroad-University | Business Verification Services</title>
        <meta
          id="meta-description"
          name="description"
          content=" This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
        <meta
          property="og:title"
          content="Study Abroad-University | Business Verification Services"
        />
        <meta
          property="og:description"
          content="This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
        <meta
          property="twitter:title"
          content="Certified Academy | Academy Verification | VerifyKiya"
        />
        <meta
          property="twitter:description"
          content="This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
      </Head>
      <PageLayout>
        <div className="resources">
          <div className="HeroSectionWrapper">
            <div className="frame1 d-xl-block d-none"></div>
            <div className="frame2 d-xl-block d-none"></div>
            <Container fluid>
              <Row>
                <Col md={12}>
                  <div className="headingWhileLabel mb-5">
                    <div className="text-center">
                      <h1>Harvard University</h1>
                      <h4 className="mb-4">
                        Harvard University, Cambridge United States
                      </h4>
                    </div>
                    <p>
                      USA is a coveted destination among international students,
                      known for its ground-breaking research, innovation and
                      entrepreneurship opportunities. USA ranks 1st in the world
                      in attracting international students to study abroad after
                      Canada and UAE. Every year, more than 1.1 million
                      aspirants.
                    </p>
                  </div>
                </Col>
              </Row>
            </Container>
            <Container fluid>
              <Row>
                <Col lg={4}>
                  <Card>
                    <Image
                      src="/images/StydyAbroad/1.svg"
                      height={293}
                      width={263}
                    />
                    <Card.Body>
                      <Card.Title>
                        <strong>Year of Established</strong>
                      </Card.Title>
                      <Card.Text>
                        <span className="number">1636</span>
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </Col>
                <Col lg={4}>
                  <Card>
                    <Image
                      src="/images/StydyAbroad/2.svg"
                      height={293}
                      width={263}
                    />
                    <Card.Body>
                      <Card.Title>
                        <strong>Course Offered</strong>
                      </Card.Title>
                      <Card.Text>
                        <span className="number">165</span>
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </Col>
                <Col lg={4}>
                  <Card>
                    <Image
                      src="/images/StydyAbroad/3.svg"
                      height={293}
                      width={263}
                    />
                    <Card.Body>
                      <Card.Title>
                        <strong>Total students</strong>
                      </Card.Title>
                      <Card.Text>
                        <span className="number">21,877</span>
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </Col>
              </Row>
            </Container>
            <div className="frame3"></div>
            <div className="frame4"></div>
          </div>
          <div className="wrapper contact">
            <div className="contact-box pb-5">
              <Container fluid>
                <div className="bg p-5">
                  <div className="z-index">

                    <Row className="align-items-end">
                      <Col xl={2} lg={12}>
                        <div className="icons">
                          <img src="images/Immutable.png" className="img-fluid" />
                        </div>
                      </Col>
                      <Col xl={10} lg={12}>
                        <div className="end">
                          <div>
                            <h1>Master of Business Administration</h1>
                            <h4 className="text-black">
                              Harvard University, Cambridge United States
                            </h4>
                          </div>
                          <div></div>
                        </div>
                      </Col>
                    </Row>
                    <Row className="mt-3">
                      <Col xl={2} lg={12}>
                        <FormLabel className="text-white">
                          <strong>Search all locations</strong>
                        </FormLabel>
                        <FormControl
                          sx={{
                            m: 0,
                            width: "100%",
                            backgroundColor: "#B1B2FF",
                            borderRadius: 1,
                          }}
                          size="small"
                        >
                          {/* <InputLabel id="demo-multiple-checkbox-label">
                          Search all locations
                        </InputLabel> */}
                          <Select
                            labelId="demo-multiple-checkbox-label"
                            id="demo-multiple-checkbox"
                            multiple
                            value={personName}
                            onChange={handleChange}
                            input={<OutlinedInput />}
                            renderValue={(selected) => selected.join(", ")}
                            MenuProps={MenuProps}
                          >
                            {names.map((name) => (
                              <MenuItem key={name} value={name}>
                                <Checkbox
                                  checked={personName.indexOf(name) > -1}
                                />
                                <ListItemText primary={name} />
                              </MenuItem>
                            ))}
                          </Select>
                        </FormControl>
                      </Col>
                      <Col xl={3} lg={12}>
                        <FormLabel className="text-white">
                          <strong>Browse by Courses</strong>
                        </FormLabel>
                        <FormControl
                          sx={{
                            m: 0,
                            width: "100%",
                            backgroundColor: "#B1B2FF",
                            borderRadius: 1,
                          }}
                          size="small"
                        >
                          {/* <InputLabel id="demo-multiple-checkbox-label">
                          Browse by Courses
                        </InputLabel> */}
                          <Select
                            labelId="demo-multiple-checkbox-label"
                            id="demo-multiple-checkbox"
                            multiple
                            value={coursesName}
                            onChange={coursesHandleChange}
                            input={<OutlinedInput />}
                            renderValue={(selected) => selected.join(", ")}
                            MenuProps={MenuProps}
                          >
                            {courses.map((courses) => (
                              <MenuItem key={courses} value={courses}>
                                <Checkbox
                                  checked={coursesName.indexOf(courses) > -1}
                                />
                                <ListItemText primary={courses} />
                              </MenuItem>
                            ))}
                          </Select>
                        </FormControl>
                      </Col>
                      <Col xl={3} lg={12}>
                        <FormLabel className="text-white">
                          <strong>1st Years Fees</strong>
                        </FormLabel>
                        <FormControl
                          sx={{
                            m: 0,
                            width: "100%",
                            backgroundColor: "#B1B2FF",
                            borderRadius: 1,
                          }}
                          size="small"
                        >
                          {/* <InputLabel id="demo-multiple-checkbox-label">
                          1st Years Fees
                        </InputLabel> */}
                          <Select
                            labelId="demo-multiple-checkbox-label"
                            id="demo-multiple-checkbox"
                            multiple
                            value={feesName}
                            onChange={feesHandleChange}
                            input={<OutlinedInput />}
                            renderValue={(selected) => selected.join(", ")}
                            MenuProps={MenuProps}
                          >
                            {fees.map((fees) => (
                              <MenuItem key={fees} value={fees}>
                                <Checkbox checked={feesName.indexOf(fees) > -1} />
                                <ListItemText primary={fees} />
                              </MenuItem>
                            ))}
                          </Select>
                        </FormControl>
                      </Col>
                      <Col xl={3} lg={12}>
                        <FormLabel className="text-white">
                          <strong>Tuition Fees</strong>
                        </FormLabel>
                        <FormControl
                          sx={{
                            m: 0,
                            width: "100%",
                            backgroundColor: "#B1B2FF",
                            borderRadius: 1,
                          }}
                          size="small"
                        >
                          {/* <InputLabel id="demo-multiple-checkbox-label">
                          Tuition Fees
                        </InputLabel> */}
                          <Select
                            labelId="demo-multiple-checkbox-label"
                            id="demo-multiple-checkbox"
                            multiple
                            value={tutionName}
                            onChange={tutionHandleChange}
                            input={<OutlinedInput />}
                            renderValue={(selected) => selected.join(", ")}
                            MenuProps={MenuProps}
                          >
                            {tution.map((tution) => (
                              <MenuItem key={tution} value={tution}>
                                <Checkbox
                                  checked={tutionName.indexOf(tution) > -1}
                                />
                                <ListItemText primary={tution} />
                              </MenuItem>
                            ))}
                          </Select>
                        </FormControl>
                      </Col>
                      <Col
                        lg={1}
                        className="d-flex align-items-end justify-content-end-xl"
                      >
                        <div>
                          <button className="btn btn-success">Search</button>
                        </div>
                      </Col>
                    </Row>
                    <Row>
                      <Col lg={12}>
                        {items.map((item, index) => (
                          <div key={index}>
                            <div className="price my-5">
                              <div className="d-flex justify-content-between align-items-center header flex-wrap">
                                <div className="text d-flex flex-wrap align-items-center gap-2">
                                  <div className="ico">
                                    <img src={item.img} alt="img" />
                                  </div>
                                  <strong>{item.title}</strong>
                                </div>
                                <div className="btns">
                                  <Link href="/institutesDetails">
                                    <span className="primary">View details</span>
                                  </Link>
                                  <Link href="">
                                    <span className="apply">
                                      Check Eligibility
                                    </span>
                                  </Link>
                                </div>
                              </div>
                              <Table responsive borderless>
                                <tbody>
                                  <tr>
                                    <td>Courses</td>
                                    <td>Classes</td>
                                    <td>Course Type</td>
                                    <td>Duration</td>
                                    <td>Batches Dates</td>
                                    <td>Total Fees ₹</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      <strong>{item.courses}</strong>
                                    </td>
                                    <td>
                                      <strong>{item.classes}</strong>
                                    </td>
                                    <td>
                                      <strong>{item.coursetype}</strong>
                                    </td>
                                    <td>
                                      <strong>{item.duration}</strong>
                                    </td>
                                    <td>
                                      <strong>{item.batch}</strong>
                                    </td>
                                    <td>
                                      <strong>{item.total}</strong>
                                    </td>
                                  </tr>
                                </tbody>
                              </Table>
                            </div>
                          </div>
                        ))}
                      </Col>
                    </Row>
                  </div>
                </div>
              </Container>
            </div>
          </div>
        </div>
      </PageLayout>
    </div>
  );
};

export default StudyabroadUniversity;
