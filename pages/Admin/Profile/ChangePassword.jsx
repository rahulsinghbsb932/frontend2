import Image from "next/image";
import React from "react";
import Adminheader from "@components/includes/Adminheader";
import Sidebar from "@components/includes/Sidebar";
import { Row, Col } from "react-bootstrap";
import Form from "react-bootstrap/Form";
const Demographic = () => {
  return (
    <div className="admin-first">
      <div className="rel-abs">
        <Adminheader />
      </div>

      <div>
        <Sidebar />
      </div>
      <div className="main-admin">
        <div className="profile-m profile-register-form p-4">
          <Row>
            <Col lg={12}>
              <div className="profile-p">
                <div className="pro-flex ms-lg-5 ms-0 mb-3">
                  <h2 className="profile-head">Change Password</h2>
                  <p className="profile-desc">
                    You can control the their Profile and other settings in
                    relation to their account on the Platform
                  </p>
                </div>
              </div>
            </Col>
            <Col xxl={6} xl={12} lg={12}>
              <div className="profile-pics">
                <div className="profile-icon">
                  <img src="/images/profile-icon.svg" />
                </div>
                <div className="profile-photo">
                  <div className="photo">
                    <Image src='/images/image.png' width={498} height={600}/>
                    {/* <img src="/images/image.png" alt="" className="img-fluid" /> */}
                  </div>
                  <div className="edit">
                    <img src="/images/edit.png" alt="" />
                  </div>
                </div>
                <div className="frame">
                  <img src="/images/frame-p.png" />
                </div>
              </div>
            </Col>
            <Col xxl={6} xl={12} lg={12} className="position-relative">
              <Form>
                <Form.Group className="mb-3" controlId="formGroupEmail">
                  <Form.Label className="label-change">Old Password</Form.Label>
                  <Form.Control type="text" className="input-change" />
                </Form.Group>
                <Form.Group className="mb-3" controlId="formGroupPassword">
                  <Form.Label className="label-change">
                    Create new Password
                  </Form.Label>
                  <Form.Control type="text" className="input-change" />
                </Form.Group>
                <Form.Group className="mb-3" controlId="formGroupPassword">
                  <Form.Label className="label-change">
                    Conform new Password
                  </Form.Label>
                  <Form.Control type="text" className="input-change" />
                </Form.Group>
                <Form.Group className="mb-3" controlId="formGroupPassword">
                  <p>
                    Strong password required. Enter 8-256 characters. Do not
                    include common words or name. Combine uppercase letters,
                    lowercase letters, numbers and symbols.
                  </p>
                </Form.Group>
                <div>
                  <button className="btn btn-success">Submit</button>&nbsp;&nbsp;&nbsp;
                  <button className="btn btn-light">Cancel</button>
                </div>
              </Form>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  );
};

export default Demographic;
