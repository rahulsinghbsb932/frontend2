import React, { useEffect, useState } from "react";
import Adminheader from "@components/includes/Adminheader";
import Sidebar from "@components/includes/Sidebar";
// import { useState } from "react";
import { Steps, message, Icon } from "antd";
import Button from "react-bootstrap/Button";
import { CheckSquareOutlined } from "@ant-design/icons";
import { Container } from "react-bootstrap";
import PersonalInfo from "./PersonalInfo";
import Pro from "./Pro";
import ContactInfo from "./ContactInfo";
import SocialMedia from "./SocialMedia";
import Demographic from "./Demographic";
import { devInstance } from "plugins/axios";

const Profile = () => {
  const [current, setCurrent] = useState(0);
  const [selectedImage, setSelectedImage] = useState(null);
  const [fname, setfName] = useState("");
  const [lname, setlName] = useState("");
  const [dateOfBirth, setDateOfBirth] = useState("");
  const [fsName, setFsName] = useState("");
  const [msName, setMsName] = useState("");
  const [gender, setGender] = useState("");
  const [mNumber, setMNumber] = useState(null);

  const [mail, setMail] = useState("");
  const [addressLine1, setAddressLine1] = useState("");
  const [addressLine2, setAddressLine2] = useState("");
  const [pincode, setPincode] = useState("");

  const [country, setCountry] = useState("");
  const [state, setState] = useState("");


  const [nation, setNation] = useState("");
  const [relig, setReli] = useState("");
  const [langs, setLangs] = useState("");
  const [conts, setConts] = useState("");

  const [fb, setFb] = useState("");
  const [insta, setInsta] = useState("");
  const [tweet, setTweet] = useState("");
  const [yt, setYT] = useState("");
  const [link, setLink] = useState("");



  useEffect(() => {
    const uploadProfilePicture = async () => {
      if (current === 1 && selectedImage) {
        const formData = new FormData();
        formData.append("profilePic", selectedImage);
        const token = localStorage.getItem("token");
        try {
          const response = await devInstance.put("/api/v1/update/profilepic", formData, {
            headers: {
              Authorization: token,
              Cookie: "token=" + token,
            },
          });

          if (response.ok) {
            console.log("Profile picture uploaded successfully");
            // Handle success case
          } else {
            console.error("Failed to upload profile picture");
            // Handle error case
          }
        } catch (error) {
          console.error(
            "Error occurred while uploading profile picture:",
            error
          );
          // Handle error case
        }
      }
    };

    uploadProfilePicture();
  }, [current, selectedImage]);

  //for ant
  const Step = Steps.items;
  const steps = [
    {
      title: "Profile Pic",
      icon: <CheckSquareOutlined />,
      component: <Pro current={current} setSelectedImage={setSelectedImage} />,
    },
    {
      title: "Personal info",
      icon: <CheckSquareOutlined />,
      component: <PersonalInfo current={current} selectedImage={selectedImage} setfName={setfName} setFsName={setFsName} setlName={setlName} setMsName={setMsName} setGender={setGender} setDateOfBirth={setDateOfBirth} />,
    },
    {
      title: "Contact info",
      icon: <CheckSquareOutlined />,
      component: <ContactInfo selectedImage={selectedImage} setMNumber={setMNumber} setMail={setMail} setAddressLine1={setAddressLine1} setAddressLine2={setAddressLine2} setPincode={setPincode} setCountry={setCountry} setState={setState} />,
    },
    {
      title: "Social media link",
      icon: <CheckSquareOutlined />,
      component: <SocialMedia selectedImage={selectedImage} setFb={setFb} setInsta={setInsta} setTweet={setTweet} setYT={setYT} setLink={setLink} />,
    },
    {
      title: "Demographic info",
      icon: <CheckSquareOutlined />,
      component: <Demographic current={current} selectedImage={selectedImage} setNation={setNation} setReli={setReli} setLangs={setLangs} setConts={setConts} />,
    },
  ];
  const next = () => {
    current = current + 1;
    setCurrent(current);
    console.log("current", current);
  };
  const prev = () => {
    current = current - 1;
    setCurrent(current);
  };

  const handleProfileSubmit = async () => {
    const formData = new FormData();
    formData.append("profilePic", selectedImage);
    formData.append("firstName", fname);
    formData.append("lastName", lname);
    formData.append("dob", dateOfBirth);
    formData.append("fatherName", fsName);
    formData.append("motherName", msName);
    formData.append("gender", gender);
    formData.append("contactNumber", mNumber);
    formData.append("emailId", mail);
    formData.append("addressLine1", addressLine1);
    formData.append("addressLine2", addressLine2);
    formData.append("postCode", pincode);
    formData.append("country", country);
    formData.append("state", state);
    formData.append("Nationality", nation);
    formData.append("religion", relig);
    formData.append("language", langs);
    formData.append("continent", conts);
    formData.append("fbProfileLink", fb);
    formData.append("twitterProfileLink", tweet);
    formData.append("LinkdinprofileLink", link);
    formData.append("instagramProfileLink", insta);
    formData.append("youtubeProfileLink", yt);


    const token = localStorage.getItem("token");
    try {
      const response = await devInstance.put("/api/v1/update/profile", formData, {
        headers: {
          Authorization: token,
          Cookie: "token=" + token,
        },
      });

      if (response.ok) {
        console.log("Profile picture uploaded successfully");
        // Handle success case
      } else {
        console.error("Failed to upload profile picture");
        // Handle error case
      }
    } catch (error) {
      console.error(
        "Error occurred while uploading profile picture:",
        error
      );
      // Handle error case
    }
  }


  return (
    <>
      <div className="admin-first">
        <div className="rel-abs">
          <Adminheader />
        </div>
        <div>
          <Sidebar />
        </div>
        <div className="main-admin">
          <div className="profile-m profile-register-form">
            <Container >

              <Steps current={current}>
                {steps.map((item) => (
                  <Step key={item.title} title={item.title} icon={item.icon} />
                ))}
              </Steps>
              <div className="mt-3">{steps[current].component}</div>
              <div className="text-end">
                <div className="steps-action">
                  {current < steps.length - 1 && (
                    <Button type="Next" className="btn-success" onClick={() => next()}>
                      Next
                    </Button>
                  )}
                  {current === steps.length - 1 && (
                    <Button
                      type="Done"
                      className="btn-success"
                      onClick={handleProfileSubmit}
                    >
                      Done
                    </Button>
                  )}
                  {/* {current > 0 && (
                    <Button style={{ marginLeft: 8 }} className="btn-success" onClick={() => prev()}>
                      Previous
                    </Button>
                  )} */}
                </div>
              </div>
            </Container>
          </div>
        </div>
      </div>
    </>
  );
};

export default Profile;
