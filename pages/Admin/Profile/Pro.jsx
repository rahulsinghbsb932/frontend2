import React, { useState, useEffect } from "react";
import { Row, Col, Form } from "react-bootstrap";
import * as Yup from "yup";
import { Formik } from "formik";
import Image from "next/image";

const validationSchema = Yup.object().shape({
  file: Yup.mixed().required("Please select an image file"),
});

const Profile = ({ current, setSelectedImage }) => {
  const [selectedFile, setSelectedFile] = useState(null);
  const handleFileChange = (event) => {
    const file = event.currentTarget.files[0];
    if (file) {
      setSelectedFile(URL.createObjectURL(file));
      console.log("hcgvjbn",selectedFile)
      setSelectedImage(URL.createObjectURL(file));
      // handleImageSelection=selectedFile
    }
  };
  
 

  return (
    <div>
      <Row>
        <Col lg={12}>
          <div className="profile-p">
            <div className="pro-flex ms-lg-5 ms-0 mb-3">
              <h2 className="profile-head">Profile Picture</h2>
              <p className="profile-desc">
                A picture helps people recognize you and lets you know when
                you're signed in to your account
              </p>
            </div>
          </div>
        </Col>
        <Col xxl={6} xl={12} lg={12}>
          <div className="profile-pics">
            <div className="profile-icon">
              <Image src="/images/profile-icon.svg" width={125} height={125} />
            </div>
            <div className="profile-photo">
              <div className="photo">
                {selectedFile ? (
                  <img
                    src={selectedFile}
                    alt="Uploaded"
                    width={499}
                    height={656}
                  />
                ) : (
                  <Image src="/images/image.png" width={499} height={656} />
                )}
              </div>
              <div className="edit">
                <Image src="/images/edit.png" width={125} height={125} />
              </div>
            </div>
            <div className="frame">
              <Image src="/images/frame-p.png" width={300} height={145} />
            </div>
          </div>
        </Col>
        <Col xxl={6} xl={12} lg={12} className="position-relative">
          <Formik
            validationSchema={validationSchema}
            initialValues={{
              file: null,
              terms: false,
            }}
            onSubmit={() => {}}
          >
            {({ setFieldValue }) => (
              <Form noValidate>
                <Form.Group className="position-relative mb-3 input">
                  <label htmlFor="fileInput" style={{ cursor: "pointer" }}>
                    <Image src="/images/dekstop.svg" width={32} height={29} />
                    <span>Upload from computer</span>
                  </label>
                  <Form.Control
                    type="file"
                    id="fileInput"
                    required
                    name="file"
                    onChange={(event) => {
                      setFieldValue("file", event.currentTarget.files[0]);
                      handleFileChange(event);
                    }}
                    style={{ display: "none" }}
                  />
                </Form.Group>
                <Form.Group className="position-relative mb-3 input">
                  <label htmlFor="cameraInput">
                    <Image src="/images/camera.svg" width={32} height={26} />
                    <span>Take a picture</span>
                  </label>
                  <Form.Control
                    type="file"
                    id="cameraInput"
                    required
                    name="file"
                    onChange={(event) => {
                      setFieldValue("file", event.currentTarget.files[0]);
                      handleFileChange(event);
                    }}
                    style={{ display: "none" }}
                  />
                </Form.Group>
              </Form>
            )}
          </Formik>
        </Col>
      </Row>
    </div>
  );
};

export default Profile;