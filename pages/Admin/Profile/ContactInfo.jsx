import Image from "next/image";
import React, { useState, useEffect } from "react";
import { Row, Col } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import { Country, State } from 'country-state-city';

const country = [];
const codee = [];
let state = [];
const countryname = [];

const ContactInfo = ({ selectedImage, setMNumber, setMail, setAddressLine1, setAddressLine2, setPincode, setCountry, setState }) => {
  const [mobilenumber, setMobileNumber] = useState(null);
  const [email, setEmail] = useState("");
  const [address1, setAddress1] = useState("");
  const [address2, setAddress2] = useState("");
  const [postcode, setPostcode] = useState("");
  const [contactNumber, setContactNumber] = useState("");
  const [countryName, setCountryName] = useState("");
  const [stateName, setStateName] = useState("");
  const [countrycode, setCountryCode] = useState("");

  const handleMobileNumber = (e) => {
    setMobileNumber(e.target.value);
  };

  useEffect(() => {
    country = Country.getAllCountries();
    codee = country.map((item) => item.phonecode);
    countryname = country.filter((item) => item.phonecode === countrycode);
    setCountryName(countryname[0]?.name)
    setCountry(countryname[0]?.name);
    state = State.getAllStates();
    state = state.filter((item) => item.countryCode === countryname[0]?.isoCode);
    const contact = String(countrycode) + String(mobilenumber);
    setContactNumber(contact);
    setMNumber(contact)
  }, [countrycode, countryname, state, mobilenumber])

  const handleCountryName = (e) => {
    setCountryName(e.target.value);
    setCountry(e.target.value);
  }

  const handleStateName = (e) => {
    setStateName(e.target.value);
    setState(e.target.value);
  }

  const handleCountryCode = (e) => {
    setCountryCode(e.target.value);
  }

  const handleEmail = (e) => {
    setEmail(e.target.value);
    setMail(e.target.value)
  };

  const handleAddress1 = (e) => {
    setAddress1(e.target.value);
    setAddressLine1(e.target.value)
  };

  const handleAddress2 = (e) => {
    setAddress2(e.target.value);
    setAddressLine2(e.target.value)
  };

  const handlePostalCode = (e) => {
    setPostcode(e.target.value);
    setPincode(e.target.value);
  };

  return (
    <div className="admin-first">
      <Row>
        <Col lg={12}>
          <div className="profile-p">
            <div className="pro-flex ms-lg-5 ms-0 mb-3">
              <h2 className="profile-head">Contact Info</h2>
              <p className="profile-desc">
                You can control the their Profile and other settings in relation
                to their account on the Platform
              </p>
            </div>
          </div>
        </Col>
        <Col xxl={6} xl={12} lg={12}>
          <div className="profile-pics">
            <div className="profile-icon">
              <img
                src="/images/profile-icon.svg"
              />
            </div>
            <div className="profile-photo">
              <div className="photo">
                {selectedImage ? (
                  <img
                    src={selectedImage}
                    alt="Uploaded"
                    width={499}
                    height={656}
                  />
                ) : (
                  <Image src="/images/image.png" width={499} height={656} />
                )}
              </div>
              <div className="edit">
                <img src="/images/edit.png" alt="" />
              </div>
            </div>
            <div className="frame">
              <img src="/images/frame-p.png" alt="" />
            </div>
          </div>
        </Col>
        <Col xxl={6} xl={12} lg={12} className="position-relative">
          <Form>
            <Row className="mb-3">
              <Col sm={4}>
                <Form.Group as={Col} controlId="formGridState">
                  <Form.Label className="label-change">Country Code</Form.Label>
                  <Form.Select
                    defaultValue="Choose..."
                    className="input-change country-code"
                    value={countrycode}
                    onChange={handleCountryCode}
                  >
                    {codee.length > 0 ?
                      codee.map((item, index) => (
                        <option key={index}>
                          {item}</option>
                      )) : <option>...</option>
                    }
                  </Form.Select>
                </Form.Group>
              </Col>
              <Col sm={8}>
                <Form.Group as={Col} controlId="formGridZip">
                  <Form.Label className="label-change">
                    Mobile Number
                  </Form.Label>
                  <Form.Control
                    type="number"
                    className="input-change mobile-number"
                    placeholder=""
                    value={mobilenumber}
                    onChange={handleMobileNumber}
                    required
                  />
                </Form.Group>
              </Col>
            </Row>
            <Form.Group className="mb-3" controlId="formGroupEmail">
              <Form.Label className="label-change">Email ID</Form.Label>
              <Form.Control type="email" className="input-change" placeholder=""
                value={email}
                onChange={handleEmail} required />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formGroupPassword">
              <Form.Label className="label-change">Address Line 1</Form.Label>
              <Form.Control type="text" className="input-change" placeholder=""
                value={address1}
                onChange={handleAddress1} required />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formGroupPassword">
              <Form.Label className="label-change">Address Line 2</Form.Label>
              <Form.Control type="text" className="input-change" placeholder=""
                value={address2}
                onChange={handleAddress2} required />
            </Form.Group>
            <Row className="mb-3">
              <Form.Group as={Col} controlId="formGridCity">
                <Form.Label className="label-change">Postcode</Form.Label>
                <Form.Control type="text" className="input-change" placeholder=""
                  value={postcode}
                  onChange={handlePostalCode} required />
              </Form.Group>
              <Form.Group as={Col} controlId="formGridState">
                <Form.Label className="label-change">Country</Form.Label>
                <Form.Select defaultValue="Choose..." className="input-change" value={countryName} onChange={handleCountryName}>
                  {<option>{countryName}</option>
                  }

                </Form.Select>
              </Form.Group>
              <Form.Group as={Col} controlId="formGridState">
                <Form.Label className="label-change">State</Form.Label>
                <Form.Select defaultValue="Choose..." className="input-change" value={stateName} onChange={handleStateName}>
                  {state.length > 0 ?
                    state.map((item, index) => (
                      <option key={index}>{item.name}</option>
                    )) : <option>...</option>
                  }
                </Form.Select>
              </Form.Group>
            </Row>
          </Form>
        </Col>
      </Row>
    </div>
  );
};

export default ContactInfo;
