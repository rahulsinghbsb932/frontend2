import Image from "next/image";
import React, { useState, useEffect } from "react";
import { Row, Col } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import * as formik from "formik";
import * as yup from "yup";

const { Formik } = formik;

const schema = yup.object().shape({
  file: yup.mixed().required(),
});
const Demographic = ({current, selectedImage, setNation, setReli,setLangs, setConts}) => {
  const[nationality, setNationality] = useState("");
  const[religion, setReligion] = useState("");
  const[language, setLanguage] = useState("");
  const[continent, setContinent] = useState("");
  
  const handleNationality = (e) => {
    setNationality(e.target.value);
    console.log(e);
    setNation(e.target.value);
  };
  const handleReligion = (e) => {
    setReligion(e.target.value);
    setReli(e.target.value);
  };
  const handleLanguage = (e) => {
    setLanguage(e.target.value);
    setLangs(e.target.value);
  };
  const handleContinent = (e) => {
    setContinent(e.target.value);
    setConts(e.target.value);
  };

  return (
    <div className="admin-first">
      <Row>
        <Col lg={12}>
          <div className="profile-p">
            <div className="pro-flex ms-lg-5 ms-0 mb-3">
              <h2 className="profile-head">Demographic Link</h2>
              <p className="profile-desc">
                You can control the their Profile and other settings in relation
                to their account on the Platform
              </p>
            </div>
          </div>
        </Col>
        <Col xxl={6} xl={12} lg={12}>
          <div className="profile-pics">
            <div className="profile-icon">
              <img
                src="/images/profile-icon.svg"
              />
              {/* <img
                src={require("../../../assets/img/profile-icon.svg").default}
              /> */}
            </div>
            <div className="profile-photo">
            <div className="photo">
                {selectedImage ? (
                  <img
                    src={selectedImage}
                    alt="Uploaded"
                    width={499}
                    height={656}
                  />
                ) : (
                  <Image src="/images/image.png" width={499} height={656} />
                )}
              </div>
              <div className="edit">
                <img src="/images/edit.png" alt="" />
              </div>
            </div>
            <div className="frame">
              <img src="/images/frame-p.png" />
            </div>
          </div>
        </Col>
        <Col xxl={6} xl={12} lg={12} className="position-relative">
          <Form>
            <Form.Group className="mb-3" controlId="formGroupEmail">
              <Form.Label className="label-change">Nationality</Form.Label>
              <Form.Control
                type="text"
                className="input-change"
                placeholder=""
                value={nationality}
                onChange={handleNationality}
                required
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formGroupPassword">
              <Form.Label className="label-change">Religion</Form.Label>
              <Form.Control
                type="text"
                className="input-change"
                placeholder=""
                value={religion}
                onChange={handleReligion}
                required
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formGroupPassword">
              <Form.Label className="label-change">Language</Form.Label>
              <Form.Control
                type="text"
                className="input-change"
                placeholder=""
                value={language}
                onChange={handleLanguage}
                required
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formGroupPassword">
              <Form.Label className="label-change">Continent</Form.Label>
              <Form.Control
                type="text"
                className="input-change"
                placeholder=""
                value={continent}
                onChange={handleContinent}
                required
              />
            </Form.Group>
          </Form>
        </Col>
      </Row>
    </div>
  );
};

export default Demographic;
