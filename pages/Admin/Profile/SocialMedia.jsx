import Image from "next/image";
import React, {useState} from "react";
import { Container, Row, Col, Button } from "react-bootstrap";
import Form from "react-bootstrap/Form";

const SocialMedia = ({selectedImage, setFb, setInsta, setTweet, setYT, setLink}) => {
  const [facebook, setFacebook] = useState("");
  const [instagram, setInstagram] = useState("");
  const [linkedin, setLinkedIn] = useState("");
  const [twitter, setTwitter] = useState("");
  const [youtube, setYoutube] = useState("");

  const handleFacebookChange = (e) => {
    setFacebook(e.target.value);
    setFb(e.target.value);

  };
  const handleLinkedinChange = (e) => {
    setLinkedIn(e.target.value);
    setLink(e.target.value);
  };
  const handleInstagramChange = (e) => {
    setInstagram(e.target.value);
    setInsta(e.target.value);
  };
  const handleYoutubeChange = (e) => {
    setYoutube(e.target.value);
    setYT(e.target.value);
  };
  const handleTwitterChange = (e) => {
    setTwitter(e.target.value);
    setTweet(e.target.value);
  };


  return (
    <div className="admin-first">
      <Row>
        <Col lg={12}>
          <div className="profile-p">
            <div className="pro-flex ms-lg-5 ms-0 mb-3">
              <h2 className="profile-head">Social Media Link</h2>
              <p className="profile-desc">
                You can control the their Profile and other settings in relation
                to their account on the Platform
              </p>
            </div>
          </div>
        </Col>
        <Col xxl={6} xl={12} lg={12}>
          <div className="profile-pics">
            <div className="profile-icon">
              <img
                src="/images/profile-icon.svg"
              />
              {/* <img
                src={require("../../../assets/img/profile-icon.svg").default}
              /> */}
            </div>
            <div className="profile-photo">
            <div className="photo">
                {selectedImage ? (
                  <img
                    src={selectedImage}
                    alt="Uploaded"
                    width={499}
                    height={656}
                  />
                ) : (
                  <Image src="/images/image.png" width={499} height={656} />
                )}
              </div>
              <div className="edit">
                <img src="/images/edit.png" alt="" />
              </div>
            </div>
            <div className="frame">
              <img src="/images/frame-p.png" />
            </div>
          </div>
        </Col>
        <Col xxl={6} xl={12} lg={12} className="position-relative">
          <Form>
            <Form.Group className="mb-3" controlId="formGroupEmail">
              <Form.Label className="label-change">
                Facebook profile link
              </Form.Label>
              <Form.Control
                    aria-label="Small"
                    aria-describedby="inputGroup-sizing-sm"
                    placeholder=""
                    value={facebook}
                    required
                    onChange={handleFacebookChange}
                  />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formGroupPassword">
              <Form.Label className="label-change">
                Twitter profile link
              </Form.Label>
              <Form.Control
                    aria-label="Small"
                    aria-describedby="inputGroup-sizing-sm"
                    placeholder=""
                    value={twitter}
                    required
                    onChange={handleTwitterChange}
                  />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formGroupPassword">
              <Form.Label className="label-change">
                Linkedin profile link
              </Form.Label>
              <Form.Control
                    aria-label="Small"
                    aria-describedby="inputGroup-sizing-sm"
                    placeholder=""
                    value={linkedin}
                    required
                    onChange={handleLinkedinChange}
                  />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formGroupPassword">
              <Form.Label className="label-change">
                Instagram profile link
              </Form.Label>
              <Form.Control
                    aria-label="Small"
                    aria-describedby="inputGroup-sizing-sm"
                    placeholder=""
                    value={instagram}
                    required
                    onChange={handleInstagramChange}
                  />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formGroupPassword">
              <Form.Label className="label-change">
                Youtube profile link
              </Form.Label>
              <Form.Control
                    aria-label="Small"
                    aria-describedby="inputGroup-sizing-sm"
                    placeholder=""
                    value={youtube}
                    required
                    onChange={handleYoutubeChange}
                  />
            </Form.Group>

            <span className="Add-more">Add more</span>
          </Form>
        </Col>
      </Row>
    </div>
  );
};

export default SocialMedia;
