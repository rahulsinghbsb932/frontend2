import React, { useEffect } from "react";
import { Row, Col } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import * as formik from "formik";
import * as yup from "yup";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import ToggleButton from "react-bootstrap/ToggleButton";
import { useState } from "react";
import { DatePicker } from "antd";
import { Input } from "@mui/material";
import Image from "next/image";

const { Formik } = formik;

const schema = yup.object().shape({
  file: yup.mixed().required(),
});

const PersonalInfo = ({current, selectedImage, setfName, setFsName, setlName, setMsName, setGender, setDateOfBirth}) => {
  const [radioValue, setRadioValue] = useState("1");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [dob, setDob] = useState("");
  const [day, setDay] = useState(null);
  const [month, setMonth] = useState(null);
  const [year, setYear] = useState(null);
  const [fathersName, setFathersName] = useState("");
  const [mothersName, setMothersName] = useState("");


  const handleFirstNameChange = (e) => {
    setFirstName(e.target.value);
    setfName(e.target.value);
  };
  const handleLastNameChange = (e) => {
    setLastName(e.target.value);
    setlName(e.target.value)
  };
  
  useEffect( () => {
    const dob = String(day) + "-"+ String(month) + "-" + String(year) 
    setDob(dob);
    setDateOfBirth(dob);
    console.log("dob", dob);
  }, [day, month, year])

  const handlefathersNameChange = (e) => {
    setFathersName(e.target.value);
    setFsName(e.target.value);
  };
  const handleMothersNameChange = (e) => {
    setMothersName(e.target.value);
    setMsName(e.target.value);
  };
const handleRadioChange = (e) => {
  console.log(e.target.value);
  const radioName = radios.find((radio) => radio.value === e.target.value)?.name;
  console.log(radioName)
   setRadioValue(e.currentTarget.value)
   setGender(radioName);
}

  const radios = [
    { name: "Male", value: "1" },
    { name: "Female", value: "2" },
    { name: "Others", value: "3" },
  ];
  return (
    <div className="admin-first">
      <Row>
        <Col lg={12}>
          <div className="profile-p">
            <div className="pro-flex ms-lg-5 ms-0 mb-3">
              <h2 className="profile-head">Personal info</h2>
              <p className="profile-desc">
                You can control the their Profile and other settings in relation
                to their account on the Platform
              </p>
            </div>
          </div>
        </Col>
        <Col xxl={6} xl={12} lg={12}>
          <div className="profile-pics">
            <div className="profile-icon">
              <img
                src="/images/profile-icon.svg"
              />
              {/* <img
                src={require("../../../assets/img/profile-icon.svg").default}
              /> */}
            </div>
            <div className="profile-photo">
            <div className="photo">
                {selectedImage ? (
                  <img
                    src={selectedImage}
                    alt="Uploaded"
                    width={499}
                    height={656}
                  />
                ) : (
                  <Image src="/images/image.png" width={499} height={656} />
                )}
              </div>
              <div className="edit">
                <img src="/images/edit.png" alt="" />
              </div>
            </div>
            <div className="frame">
              <img src="/images/frame-p.png" />
            </div>
          </div>
        </Col>
        <Col xxl={6} xl={12} lg={12} className="position-relative">
          <Form>
            <Row>
              <Col>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                  <Form.Label>First Name</Form.Label>
                  <Form.Control
                    aria-label="Small"
                    aria-describedby="inputGroup-sizing-sm"
                    placeholder=""
                    value={firstName}
                    required
                    onChange={handleFirstNameChange}
                  />
                </Form.Group>
              </Col>
              <Col>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                  <Form.Label>Last Name</Form.Label>
                  <Form.Control
                    aria-label="Small"
                    aria-describedby="inputGroup-sizing-sm"
                    placeholder=""
                    value={lastName}
                    required
                    onChange={handleLastNameChange}
                  />
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                  <Form.Label>Date of Birth</Form.Label>
                  <div className="d-flex md-flex-wrap gap-3 w-50">
                  <Col>
                      <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Control type="number" placeholder="DD"  value={day} onChange={(e) => setDay(e.currentTarget.value)} />
                      </Form.Group>
                    </Col>
                    <Col>
                      <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Control type="number" placeholder="MM"  value={month} onChange={(e) => setMonth(e.currentTarget.value)}/>
                      </Form.Group>
                    </Col>
                    <Col>
                      <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Control type="number" placeholder="YYYY"  value={year} onChange={(e) => setYear(e.currentTarget.value)}/>
                      </Form.Group>
                    </Col>
                  </div>
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                  <Form.Label>Gender</Form.Label>
                  <br />
                  <ButtonGroup className="gap-3">
                    {radios.map((radio, idx) => (
                      <ToggleButton
                        key={idx}
                        defaultValue={'DEFAULT'}
                        id={`radio-${idx}`}
                        type="radio"
                        variant={
                          idx % 2 ? "outline-primary" : "outline-primary"
                        }
                        name="radio"
                        value={radio.value}
                        checked={radioValue === radio.value}
                        onChange={handleRadioChange}
                      >
                        {radio.name}
                      </ToggleButton>
                    ))}
                  </ButtonGroup>
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col></Col>
            </Row>
            <Row>
              <Col>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                  <Form.Label>Father Name</Form.Label>
                  <Form.Control
                    aria-label="Small"
                    aria-describedby="inputGroup-sizing-sm"
                    placeholder=""
                    value={fathersName}
                    onChange={handlefathersNameChange}
                  />
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                  <Form.Label>Mother Name</Form.Label>
                  <Form.Control
                    aria-label="Small"
                    aria-describedby="inputGroup-sizing-sm"
                    placeholder=""
                    value={mothersName}
                    onChange={handleMothersNameChange}
                  />
                </Form.Group>
              </Col>
            </Row>
          </Form>
        </Col>
      </Row>
    </div>
  );
};

export default PersonalInfo;
