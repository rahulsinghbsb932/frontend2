import React, { useState } from 'react';
import { createWorker } from 'tesseract.js';

const FormPage = () => {
  const [extractedText, setExtractedText] = useState('');

  const handleImageUpload = async (event) => {
    const file = event.target.files[0];

    const worker = await createWorker({
      logger: (m) => console.log(m),
    });
    await worker.load();
    await worker.loadLanguage('eng');
    await worker.initialize('eng');
    const { data } = await worker.recognize(file);
    console.log("data",data);
    setExtractedText(data.text);

    await worker.terminate();
  };

  return (
    <div>
      <input type="file" onChange={handleImageUpload} />
      <form>
        <input
          type="text"
          value={extractedText}
          onChange={(event) => setExtractedText(event.target.value)}
        />
        
      </form>
    </div>
  );
};

export default FormPage;
