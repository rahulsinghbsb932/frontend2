import React from "react";
import Adminheader from "@components/includes/Adminheader";
import Sidebar from "@components/includes/Sidebar";

const UserDashboard = () => {
  return (
    <>
      <div className="admin-first">
        <div className="rel-abs">
          <Adminheader/>
        </div>

        <div>
          <Sidebar />
        </div>
        <div className="main-admin">
          <div className="profile-m">
            
          </div>
        </div>
      </div>
    </>
  );
};

export default UserDashboard;
