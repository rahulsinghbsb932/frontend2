import React from "react";
import { Row, Col,Container } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import Image from "next/image";

const CoachingRegistration = () => {
    return (
      <>
        <Container>
          <Row className="school-registration">
            <Col lg={3}>
              <div className="p-image-upload">
                <div className="inner-circle">
                  <div className="upload-icon">
                    <label for="fileInput">
                      <Image
                        src="/images/AdmilElSchool/uploadicon.svg"
                        width={35}
                        height={35}
                      />
                    </label>
                    <Form.Control
                      type="file"
                      id="fileInput"
                      required
                      name="file"
                      size="lg"
                    />

                    <hr />
                    <p>
                      <strong>Profile Image</strong>
                      <br />
                      SVG, JPEG, PNG
                      <br />
                      BMP, GIF
                    </p>
                  </div>
                </div>
              </div>
            </Col>
            <Col lg={9} className="bg primary-subtle">
              <h1>Coaching Center Registration</h1>
              <Form className="basic-info-form">
                <Form.Group controlId="exampleForm.ControlInput1">
                  <div className="basic-info-form_icon">
                    <Image
                      src="/images/AdmilElSchool/universityicon.svg"
                      alt="university icon"
                      width={30}
                      height={30}
                    />
                  </div>
                  <Form.Control
                    className="bg-white"
                    type="text"
                    placeholder="Name of the Center"
                  />
                </Form.Group>

                <Form.Group controlId="exampleForm.ControlInput1">
                  <div className="basic-info-form_icon">
                    <Image
                      src="/images/AdmilElSchool/locationicon.svg"
                      alt="university icon"
                      width={30}
                      height={30}
                    />
                  </div>
                  <Form.Control
                    className="bg-white"
                    type="text"
                    placeholder="Use Current Location from Google Map"
                  />
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlTextarea1">
                  <div className="basic-info-form_icon">
                    <Image
                      src="/images/AdmilElSchool/textareaicon.svg"
                      alt="university icon"
                      width={30}
                      height={30}
                    />
                  </div>
                  <Form.Control
                    className="bg-white"
                    as="textarea"
                    placeholder="Brief About Your School ( Minimum 100 words *)"
                    style={{ height: "150px" }}
                  />
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlInput1">
                  <div className="basic-info-form_icon">
                    <Image
                      src="/images/AdmilElSchool/linkicon.svg"
                      alt="university icon"
                      width={30}
                      height={30}
                    />
                  </div>
                  <Form.Control
                    className="bg-white"
                    type="text"
                    placeholder="School Website Link"
                  />
                </Form.Group>
                <Row>
                  <Col lg={4} xs={12}>
                    <Form.Group>
                      <div className="basic-info-form_icon">
                        <Image
                          src="/images/AdmilElSchool/contacticon.svg"
                          alt="email icon"
                          width={30}
                          height={30}
                        />
                      </div>
                      <Form.Control
                        className="bg-white"
                        placeholder="Contact Person Name"
                      />
                    </Form.Group>
                  </Col>
                  <Col lg={4} xs={12}>
                    <Form.Group>
                      <div className="basic-info-form_icon">
                        <Image
                          src="/images/AdmilElSchool/emailicon.svg"
                          alt="email icon"
                          width={30}
                          height={30}
                        />
                      </div>
                      <Form.Control
                        type="email"
                        className="bg-white"
                        placeholder="email@domainname.com"
                      />
                    </Form.Group>
                  </Col>
                  <Col lg={4} xs={12}>
                    <Form.Group>
                      <div className="basic-info-form_icon">
                        <Image
                          src="/images/AdmilElSchool/mobileicon.svg"
                          alt="email icon"
                          width={30}
                          height={30}
                        />
                      </div>
                      <Form.Control
                        className="bg-white"
                        placeholder="+000 00000 00000"
                      />
                    </Form.Group>
                  </Col>
                </Row>
                <Row>
                  <Col lg={8} xs={12}>
                    <Form.Group>
                      <div className="basic-info-form_icon">
                        <Image
                          src="/images/AdmilElSchool/linkicon.svg"
                          alt="email icon"
                          width={30}
                          height={30}
                        />
                      </div>
                      <Form.Control
                        className="bg-white"
                        placeholder="Online Admission Form Link"
                      />
                    </Form.Group>
                  </Col>
                  <Col lg={4} xs={12}>
                    <Form.Group>
                    <div className="basic-info-form_icon">
                        <Image
                          src="/images/AdmilElSchool/upload.svg"
                          alt="email icon"
                          width={30}
                          height={30}
                        />
                      </div>
                      <Form.Control
                        className="bg-white"
                        type="file"
                        id="fileInput"
                        placeholder="Upload Form (PDF)"
                      />
                      {/* <label for="fileInput">
                        <Image
                          src="/images/AdmilElSchool/upload.svg"
                          width={25}
                          height={25}
                        />
                      </label>
                      <Form.Control
                        type="file"
                        id="fileInput"
                        required
                        placeholder="Upload Form (PDF)"
                        name="file"
                      /> */}
                    </Form.Group>
                  </Col>
                </Row>
                <Row>
                  <Col lg={4} xs={12}>
                    <Form.Group>
                      <div className="basic-info-form_icon">
                        <Image
                          src="/images/AdmilElSchool/establishmenticon.svg"
                          alt="email icon"
                          width={30}
                          height={30}
                        />
                      </div>
                      <Form.Control
                        className="bg-white"
                        placeholder="Year of Establishment"
                      />
                    </Form.Group>
                  </Col>
                  <Col lg={4} xs={12}>
                    <Form.Group>
                      <div className="basic-info-form_icon">
                        <Image
                          src="/images/AdmilElSchool/establishmenticon.svg"
                          alt="email icon"
                          width={30}
                          height={30}
                        />
                      </div>

                      <Form.Control
                        className="bg-white"
                        placeholder="Student Faculty Ratio"
                      />
                    </Form.Group>
                  </Col>
                  <Col lg={4} xs={12}>
                    <Form.Group>
                      <div className="basic-info-form_icon">
                        <Image
                          src="/images/AdmilElSchool/totalseats.svg"
                          alt="email icon"
                          width={30}
                          height={30}
                        />
                      </div>
                      <Form.Control
                        className="bg-white"
                        placeholder="Total Seats"
                      />
                    </Form.Group>
                  </Col>
                </Row>
              </Form>
            </Col>
          </Row>
        </Container>
      </>
    );
}

export default CoachingRegistration;