import React from "react";
import Sidebar from "@components/includes/Sidebar";
import Adminheader from "@components/includes/Adminheader";

import { useState } from "react";
import { Steps, message, Icon } from "antd";
import Button from "react-bootstrap/Button";
import { CheckSquareOutlined } from "@ant-design/icons";
import { Container } from "react-bootstrap";
import CoachingRegistration from "./CoachingRegistration";
import AdmissionNFee from "./AdmissionNFee";
import Gallery from "../AdminElSchool/Gallery"
import HallOfFame from "../AdminElSchool/HallOfFame"


const AdminEICoaching = () => {
  let [current, setCurrent] = useState(0);
  
   const Step = Steps.items;
  const steps = [
    {
      title: "Coaching Registration",
      icon: <CheckSquareOutlined />,
      component: <CoachingRegistration />,
    },
    {
      title: "Admission &Fee",
      icon: <CheckSquareOutlined />,
      component: <AdmissionNFee />,
    },
    
    {
      title: "Gallery",
      icon: <CheckSquareOutlined />,
      component: <Gallery />,
    },
    {
      title: "Hall Of Fame",
      icon: <CheckSquareOutlined />,
      component: <HallOfFame />,
    },
  ];
  const next = () => {
    current = current + 1;
    setCurrent(current);
  };
  const prev = () => {
    current = current - 1;
    setCurrent(current);
  };
  return (
    <React.Fragment>
      <div className="admin-first">
        <div className="rel-abs">
          <Adminheader />
        </div>

        <div>
          <Sidebar />
        </div>
        <div className="main-admin">
          <div className="registration registration-form">
          <Container >

            <Steps current={current}>
              {steps.map((item) => (
                <Step key={item.title} title={item.title} icon={item.icon} />
              ))} 
            </Steps>
            <div className="mt-3">{steps[current].component}</div>
            <div className="text-end">
              <div className="steps-action">
                {current < steps.length - 1 && (
                  <Button type="Next" className="btn-success" onClick={() => next()}>
                    Next
                  </Button>
                )}
                {current === steps.length - 1 && (
                  <Button
                    type="Done"
                    className="btn-success"
                    onClick={() => message.success("Processing complete!")}
                  >
                    Done
                  </Button>
                )}
                {current > 0 && (
                  <Button style={{ marginLeft: 8 }} className="btn-success" onClick={() => prev()}>
                    Previous
                  </Button>
                )}
              </div>
            </div>
          </Container>


          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default AdminEICoaching;
