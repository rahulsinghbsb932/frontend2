import React from "react";
import { Row, Col,Container, Button } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import * as formik from "formik";
import * as yup from "yup";
import Image from "next/image";
const Certificates = () => {
    return (
        <>
            <Container>
                <Row className="certificate">
                    <Col lg={3} >
                        <div className="p-image-upload">

                            <div className="inner-circle">
                                <div className="upload-icon">
                                <label for="fileInput">
                                    <Image
                                        src="/images/Registration/uploadicon.svg"
                                        width={35}
                                        height={35}
                                    />
                                </label>
                                <Form.Control type="file"
                                    id="fileInput"
                                    required
                                    name="file" size="lg" />
                                    {/* <Image
                                        src="/images/Registration/uploadicon.svg"
                                        alt="upload icon"
                                        width={35}
                                        height={35}
                                    /> */}
                                    <hr />
                                    <p><strong>Profile Image</strong><br />SVG, JPEG, PNG<br />BMP, GIF</p>
                                </div>
                                {/* <div className="upload-text">
                    <p><strong>Profile Image</strong><br/>SVG, JPEG, PNG<br/>BMP, GIF</p>
                  </div> */}
                            </div>
                        </div>
                    </Col>
                    <Col lg={9} className="bg primary-subtle">
                        <h1>Certificates</h1>
                        <Row>
                            <Col lg={10} xs={7} className="mt-3 mb-3">
                                <Form.Control size="lg"  placeholder="1. Certificate Title " />
                                
                            </Col>
                            <Col lg={1} className="icon-div">
                                <label for="fileInput">
                                    <Image
                                        src="/images/Registration/linkicon.svg"
                                        width={25}
                                        height={25}
                                    />
                                </label>
                                <Form.Control type="file"
                                    id="fileInput"
                                    required
                                    name="file" size="lg" />
                            </Col>
                            <Col lg={1} className="icon-div">
                                <label for="fileInput">
                                    <Image
                                        src="/images/Registration/upload.svg"
                                        width={25}
                                        height={25}
                                    />
                                </label>
                                <Form.Control type="file"
                                    id="fileInput"
                                    required
                                    name="file" size="lg" />
                            </Col>
                            <Col lg={10} xs={7} className="mt-3 mb-3">
                                <Form.Control size="lg"  placeholder="2. Certificate Title " />
                            </Col>
                            <Col lg={1} className="icon-div">
                                <label for="fileInput">
                                    <Image
                                        src="/images/Registration/linkicon.svg"
                                        width={25}
                                        height={25}
                                    />
                                </label>
                                <Form.Control type="file"
                                    id="fileInput"
                                    required
                                    name="file" size="lg" />
                            </Col>
                            <Col lg={1} className="icon-div">
                                <label for="fileInput">
                                    <Image
                                        src="/images/Registration/upload.svg"
                                        width={25}
                                        height={25}
                                    />
                                </label>
                                <Form.Control type="file"
                                    id="fileInput"
                                    required
                                    name="file" size="lg" />
                            </Col>
                            <Col lg={10} xs={7} className="mt-3 mb-3">
                                <Form.Control size="lg"  placeholder="3. Certificate Title " />
                            </Col>
                            <Col lg={1} className="icon-div">
                                <label for="fileInput">
                                    <Image
                                        src="/images/Registration/linkicon.svg"
                                        width={25}
                                        height={25}
                                    />
                                </label>
                                <Form.Control type="file"
                                    id="fileInput"
                                    required
                                    name="file" size="lg" />
                            </Col>
                            <Col lg={1} className="icon-div">
                                <label for="fileInput">
                                    <Image
                                        src="/images/Registration/upload.svg"
                                        width={25}
                                        height={25}
                                    />
                                </label>
                                <Form.Control type="file"
                                    id="fileInput"
                                    required
                                    name="file" size="lg" />
                            </Col>
                            <Col lg={10} xs={7} className="mt-3 mb-3">
                                <Form.Control size="lg"  placeholder="4. Certificate Title " />
                            </Col>
                            <Col lg={1} className="icon-div">
                                <label for="fileInput">
                                    <Image
                                        src="/images/Registration/linkicon.svg"
                                        width={25}
                                        height={25}
                                    />
                                </label>
                                <Form.Control type="file"
                                    id="fileInput"
                                    required
                                    name="file" size="lg" />
                            </Col>
                            <Col lg={1} className="icon-div">
                                <label for="fileInput">
                                    <Image
                                        src="/images/Registration/upload.svg"
                                        width={25}
                                        height={25}
                                    />
                                </label>
                                <Form.Control type="file"
                                    id="fileInput"
                                    required
                                    name="file" size="lg" />
                            </Col>
                            <Col lg={10} xs={7} className="mt-3 mb-3">
                                <Form.Control size="lg"  placeholder="5. Certificate Title " />
                            </Col>
                            <Col lg={1} className="icon-div">
                                <label for="fileInput">
                                    <Image
                                        src="/images/Registration/linkicon.svg"
                                        width={25}
                                        height={25}
                                    />
                                </label>
                                <Form.Control type="file"
                                    id="fileInput"
                                    required
                                    name="file" size="lg" />
                            </Col>
                            <Col lg={1} className="icon-div">
                                <label for="fileInput">
                                    <Image
                                        src="/images/Registration/upload.svg"
                                        width={25}
                                        height={25}
                                    />
                                </label>
                                <Form.Control type="file"
                                    id="fileInput"
                                    required
                                    name="file" size="lg" />
                            </Col>
                            
                            
                        </Row>
                <Button variant="light" size="lg" className="mb-3">+ Add more</Button>
                    </Col>
                </Row>
            </Container>
        </>
    );
}

export default Certificates;