import React from "react";
import Sidebar from "@components/includes/Sidebar";
import Adminheader from "@components/includes/Adminheader";

import { useState } from "react";
import { Steps, message, Icon } from "antd";
import Button from "react-bootstrap/Button";
import { CheckSquareOutlined } from "@ant-design/icons";
import { Container } from "react-bootstrap";
import Information from "./Information"
import ReportCards from "./ReportCards"
import Certificates from "./Certificates"
import Badges from "./Badges"
import RecommendationLetter from "./RecommendationLetter"
import Gallery from "./Gallery"
//import Adminheader from "../../layout/Adminheader";
//import Grid from "@mui/material/Unstable_Grid2"; // Grid version 2

const Registration = () => {
  let [current, setCurrent] = useState(0);
  
   const Step = Steps.items;
  const steps = [
    {
      title: "Information",
      icon: <CheckSquareOutlined />,
      component: <Information />,
    },
    {
      title: "Report Cards",
      icon: <CheckSquareOutlined />,
      component: <ReportCards />,
    },
    {
      title: "Certificates",
      icon: <CheckSquareOutlined />,
      component: <Certificates />,
    },
    {
      title: "Badges",
      icon: <CheckSquareOutlined />,
      component: <Badges />,
    },
    {
      title: "Recommendation Letter",
      icon: <CheckSquareOutlined />,
      component: <RecommendationLetter />,
    },
    {
      title: "Gallery",
      icon: <CheckSquareOutlined />,
      component: <Gallery />,
    },
  ];
  const next = () => {
    current = current + 1;
    setCurrent(current);
  };
  const prev = () => {
    current = current - 1;
    setCurrent(current);
  };
  return (
    <React.Fragment>
      <div className="admin-first">
        <div className="rel-abs">
          <Adminheader />
        </div>

        <div>
          <Sidebar />
        </div>
        <div className="main-admin">
          <div className="registration registration-form">
          <Container fluid>

            <Steps current={current}>
              {steps.map((item) => (
                <Step key={item.title} title={item.title} icon={item.icon} />
              ))} 
            </Steps>
            <div className="mt-3">{steps[current].component}</div>
            <div className="text-end">
              <div className="steps-action">
                {current < steps.length - 1 && (
                  <Button type="Next" className="btn-success" onClick={() => next()}>
                    Next
                  </Button>
                )}
                {current === steps.length - 1 && (
                  <Button
                    type="Done"
                    className="btn-success"
                    onClick={() => message.success("Processing complete!")}
                  >
                    Done
                  </Button>
                )}
                {current > 0 && (
                  <Button style={{ marginLeft: 8 }} className="btn-success" onClick={() => prev()}>
                    Previous
                  </Button>
                )}
              </div>
            </div>
          </Container>


          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Registration;
