import React from "react";
import { Row, Col,Container } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import Table from "react-bootstrap/Table";
import Button from 'react-bootstrap/Button';
import { AiTwotoneCalendar } from "react-icons/bs";
import * as formik from "formik";
import * as yup from "yup";
import Image from "next/image";
const ReportCards = () => {
    return (
        <>
            <Container>
        <Row className="reportcard">
          <Col lg={3} >
            <div className="p-image-upload">
                
                <div className="inner-circle">
                  <div className="upload-icon">
                  <label for="fileInput">
                                    <Image
                                        src="/images/Registration/uploadicon.svg"
                                        width={35}
                                        height={35}
                                    />
                                </label>
                                <Form.Control type="file"
                                    id="fileInput"
                                    required
                                    name="file" size="lg" />
                    {/* <Image
                    src="/images/Registration/uploadicon.svg"
                    alt="upload icon"
                    width={35}
                    height={35}
                    /> */}
                    <hr/>
                    <p><strong>Profile Image</strong><br/>SVG, JPEG, PNG<br/>BMP, GIF</p>
                  </div>
                  {/* <div className="upload-text">
                    <p><strong>Profile Image</strong><br/>SVG, JPEG, PNG<br/>BMP, GIF</p>
                  </div> */}
                </div>
            </div>
          </Col>
          <Col lg={9} className="bg primary-subtle">
            <h1>Report Card</h1>
            {/* <div className="px-5 my-5"> */}
            <Table responsive className="report-card-table">
                <thead>
                <tr className="bg-white">
                                    <th >
                                    <strong>Subjects</strong>
                                    </th>
                                    <th >
                                    <strong>1st Sem.</strong>
                                    </th>
                                    <th >
                                    <strong>2nd Sem.</strong>
                                    </th>
                                    <th >
                                    <strong>Combined</strong>
                                    </th>
                                    <th >
                                    <strong>Grade</strong>
                                    </th>
                                </tr>
                </thead>
                                <tbody >
                             
                                <tr> 
                                    <td>1st Subjects</td>
                                    <td>00</td>
                                    <td>00</td>
                                    <td>00</td>
                                    <td>A</td>
                                </tr>
                                <tr>
                                <td>2nd Subjects</td>
                                    <td>00</td>
                                    <td>00</td>
                                    <td>00</td>
                                    <td>A</td>
                                </tr>
                                <tr>
                                <td>3rd Subjects</td>
                                    <td>00</td>
                                    <td>00</td>
                                    <td>00</td>
                                    <td>A</td>
                                </tr>
                                <tr>
                                <td>4th Subjects</td>
                                    <td>00</td>
                                    <td>00</td>
                                    <td>00</td>
                                    <td>A</td>
                                </tr>
                                <tr>
                                <td>5th Subjects</td>
                                    <td>00</td>
                                    <td>00</td>
                                    <td>00</td>
                                    <td>A</td>
                                </tr>
                                <tr>
                                <td>6th Subjects</td>
                                    <td>00</td>
                                    <td>00</td>
                                    <td>00</td>
                                    <td>A</td>
                                </tr>
                                <tr>
                                <td>+ Add Subjects</td>
                                    <td>00</td>
                                    <td>00</td>
                                    <td>00</td>
                                    <td>A</td>
                                </tr>
                                
                                </tbody>
            </Table>   
                  <div className="badge-div">
                    <div className="badge-1">
                        <Image
                    src="/images/Registration/batchicon1.svg"
                    width={25}
                    height={25}
                    />
                    <span>Obtained %</span>
                    </div>
                    <div className="badge-2">
                        <Image
                    src="/images/Registration/batchicon2.svg"
                    width={25}
                    height={25}
                    />
                    <span>Total School Days</span>
                    </div>
                    <div className="badge-3">
                        <Image
                    src="/images/Registration/batchicon3.svg"
                    width={25}
                    height={25}
                    />
                    <span>Total Present</span>
                    </div>
                    <div className="badge-4">
                        <Image
                    src="/images/Registration/batchicon3.svg"
                    width={25}
                    height={25}
                    />
                    <span>Total Absent</span>
                    </div>
                  </div>
                  
          </Col>
        </Row>
      </Container>
        </>
    );
}

export default ReportCards;