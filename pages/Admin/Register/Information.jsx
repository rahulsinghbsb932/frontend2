import React from "react";
import { Row, Col,Container } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import * as formik from "formik";
import * as yup from "yup";
import Image from "next/image";


const Information = () => {
  return (
    <>
      <Container>
        <Row className="information">
          <Col lg={3} >
            <div className="p-image-upload">
                
                <div className="inner-circle">
                  <div className="upload-icon">
                  <label for="fileInput">
                                    <Image
                                        src="/images/Registration/uploadicon.svg"
                                        width={35}
                                        height={35}
                                    />
                                </label>
                                <Form.Control type="file"
                                    id="fileInput"
                                    required
                                    name="file" size="lg" />
                   
                    <hr/>
                    <p><strong>Profile Image</strong><br/>SVG, JPEG, PNG<br/>BMP, GIF</p>
                  </div>
                </div>
            </div>
          </Col>
          <Col lg={9} className="bg primary-subtle">
            <h1>Basic Information</h1>
            <Form className="basic-info-form">
              <Form.Group  controlId="exampleForm.ControlInput1">
                <div className="basic-info-form_icon">
                <Image
                    src="/images/Registration/universityicon.svg"
                    alt="university icon"
                    width={30}
                    height={30}
                    />
                </div>
                <Form.Control className="bg-white" type="text" placeholder="Name of The School" />
              </Form.Group>
              <Form.Group  controlId="exampleForm.ControlInput1">
                 <div className="basic-info-form_icon">
                <Image
                    src="/images/Registration/admissionicon.svg"
                    alt="university icon"
                    width={30}
                    height={30}
                    />
                </div>
                <Form.Control className="bg-white" type="text" placeholder="Admission No." />
              </Form.Group>
              <Form.Group  controlId="exampleForm.ControlInput1">
                 <div className="basic-info-form_icon">
                <Image
                    src="/images/Registration/locationicon.svg"
                    alt="university icon"
                    width={30}
                    height={30}
                    />
                </div>
                <Form.Control className="bg-white" type="text" placeholder="Use Current Location from Google Map" />
              </Form.Group>
              <Form.Group  controlId="exampleForm.ControlInput1">
              <div className="basic-info-form_icon">
                <Image
                    src="/images/Registration/linkicon.svg"
                    alt="university icon"
                    width={30}
                    height={30}
                    />
                </div>
                <Form.Control className="bg-white" type="text" placeholder="School Website Link" />
              </Form.Group>
              <Form.Group  controlId="exampleForm.ControlInput1">
              <div className="basic-info-form_icon">
                <Image
                    src="/images/Registration/contacticon.svg"
                    alt="university icon"
                    width={30}
                    height={30}
                    />
                </div>
                <Form.Control className="bg-white" type="text" placeholder="Contact Person Name" />
              </Form.Group>
              {/*  */}
              <Row className="mb-3 email-mobile">
                <Form.Group as={Col} controlId="formGridEmail">
                  <div className="basic-info-form_icon">
                    <Image
                      src="/images/Registration/emailicon.svg"
                      alt="email icon"
                      width={30}
                      height={30}
                    />
                  </div>
                  <Form.Control className="bg-white" type="email" placeholder="email@domainname.com" />
                </Form.Group>

                <Form.Group as={Col} controlId="formGridPassword">
                  <div className="basic-info-form_icon">
                    <Image
                      src="/images/Registration/mobileicon.svg"
                      alt="mobile icon"
                      width={30}
                      height={30}
                    />
                  </div>
                  <Form.Control className="bg-white w-50 mobile" type="text" placeholder="+000 | 00000 00000" />
                </Form.Group>
              </Row>
              
              
             
              {/* <Form.Group  controlId="exampleForm.ControlInput1">
                <Form.Control type="email" placeholder="Contact Person Name" />
              </Form.Group> */}
            </Form>
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default Information;