import React from "react";
import { Row, Col, Container } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import * as formik from "formik";
import * as yup from "yup";
import Image from "next/image";
const AdminProfile1 = () => {
  
  return (
    <>
      <Container className="admin-profile-container">
        <Row>
          <Col lg={12} className="m-3">
            <div className="icon-text-div">
              <div className="icn-sec">
                <Image
                src="/images/Wallet/icnSec.svg"
                width={135}
                height={135}
                />
              </div>
              <h3 className="main-heading">Profiles</h3>
            </div>
            
          </Col>
        </Row>
        <Row className="admin-profile1">
          <Row className="container1 w-75">
            <Col lg={2}>
            <div className="img-container">
                <Image
                src="/images/AdminProfile/academic.svg"
                width={101}
                height={101}/>
            </div>
            </Col>
            <Col lg={10}>
            <div className="text-container">
              <h3>Academic Institutions</h3>
              <p>Academic learning is a cornerstone of higher education. 
                The US has some the best and brightest faculty teaching 
                college students across the country. These learning experiences 
                are one of many reasons why learning management systems have 
                become so valuable to institutions.</p>
            </div>
            </Col>
          </Row>
          <Row className="container1 w-75">
            <Col lg={2}>
            <div className="img-container">
            <Image
                src="/images/AdminProfile/nonacademic.svg"
                width={101}
                height={101}/>
            </div>
            </Col>
            <Col lg={10}>
            <div className="text-container">
              <h3>Non-Academic Institutions</h3>
              <p>Non-academic experiences like being involved in 
                extracurricular activities and holding leadership 
                positions had a bigger influence than both the 
                students GPA and school attended.</p>
            </div>
            </Col>
          </Row>
         
          <Row></Row>
         
        </Row>
      </Container>


    </>
  );
}

export default AdminProfile1;