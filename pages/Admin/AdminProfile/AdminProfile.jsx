import React from "react";
//import "./wallet.scss";
import Adminheader from "@components/includes/Adminheader";
import Sidebar from "@components/includes/Sidebar";
import { Container, Row, Col } from "react-bootstrap";
import AdminProfile1 from "./AdminProfile1";
import NonAcademic from "./NonAcademic";
import AdminProfile2 from "./AdminProfile2";
// import List from "./List";


const AdminProfile = () => {
  return (
    <>
      <div className="admin-first">
        <div className="rel-abs">
          <Adminheader />
        </div>
        <div>
          <Sidebar />
        </div>
        <div className="main-admin">
          <div className="profile-m">
            <AdminProfile1/>
            <NonAcademic/>
             <AdminProfile2/>
          </div>
        </div>
      </div>
    </>
  );
};

export default AdminProfile;
