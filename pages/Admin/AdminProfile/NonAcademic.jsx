import React from "react";
import { Row, Col, Container } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import * as formik from "formik";
import * as yup from "yup";
import Image from "next/image";
const NonAcademic = () => {
  const data=[[
    {src:"/images/AdminProfile/coachingcenter.svg",name:"Coaching Center"},
    {src:"/images/AdminProfile/tutioncenter.svg",name:"Tuition Center"},
    {src:"/images/AdminProfile/hometutor.svg",name:"Home Tutor"},
    {src:"/images/AdminProfile/training.svg",name:"Training Institutes"},
    {src:"/images/AdminProfile/event.svg",name:"Event"},
    {src:"/images/AdminProfile/ngo.svg",name:"NGO"}
  ]
  ]
  
  
  return (
    <>
      <Container className="non-academic-container">
        <Row>
          <Col lg={12} className="m-3">
            <div className="icon-text-div">
              <div className="icn-sec">
                <Image
                src="/images/AdminProfile/icnSec.svg"
                width={130}
                height={130}
                alt="Logo"
                />
              </div>
              <h3 className="main-heading">Non-Academic Institutions</h3>
            </div>
            
          </Col>
        </Row>
        <Row className="main-container">
          <Col lg={12} className="d-flex flex-column">
            <div className="text-container">

                <h3>Categories of documents</h3>
                <p className="para">A picture helps people recognize you and lets you know when you’re signed in to your account.</p>
            </div>

                <div className="card-container">
                  {
                    data[0].map((item,index)=>(
                      <div key={index} className="card-div">
                      <div className="card-img">
  
                        <Image src={item.src} width={110} height={110} class="card-img-top" alt="..." />
                      </div>
                      <p>{item.name}</p>
                    </div>
                    ))
                  }
                </div>
              
          </Col>
        </Row>
      
      </Container>


    </>
  );
}

export default NonAcademic;