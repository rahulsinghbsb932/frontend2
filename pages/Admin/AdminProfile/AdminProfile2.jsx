import React,{useState} from "react";
import { Row, Col, Container } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import Dropdown from 'react-bootstrap/Dropdown';
import DropdownButton from 'react-bootstrap/DropdownButton';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import * as formik from "formik";
import * as yup from "yup";
import Image from "next/image";
const AdminProfile2 = () => {
    const [clicked,setClicked]=useState(true);
    const clickHandler=()=>{
        setClicked(!clicked);
    }
  return (
    <>
      <Container className="profile2-container">
        <Row>
          <Col lg={12} className="m-3">
            <div className="icon-text-div">
              <div className="icn-sec">
                <Image
                  src="/images/AdminProfile/icnSec.svg"
                  width={135}
                  height={135}
                  alt="Logo"
                />
              </div>
              <h3 className="main-heading">Title</h3>
            </div>
          </Col>
        </Row>
        <Row className="wallet1 profile2">
          <Col lg={12} className="my-3">
            <div className="text-container">
              <h3>Categories of ...</h3>
              <p className="para">
                An educational institution is a place where people of different
                ages gain an education, including preschools, childcare,
                primary-elementary schools, secondary-high schools, and
                universities. They provide a large variety of learning
                environments and learning spaces.
              </p>
            </div>
            <div className="button-container">
              <div className="add-icon">
                <button onClick={clickHandler}>
                  <span>+</span>
                </button>
              </div>
              {!clicked && (
                <div className="checkboxes">
                  <label for="one">
                    <input type="checkbox" id="one" />
                    First checkbox
                  </label>
                  <label for="two">
                    <input type="checkbox" id="two" />
                    Second checkbox{" "}
                  </label>
                  <label for="three">
                    <input type="checkbox" id="three" />
                    Third checkbox
                  </label>
                  <label for="four">
                    <input type="checkbox" id="four" />
                    Fourth checkbox
                  </label>
                  <label for="five">
                    <input type="checkbox" id="five" />
                    Fifth checkbox{" "}
                  </label>
                  <label for="six">
                    <input type="checkbox" id="six" />
                    Sixth checkbox
                  </label>
                  <label for="seven">
                    <input type="checkbox" id="seven" />
                    Seventh checkbox
                  </label>
                  <label for="eight">
                    <input type="checkbox" id="eight" />
                    Eight checkbox{" "}
                  </label>
                  <label for="nine">
                    <input type="checkbox" id="nine" />
                    Nineth checkbox
                  </label>
                </div>
              )}
            </div>
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default AdminProfile2;