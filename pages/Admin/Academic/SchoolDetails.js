
// import JuniorSchool from "../../../assets/img/academys/JuniorSchool.svg";
// import MiddleSchool from "../../../assets/img/academys/MiddleSchool.svg";
// import High from "../../../assets/img/academys/HighSchool.svg";
// import Graduation from "../../../assets/img/academys/Graduation.svg";
// import PostGraduation from "../../../assets/img/academys/PostGraduation.svg";
// import Skills from "../../../assets/img/academys/SkillsBuilding.svg";
// import Extra from "../../../assets/img/academys/ExtraCurricular.svg";



export const SchoolDetails = [
    {
        "schoolName": "Junior School",
        "schoolImage": JuniorSchool
    },
    {
        "schoolName": "Middle School",
        "schoolImage": MiddleSchool
    },
    {
        "schoolName": "High School",
        "schoolImage": High
    },
    {
        "schoolName": "Graduation",
        "schoolImage": Graduation
    },
    {
        "schoolName": "Post Graduation",
        "schoolImage": PostGraduation
    },
    {
        "schoolName": "Skills Building",
        "schoolImage": Skills
    },
    {
        "schoolName": "Extra curricular",
        "schoolImage": Extra
    }
]