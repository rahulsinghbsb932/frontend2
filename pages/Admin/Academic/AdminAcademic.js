import React from "react";
import { Row, Col, Container } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import * as formik from "formik";
import * as yup from "yup";
import Image from "next/image";
const AdminAcademic = () => {
  const academicCardData=[[
    {src:"/images/Wallet/playschool.svg",name:"Play School"},
    {src:"/images/Wallet/junior.svg",name:"Junior School"},
    {src:"/images/Wallet/middle.svg",name:"Middle School"},
    {src:"/images/Wallet/highschool.svg",name:"High School"},
    {src:"/images/Wallet/graduation.svg",name:"Graduation"},
    {src:"/images/Wallet/postgrad.svg",name:"Post Graduation"}
   
  ],
  [
    {src:"/images/Wallet/marksheet5.svg",name:"MarkSheet"},
    {src:"/images/Wallet/marksheet5.svg",name:"MarkSheet"},
    {src:"/images/Wallet/marksheet5.svg",name:"MarkSheet"},
    {src:"/images/Wallet/marksheet5.svg",name:"MarkSheet"},
    {src:"/images/Wallet/marksheet5.svg",name:"MarkSheet"},
    {src:"/images/Wallet/marksheet5.svg",name:"MarkSheet"},
    {src:"/images/Wallet/marksheet5.svg",name:"MarkSheet"},
    
  ]
    
    
  ]
  
  
  return (
    <>
      <Container className="wallet-container">
        <Row>
          <Col lg={12} className="m-3">
            <div className="icon-text-div">
              <div className="icn-sec">
                <Image
                src="/images/Wallet/icnSec.svg"
                width={135}
                height={135}
                />
              </div>
              <h3 className="main-heading">Upload your academic data or documents</h3>
            </div>
            
          </Col>
        </Row>
        <Row className="wallet1">
          <Col lg={12} className="d-flex flex-column">
            <div className="text-container">

                <h3>Categories</h3>
                <p className="para">A picture helps people recognize you and lets you know when you’re signed in to your account</p>
            </div>

                <div className="card-container">
                  {
                    academicCardData[0].map((item,index)=>(
                      <div key={index} className="card-div">
                      <div className="card-img">
  
                        <Image src={item.src} width={110} height={110} class="card-img-top" alt="..." />
                      </div>
                      <p>{item.name}</p>
                    </div>
                    ))
                  }
                </div>
              
          </Col>
        </Row>
            <h2 className="sub-heading">Recently uploaded documents</h2>
        <Row className="wallet1">
          
              <Col lg={12} className="d-flex flex-column">
                <div className="second-section-flex" style={{marginLeft:"10px"}}>
                  {
                    academicCardData[1].map((item,index)=>(
                      <div key={index} className="rel-sec">
                        <Image 
                        src={item.src} 
                        alt=""
                        width={163} 
                        height={133}
                        className="doc-sec" />
                        <span className="abst">{item.name}</span>
                      </div>
                    ))
                  }
                </div>
              </Col>
        </Row>
      </Container>


    </>
  );
}

export default AdminAcademic;