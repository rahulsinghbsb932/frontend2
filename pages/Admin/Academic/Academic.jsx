import React from "react";
//import "./wallet.scss";
import Adminheader from "@components/includes/Adminheader";
import Sidebar from "@components/includes/Sidebar";
import { Container, Row, Col } from "react-bootstrap";
import AdminAcademic from "./AdminAcademic";
import AdminAcademicUpload from "./AdminAcademicUpload";




const Academic = () => {
 
  return (
    <>
      <div className="admin-first">
        <div className="rel-abs">
          <Adminheader />
        </div>
        <div>
          <Sidebar />
        </div>
        <div className="main-admin">
          <div className="profile-m">
            <AdminAcademic/>
            <AdminAcademicUpload/>
          </div>
        </div>
      </div>
    </>
  );
};

export default Academic;
