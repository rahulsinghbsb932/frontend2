import React,{useState} from "react";
import { Row, Col, Container,Button} from "react-bootstrap";
import Form from "react-bootstrap/Form";
import * as formik from "formik";
import * as yup from "yup";
import Image from "next/image";
const AdminAcademicUpload = () => {
    const [validated, setValidated] = useState(false);

  const handleSubmit = (event) => {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }

    setValidated(true);
  };
  return (
    <>
      <Container className="wallet-container">
        <Row>
          <Col lg={12} className="m-3">
            <div className="icon-text-div">
              <div className="icn-sec">
                <Image
                  src="/images/Wallet/icnSec.svg"
                  width={135}
                  height={135}
                />
              </div>
              <h3 className="main-heading">
                Upload your academic data or documents
              </h3>
            </div>
          </Col>
        </Row>
        <Row className="wallet1 bg-white">
          <Col lg={12} className="d-flex flex-column">
            <Row>
              <Col lg={8} className="mx-5 my-3">
                <div>
                <Form noValidate validated={validated} onSubmit={handleSubmit}>
                  <Form.Group as={Col} lg={8}  className="mb-3"  controlId="formGridAddress1">
                    <Form.Label  className="title-form">
                      University Name
                    </Form.Label>
                    <Form.Control
                    size="lg" 
                      placeholder="University Name"
                      type="text"
                      required
                      className="bg-col"
                    />
                    <Form.Control.Feedback type="invalid">
                      Please provide a University Name.
                    </Form.Control.Feedback>
                  </Form.Group>

                  <Form.Group as={Col} lg={8} className="mb-3" controlId="formGridAddress2">
                    <Form.Label className="title-form">Course</Form.Label>
                    <Form.Control
                        size="lg"
                      placeholder="Course"
                      type="text"
                      required
                      className="bg-col"
                    />
                    <Form.Control.Feedback type="invalid">
                      Please provide a Course Name.
                    </Form.Control.Feedback>
                  </Form.Group>
                  <Row className="mb-3">
                    <p>Session (Course Time Duration)<span>{" "}Years</span></p>
                    <Form.Group as={Col} lg={2} controlId="formGridCity">
                    {/* <Form.Label className="title-form"></Form.Label> */}
                      <Form.Control 
                      size="lg"
                      type="date"
                      name="date"
                      placeholder="Date"
                      required
                      className="bg-col"/>
                    </Form.Group>

                    <Form.Group as={Col} lg={2} controlId="formGridState">
                    {/* <Form.Label className="title-form"></Form.Label> */}
                      <Form.Control 
                            size="lg"
                          type="date"
                          name="date"
                          placeholder="Date"
                          required
                          className="bg-col"
                        />
                    </Form.Group>

                    <Form.Group as={Col} lg={2} controlId="formGridZip">
                    {/* <Form.Label className="title-form"></Form.Label> */}
                      <Form.Control 
                      size="lg"
                        type="date"
                        name="date"
                        placeholder="Date"
                        required
                        className="bg-col"
                      />
                    </Form.Group>
                  </Row>
                  <Row className="mb-3">
                    <Form.Group as={Col} lg={4} controlId="formGridEmail">
                      <Form.Label className="title-form">
                        Enrollment No.
                      </Form.Label>
                      <Form.Control
                      size="lg"
                        type="text"
                        placeholder="Enrollment No."
                        required
                        className="bg-col"
                      />
                      <Form.Control.Feedback type="invalid">
                        Please provide a Enrollment No.
                      </Form.Control.Feedback>
                    </Form.Group>

                    <Form.Group as={Col} lg={4} controlId="formGridPassword">
                      <Form.Label className="title-form">Roll no.</Form.Label>
                      <Form.Control
                      size="lg"
                        type="text"
                        placeholder="Roll no"
                        required
                        className="bg-col"
                      />
                      <Form.Control.Feedback type="invalid">
                        Please provide a Roll no.
                      </Form.Control.Feedback>
                    </Form.Group>
                  </Row>
                  <Row className="mb-3">
                  <p>Date of issue</p>
                    <Form.Group as={Col} lg={2} controlId="formGridCity">
                    {/* <Form.Label className="title-form"></Form.Label> */}
                      <Form.Control
                      size="lg" 
                      type="date"
                      name="date"
                      placeholder="Date"
                      required
                      className="bg-col"/>
                    </Form.Group>

                    <Form.Group as={Col} lg={2} controlId="formGridState">
                    {/* <Form.Label className="title-form"></Form.Label> */}
                      <Form.Control
                          size="lg"
                          type="date" 
                          name="date"
                          placeholder="Date"
                          required
                          className="bg-col"
                        />
                    </Form.Group>

                    <Form.Group as={Col} lg={2} controlId="formGridZip">
                    {/* <Form.Label className="title-form"></Form.Label> */}
                      <Form.Control
                      size="lg"
                         type="date" 
                        
                         name="date"
                       
                        placeholder="Date"
                        required
                        className="bg-col"
                      />
                    </Form.Group>
                  </Row>


                  <Button type="submit">Submit form</Button>
                </Form>
                </div>
                
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default AdminAcademicUpload;