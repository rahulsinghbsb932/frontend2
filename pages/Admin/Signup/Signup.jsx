import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import Button from "react-bootstrap/Button";
import Header from "../../layout/Header";
import Footer from "../../layout/Footer";
import "./signup.scss";
import { Link } from "react-router-dom";

const Signup = () => {
  return (
    <React.Fragment>
      <Header/>
      <div className="sign-up">
        <div className="main-box">
          <Container fluid>
            <Row>
              <Col lg={3}>
                <div className="relative">
                  <div className="s-icon">
                    <img
                      src={require("../../../assets/img/signup/icon.svg").default}
                    />
                  </div>
                  <div className="img-box">
                    <img src={require("../../../assets/img/signup/img.png")} />
                  </div>
                  <div className="frame"></div>
                </div>
              </Col>
              <Col lg={9}>
              <div className="top-blue-frame"></div>
              <div className="top-frame"></div>
                <div className="ms-5">
                  <h1>Sign Up</h1>
                  <p>Create a new account for One-stop solution</p>
                </div>
                <Form>
                  <div className="box my-5">
                    <div className="google-icon"></div>
                    <div> Sign in with Google</div>
                  </div>
                  <div className="line">
                    <div className="h">or</div>
                  </div>
                  <InputGroup size="sm" className="my-3">
                    <InputGroup.Text id="inputGroup-sizing-sm">
                      <div>
                        <img
                          src={
                            require("../../../assets/img/signup/msg.svg").default
                          }
                          className="img-fluid me-2"
                        />
                      </div>
                    </InputGroup.Text>
                    <Form.Control
                      aria-label="Small"
                      aria-describedby="inputGroup-sizing-sm"
                      placeholder="Sign in with Phone or Email"
                    />
                  </InputGroup>
                  <InputGroup size="sm" className="mb-3">
                    <InputGroup.Text id="inputGroup-sizing-sm">
                      <div>
                        <img
                          src={
                            require("../../../assets/img/signup/lock.svg").default
                          }
                          className="img-fluid me-2"
                        />
                      </div>
                    </InputGroup.Text>
                    <Form.Control
                      aria-label="Small"
                      aria-describedby="inputGroup-sizing-sm"
                      placeholder="OTP"
                    />
                  </InputGroup>
                  <Form.Group className="mb-3">
                    <Form.Check
                      required
                      label="I agree all statements in Terms of service"
                      feedback="You must agree before submitting."
                      feedbackType="invalid"
                    />
                  </Form.Group>
                  <div className="text-center mt-3">
                    <Button type="Send">Send</Button>
                    <p className="mt-3">
                      Have already an account ?<strong><Link to='/dashboard'>Login here</Link> </strong>
                    </p>
                  </div>
                </Form>
                <div className="frame"></div>
              </Col>
            </Row>
          </Container>
        </div>
      </div>
      <Footer/>
    </React.Fragment>
  );
};

export default Signup;
