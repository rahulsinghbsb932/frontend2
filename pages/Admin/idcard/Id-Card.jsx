import React from "react";
import Grid from "@mui/material/Unstable_Grid2";
import Adminheader from "@components/includes/Adminheader";
import Sidebar from "@components/includes/Sidebar";
import APILink from "./APILink";
import ListFetch from "./ListFetch";
import Third from "./Third";

const IDCard = () => {
  return (
    <React.Fragment>
      <div className="admin-first">
        <div className="rel-abs">
          <Adminheader />
        </div>

        <div>
          <Sidebar />
        </div>
        <div className="main-admin">
          <div className="wrapper">
            {/* <Grid container spacing={2}>
              <Grid lg={10} xs={12} xsOffset={1}>
                <div className="mt-5">
                  <h4 className="ms-7">
                    <strong>ID Card</strong>
                  </h4>
                  <div className="bg">
                    <div className="icons">
                      <img
                        src="/images/id-card/id-card.svg"
                        className="img-fluid"
                      />
                    </div>
                    <h2>Create ID Card or Auto Generate</h2>
                    <p>
                      Easily create certificates of your student's achievement
                      to some simple stapes. Just enter a few details and
                      generate your certificate PDF with one click. You can also
                      download and share PDF certificates.
                    </p>
                    <div className="mb-3">
                      <strong>
                        You can create a certificate in three ways:
                      </strong>
                    </div>
                    <div className="d-flex flex-wrap gap-3 align-items-center hover">
                      <div className="main-icons">
                        <div className="sm-icons">
                          <img
                            src="/images/id-card/api.svg"
                            className="img-fluid"
                          />
                        </div>
                      </div>
                        <div className="content">
                          <h5 className="m-0">
                            ID Card List Fetch Through <strong>API Link</strong>{" "}
                          </h5>
                          <p>
                            Fetch a data of admission and need to the process?
                            Look into our API or No code automation to generate
                            admission data list.
                          </p>
                        </div>
                    </div>
                    <div className="d-flex flex-wrap gap-3 align-items-center hover my-4">
                      <div className="main-icons">
                        <div className="sm-icons">
                          <img
                            src="/images/id-card/csv.svg"
                            className="img-fluid"
                          />
                        </div>
                      </div>
                        <div className="content">
                        <h5 className="m-0">ID Card List upload Through<strong> Upload CSV</strong></h5>
                          <p>
                            Fetch a data of admission and need to the process?
                            Look into our API or No code automation to generate
                            admission data list.
                          </p>
                        </div>
                    </div>
                    <div className="d-flex flex-wrap gap-3 align-items-center hover">
                      <div className="main-icons">
                        <div className="sm-icons">
                          <img
                            src="/images/id-card/generate.svg"
                            className="img-fluid"
                          />
                        </div>
                      </div>
                        <div className="content">
                        <h5 className="m-0"><strong>Generate </strong>ID Card  </h5>
                          <p>
                            Fetch a data of admission and need to the process?
                            Look into our API or No code automation to generate
                            admission data list.
                          </p>
                        </div>
                    </div>
                  </div>
                </div>
              </Grid>
            </Grid> */}
            {/* <APILink/> */}
            {/* <ListFetch/> */}
            <Third/>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default IDCard;
