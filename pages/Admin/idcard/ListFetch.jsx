import React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";

function createData(name, calories, fat, carbs, protein) {
  return { name, calories, fat, carbs, protein };
}

const rows = [
  createData("Frozen yoghurt", 159, 6.0, 24, 4.0),
  createData("Ice cream sandwich", 237, 9.0, 37, 4.3),
  createData("Eclair", 262, 16.0, 24, 6.0),
  createData("Cupcake", 305, 3.7, 67, 4.3),
  createData("Gingerbread", 356, 16.0, 49, 3.9),
];
const ListFetch = () => {
  return (
    <div>
      <div className="p-4">
        <div className="text-end">
          <button className="btn btn-success">Back</button>
        </div>
        <h2>
          <strong>ID Card List Fetch Through API Link</strong>
        </h2>
        <h4>
          <strong>2nd Step:</strong> Check and verify the information. After
          verifying click on the Submit Button.{" "}
        </h4>
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 350, height: 200 }} aria-label="simple table">
            <TableHead sx={{ backgroundColor: "rgba(177, 178, 255, 0.5)" }}>
              <TableRow>
                <TableCell>
                  <strong>Adm. No.</strong>
                </TableCell>
                <TableCell>
                  <strong>Student Name</strong>
                </TableCell>
                <TableCell>
                  <strong>Profile</strong>
                </TableCell>
                <TableCell>
                  <strong>Course Applied</strong>
                </TableCell>
                <TableCell>
                  <strong>Accademic</strong>
                </TableCell>
                <TableCell>
                  <strong>Extra Curricular</strong>
                </TableCell>
                <TableCell>
                  <strong>Document</strong>
                </TableCell>
                <TableCell>
                  <strong>Payment</strong>
                </TableCell>
                <TableCell>
                  <strong>Date</strong>
                </TableCell>
                <TableCell>
                  <strong>Yes/No</strong>
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row) => (
                <TableRow key={row.name}>
                  <TableCell component="th" scope="row">
                    {row.name}
                  </TableCell>
                  <TableCell>{row.calories}</TableCell>
                  <TableCell>{row.fat}</TableCell>
                  <TableCell>{row.carbs}</TableCell>
                  <TableCell>{row.protein}</TableCell>
                  <TableCell>{row.protein}</TableCell>
                  <TableCell>{row.protein}</TableCell>
                  <TableCell>{row.protein}</TableCell>
                  <TableCell>{row.protein}</TableCell>
                  <TableCell>{row.protein}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        <div className="text-end mt-3">
            <button className="btn btn-pink">Submit all</button>
        </div>
      </div>
    </div>
  );
};

export default ListFetch;
