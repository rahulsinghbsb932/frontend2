import React from "react";
import { Row,Col } from "react-bootstrap";

const Third = () => {
  return (
    <div>
      <div className="p-4">
        <Row>
          <Col>
            <h2>
              <strong>ID Card List Fetch Through API Link</strong>
            </h2>
          </Col>
          <Col>
            <div className="text-end">
              <button className="btn btn-success">Back</button>
            </div>
          </Col>
        </Row>
        <Row>
          <Col>
            <div className="bg">
              <p><strong>Preview:</strong> Check and verify the information. After verifying click on the Submit Button. </p>
            </div>
          </Col>
        </Row>
        <div className="text-end mt-3">
          <button className="btn btn-pink">Submit all</button>
        </div>
      </div>
    </div>
  );
};

export default Third;
