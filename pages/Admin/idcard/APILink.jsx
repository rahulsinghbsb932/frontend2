import React from "react";
import Grid from "@mui/material/Unstable_Grid2";
import SaveAltSharpIcon from "@mui/icons-material/SaveAltSharp";

const APILink = () => {
  return (
    <div>
      <Grid container spacing={2}>
        <Grid lg={10} xs={12} xsOffset={1}>
          <div className="mt-5">
            <div className="text-end">
              <button className="btn btn-success">Back</button>
            </div>
            <h4 className="ms-7">
              <strong>ID Card List Fetch Through API Link</strong>
            </h4>
            <div className="bg">
              <div className="icons">
                <img src="/images/id-card/api.svg" className="img-fluid" />
              </div>
              <h2>Fetch data some simple steps:</h2>
              <p>
                Select our beautiful template or upload and enter a few details
                and generate your certificate PDF with one click. You can also
                download and share PDF certificates.
              </p>
              <div>
                <h3>
                  <strong>1st Stape:</strong> Add API link
                </h3>
                <div className="box">
                  <p className="m-0">Past API link here and click on “Fetch API” button.</p>
                </div>
                <button className="btn btn-pink my-3">
                  <SaveAltSharpIcon /> Download API Sample File
                </button>
                <p>
                  Note: HERE API documentation, examples and guides for our
                  libraries. Explorer is a tool available on most REST API
                  reference documentation pages that lets you try Google API
                  methods without writing code.
                </p>
                <div className="text-end mt-5">
                  <button className="btn btn-pink">Fetch API</button>
                </div>
              </div>
            </div>
          </div>
        </Grid>
      </Grid>
    </div>
  );
};

export default APILink;
