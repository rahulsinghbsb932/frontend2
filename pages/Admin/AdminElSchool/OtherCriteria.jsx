import React from "react";
import { Row, Col,Container } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import * as formik from "formik";
import * as yup from "yup";
import Image from "next/image";


const OtherCriteria = () => {
  return (
    <>
      <Container>
        <Row className="school-registration">
          <Col lg={3}>
            <div className="p-image-upload">
              <div className="inner-circle">
                <div className="upload-icon">
                  <label for="fileInput">
                    <Image
                      src="/images/AdmilElSchool/uploadicon.svg"
                      width={35}
                      height={35}
                    />
                  </label>
                  <Form.Control
                    type="file"
                    id="fileInput"
                    required
                    name="file"
                    size="lg"
                  />
                 
                  <hr />
                  <p>
                    <strong>Profile Image</strong>
                    <br />
                    SVG, JPEG, PNG
                    <br />
                    BMP, GIF
                  </p>
                </div>
               
              </div>
            </div>
          </Col>
          <Col lg={9} className="bg primary-subtle">
            <h1>Other Criteria</h1>
            <Form className="basic-info-form">
              <Form.Group
                
                controlId="exampleForm.ControlInput1"
              >
                <div className="basic-info-form_icon">
                  <Image
                    src="/images/AdmilElSchool/students.svg"
                    alt="students icon"
                    width={30}
                    height={30}
                  />
                </div>
                <Form.Select className="bg-white px-5" defaultValue="Choose...">
                      <option>Student Interaction</option>
                      <option>...</option>
                    </Form.Select>
              </Form.Group>
              <Form.Group
                
                controlId="exampleForm.ControlInput1"
              >
                <div className="basic-info-form_icon">
                  <Image
                    src="/images/AdmilElSchool/parents.svg"
                    alt="parents icon"
                    width={30}
                    height={30}
                  />
                </div>
                <Form.Select className="bg-white px-5" defaultValue="Choose...">
                      <option>Parents Interaction</option>
                      <option>...</option>
                    </Form.Select>
              </Form.Group>
              <Form.Group
                
                controlId="exampleForm.ControlInput1"
              >
                <div className="basic-info-form_icon">
                  <Image
                    src="/images/AdmilElSchool/universityicon.svg"
                    alt="university icon"
                    width={30}
                    height={30}
                  />
                </div>
                <Form.Select className="bg-white px-5" defaultValue="Choose...">
                      <option>Infrastructure</option>
                      <option>...</option>
                    </Form.Select>
              </Form.Group>
              <Form.Group
                
                controlId="exampleForm.ControlInput1"
              >
                <div className="basic-info-form_icon">
                  <Image
                    src="/images/AdmilElSchool/safety.svg"
                    alt="safety icon"
                    width={30}
                    height={30}
                  />
                </div>
                <Form.Select className="bg-white px-5" defaultValue="Choose...">
                      <option>Safety and Security</option>
                      <option>...</option>
                    </Form.Select>
              </Form.Group>
              <Form.Group
                
                controlId="exampleForm.ControlInput1"
              >
                <div className="basic-info-form_icon">
                  <Image
                    src="/images/AdmilElSchool/lab.svg"
                    alt="lab icon"
                    width={30}
                    height={30}
                  />
                </div>
                <Form.Select className="bg-white px-5" defaultValue="Choose...">
                      <option>Lab</option>
                      <option>...</option>
                    </Form.Select>
              </Form.Group>
              <Form.Group
                
                controlId="exampleForm.ControlInput1"
              >
                <div className="basic-info-form_icon">
                  <Image
                    src="/images/AdmilElSchool/disabled.svg"
                    alt="disabled icon"
                    width={30}
                    height={30}
                  />
                </div>
                <Form.Select className="bg-white px-5" defaultValue="Choose...">
                      <option>Disabled Friendly</option>
                      <option>...</option>
                    </Form.Select>
              </Form.Group>
              <Form.Group
                
                controlId="exampleForm.ControlInput1"
              >
                <div className="basic-info-form_icon">
                  <Image
                    src="/images/AdmilElSchool/sports.svg"
                    alt="sports icon"
                    width={30}
                    height={30}
                  />
                </div>
                <Form.Select className="bg-white px-5" defaultValue="Choose...">
                      <option>Sports and Fitness</option>
                      <option>...</option>
                    </Form.Select>
              </Form.Group>
              <Form.Group
                
                controlId="exampleForm.ControlInput1"
              >
                <div className="basic-info-form_icon">
                  <Image
                    src="/images/AdmilElSchool/linkicon.svg"
                    alt="university icon"
                    width={30}
                    height={30}
                  />
                </div>
                <Form.Control
                  className="bg-white"
                  type="text"
                  placeholder="Name of The School"
                />
              </Form.Group>
            </Form>
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default OtherCriteria;