import React from "react";
import Sidebar from "@components/includes/Sidebar";
import Adminheader from "@components/includes/Adminheader";

import { useState } from "react";
import { Steps, message, Icon } from "antd";
import Button from "react-bootstrap/Button";
import { CheckSquareOutlined } from "@ant-design/icons";
import { Container } from "react-bootstrap";
import SchoolRegistration from "./SchoolRegistration"
import AdmissionAndFee from "./AdmissionAndFee"
import OtherCriteria from "./OtherCriteria"
import Gallery from "./Gallery"
import HallOfFame from "./HallOfFame"


const AdminElSchool = () => {
  let [current, setCurrent] = useState(0);
  
   const Step = Steps.items;
  const steps = [
    {
      title: "School Registration",
      icon: <CheckSquareOutlined />,
      component: <SchoolRegistration />,
    },
    {
      title: "Admission &Fee",
      icon: <CheckSquareOutlined />,
      component: <AdmissionAndFee />,
    },
    {
      title: "Other Criteria",
      icon: <CheckSquareOutlined />,
      component: <OtherCriteria />,
    },
    {
      title: "Gallery",
      icon: <CheckSquareOutlined />,
      component: <Gallery />,
    },
    {
      title: "Hall Of Fame",
      icon: <CheckSquareOutlined />,
      component: <HallOfFame />,
    },
    
    
  ];
  const next = () => {
    current = current + 1;
    setCurrent(current);
  };
  const prev = () => {
    current = current - 1;
    setCurrent(current);
  };
  return (
    <React.Fragment>
      <div className="admin-first">
        <div className="rel-abs">
          <Adminheader />
        </div>

        <div>
          <Sidebar />
        </div>
        <div className="main-admin">
          <div className="registration registration-form">
            {/* <Grid
              container
              rowSpacing={1}
              columnSpacing={{ xs: 1, sm: 2, md: 3 }}
            >
              <Grid xs={6}>1</Grid>
              <Grid xs={6}>2</Grid>
            </Grid> */}
          <Container >

            <Steps current={current}>
              {steps.map((item) => (
                <Step key={item.title} title={item.title} icon={item.icon} />
              ))} 
            </Steps>
            <div className="mt-3">{steps[current].component}</div>
            <div className="text-end">
              <div className="steps-action">
                {current < steps.length - 1 && (
                  <Button type="Next" className="btn-success" onClick={() => next()}>
                    Next
                  </Button>
                )}
                {current === steps.length - 1 && (
                  <Button
                    type="Done"
                    className="btn-success"
                    onClick={() => message.success("Processing complete!")}
                  >
                    Done
                  </Button>
                )}
                {current > 0 && (
                  <Button style={{ marginLeft: 8 }} className="btn-success" onClick={() => prev()}>
                    Previous
                  </Button>
                )}
              </div>
            </div>
          </Container>


          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default AdminElSchool;
