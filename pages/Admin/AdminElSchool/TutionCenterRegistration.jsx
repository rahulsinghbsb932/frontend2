import React from "react";
import { Row, Col,Container } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import * as formik from "formik";
import * as yup from "yup";
import Image from "next/image";


const TutionCenterRegistration = () => {
  return (
    <>
      <Container>
        <Row className="school-registration">
          <Col lg={3}>
            <div className="p-image-upload">
              <div className="inner-circle">
                <div className="upload-icon">
                  <label for="fileInput">
                    <Image
                      src="/images/AdmilElSchool/uploadicon.svg"
                      width={35}
                      height={35}
                    />
                  </label>
                  <Form.Control
                    type="file"
                    id="fileInput"
                    required
                    name="file"
                    size="lg"
                  />
                  {/* <Image
                    src="/images/AdmilElSchool/uploadicon.svg"
                    alt="upload icon"
                    width={35}
                    height={35}
                    /> */}
                  <hr />
                  <p>
                    <strong>Profile Image</strong>
                    <br />
                    SVG, JPEG, PNG
                    <br />
                    BMP, GIF
                  </p>
                </div>
                {/* <div className="upload-text">
                    <p><strong>Profile Image</strong><br/>SVG, JPEG, PNG<br/>BMP, GIF</p>
                  </div> */}
              </div>
            </div>
          </Col>
          <Col lg={9} className="bg primary-subtle">
            <h1>Tuition Center Registration</h1>
            <Form className="basic-info-form">
              <Form.Group
                
                controlId="exampleForm.ControlInput1"
              >
                <div className="basic-info-form_icon">
                  <Image
                    src="/images/AdmilElSchool/universityicon.svg"
                    alt="university icon"
                    width={30}
                    height={30}
                  />
                </div>
                <Form.Control
                  className="bg-white"
                  type="text"
                  placeholder="Name of The Tutor"
                />
              </Form.Group>
              {/* <Form.Group  controlId="exampleForm.ControlInput1">
                 <div className="basic-info-form_icon">
                <Image
                    src="/images/AdmilElSchool/admissionicon.svg"
                    alt="university icon"
                    width={30}
                    height={30}
                    />
                </div>
                <Form.Control className="bg-white" type="text" placeholder="Admission No." />
              </Form.Group> */}
              <Form.Group
                
                controlId="exampleForm.ControlInput1"
              >
                <div className="basic-info-form_icon">
                  <Image
                    src="/images/AdmilElSchool/locationicon.svg"
                    alt="university icon"
                    width={30}
                    height={30}
                  />
                </div>
                <Form.Control
                  className="bg-white"
                  type="text"
                  placeholder="Use Current Location from Google Map"
                />
              </Form.Group>
              <Form.Group
                
                controlId="exampleForm.ControlTextarea1"
              >
                <div className="basic-info-form_icon">
                  <Image
                    src="/images/AdmilElSchool/textareaicon.svg"
                    alt="university icon"
                    width={30}
                    height={30}
                  />
                </div>
                <Form.Control
                  className="bg-white"
                  as="textarea"
                  placeholder="Brief About Your School ( Minimum 100 words *)"
                  style={{ height: "150px" }}
                />
              </Form.Group>
              <Form.Group
                
                controlId="exampleForm.ControlInput1"
              >
                <div className="basic-info-form_icon">
                  <Image
                    src="/images/AdmilElSchool/linkicon.svg"
                    alt="university icon"
                    width={30}
                    height={30}
                  />
                </div>
                <Form.Control
                  className="bg-white"
                  type="text"
                  placeholder="Website Link"
                />
              </Form.Group>
              <Row >
                <Col lg={4} xs={12}>
                  <Form.Group>
                     <div className="basic-info-form_icon">
                    <Image
                      src="/images/AdmilElSchool/contacticon.svg"
                      alt="email icon"
                      width={30}
                      height={30}
                    />
                  </div>
                    <Form.Control className="bg-white" placeholder="Contact Person Name"/>
                  </Form.Group>
                </Col>
                <Col lg={4} xs={12}>
                  <Form.Group>
                     <div className="basic-info-form_icon">
                    <Image
                      src="/images/AdmilElSchool/emailicon.svg"
                      alt="email icon"
                      width={30}
                      height={30}
                    />
                  </div>
                  <Form.Control className="bg-white" placeholder="email@domainname.com"/>
                  </Form.Group>
                </Col>
                <Col lg={4} xs={12}>
                  <Form.Group>
                     <div className="basic-info-form_icon">
                    <Image
                      src="/images/AdmilElSchool/mobileicon.svg"
                      alt="email icon"
                      width={30}
                      height={30}
                    />
                  </div>
                    <Form.Control className="bg-white" placeholder="+000 00000 00000"/>
                  </Form.Group>
                </Col>
              </Row>
              
              <Row>
              <Col lg={6} xs={12}>
              <Form.Group>
              <div className="basic-info-form_icon">
                    <Image
                      src="/images/AdmilElSchool/educationboardicon.svg"
                      alt="email icon"
                      width={30}
                      height={30}
                    />
                  </div>
                    <Form.Select className="bg-white px-5" defaultValue="Choose...">
                      <option>Subject You Teach</option>
                      <option>...</option>
                    </Form.Select>
                  </Form.Group>
                </Col>
                <Col lg={6} xs={12}>
                <Form.Group>
                <div className="basic-info-form_icon">
                    <Image
                      src="/images/AdmilElSchool/ownershipicon.svg"
                      alt="email icon"
                      width={30}
                      height={30}
                    />
                  </div>
                    <Form.Select className="bg-white px-5" defaultValue="Choose...">
                      <option>Area that you cover</option>
                      <option>...</option>
                    </Form.Select>
                  </Form.Group>
                </Col>
              </Row>

              <Row >
                <Col lg={4} xs={12}>
                  <Form.Group>
                     <div className="basic-info-form_icon">
                    <Image
                      src="/images/AdmilElSchool/establishmenticon.svg"
                      alt="email icon"
                      width={30}
                      height={30}
                    />
                  </div>
                    <Form.Control className="bg-white" placeholder="Classes You Teach"/>
                  </Form.Group>
                </Col>
                <Col lg={4} xs={12}>
                  <Form.Group>
                     <div className="basic-info-form_icon">
                    <Image
                      src="/images/AdmilElSchool/establishmenticon.svg"
                      alt="email icon"
                      width={30}
                      height={30}
                    />
                  </div>
                    <Form.Control className="bg-white" placeholder="Fees (Per Class)"/>
                  </Form.Group>
                </Col>
                <Col lg={4} xs={12}>
                  <Form.Group>
                     <div className="basic-info-form_icon">
                    <Image
                      src="/images/AdmilElSchool/establishmenticon.svg"
                      alt="email icon"
                      width={30}
                      height={30}
                    />
                  </div>
                    <Form.Select className="bg-white px-5" defaultValue="Mode Payment">
                      <option>Mode Payment</option>
                      <option>...</option>
                    </Form.Select>
                  </Form.Group>
                </Col>
                
              </Row>

              {/* <Form.Group  controlId="exampleForm.ControlInput1">
                <Form.Control type="email" placeholder="Contact Person Name" />
              </Form.Group> */}
            </Form>
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default TutionCenterRegistration;