import React from "react";
import { Row, Col,Container } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import * as formik from "formik";
import * as yup from "yup";
import Image from "next/image";


const AdmissionAndFee = () => {
  return (
    <>
      <Container>
        <Row className="school-registration">
          <Col lg={3}>
            <div className="p-image-upload">
              <div className="inner-circle">
                <div className="upload-icon">
                  <label for="fileInput">
                    <Image
                      src="/images/AdmilElSchool/uploadicon.svg"
                      width={35}
                      height={35}
                    />
                  </label>
                  <Form.Control
                    type="file"
                    id="fileInput"
                    required
                    name="file"
                    size="lg"
                  />
                  {/* <Image
                    src="/images/AdmilElSchool/uploadicon.svg"
                    alt="upload icon"
                    width={35}
                    height={35}
                    /> */}
                  <hr />
                  <p>
                    <strong>Profile Image</strong>
                    <br />
                    SVG, JPEG, PNG
                    <br />
                    BMP, GIF
                  </p>
                </div>
                {/* <div className="upload-text">
                    <p><strong>Profile Image</strong><br/>SVG, JPEG, PNG<br/>BMP, GIF</p>
                  </div> */}
              </div>
            </div>
          </Col>
          <Col lg={9} className="bg primary-subtle">
            <h1>Admission & Fee</h1>
            <Form className="basic-info-form">
            <Row>
              <Col lg={8} xs={12}>
              <Form.Group
                
                controlId="exampleForm.ControlInput1"
              >
                <div className="basic-info-form_icon">
                  <Image
                    src="/images/AdmilElSchool/linkicon.svg"
                    alt="university icon"
                    width={30}
                    height={30}
                  />
                </div>
                <Form.Control
                  className="bg-white"
                  type="text"
                  placeholder="Online Admission Form Link"
                />
              </Form.Group>
                </Col>
                <Col lg={4} xs={12}>
                <Form.Group>
                <div className="basic-info-form_icon">
                    <Image
                      src="/images/AdmilElSchool/ownershipicon.svg"
                      alt="email icon"
                      width={30}
                      height={30}
                    />
                  </div>
                    <Form.Select className="bg-white px-5" defaultValue="Choose...">
                      <option>Ownership</option>
                      <option>...</option>
                    </Form.Select>
                  </Form.Group>
                </Col>
              </Row>
              <Row >
                <Col lg={4} xs={12}>
                  <Form.Group>
                     <div className="basic-info-form_icon">
                    <Image
                      src="/images/AdmilElSchool/formavaibility.svg"
                      alt="email icon"
                      width={30}
                      height={30}
                    />
                  </div>
                  <Form.Select className="bg-white px-5" defaultValue="Choose...">
                      <option>Form Availability</option>
                      <option>...</option>
                    </Form.Select>
                  </Form.Group>
                </Col>
                <Col lg={4} xs={12}>
                  <Form.Group>
                     <div className="basic-info-form_icon">
                    <Image
                      src="/images/AdmilElSchool/formpayment.svg"
                      alt="email icon"
                      width={30}
                      height={30}
                    />
                  </div>
                    <Form.Select className="bg-white px-5" defaultValue="Choose...">
                      <option>Form Payment</option>
                      <option>...</option>
                    </Form.Select>
                  </Form.Group>
                </Col>
                <Col lg={4} xs={12}>
                  <Form.Group>
                     <div className="basic-info-form_icon">
                    <Image
                      src="/images/AdmilElSchool/totalseats.svg"
                      alt="totalseats"
                      width={30}
                      height={30}
                    />
                  </div>
                    <Form.Control className="bg-white" placeholder="Total Seats"/>
                  </Form.Group>
                </Col>
              </Row>
              <Form.Group
                
                controlId="exampleForm.ControlInput1"
              >
                <div className="basic-info-form_icon">
                  <Image
                    src="/images/AdmilElSchool/universityicon.svg"
                    alt="university icon"
                    width={30}
                    height={30}
                  />
                </div>
                <Form.Select className="bg-white px-5" defaultValue="Choose...">
                      <option>Documents required at the time of application/admission</option>
                      <option>...</option>
                    </Form.Select>
              </Form.Group>
              
              
             
              <Row >
                <Col lg={6} xs={12}>
                  <Form.Group>
                     <div className="basic-info-form_icon">
                    <Image
                      src="/images/AdmilElSchool/fee.svg"
                      alt="tution fee"
                      width={30}
                      height={30}
                    />
                  </div>
                  <Form.Control className="bg-white" placeholder="Tution Fee (Yearly)"/>
                  </Form.Group>
                </Col>
                <Col lg={6} xs={12}>
                  <Form.Group>
                     <div className="basic-info-form_icon">
                    <Image
                      src="/images/AdmilElSchool/fee.svg"
                      alt="adm fee"
                      width={30}
                      height={30}
                    />
                  </div>
                  <Form.Select className="bg-white px-5" defaultValue="Choose...">
                      <option>Admission Fee</option>
                      <option>...</option>
                    </Form.Select>
                  </Form.Group>
                </Col>
                <Col lg={6} xs={12}>
                  <Form.Group>
                     <div className="basic-info-form_icon">
                    <Image
                      src="/images/AdmilElSchool/contacticon.svg"
                      alt="Age Eligibility"
                      width={30}
                      height={30}
                    />
                  </div>
                    <Form.Control className="bg-white" placeholder="Age Eligibility ( Nursery)"/>
                  </Form.Group>
                </Col>
                <Col lg={6} xs={12}>
                  <Form.Group>
                     <div className="basic-info-form_icon">
                    <Image
                      src="/images/AdmilElSchool/eligibility.svg"
                      alt="Eligibility"
                      width={30}
                      height={30}
                    />
                  </div>
                  <Form.Control className="bg-white" placeholder="Eligibility ( other Criteria If any )"/>
                  </Form.Group>
                </Col>
                <Col lg={6} xs={12}>
                  <Form.Group>
                     <div className="basic-info-form_icon">
                    <Image
                      src="/images/AdmilElSchool/classestype.svg"
                      alt="classestype"
                      width={30}
                      height={30}
                    />
                  </div>
                  <Form.Select className="bg-white px-5" defaultValue="Choose...">
                      <option>Classes Type</option>
                      <option>...</option>
                    </Form.Select>
                  </Form.Group>
                </Col>
                <Col lg={6} xs={12}>
                  <Form.Group>
                     <div className="basic-info-form_icon">
                    <Image
                      src="/images/AdmilElSchool/boarding.svg"
                      alt="boarding"
                      width={30}
                      height={30}
                    />
                  </div>
                  <Form.Select className="bg-white px-5" defaultValue="Choose...">
                      <option>Boarding</option>
                      <option>...</option>
                    </Form.Select>
                  </Form.Group>
                </Col>
                <Col lg={6} xs={12}>
                  <Form.Group>
                     <div className="basic-info-form_icon">
                    <Image
                      src="/images/AdmilElSchool/time.svg"
                      alt="School Timing"
                      width={30}
                      height={30}
                    />
                  </div>
                    <Form.Control type="time" className="bg-white" placeholder="School Timing"/>
                  </Form.Group>
                </Col>
                <Col lg={6} xs={12}>
                  <Form.Group>
                     <div className="basic-info-form_icon">
                    <Image
                      src="/images/AdmilElSchool/time.svg"
                      alt="Admin Office Timing"
                      width={30}
                      height={30}
                    />
                  </div>
                  <Form.Select className="bg-white px-5" defaultValue="Choose...">
                      <option>Admin Office Timing</option>
                      <option>...</option>
                    </Form.Select>
                  </Form.Group>
                </Col>
                <Col lg={6} xs={12}>
                  <Form.Group>
                     <div className="basic-info-form_icon">
                    <Image
                      src="/images/AdmilElSchool/test.svg"
                      alt="test"
                      width={30}
                      height={30}
                    />
                  </div>
                  <Form.Select className="bg-white px-5" defaultValue="Choose...">
                      <option>Written Test</option>
                      <option>...</option>
                    </Form.Select>
                  </Form.Group>
                </Col>
                <Col lg={6} xs={12}>
                  <Form.Group>
                     <div className="basic-info-form_icon">
                    <Image
                      src="/images/AdmilElSchool/ratio.svg"
                      alt="ratio"
                      width={30}
                      height={30}
                    />
                  </div>
                    <Form.Control className="bg-white" placeholder="Student Faculty Ratio"/>
                  </Form.Group>
                </Col>
              </Row>
             
             

              {/* <Form.Group  controlId="exampleForm.ControlInput1">
                <Form.Control type="email" placeholder="Contact Person Name" />
              </Form.Group> */}
            </Form>
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default AdmissionAndFee;