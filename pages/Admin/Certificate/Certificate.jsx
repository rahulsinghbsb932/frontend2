import React from 'react'
import Sidebar from "../../layout/Sidebar";
import Adminheader from "../../layout/Adminheader";
import DPS from './DPS/DPS';

const Certificate = () => {
  return (
    <>
     <div className="admin-first">
        <div className="rel-abs">
          <Adminheader />
        </div>
        <div>
          <Sidebar />
        </div>
        <div className="main-admin">
          <div className="profile-m">
            <DPS/>
          </div>
        </div>
      </div>
    </>
  )
}

export default Certificate
