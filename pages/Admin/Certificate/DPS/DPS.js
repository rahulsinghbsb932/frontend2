import React from "react";

import "./DPS.scss";
import { Col, Container, Row } from "react-bootstrap";

import schoolLogo from "./Image/schoolLogo.svg";
import sign from "./Image/signatures.png";

const DPS = () => {
  return (
    <div className="DPS">
      <Container fluid>
        <Row>
          <Col lg={12}>
            <div className="Certificate-main">
              <center>
                <h1 className="school-name">Delhi Public School </h1>
                <h6 className="session">EXPRESSIONS 2023</h6>
                <img
                  src={schoolLogo}
                  alt="school Logo"
                  className="schoolLogo"
                />
                <h6 className="certificate-purpose">Certificate of Merit</h6>
              </center>

              <div className="details">
                <Row>
                  <Col lg={8} className="flex">
                    <label htmlFor="" className="label-input">
                      Name
                    </label>
                    <p className="input-form">Rohit Kumar</p>
                  </Col>
                  <Col lg={4} className="flex">
                    <label htmlFor="" className="label-input">
                      Class
                    </label>
                    <p className="input-form">IX 'B'</p>
                  </Col>
                </Row>

                <Row>
                  <Col lg={12} className="flex">
                    <label htmlFor="" className="label-input">
                      of
                    </label>
                    <p className="input-form">DPS</p>
                  </Col>
                </Row>
                <Row>
                  <Col lg={12} className="flex">
                    <label htmlFor="" className="label-input in-2">
                      is awareded
                    </label>
                    <p className="input-form">Maths Competition</p>
                  </Col>
                </Row>

                <Row>
                  <Col lg={12} className="flex">
                    <label htmlFor="" className="label-input in-3">
                      position in
                    </label>
                    <p className="input-form">3rd position</p>
                  </Col>
                </Row>
              </div>
              <div className="last-component">
                <p className="principal">
                  Principle
                  <img src={sign} alt="" className="signa" />
                </p>
                <p className="date">Date: 04-05-2023</p>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default DPS;
