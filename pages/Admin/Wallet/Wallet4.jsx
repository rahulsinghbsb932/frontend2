import React, { useEffect, useState } from 'react'
import Form from "react-bootstrap/Form";
import { Row, Col, Container, Button } from "react-bootstrap";
import Image from 'next/image';
import { useRouter } from "next/router";
import { createWorker } from 'tesseract.js';

const Wallet4 = () => {
    const router = useRouter();
    const [result, setResult] = useState('');
    const [name, setName] = useState('');
    const [fatherName, setFatherName] = useState('');
    const [motherName, setMotherName] = useState('');
    const [board, setBoard] = useState('');
    const [school, setSchool] = useState('');


    const { selectedFile } = router.query;
    const handleNext = () => {
        router.push({
            pathname: "/Admin/Wallet/Wallet5",
            query: { selectedFile: selectedFile } // Pass selectedFile as state
        });
    };

    const handleScan = async () => {
        console.log(selectedFile);
        if (selectedFile) {
            const worker = await createWorker({
                logger: (m) => console.log(m), // Optional logger to display OCR progress and logs
            });

            await worker.load();
            await worker.loadLanguage('eng');
            await worker.initialize('eng');
            await worker.setParameters({
                tessedit_char_whitelist: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789- ',
            });

            const { data: { text } } = await worker.recognize(selectedFile);
            await worker.terminate();
            console.log("data", text);
            const extractedData = extractInformation(text);
            setResult(extractedData);
            console.log("extracted", extractedData)
        }
    };
    const extractInformation = (text) => {
        const nameRegex = /cery bt\s*([A-Za-z\s]+)/;
        const boardRegex = /CENTRAL\s*([A-Za-z\s]+)/;
        const schoolRegex = /Sciool 57436\s*([^A-Za-z0-9]+)/;
        const yearRegex = /Dated\s*(\d+)/;
        const englishRegex = /ENGLISHCORE\s*(\d+)/;
        const mathsRegex = /MATHEMATICS\s*(\d+)/;
        const physicsRegex = /PHYSICS\s*(\d+)/;
        const chemistryRegex = /CHEMISTRY\s*(\d+)/;
        const economicsRegex = /economcs\s*(\d+)/;
        const fatherRegex = /Guandins Name\s*([A-Za-z\s]+)/;
        const motherRegex = /MotbersName\s*([A-Za-z\s]+)/;

        const nameMatch = text.match(nameRegex);
        const boardMatch = text.match(boardRegex);
        const schoolMatch = text.match(schoolRegex);
        const englishMatch = text.match(englishRegex);
        const mathsMatch = text.match(mathsRegex);
        const physicsMatch = text.match(physicsRegex);
        const chemistryMatch = text.match(chemistryRegex);
        const economicsMatch = text.match(economicsRegex);
        const yearMatch = text.match(yearRegex);
        const fatherMatch = text.match(fatherRegex);
        const motherMatch = text.match(motherRegex);
// console.log("school",schoolMatch)
        const extractedData = {
            name: nameMatch ? nameMatch[1] : '',
            board: boardMatch ? boardMatch[0] : '',
            year: yearMatch ? yearMatch[1] : '',
            father: fatherMatch ? fatherMatch[1] : '',
            mother: motherMatch ? motherMatch[1] : '',
            school: schoolMatch ? schoolMatch[1] : '',
            english: englishMatch ? schoolMatch[1] : '',
            maths: mathsMatch ? mathsMatch[1] : '',
            physics: physicsMatch ? physicsMatch[1] : '',
            chemistry: chemistryMatch ? chemistryMatch[1] : '',
            economics: economicsMatch ? economicsMatch[1] : '',

        };
        setBoard(extractedData.board);
        setName(extractedData.name);
        setFatherName(extractedData.father);
        setMotherName(extractedData.mother);
        setSchool(extractedData.school);

        return extractedData;
    };

    return (
        <>
            <Container>
                <Row className='marks'>
                    <Col lg={4} className='marksheet'>
                        {selectedFile ? (
                            <img
                                src={selectedFile}
                                alt="Uploaded"
                                width={407}
                                height={600}
                            />
                        ) : (
                            <Image src="/images/image.png" width={407} height={407} />
                        )}
                    </Col>
                    <Col lg={7} className="bg primary-subtle">
                        <h1 className='heading-text'>Marks of all Attempted Subjects</h1>
                        <Form className="basic-info-form">
                            <Row className='my-3'>
                                <Col lg={7} className='mb-3'>
                                    <span className='image-span'>
                                        <Image
                                            src="/images/Wallet/penicon.svg"
                                            alt="university icon"
                                            width={20}
                                            height={20}
                                        />
                                    </span>
                                    {/* <Form.Select className='select-box' size='lg' defaultValue="Board:">
                                        <option selected value="Board">Board</option>
                                        <option>CBSE</option>
                                        <option>ICSE</option>
                                        <option>State Board</option>
                                    </Form.Select> */}
                                    <Form.Control size='lg' className="bg-white" placeholder="Board:" value={board}
                                    />
                                </Col>
                                <Col lg={5}>

                                    <span className='image-span'>
                                        <Image
                                            src="/images/Wallet/penicon.svg"
                                            alt="university icon"
                                            width={20}
                                            height={20}
                                        />
                                    </span>
                                    <Form.Control size='lg' className="bg-white" placeholder="Roll No.:"
                                    />
                                </Col>
                            </Row>
                            <Row className='mb-3'>
                                <Col lg={7} className='mb-3'>
                                    <span className='image-span'>
                                        <Image
                                            src="/images/Wallet/penicon.svg"
                                            alt="university icon"
                                            width={20}
                                            height={20}
                                        />
                                    </span>
                                    <Form.Select className='select-box' size='lg' defaultValue="Type of Document">
                                        <option>Type of Document:</option>
                                        <option>CBSE</option>
                                        <option>ICSE</option>
                                        <option>State Board</option>
                                    </Form.Select>
                                </Col>
                                <Col lg={5}>
                                    <span className='image-span'>
                                        <Image
                                            src="/images/Wallet/penicon.svg"
                                            alt="university icon"
                                            width={20}
                                            height={20}
                                        />
                                    </span>
                                    <Form.Control size='lg' type="date" className="bg-white" />
                                </Col>
                            </Row>
                            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                <span className='image-span'>
                                    <Image
                                        src="/images/Wallet/universityicon.svg"
                                        alt="university icon"
                                        width={20}
                                        height={20}
                                    />
                                </span>
                                <Form.Control size='lg' className="bg-white" type="text" placeholder="School Name:" value={school} />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                <div className='input-container'>

                                    <span className='image-span'>
                                        <Image
                                            src="/images/Wallet/locationicon.svg"
                                            alt="university icon"
                                            width={20}
                                            height={20}
                                        />
                                    </span>
                                    <Form.Control size='lg' className="input-box bg-white" type="text" placeholder="Location :" />
                                </div>
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                <span className='image-span'>
                                    <Image
                                        src="/images/Wallet/contacticon.svg"
                                        alt="university icon"
                                        width={20}
                                        height={20}
                                    />
                                </span>
                                <Form.Control size='lg' className="bg-white" type="text" placeholder="Student Name" value={name} />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                <span className='image-span'>
                                    <Image
                                        src="/images/Wallet/contacticon.svg"
                                        alt="university icon"
                                        width={20}
                                        height={20}
                                    />
                                </span>
                                <Form.Control size='lg' className="bg-white" type="text" placeholder="Mother's Name" value={motherName} />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                <span className='image-span'>
                                    <Image
                                        src="/images/Wallet/contacticon.svg"
                                        alt="university icon"
                                        width={20}
                                        height={20}
                                    />
                                </span>
                                <Form.Control size='lg' className="bg-white" type="text" placeholder="Father’s Name" value={fatherName} />
                            </Form.Group>
                        </Form>
                        <Button type="next" onClick={handleScan}>Scan</Button>
                        <Button type="next" onClick={handleNext}>Next</Button>
                    </Col>
                </Row>
            </Container>


        </>
    );
}

export default Wallet4;