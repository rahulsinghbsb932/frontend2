import React,{useEffect} from "react";
//import "./wallet.scss";
import Adminheader from "@components/includes/Adminheader";
import Sidebar from "@components/includes/Sidebar";
// import { Row, Col } from "react-bootstrap";
import { useRouter } from "next/router";
import { Row, Col, Container, Button } from "react-bootstrap";
import SystemUpdateAltIcon from '@mui/icons-material/SystemUpdateAlt';
import Image from "next/image";
import axios from "axios"; // Import Axios
import { useState } from "react";


  
const DocumentList = () => {
  const router = useRouter();
  const { code, state } = router.query;
  const {data, setData} = useState('');
  useEffect(async() => {
    if (typeof window !== 'undefined') {
      
      localStorage.setItem("code",code);
      localStorage.setItem("state",state);
    }
    const accessTokenResponse = await axios.post('https://api.digitallocker.gov.in/public/oauth2/1/token', {
        grant_type: 'authorization_code',
        client_id: 'TO9720B9D2',
        client_secret: '709f01f25654d42bef6c',
        redirect_uri: 'http://localhost:3000/Admin/Wallet/DocumentList',
        code_verifier: 'fcd1b3c5f4d1cc285d56aa375e843c85a58cee586aa748f2b8555f8f',
        code:code,
      });
      
      console.log("----------------**************",accessTokenResponse.data);

      const { access_token } = accessTokenResponse.data;
      setData("accessTokenResponse----------",accessTokenResponse);
      console.log(accessTokenResponse);
      console.log("dat1----------------",data);
  }, [code, state]);
  
  
  const handleManually =() => {
    router.push("/Admin/Wallet/UserWallet");
  }
 
  function handleDownload() {
    fetch('/download-endpoint') 
      .then(response => response.blob())
      .then(blob => {
        
        const link = document.createElement('a');
        link.href = URL.createObjectURL(blob);
        link.download = 'filename'; 
        link.click();
        
        URL.revokeObjectURL(link.href);
      })
      .catch(error => {
        console.error('Error occurred during download:', error);
      });
  };
  const handler = async(req, res) => {
    router.push("https://api.digitallocker.gov.in/public/oauth2/1/authorize?client_id=TO9720B9D2&redirect_uri=http://localhost:3000/Admin/Wallet/DocumentList&response_type=code&state=12345&code_challenge=5pUAGRDNHAzCftF9RAgiZnoEhx5O70BA0lA3pao35Sk&code_challenge_method=S256");
  }
console.log("data-----------------",data);

  return (
    <>
      <div className="admin-first">
        <div className="rel-abs">
          <Adminheader />
        </div>
        <div>
          <Sidebar />
        </div>
        <div className="main-admin">
        <Button type="next" onClick={handler}>Add with Digi</Button>
        <Button type="next" onClick={handleManually}>Add Manually</Button>
        <div className="upload-list-container">
            {Array.from(Array(1)).map((_, index) => (
              <div className="upload-list">
                <div className="box-1">
                  <div className="image-box">
                    <Image
                    src="/images/Wallet/icnSec.svg"
                    width={80}
                    height={80}
                    />
                  </div>
                  <div className="text-box">
                    <h3>Document Name</h3>
                    <p>Document ID</p>
                  </div>
                </div>
                <div className="box-2">
                  <div className="text-box">
                    <h3>Document Name</h3>
                    <p>Document ID</p>
                  </div>
                </div>
                <div className="box-3">
                    <div className="button-box">
                  <button onClick={handleDownload} ><SystemUpdateAltIcon/></button>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </>
  );
};

export default DocumentList;
