import React from "react";
//import "./wallet.scss";
import Adminheader from "@components/includes/Adminheader";
import Sidebar from "@components/includes/Sidebar";
import { Container, Row, Col } from "react-bootstrap";
import List from "./List";
import Wallet1 from "./Wallet1";
// import Wallet2 from "./Wallet2";
import Wallet4 from "./Wallet4";
import Wallet5 from "./Wallet5";
// import Wallet3 from "./Wallet3";
import WalletUpload from "./WalletUpload";

  
const Wallet = () => {
  return (
    <>
      <div className="admin-first">
        <div className="rel-abs">
          <Adminheader />
        </div>
        <div>
          <Sidebar />
        </div>
        <div className="main-admin">
          <div className="profile-m">
            {/* <List/> */}
             <Wallet1/>
            {/* <WalletUpload/> */}
            {/* <Wallet4/> */}
            {/* <Wallet5/>   */}
            
          </div>
        </div>
      </div>
    </>
  );
};

export default Wallet;
