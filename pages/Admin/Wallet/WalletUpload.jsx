import React, { useState } from "react";
import { useRouter } from "next/router";
import { Row, Col, Container } from "react-bootstrap";
import Image from "next/image";

const WalletUpload = () => {
  const [toggleForm, setToggleForm] = useState(false);
  const [selectedFile, setSelectedFile] = useState(null);
  const [imgName, setImgName] = useState('');
  const router = useRouter();

  const handleImageUpload = async (event) => {
    const file = event.target.files[0];
    setSelectedFile(URL.createObjectURL(file));
    setImgName(event.target.files[0].name);
  };

  const toggleFormHandler = () => {
    setToggleForm(!toggleForm);
  }

  const handleUpload = () => {
    router.push({
      pathname: "/Admin/Wallet/Wallet4",
      query: { selectedFile: selectedFile } // Pass selectedFile as state
    });
  };

  return (
    <>
      <Container className="wallet-upload">
        <Row>
          <Col lg={12} className="m-3">
            <div className="icon-text-div">
              <div className="icn-sec">
                <Image
                  src="/images/Wallet/icnSec.svg"
                  width={135}
                  height={135}
                />
              </div>
              <div className="main-heading">
                <h3 >Documents you might need</h3>
                <p>You can drag the files or folders from file explorer and drop into the drop area.</p>
              </div>

            </div>

          </Col>
        </Row>
        <Row className="wallet1">
          <Col lg={12} className="d-flex flex-column">
            <Row className="second-row my-5">
              <Col lg={5}>
                <div className="IMG-sec1">
                  <div className="img-doc1">
                    {/* <div className="docImg1">
                      <Image src="/images/profile-icon.svg" width={125} height={125} />
                    </div> */}
                    <div className="profile-photo">
                      <div className="photo">
                        {selectedFile ? (
                          <img
                            src={selectedFile}
                            alt="Uploaded"
                            width={407}
                            height={600}
                          />
                        ) : (
                          <Image src="/images/image.png" width={407} height={407} />
                        )}
                      </div>
                    </div>
                    <span className="docText1">Document Preview</span>
                  </div>
                </div>
                <div className="rec-sec1">
                  <div className="rectangle-img1">
                    <Image
                      src="/images/Wallet/Rectangle.svg"
                      alt="rectangle"
                      width={183}
                      height={190}
                    />
                    <dev className="rectangle-img2">
                      <Image
                        src="/images/Wallet/Rectangle2.svg"
                        alt="rectangle"
                        width={95}
                        height={95}
                      />
                    </dev>

                  </div>
                  <div className="frame-img1">
                    <Image
                      src="/images/Wallet/Frame.svg"
                      alt="frame"
                      width={205}
                      height={241}
                    />

                  </div>
                </div>
              </Col>
              <Col lg={6} style={{ margin: "0px auto" }}>
                <Row>
                  <Col>
                    <div className="docu-sec">
                      <h2 className="docu-head">
                        Upload your document or file{" "}
                      </h2>
                      <button className="computer bg-white" onClick={toggleFormHandler}>
                        <img
                          src="/images/Wallet/computer.svg"
                          alt=""
                          className="com-photo"
                        />
                        <span style={{ paddingLeft: "5px" }}>Upload a file from your computer</span>
                        {/* <label className="label">
                          <input type="text" required />
                          <span>Upload a file from your computer</span>
                        </label> */}
                      </button>{" "}
                      &nbsp;
                      <div className="computer bg-white">
                        <img
                          src="/images/Wallet/link.svg"
                          alt=""
                          className="com-photo"
                        />
                        <label className="label">
                          <input type="file" capture required />
                          <span>Upload a file from web link</span>
                        </label>
                      </div>
                    </div>
                  </Col>
                  {
                    toggleForm && <Col>
                      <div className="form-AWU">
                        <h2 className="docu-head">Choose file to upload </h2>
                        <div className="computer">
                          <img
                            src="/images/Wallet/Group.svg"
                            alt=""
                            className="com-photo"
                          />
                          <label className="label">

                            <input type="file" required onChange={handleImageUpload} />
                            {/* <form>
                              <input
                                type="text"
                                value={extractedText}
                                onChange={(event) => setExtractedText(event.target.value)}
                              />
                            </form> */}
                            {
                              !selectedFile ? <span>No file chosen</span> : <span>{imgName}</span>
                            }
                          </label>
                        </div>{" "}
                        &nbsp;
                        <h2 className="docu-head">Document type </h2>
                        <div className="computer">
                          <img
                            src="/images/Wallet/Fram.svg"
                            alt=""
                            className="com-photo"
                          />
                          <label className="label">
                            <input type="file" capture required />
                            <span>No file chosen</span>
                          </label>
                        </div>
                        &nbsp;
                        <h2 className="docu-head">Comment</h2>
                        <textarea
                          name=""
                          id=""
                          className="computer-text"
                          placeholder="Adding comments for document "
                        ></textarea>
                        <div className="bttn-sec">
                          <button className="upload" onClick={handleUpload}>Upload</button>
                          <span className="or">or</span>
                          <span className="Cancel">Cancel</span>
                        </div>
                      </div>
                    </Col>
                  }

                </Row>



              </Col>
            </Row>
          </Col>
        </Row>


      </Container>


    </>
  );
}

export default WalletUpload;