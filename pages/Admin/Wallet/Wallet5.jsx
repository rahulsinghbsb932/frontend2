import React,{useState} from 'react';
import Form from "react-bootstrap/Form";
import { Row, Col,Dropdown, Container,Button,Table } from "react-bootstrap";
import Image from 'next/image';
import { useRouter } from "next/router";

const Wallet5 = () => {
    const [toggleInputForm,setToggleInputForm]=useState(false);
    const [inputData,setInputData]=useState([{}]);
    // const [inputValue,setInputValue]=useState([{subject:"",mark1:"",mark2:"",combined:"",grade:""}]);
    const [subject,setSubject] = useState('');
    const [marks1,setMarks1] = useState('');
    const [marks2,setMarks2] = useState('');
    const [combined,setCombined] = useState('');
    const [grade,setGrade] = useState('');

    const router = useRouter();
    const { selectedFile } = router.query;
    const handleNext = () => {
        router.push({pathname:"/Admin/Wallet/DocumentList",
        query:{ selectedFile: selectedFile }});
    }

    // const onInputChange=(e)=> {
    //     const {name,value}=e.target;
    //     setInputValue({...inputValue,[name]:value})
    //    console.log(inputValue);
        
    // }
    // const buttonClickHandler=(e)=>{
    //     e.preventDefault()
    //     const data={subject,marks1,marks2,combined,grade};
    //     setInputData([{...inputData,data}]);
    //     console.log("Row Data",inputValue)
    // }
    const handleAddRow = (e) => {
        
        const newRow = {subject,marks1,marks2,combined,grade};
        setInputData([...inputData,newRow]);
       // console.log("INPUT DATA",inputData);
        setSubject('');
        setMarks1('');
        setMarks2('');
        setCombined('');
        setGrade('');
        setToggleInputForm(!toggleInputForm);
      };
    return (
        <>
            <Container>

            <Row className='marks'>
                <Col lg={4} className='marksheet'>
                <div className="photo">
                        {selectedFile ? (
                          <img
                            src={selectedFile}
                            alt="Uploaded"
                            width={407}
                            height={600}
                          />
                        ) : (
                          <Image src="/images/Wallet/dummy-marksheet.png" width={407} height={407} />
                        )}
                      </div>
                </Col>
                <Col lg={7} className="bg primary-subtle">
                    <h1 className='heading-text'>Marks of all Attempted Subjects</h1>
                   
                    <div className='table-container'>
                        <Table responsive className="marks-table">
                            <thead>
                                <tr className="bg-white">
                                    <th >
                                        <strong>Subjects</strong>
                                    </th>
                                    <th >
                                        <strong>1st Sem.</strong>
                                    </th>
                                    <th >
                                        <strong>2nd Sem.</strong>
                                    </th>
                                    <th >
                                        <strong>Combined</strong>
                                    </th>
                                    <th >
                                        <strong>Grade</strong>
                                    </th>
                                </tr>
                            </thead>
                            <tbody >
                                    {inputData.map((row, index) => (
                                        <tr key={index}>
                                            <td contenteditable="true">{row.subject}</td>
                                            <td contenteditable="true">{row.marks1}</td>
                                            <td contenteditable="true">{row.marks2}</td>
                                            <td contenteditable="true">{row.combined}</td>
                                            <td contenteditable="true">{row.grade}</td>
                                        </tr>
                                    ))}
                            
                            </tbody>
                        </Table>
                    </div>
                        <Button onClick={()=>setToggleInputForm(!toggleInputForm)} variant="light" >
                        + Add Subjects
                        </Button>
                    <Row>
                        <Col className='mt-3'>
                            {
                               toggleInputForm && <Form className='input-form'>
                               <Form.Group as={Col} lg={2} className="mb-3" controlId="exampleForm.ControlInput1">
                                   <Form.Control 
                                   type="text" 
                                   name="subject" 
                                   value={subject} 
                                   placeholder="Subject"
                                    onChange={(e) => setSubject(e.target.value)} 
                                   className='bg-white'/>
                               </Form.Group>
                               <Form.Group as={Col} lg={2} 
                               className="mb-3" controlId="exampleForm.ControlInput1">
                                   <Form.Control 
                                   type="text" 
                                   name="mark1" 
                                   value={marks1} 
                                   placeholder="Marks"
                                  
                                   onChange={(e) => setMarks1(e.target.value)} 
                                   className='bg-white'/>
                               </Form.Group>
                               <Form.Group as={Col} lg={2} className="mb-3" controlId="exampleForm.ControlInput1">
                                   <Form.Control 
                                   type="text" 
                                   name="mark2" 
                                   value={marks2} 
                                   placeholder="Marks"
                                  
                                   onChange={(e) => setMarks2(e.target.value)}
                                   className='bg-white'/>
                               </Form.Group>
                               <Form.Group as={Col} lg={2} className="mb-3" controlId="exampleForm.ControlInput1">
                                   <Form.Control 
                                   type="text" 
                                   name="combined" 
                                   value={combined} 
                                   placeholder="Combined"
                                  
                                   onChange={(e) => setCombined(e.target.value)}
                                   className='bg-white'/>
                               </Form.Group>
                               <Form.Group as={Col} lg={2} className="mb-3" controlId="exampleForm.ControlInput1">
                                   <Form.Control 
                                   type="text" 
                                   name="grade" 
                                   value={grade} 
                                   placeholder="Grade"
                                  
                                   onChange={(e) => setGrade(e.target.value)} 
                                   className='bg-white'/>
                               </Form.Group>
                                       <Button className='input-submit' onClick={handleAddRow} variant="success" size="sm" type="submit">
                                           Submit
                                       </Button>
                           </Form>
                            }
                        
                        </Col>
                    </Row>
                    <Row className='m-3'>
                        <Col className='my-1'>
                            <Dropdown>
                                <span className='image-span'>
                                    <Image
                                        src="/images/Wallet/contacticon.svg"
                                        alt="university icon"
                                        width={20}
                                        height={20}
                                    />
                                </span>
                                <Dropdown.Toggle variant="light" className='drop-btn' id="dropdown-basic">
                                Result: PASS
                                </Dropdown.Toggle>

                                <Dropdown.Menu>
                                    <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                                    <Dropdown.Item href="#/action-2">Another action</Dropdown.Item>
                                    <Dropdown.Item href="#/action-3">Something else</Dropdown.Item>
                                </Dropdown.Menu>
                            </Dropdown>
                        </Col>
                        <Col className='my-1'>
                            <Dropdown>
                                <span className='image-span'>
                                    <Image
                                        src="/images/Wallet/contacticon.svg"
                                        alt="university icon"
                                        width={20}
                                        height={20}
                                    />
                                </span>
                                <Dropdown.Toggle variant="light" id="dropdown-basic">
                                Fail in Practical
                                </Dropdown.Toggle>

                                <Dropdown.Menu>
                                    <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                                    <Dropdown.Item href="#/action-2">Another action</Dropdown.Item>
                                    <Dropdown.Item href="#/action-3">Something else</Dropdown.Item>
                                </Dropdown.Menu>
                            </Dropdown>
                        </Col>
                        <Col className='my-1'>
                            <Dropdown>
                                <span className='image-span'>
                                    <Image
                                        src="/images/Wallet/contacticon.svg"
                                        alt="university icon"
                                        width={20}
                                        height={20}
                                    />
                                </span>
                                <Dropdown.Toggle variant="light" id="dropdown-basic">
                                Fail in Theory
                                </Dropdown.Toggle>

                                <Dropdown.Menu>
                                    <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                                    <Dropdown.Item href="#/action-2">Another action</Dropdown.Item>
                                    <Dropdown.Item href="#/action-3">Something else</Dropdown.Item>
                                </Dropdown.Menu>
                            </Dropdown>
                        </Col>
                        <Col className='my-1'>
                            <Dropdown>
                                <span className='image-span'>
                                    <Image
                                        src="/images/Wallet/contacticon.svg"
                                        alt="university icon"
                                        width={20}
                                        height={20}
                                    />
                                </span>
                                <Dropdown.Toggle variant="light" id="dropdown-basic">
                                Absent
                                </Dropdown.Toggle>

                                <Dropdown.Menu>
                                    <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                                    <Dropdown.Item href="#/action-2">Another action</Dropdown.Item>
                                    <Dropdown.Item href="#/action-3">Something else</Dropdown.Item>
                                </Dropdown.Menu>
                            </Dropdown>
                        </Col>
                    </Row>
                    <Button type="next" onClick={handleNext}>Next</Button>
                </Col>
            </Row>
            </Container>
          
           
        </>
    );
}

export default Wallet5;