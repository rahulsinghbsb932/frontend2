import React from 'react'
import Form from "react-bootstrap/Form";
import { Row, Col } from "react-bootstrap";
import { useRouter } from "next/router";


const DocumentGallery = () => {
  
  const router = useRouter();
  const { selectedFile } = router.query;

  return (
    <React.Fragment>
        <Row>
          <Col lg={12}>
            <div className="second-main">
              <Row>
                <Col lg={2}>
                  <div className="img-sec">
                    <img src="/images/Wallet/icnSec.svg" alt="Second img" className="second-img" />
                  </div>
                </Col>
                <Col lg={10}>
                  <div className="txt-sec">
                    <h1 className="text-heading">Documents Gallery</h1>
                    <p className="para-text">
                      A picture helps people recognize you and lets you know
                      when you’re signed in to your account
                    </p>

                    <div className="text-flex">
                      <div className="drop-down">
                        <div className="category-s">
                          <Form.Select
                            aria-label="Default select example"
                            className="select-sec"
                          >
                            <option>Categories</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                          </Form.Select>
                        </div>
                        <div className="document-type-s">
                          <Form.Select
                            aria-label="Default select example"
                            className="select-sec"
                          >
                            <option>Document Type</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                          </Form.Select>
                        </div>
                        <div className="all-date-s">
                          <Form.Select
                            aria-label="Default select example"
                            className="select-sec"
                          >
                            <option>All Date</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                          </Form.Select>
                        </div>
                      </div>
                      <div className="icn-flex">
                        <img src="/images/Wallet/window.svg" alt="icn" />
                        <img src="/images/Wallet/window2.svg" alt="icn" />
                      </div>
                    </div>
                  </div>
                </Col>
              </Row>

              <div className="second-section">
                <div className="second-section-flex">
                  <div className="rel-sec">
                    <img src={ selectedFile } alt="marksheet" className="doc-sec" />
                    <span className="abst">MarkSheet</span>
                  </div>
                  
                </div>

                <div className="btn-next">
                  <span className="txt">01/10</span>
                  <span className="next-bt">Next</span>
                </div>
              </div>
            </div>
          </Col>
        </Row>
      </React.Fragment>
  )
}

export default DocumentGallery
