import React from "react";
import Head from "next/head";
import Image from "next/image";
import PageLayout from "@components/PageLayout";
import { Container, Row, Col } from "react-bootstrap";
import Card from "react-bootstrap/Card";

// import "./services.scss";

// import "../styles/services.scss";

const digitalWallet = () => {
  return (
    <div>
      <Head>
        <title>
          Blockchain Based Wallets For Documents storage | Credentials Wallet
        </title>
        <meta
          id="meta-description"
          name="description"
          content="Securely store and verify your documents with our blockchain-based credentials wallet. Get started with document verification today."
        />
        <meta
          property="og:title"
          content="  Blockchain Based Wallets For Documents storage | Credentials Wallet"
        />
        <meta
          property="og:description"
          content="Securely store and verify your documents with our blockchain-based credentials wallet. Get started with document verification today."
        />
        <meta
          property="twitter:title"
          content="  Blockchain Based Wallets For Documents storage | Credentials Wallet"
        />
        <meta
          property="twitter:description"
          content="Securely store and verify your documents with our blockchain-based credentials wallet. Get started with document verification today."
        />
      </Head>

      <PageLayout>
        <div className="HeroSectionWrapper">
          <Container fluid>
            <Row>
              <Col md={12}>
                <div className="headingWhileLabel Montserrat">
                  <h1>Digital Wallet</h1>
                  <p>
                    Document management is becoming increasingly important as
                    the idea of a paperless office becomes a commonplace
                    reality. Hold up your documents on a single platform
                    effortlessly. Verifykiya enables you to store documents in a
                    blockchain protected Digital Wallet which provides
                    protection against vulnerable situations and hackers.
                  </p>
                </div>
              </Col>
            </Row>
          </Container>

          <Container fluid className="mb-5">
            <Row>
              <Col md={3} className="rowCenter mt-5">
                <a className="cardanchor" href="#Secured-Digital-Wallet">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Digital_Wallet/Secured Digital Wallet.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Digital_Wallet/Secured Digital Wallet.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Secured Digital Wallet
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={3} className="rowCenter mt-5">
                <a
                  className="cardanchor"
                  href="#Upload-both-current-and-previous-credentials"
                >
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Digital_Wallet/Upload Credentials.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Digital_Wallet/Upload Credentials.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Upload both current and previous credentials
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={3} className="rowCenter mt-5">
                <a className="cardanchor" href="#Permissioned-Access">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Digital_Wallet/Permissioned Access.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Digital_Wallet/Permissioned Access.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Permissioned Access
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={3} className="rowCenter mt-5">
                <a className="cardanchor" href="#Digilocker-Integration">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Digital_Wallet/Digilocker Integration.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Digital_Wallet/Digilocker Integration.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Digilocker Integration
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={3} className="rowCenter mt-5">
                {" "}
                <a className="cardanchor" href="#Data Protection">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Digital_Wallet/Data Protection.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Digital_Wallet/Data Protection.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Data Protection
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={3} className="rowCenter mt-5">
                {" "}
                <a className="cardanchor" href="#Automated-Resume-Builder">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Digital_Wallet/Automated Resume Builder.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Digital_Wallet/Automated Resume Builder.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Automated Resume Builder
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={3} className="rowCenter mt-5">
                {" "}
                <a className="cardanchor" href="#Social-Media-integration">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Digital_Wallet/Social Media Integration.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Digital_Wallet/Social Media Integration.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Social Media integration
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>

              <Col md={3} className="rowCenter mt-5"></Col>
            </Row>
          </Container>
        </div>

        <section id="Secured-Digital-Wallet">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Digital_Wallet/Secured Digital Wallet.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Digital_Wallet/Secured Digital Wallet.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Digital_Wallet/Secured Digital Wallet.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Digital_Wallet/Secured Digital Wallet.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Secured Digital Wallet
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Document management is becoming increasingly important as
                      the idea of a paperless office becomes a commonplace
                      reality. Hold up your documents on a single platform
                      effortlessly. Verifykiya enables you to store documents in
                      a blockchain protected Digital Wallet which provides
                      protection against vulnerable situations and hackers.
                      Securely share all your privy documents with great ease.
                      Digital wallets are secured by two level authentication
                      and additionally 2FA can be configured for enhanced
                      security.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section id="Upload-Credentials">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Digital_Wallet/Upload Credentials.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Digital_Wallet/Upload Credentials.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Upload both current and previous credentials
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Miffed with storing your documents physically? Here is the
                      easy and time saving solution. Verifykiya facilitates you
                      to digitize your former and existing credentials
                      hasslefree. A reliable digital wallet to have full control
                      over your data. You can access all your data from anywhere
                      at any time. Similar to physical credentials you can store
                      your credentials in a blockchain protected wallet with
                      complete confidence.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
                <Col xl={3} lg={12} className="order-xl-2 order-1">
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Digital_Wallet/Upload both current and previous credentials.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid "
                    /> */}

                    <Image
                      src="/images/Services/images/Digital_Wallet/Upload both current and previous credentials.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section id="Permissioned-Access">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Digital_Wallet/Permissioned Access.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Digital_Wallet/Permissioned Access.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Digital_Wallet/Permissioned Access.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Digital_Wallet/Permissioned Access.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Permissioned Access
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      An important feature of Verifykiya is “Permissioned
                      Access” which allows you to control who can view and
                      access the stored documents. With this, Verifykiya helps
                      keep your stored documents in your wallet away from the
                      risk of unauthorized access and data breach. The users
                      will be able to access permissions as needed, giving you
                      greater control. Adding on it would prevent the physical
                      exchange of documents, reducing the possibility of
                      documents being misplaced or shared through any insecure
                      source.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section id="Digilocker-Integration">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Digital_Wallet/Digilocker Integration.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Digital_Wallet/Digilocker Integration.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Digital_Wallet/Digilocker Integration.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Digital_Wallet/Digilocker Integration.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Digilocker Integration
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Verifykiya is fully integrated with Digilocker. Now
                      connect your Digilocker wallet with VerifyKiya’s digital
                      certificate wallet and access all your documents from one
                      place. You will be able to access those credentials which
                      has been generated on Verifykiya’s platform or stored on
                      Digilocker on a single platform either on Digilocker or
                      Verifykiya
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section id="Data-Protection">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Digital_Wallet/Data Protection.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Digital_Wallet/Data Protection.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">Data Protection</h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      With VerifyKiya’s Blockchain based NFT-enabled digital
                      certification, never worry again about protecting your
                      documents, certificates, and marksheets, from getting
                      lost, damaged, or corrupted. All your digital
                      certificates, marksheets, and documents will be stored on
                      the blockchain so that they are immutable and remain safe
                      on blockchain, and you can secure your lifelong
                      achievements with just a click. With the increase in use
                      of cloud storage, data theft has also risen. With our
                      solution, your documents are always protected and
                      available under all circumstances. No one can access your
                      documents unless you grant them permission.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
                <Col xl={3} lg={12} className="order-xl-2 order-1">
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Digital_Wallet/Data Protection.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Digital_Wallet/Data Protection.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section id="Automated-Resume-Builder">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Digital_Wallet/Automated Resume Builder.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Digital_Wallet/Automated Resume Builder.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Digital_Wallet/Automated Resume Builder.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Digital_Wallet/Automated Resume Builder.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Automated Resume Builder
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      VerifyKiya's solution can be easily integrated with your
                      SAP, LMS or any other software without much disruption to
                      your normal processes. Companies can focus on their normal
                      processes without having to develop software like ours
                      from scratch.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section id="Social-Media-integration" className="mb-5">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Digital_Wallet/Social Media Integration.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Digital_Wallet/Social Media Integration.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Social Media integration
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Share your achievements in real-time with your friends and
                      family, and let them be a part of your journey and
                      happiness. Let your loved ones know what’s going on in
                      your academic/professional life with just a click. With
                      VerifyKiya’s social media integration, never fall behind
                      in sharing your badges, certificates, and achievements as
                      you make progress with your skills and career.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Digital_Wallet/Social Media integration.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Digital_Wallet/Social Media integration.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
      </PageLayout>
    </div>
  );
};

export default digitalWallet;
