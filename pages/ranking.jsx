import React, { useState } from "react";
import Card from "react-bootstrap/Card";
import { Container, Row, Col, Table } from "react-bootstrap";
import Head from "next/head";
import Image from "next/image";
import PageLayout from "@components/PageLayout";
import Signup from "./form/Signup";
import RegisterRecieve from "./form/RegisterRecieve";
import Subscribe from "./form/Subscribe";

const Ranking = () => {
  const [row, SetRow] = useState([
    {
      id: "1",
      university: "Stanford Graduate School of Business",
      state: "Boston (MA), United States",
      overall: "100.0",
      Academic: "100.0",
      employer: "100.0",
    },
    {
      id: "2",
      university: "Harvard Business School",
      state: "Boston (MA), United States",
      overall: "100.0",
      Academic: "90.0",
      employer: "70.0",
    },
  ]);
  return (
    <div>
      <Head>
        <title>Certified Corporate | Business Verification Services</title>
        <meta
          id="meta-description"
          name="description"
          content=" This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
        <meta
          property="og:title"
          content="Certified Corporate | Business Verification Services"
        />
        <meta
          property="og:description"
          content="This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
        <meta
          property="twitter:title"
          content="Certified Academy | Academy Verification | VerifyKiya"
        />
        <meta
          property="twitter:description"
          content="This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
      </Head>
      <PageLayout>
        <div className="resources">
          <div className="HeroSectionWrapper">
            <div className="frame1 d-xl-block d-none"></div>
            <div className="frame2 d-xl-block d-none"></div>
            <Container fluid>
              <Row>
                <Col md={12}>
                  <div className="headingWhileLabel text-center mb-5">
                    <h1>World University Rankings</h1>
                    <h4 className="mb-4">
                      Discover The World's Top Universities. Explore The
                      University Rankings 2023.
                    </h4>
                    <p>
                      Our rankings are the fast and easy way to compare
                      institutions on a host of different criteria: from
                      academic reputation to the number of international
                      students enrolled. You’ll find even more information about
                      each university on their dedicated QS profile page, where
                      you can explore the full range of study options available
                      and enquire with their admissions team.
                    </p>
                  </div>
                </Col>
              </Row>
            </Container>

            <Container fluid>
              <Row>
                <Col lg={6}>
                  <Card>
                    <Image
                      src="/images/ranking/icon-1.svg"
                      height={107}
                      width={107}
                    />
                    <Card.Body>
                      <Card.Title>
                        <strong>QS World University Rankings</strong>
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </Col>
                <Col lg={6}>
                  <Card>
                    <Image
                      src="/images/ranking/icon-2.svg"
                      height={107}
                      width={107}
                    />
                    <Card.Body>
                      <Card.Title>
                        <strong>Times Higher Education</strong>
                      </Card.Title>
                      <Card.Text>World University Rankings</Card.Text>
                    </Card.Body>
                  </Card>
                </Col>
              </Row>
            </Container>
            <div className="frame3"></div>
            <div className="frame4"></div>
          </div>
          <div className="ranking wrapper">
            <Container fluid>
              <Row>
                <Col xl={9} lg={12}>
                  <div className="d-flex flex-wrap gap-5 mb-5">
                    <div className="wrap">World Ranking</div>
                    <div className="wrap">Region Wise</div>
                    <div className="wrap">Country Wise</div>
                    <div className="wrap">Subject Wise</div>
                  </div>
                  <div className="bg">
                    <Table borderedless responsive="sm">
                      <thead>
                        <tr>
                          <th>Rank</th>
                          <th>University</th>
                          <th>Overall</th>
                          <th>Academic</th>
                          <th>Employer</th>
                        </tr>
                      </thead>
                      <tbody>
                        {row.map((rows, index) => {
                          return (
                            <tr>
                              <td>
                                <span className="arrow">{rows.id}</span>
                              </td>
                              <td>
                                <div className="d-flex gap-3">
                                  <div className="icons">
                                    <Image
                                      src="/images/ranking/icon-1.svg"
                                      height={30}
                                      width={30}
                                    />
                                  </div>
                                  <div className="content">
                                    <h6 className="m-0">
                                      <strong>{rows.university}</strong>
                                    </h6>
                                    <p className="text-black m-0">
                                      {rows.state}
                                    </p>
                                  </div>
                                </div>
                              </td>
                              <td>{rows.overall}</td>
                              <td>{rows.Academic}</td>
                              <td>{rows.employer}</td>
                            </tr>
                          );
                        })}
                      </tbody>
                    </Table>
                    <div className="pull-right me-5 mb-2">
                      <div className="more">
                        <div className="circle opacity-50"></div>
                        <div className="circle opacity-75"></div>
                        <div className="circle"></div>
                        <div className="arrow"></div>
                        <div>
                          <strong>Show more...</strong>
                        </div>
                      </div>
                    </div>
                  </div>
                </Col>
                <Col xl={3} lg={12}>
                <Signup />
                    <RegisterRecieve />
                    <Subscribe />
                </Col>
              </Row>
            </Container>
          </div>
        </div>
      </PageLayout>
    </div>
  );
};

export default Ranking;
