import React from "react";
import { Form, InputGroup} from "react-bootstrap";
const Subscribe = () => {
  return (
    <div>
      <div className="subscribe-form my-4">
        <div className="text-center">
          <img src="/images/result/subscribe.png" alt="img" />
        </div>
        <h1>Subscribe</h1>
        <h2>To our newsletter</h2>
        <Form>
          <InputGroup size="sm" className="my-3">
            <Form.Control
              aria-label="Small"
              aria-describedby="inputGroup-sizing-sm"
              placeholder="PHONE OR EMAIL"
            />
          </InputGroup>
          <div className="text-center">
            <button className="btn btn-primary">Submit</button>
          </div>
        </Form>
      </div>
    </div>
  );
};

export default Subscribe;
