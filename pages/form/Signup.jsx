import React from "react";
import { Form, InputGroup} from "react-bootstrap";

const Signup = () => {
  return (
    <div>
      <div className="side-form my-lg-0 my-4">
        <Form>
          <div className="text-center">
            <img src="/images/result/sign.png" alt="img" />
          </div>
          <h2>Sign up</h2>
          <h3>FOR UNIVERSITIES & EDUCATIONAL INSTITUTIONS</h3>
          <InputGroup size="sm" className="my-3">
            <Form.Control
              aria-label="Small"
              aria-describedby="inputGroup-sizing-sm"
              placeholder="PHONE OR EMAIL"
            />
          </InputGroup>
          <InputGroup size="sm" className="mb-3">
            <Form.Control
              aria-label="Small"
              aria-describedby="inputGroup-sizing-sm"
              placeholder="OTP"
            />
          </InputGroup>
          <div className="text-center mb-3">
            <button>Sign up</button>
          </div>
          <p>
            Don't want to miss out on Board updates? Fill in and register to
            receive exclusive and reliable:
          </p>
          <ul>
            <li>Board details such as dates </li>
            <li>Time-table</li>
            <li>Results</li>
            <li>Exam pattern</li>
            <li>Sample papers and </li>
            <li>Much more!</li>
          </ul>
        </Form>
      </div>
    </div>
  );
};

export default Signup;
