import React from "react";
import { Form, InputGroup} from "react-bootstrap";

const RegisterRecieve = () => {
  return (
    <div>
      <div className="register-form my-4">
        <h1>Register & Receive</h1>
        <h2>Results Updates</h2>
        <Form>
          <InputGroup size="sm" className="my-3">
            <Form.Control
              aria-label="Small"
              aria-describedby="inputGroup-sizing-sm"
              placeholder="PHONE OR EMAIL"
            />
          </InputGroup>
          <InputGroup size="sm" className="my-3">
            <Form.Control
              aria-label="Small"
              aria-describedby="inputGroup-sizing-sm"
              placeholder="PHONE OR EMAIL"
            />
          </InputGroup>
          <InputGroup size="sm" className="my-3">
            <Form.Control
              aria-label="Small"
              aria-describedby="inputGroup-sizing-sm"
              placeholder="PHONE OR EMAIL"
            />
          </InputGroup>
          <InputGroup size="sm" className="my-3">
            <Form.Control
              aria-label="Small"
              aria-describedby="inputGroup-sizing-sm"
              placeholder="PHONE OR EMAIL"
            />
          </InputGroup>
          <InputGroup size="sm" className="my-3">
            <Form.Control
              aria-label="Small"
              aria-describedby="inputGroup-sizing-sm"
              placeholder="PHONE OR EMAIL"
            />
          </InputGroup>
          <div className="text-center">
            <button className="btn btn-primary">Submit</button>
          </div>
        </Form>
      </div>
    </div>
  );
};

export default RegisterRecieve;
