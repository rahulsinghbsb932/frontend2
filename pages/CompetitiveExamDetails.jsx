import React from "react";
import { Col, Container, Row, Form, InputGroup } from "react-bootstrap";

import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import Typography from "@mui/material/Typography";
import PageLayout from "@components/PageLayout";
import Head from "next/head";
import Signup from "./form/Signup";
import RegisterRecieve from "./form/RegisterRecieve";
import Subscribe from "./form/Subscribe";
// import ExpandMoreIcon from "@mui/icons-material/ExpandMore";

const DiplomaCourses = [
  {
    name: "Diploma in Engineering",
  },
  {
    name: "Diploma in Hotel Management",
  },
  {
    name: "Diploma in Journalism",
  },
  {
    name: "Diploma in Education",
  },
  {
    name: "Diploma in Photography",
  },
  {
    name: "Diploma in Peychulagy",
  },
  {
    name: "Diploma in Elementary Education",
  },
  {
    name: "Diploma in Digital Marketing",
  },
  {
    name: "Diploma in Fine Arts",
  },
  {
    name: "Diploma in English",
  },
];

const DiplomaBranch = [
  {
    profile: "Course Overview",
    name: "Diploma in Automobile Engineering",
  },
  {
    profile: "Eligibility Criteria",
    name: "Diploma in Electrical Engineering",
  },
  {
    profile: "Subjects",
    name: " Diploma in Mechanical Engineering",
  },
  {
    profile: "Popular Courses",
    name: "Diploma in Automobile Engineering",
  },
];

const PopularCourses = [
  {
    img: "/images/checkIcon.svg",
    degree: "Bachelor/Undergradute",
    courses: "Arts, Humanities and Social Science",
  },
  {
    img: "/images/checkIcon.svg",
    degree: "Master with Pre-Master Programme",
    courses: "Medicine, Pharmacy , Health and Life Sciences",
  },

  {
    img: "/images/checkIcon.svg",
    degree: "Bachelor with Foundation Year",
    courses: "Computer Science, Data Sciene & IT",
  },
  {
    img: "/images/checkIcon.svg",
    degree: "PhD / Doctorate / Master",
    courses: "Science",
  },
];

const UniversityDetails = [
  {
    logo: "/images/CoursesDetails/Stanford.svg",
    name: "Stanford Graduate School of Business",
    place: "Stanford (CA), United States",
  },
  {
    logo: "/images/CoursesDetails/Stanford.svg",
    name: "Stanford Graduate School of Business",
    place: "Stanford (CA), United States",
  },
  {
    logo: "/images/CoursesDetails/Stanford.svg",
    name: "Stanford Graduate School of Business",
    place: "Stanford (CA), United States",
  },
  {
    logo: "/images/CoursesDetails/Stanford.svg",
    name: "Stanford Graduate School of Business",
    place: "Stanford (CA), United States",
  },
];

const CompetitiveExamDetails = () => {
  return (
    <>
      <Head>
        <title>Competitive Exam Details | Business Verification Services</title>
        <meta
          id="meta-description"
          name="description"
          content=" This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
        <meta
          property="og:title"
          content="Competitive Exam Details | Business Verification Services"
        />
      </Head>
      <PageLayout>
        <div className="HeroSectionWrapper CoursesDetails">
          <Container fluid>
            <Row>
              <Col lg={12}>
                <div className="HeroSection">
                  <h1 className="HeroSection-Heading">JEE Advanced 2023</h1>
                  <p className="HeroSection-Para">
                    JEE Advanced is an engineering entrance exam conducted for
                    B.Tech admission to IIT. The exam is held only for top
                    2,50,000 students who qualify JEE Main. JEE Advanced scores
                    are accepted at 23 IIT for B.Tech/B.E., B.Arch admissions.
                  </p>
                  <div>
                    <div>
                      <strong>April 24, 2023 -</strong> Online Registration for
                      JEE (Advanced) 2023 is now open for OCI/PIO and Foreign
                      National Candidates who have NOT participated in JEE
                      (Main) 2023.Please to visit the Registration Portal.
                    </div>
                    <div>
                      <strong>April 22, 2023 -</strong> Updated FAQs are now
                      available on the website.{" "}
                    </div>
                    <div>
                      <strong>March 23, 2023 -</strong> Information Brochure
                      V.2.0 in Hindi)
                    </div>
                    <div>
                      <strong>March 16, 2023 -</strong> Press Statement{" "}
                    </div>
                    <div>
                      <strong>March 16, 2023 -</strong> The updated JEE
                      (Advanced) 2023 Information Brochure V.2.0, information
                      for foreign national candidates and OCI/PIO card holders,
                      and registration fees are now available on the website.
                    </div>
                  </div>

                  <div className="Sub-HeroSection">
                    <Row>
                      <Col lg={4}>
                        <Row>
                          <Col lg={2}>
                            <div className="Sub-HeroSection-Img">
                              <img
                                src="/images/checkIcon.svg"
                                className="Sub-HeroSection-Img-Icn"
                                alt=""
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <div className="Sub-HeroSection-Text">
                              <p className="Sub-HeroSection-Text-Para">
                                Class XI-Class XII
                              </p>
                            </div>
                          </Col>
                        </Row>
                      </Col>
                      <Col lg={4}>
                        <Row>
                          <Col lg={2}>
                            <div className="Sub-HeroSection-Img">
                              <img
                                src="/images/checkIcon.svg"
                                className="Sub-HeroSection-Img-Icn"
                                alt=""
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <div className="Sub-HeroSection-Text">
                              <p className="Sub-HeroSection-Text-Para">
                                Diploma Courses
                              </p>
                            </div>
                          </Col>
                        </Row>
                      </Col>
                      <Col lg={4}>
                        <Row>
                          <Col lg={2}>
                            <div className="Sub-HeroSection-Img">
                              <img
                                src="/images/checkIcon.svg"
                                className="Sub-HeroSection-Img-Icn"
                                alt=""
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <div className="Sub-HeroSection-Text">
                              <p className="Sub-HeroSection-Text-Para">
                                Polytechnic Courses
                              </p>
                            </div>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                    <Row>
                      <Col lg={4}>
                        <Row>
                          <Col lg={2}>
                            <div className="Sub-HeroSection-Img">
                              <img
                                src="/images/checkIcon.svg"
                                className="Sub-HeroSection-Img-Icn"
                                alt=""
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <div className="Sub-HeroSection-Text">
                              <p className="Sub-HeroSection-Text-Para">
                                Paramedical Courses
                              </p>
                            </div>
                          </Col>
                        </Row>
                      </Col>
                      <Col lg={4}>
                        <Row>
                          <Col lg={2}>
                            <div className="Sub-HeroSection-Img">
                              <img
                                src="/images/checkIcon.svg"
                                className="Sub-HeroSection-Img-Icn"
                                alt=""
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <div className="Sub-HeroSection-Text">
                              <p className="Sub-HeroSection-Text-Para">
                                ITI Courses
                              </p>
                            </div>
                          </Col>
                        </Row>
                      </Col>
                      <Col lg={4}>
                        <Row>
                          <Col lg={2}>
                            <div className="Sub-HeroSection-Img">
                              <img
                                src="/images/checkIcon.svg"
                                className="Sub-HeroSection-Img-Icn"
                                alt=""
                              />
                            </div>
                          </Col>
                          <Col lg={10}>
                            <div className="Sub-HeroSection-Text">
                              <p className="Sub-HeroSection-Text-Para">
                                Vocational Courses
                              </p>
                            </div>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                  </div>
                </div>
              </Col>
            </Row>
          </Container>
        </div>

        <div className="DiplomaCourses">
          <h1 className="DiplomaCourses-inner-heading">Diploma Courses</h1>
          <Container fluid>
            <Row>
              <Col lg={12}>
                <div className="DiplomaCourses-inner">
                  <ul className="DiplomaCourses-branch">
                    {DiplomaCourses.map((val) => {
                      return (
                        <li className="DiplomaCourses-branch-width">
                          {val.name}
                        </li>
                      );
                    })}
                  </ul>
                </div>
              </Col>
            </Row>
          </Container>
        </div>
        <Container fluid>
          <div className="d-flex flex-wrap flex-lg-nowrap ">
            <div className="mx-auto">
              <div className="frame-width">
                <iframe
                  src="/images/1.png"
                  scrolling="no"
                  width="100%"
                  height="100%"
                  class="responsive-iframe"
                ></iframe>
              </div>
            </div>
          </div>
        </Container>

        <div className="MidSection ">
          <Container fluid>
            <Row>
              <Col lg={9}>
                <div className="MidSection-component">
                  <div className="MidSection-component-inner">
                    <div className="align-items-end d-flex flex-wrap">
                      <div className="me-3">
                        <div className="MidSection-component-img">
                          <img
                            src="/images/CoursesDetails/midicon.svg"
                            alt=""
                            className="img-fluid"
                          />
                        </div>
                      </div>
                      <div>
                        <h1 className="MidSection-Conponent-heading">
                          Diploma in Engineering
                        </h1>
                      </div>
                    </div>
                  </div>
                  <div className="MidSection-Conponent-para">
                    <p className="MidSection-Conponent-para-inner">
                      Diploma in Engineering is a great option for those who
                      want to pursue technical education as well as start
                      working early. The course concentrated on skill
                      development and one is likely to master advanced
                      computing, scientific skills, and mathematical techniques
                      in the course duration.
                    </p>
                  </div>
                  <hr />

                  <div className="MidSection-Accordian">
                    {DiplomaBranch.map((val, index) => {
                      return (
                        <div key={index}>
                          <Accordion
                            style={{
                              backgroundColor: "unset",
                              boxShadow: "unset",
                              zIndex: 50,
                            }}
                          >
                            <AccordionSummary
                              sx={{
                                transform: "unset",
                                color: "white",
                              }}
                              expandIcon="Know more"
                              aria-controls="panel1a-content"
                              id="panel1a-header"
                            >
                              <Typography className="accor-heading">
                                {val.profile}
                              </Typography>
                            </AccordionSummary>
                            <AccordionDetails>
                              <Typography>
                                {/* <p className="accor1"></p> */}

                                {/* <ul className="accor1-branch">
                                <li className="accor1-branch-width"> */}
                                {val.name}
                                {/* </li>
                              </ul> */}
                              </Typography>
                            </AccordionDetails>
                          </Accordion>
                        </div>
                      );
                    })}
                  </div>
                </div>

                <div className="frame-width ">
                  <iframe
                    src="/images/1.png"
                    height="100"
                    width="100%"
                    scrolling="no"
                  ></iframe>
                </div>

                <div className="MidSection-secondComponent">
                  <div className="MidSection-secondConmponent-img">
                    <img
                      src="/images/CoursesDetails/mssci1.svg"
                      alt=""
                      className="img-fluid"
                    />
                  </div>

                  <div className="MidSection-secondConmponent-hero">
                    <h1 className="MidSection-secondConmponent-hero-heading">
                      Popular Courses
                    </h1>

                    <p className="MidSection-secondConmponent-hero-para">
                      Though there is no perfect time to study abroad, you’ll
                      find certain school years and semesters to be most common
                      for students seeking an international academic experience.
                      While it isn’t unusual to hear of high school graduates
                      going overseas before their freshman year in college,
                      sophomore and junior years are often popular for study
                      abroad. By this time students have realized why everyone
                      should study abroad, and are more likely to have narrowed
                      down the direction of their major.
                    </p>
                  </div>

                  <div className="MidSection-secondConmponent-hero-courses">
                    {PopularCourses.map((val) => {
                      return (
                        <>
                          <div className="MidSection-secondConmponent-hero-courses-inner">
                            <div className="MidSection-secondConmponent-hero-courses-inner-img">
                              <img
                                src={val.img}
                                className="Sub-HeroSection-Img-Icn"
                                alt=""
                              />
                            </div>
                            <div className="MidSection-secondConmponent-hero-courses-inner-text">
                              <h1 className="MidSection-secondConmponent-hero-courses-inner-text-head">
                                {val.degree}
                              </h1>
                              <h1 className="MidSection-secondConmponent-hero-courses-inner-text-para">
                                {val.courses}
                              </h1>
                            </div>
                          </div>
                        </>
                      );
                    })}
                  </div>

                  <div className="more">
                    <div className="circle opacity-50"></div>
                    <div className="circle opacity-75"></div>
                    <div className="circle"></div>
                    <div className="arrow"></div>
                    <div>
                      <strong>Know more</strong>
                    </div>
                  </div>
                </div>

                <div className="frame-width ">
                  <iframe
                    src="/images/1.png"
                    title="description"
                    height="100"
                    width="100%"
                    scrolling="no"
                  ></iframe>
                </div>

                <div className="MidSection-secondComponent">
                  <div className="MidSection-secondConmponent-imgg">
                    <img
                      src="/images/CoursesDetails/mssci1.svg"
                      alt=""
                      className="img-fluid"
                    />
                  </div>

                  <div className="MidSection-secondConmponent-hero">
                    <h1 className="MidSection-secondConmponent-hero-heading">
                      Top Universities
                    </h1>

                    <p className="MidSection-secondConmponent-hero-para">
                      Though there is no perfect time to study abroad, you’ll
                      find certain school years and semesters to be most common
                      for students seeking an international academic experience.
                      While it isn’t unusual to hear of high school graduates
                      going overseas before their freshman year in college,
                      sophomore and junior years are often popular for study
                      abroad. By this time students have realized why everyone
                      should study abroad, and are more likely to have narrowed
                      down the direction of their major.
                    </p>
                  </div>

                  <div className="MidSection-secondConmponent-hero-courses">
                    {UniversityDetails.map((val) => {
                      return (
                        <>
                          <div className="MidSection-secondConmponent-hero-courses-inner">
                            <div className="MidSection-secondConmponent-hero-courses-inner-img">
                              <img
                                src={val.logo}
                                className="Sub-HeroSection-Img-Icn"
                                alt=""
                              />
                            </div>
                            <div className="MidSection-secondConmponent-hero-courses-inner-text">
                              <h1 className="MidSection-secondConmponent-hero-courses-inner-text-head">
                                {val.name}
                              </h1>
                              <h1 className="MidSection-secondConmponent-hero-courses-inner-text-para">
                                {val.place}
                              </h1>
                            </div>
                          </div>
                        </>
                      );
                    })}
                  </div>

                  <div className="more">
                    <div className="circle opacity-50"></div>
                    <div className="circle opacity-75"></div>
                    <div className="circle"></div>
                    <div className="arrow"></div>
                    <div>
                      <strong>Know more</strong>
                    </div>
                  </div>
                </div>
              </Col>
              <Col lg={3}>
                <iframe
                  src="/images/host.png"
                  scrolling="no"
                  width="100%"
                  height="250"
                  class="responsive-iframe"
                ></iframe>
                <div className="resources">
                  <div className="contact-box">
                    <div className="w-300 ">
                      <Signup />
                      <RegisterRecieve />
                      <Subscribe />
                    </div>
                  </div>
                </div>
              </Col>
            </Row>
          </Container>
        </div>
      </PageLayout>
    </>
  );
};

export default CompetitiveExamDetails;
