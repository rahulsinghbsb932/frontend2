import React, { useState } from "react";
import {
  Container,
  Row,
  Col,
  Card,
  Table,
  FormLabel,
  Button,
  Form,
} from "react-bootstrap";
import Head from "next/head";
import Link from "next/link";
import Image from "next/image";
import PageLayout from "@components/PageLayout";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import ListItemText from "@mui/material/ListItemText";
import Select from "@mui/material/Select";
import Checkbox from "@mui/material/Checkbox";

const InstituteDetails = () => {
  // multiple select in mui
  const ITEM_HEIGHT = 48;
  const ITEM_PADDING_TOP = 4;
  const MenuProps = {
    PaperProps: {
      style: {
        maxHeight: ITEM_HEIGHT * 3.5 + ITEM_PADDING_TOP,
        maxWidth: "100%",
        backgroundColor: "#B1B2FF",
        borderColor: "#B1B2FF",
        borderRadius: 1,
      },
    },
  };

  const names = [
    "Boston (6)",
    "Chicago (6)",
    "Hawaii (6)",
    "Los Angeles (6)",
    "Miami (5)",
    "New York (14)",
    "San Diego (1)",
    "San Francisco (9)",
    "Washington D.C. (5)",
  ];
  const courses = [
    "Masters Degree",
    "Bachelors Degree",
    "PG Diploma",
    "UG Diploma",
    "Short program",
    "PhD",
    "Diploma",
    "MBA",
    "Short Term",
  ];
  const tution = [
    "No Budget Constraints",
    "Upto 5 Lakh INR",
    "Upto 10 Lakh INR",
    "Upto 15 Lakh INR",
    "Upto 25 Lakh INR",
    "Upto 35 Lakh INR",
    "Upto 45 Lakh INR",
    "Upto 55 Lakh INR",
    "More than 55Lakh INR",
  ];
  const [personName, setPersonName] = React.useState([]);
  const [coursesName, setCoursesName] = React.useState([]);
  const [tutionName, setTutionName] = React.useState([]);

  const handleChange = (event) => {
    const {
      target: { value },
    } = event;
    setPersonName(
      // On autofill we get a stringified value.
      typeof value === "string" ? value.split(",") : value
    );
  };
  const coursesHandleChange = (event) => {
    const {
      target: { value },
    } = event;
    setCoursesName(
      // On autofill we get a stringified value.
      typeof value === "string" ? value.split(",") : value
    );
  };
  const tutionHandleChange = (event) => {
    const {
      target: { value },
    } = event;
    setTutionName(
      // On autofill we get a stringified value.
      typeof value === "string" ? value.split(",") : value
    );
  };
  // multiple select in mui
  const [items, setItems] = useState([
    {
      title: "UPSC Course - Regular",
      para: "Central Delhi Classroom Programs",
      content:
        "UPSC is one of the toughest examinations. The institute helps the students in making the examination preparation a little easier by giving the right guidance to the students. The institute also conducts test series and provide study material that is made by the best and experienced faculty of the institute. The institute conducts classes for 3 to 4 hour long on weekdays and 7 to 8 hours long on weekends.",
      course: "UPSC",
      payment: "Part Payment Avialable",
      courses: "UPSC,PCS",
      classes: "6 days",
      coursetype: "Regular, Online, Test Series",
      duration: "1 years",
      batch: "01 May 2023",
      total: "1.6 Lakh",
      coaching: "UPSC",
      fees: "160000",
      entrance: "conducted",
      scholarships: "Provided",
      doubt: "Conducted",
      Mock: "Conducted",
      test: "Conducted",
      highlights: {
        heading: "Highlights",
        list: {
          list1: "Innovative Learning methodologies.",
          list2: "A dedicated team of faculties.",
          list3: "Multiple Batches",
          list4: "Innovative Learning methodologies.",
        },
      },
    },
    {
      title: "UPSC Course - Regular",
      para: "Central Delhi Classroom Programs",
      content:
        "UPSC is one of the toughest examinations. The institute helps the students in making the examination preparation a little easier by giving the right guidance to the students. The institute also conducts test series and provide study material that is made by the best and experienced faculty of the institute. The institute conducts classes for 3 to 4 hour long on weekdays and 7 to 8 hours long on weekends.",
      course: "UPSC",
      payment: "Part Payment Avialable",
      courses: "UPSC,PCS",
      classes: "6 days",
      coursetype: "Regular, Online, Test Series",
      duration: "1 years",
      batch: "01 May 2023",
      total: "1.6 Lakh",
      coaching: "UPSC",
      fees: "160000",
      entrance: "conducted",
      scholarships: "Provided",
      doubt: "Conducted",
      Mock: "Conducted",
      test: "Conducted",
      highlights: {
        heading: "Highlights",
        list: {
          list1: "Innovative Learning methodologies.",
          list2: "A dedicated team of faculties.",
          list3: "Multiple Batches",
          list4: "Innovative Learning methodologies.",
        },
      },
    },
    {
      title: "UPSC Course - Regular",
      para: "Central Delhi Classroom Programs",
      content:
        "UPSC is one of the toughest examinations. The institute helps the students in making the examination preparation a little easier by giving the right guidance to the students. The institute also conducts test series and provide study material that is made by the best and experienced faculty of the institute. The institute conducts classes for 3 to 4 hour long on weekdays and 7 to 8 hours long on weekends.",
      course: "UPSC",
      payment: "Part Payment Avialable",
      courses: "UPSC,PCS",
      classes: "6 days",
      coursetype: "Regular, Online, Test Series",
      duration: "1 years",
      batch: "01 May 2023",
      total: "1.6 Lakh",
      coaching: "UPSC",
      fees: "160000",
      entrance: "conducted",
      scholarships: "Provided",
      doubt: "Conducted",
      Mock: "Conducted",
      test: "Conducted",
      highlights: {
        heading: "Highlights",
        list: {
          list1: "Innovative Learning methodologies.",
          list2: "A dedicated team of faculties.",
          list3: "Multiple Batches",
          list4: "Innovative Learning methodologies.",
        },
      },
    },
  ]);
  return (
    <div>
      <Head>
        <title>Institutes | Business Verification Services</title>
        <meta
          id="meta-description"
          name="description"
          content=" This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
        <meta
          property="og:title"
          content="Institutes | Business Verification Services"
        />
        <meta
          property="og:description"
          content="This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
        <meta
          property="twitter:title"
          content="Certified Academy | Academy Verification | VerifyKiya"
        />
        <meta
          property="twitter:description"
          content="This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
      </Head>
      <PageLayout>
        <div className="resources">
          <div className="HeroSectionWrapper">
            <div className="position-relative">
              <div className="frame d-xl-block d-none"></div>
              <div className="frame1 d-xl-block d-none"></div>
              <Container fluid>
                <Row>
                  <Col md={12}>
                    <div className="headingWhileLabel mb-5">
                      <div className="text-center">
                        <h1>Vajiram and Ravi Institute</h1>
                        <h4>Central Delhi, New Delhi, Delhi NCR</h4>
                      </div>
                      <p className="my-5">
                        Vajiram & Ravi is one of the top coaching institutes for
                        preparation for Civil Services examinations. The
                        institute was founded in 1976 through the effort of
                        Prof. P. Velayutham. Vajiram & Ravi handles its single
                        branch in New Delhi, and it is guided by an efficient
                        faculty team. This coaching institute consistently works
                        for the welfare of the students. Many top position
                        holders of IAS are the students from Vajiram & Ravi.
                      </p>
                      <div className="list-center">
                        <ul>
                          <li>
                            Coaching -<strong> UPSC, PSC</strong>
                          </li>
                          <li>
                            Duration - <strong> 4 to 12 Months</strong>
                          </li>
                          <li>
                            Fees - <strong>₹ 7,000 - ₹ 1,60,000</strong>
                          </li>
                          <li>
                            Classes - <strong>6 days/week</strong>
                          </li>
                          <li>
                            Course Type -
                            <strong>Regular, Online, Test Series</strong>
                          </li>
                          <li>
                            Scholarships - <strong>Provided</strong>
                          </li>
                          <li>
                            Entrance exam - <strong>Conducted</strong>
                          </li>
                          <li>
                            Mock exams - <strong>Conducted</strong>
                          </li>
                          <li>
                            Test series - <strong>Conducted</strong>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </Col>
                </Row>
              </Container>
              <div className="frame3 d-xl-block d-none"></div>
              <div className="frame4 d-xl-block d-none"></div>
            </div>
          </div>
          <div className="wrapper">
            <div className="contact pb-5">
              <Container fluid>
              {items.map((item, index) => (
                <div key={index}>
                    <div className="bg mb-5">
                      <div className="h-frame d-xl-block d-none"></div>
                      <div className="z-index">
                        <div className="justify-content-between align-items-center d-flex flex-wrap">
                          <div className="d-flex flex-wrap align-items-end text-white">
                            <div className="me-3 mb-lg-0 mb-2">
                              <div className="icons">
                                <Image
                                  src="/images/institute/icon.svg"
                                  height={100}
                                  width={100}
                                />
                              </div>
                            </div>
                            <div>
                              <div className="end">
                                <div>
                                  <h1>UPSC Course - Regular</h1>
                                  <p className="m-0">Central Delhi Classroom Programs</p>
                                </div>
                                <div></div>
                              </div>
                            </div>
                          </div>
                          <div className="btns d-flex gap-5 me-5">
                            <Link href="/">
                              <Button className="btn btn-white">
                                Download Brochure
                              </Button>
                            </Link>
                            <Link href="">
                              <Button className="btn btn-secondary">
                                Apply Now
                              </Button>
                            </Link>
                          </div>
                        </div>
                        <Row>
                          <Col lg={12} className="my-5 text-white">
                            <p>{item.content}</p>
                          </Col>
                        </Row>
                        <Row className="mb-5">
                          <Col lg={12}>
                            <div className="price">
                              <div className="d-flex justify-content-between align-items-center header flex-wrap">
                                <div className="text d-flex flex-wrap align-items-center gap-2">
                                  <strong>{item.course}-</strong>
                                  {item.payment}
                                </div>
                              </div>
                              <Table responsive borderless>
                                <tbody>
                                  <tr>
                                    <td>Courses</td>
                                    <td>Classes</td>
                                    <td>Course Type</td>
                                    <td>Duration</td>
                                    <td>Batches Dates</td>
                                    <td>Total Fees ₹</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      <strong>{item.courses}</strong>
                                    </td>
                                    <td>
                                      <strong>{item.classes}</strong>
                                    </td>
                                    <td>
                                      <strong>{item.coursetype}</strong>
                                    </td>
                                    <td>
                                      <strong>{item.duration}</strong>
                                    </td>
                                    <td>
                                      <strong>{item.batch}</strong>
                                    </td>
                                    <td>
                                      <strong>{item.total}</strong>
                                    </td>
                                  </tr>
                                </tbody>
                              </Table>
                            </div>
                          </Col>
                        </Row>
                        <Row className="my-5">
                          <Col lg={12}>
                            <div className="price">
                              <div className="d-flex justify-content-between align-items-center header flex-wrap">
                                <div className="text d-flex flex-wrap align-items-center gap-2">
                                  <strong>Course Details</strong>
                                </div>
                              </div>
                              <Table responsive>
                                <tbody>
                                  <tr>
                                    <td>Coaching</td>
                                    <td>:</td>
                                    <td>{item.course}</td>
                                  </tr>
                                  <tr>
                                    <td>Duration</td>
                                    <td>:</td>
                                    <td>{item.duration}</td>
                                  </tr>
                                  <tr>
                                    <td>Fees</td>
                                    <td>:</td>
                                    <td>{item.fees}</td>
                                  </tr>
                                  <tr>
                                    <td>Classes</td>
                                    <td>:</td>
                                    <td>{item.classes}</td>
                                  </tr>
                                  <tr>
                                    <td>Enterance Exam</td>
                                    <td>:</td>
                                    <td>{item.entrance}</td>
                                  </tr>
                                  <tr>
                                    <td>Scholarships</td>
                                    <td>:</td>
                                    <td>{item.scholarships}</td>
                                  </tr>
                                  <tr>
                                    <td>Doubt clearing sessions </td>
                                    <td>:</td>
                                    <td>{item.doubt}</td>
                                  </tr>
                                  <tr>
                                    <td>Mock exams</td>
                                    <td>:</td>
                                    <td>{item.Mock}</td>
                                  </tr>
                                  <tr>
                                    <td>Test series</td>
                                    <td>:</td>
                                    <td>{item.test}</td>
                                  </tr>
                                </tbody>
                              </Table>
                            </div>
                            <div className="list mt-5">
                              <strong className="text-white">
                                {item.highlights.heading} :
                              </strong>
                              <ul className="text-white">
                                <li>{item.highlights.list.list1}</li>
                                <li>{item.highlights.list.list2}</li>
                                <li>{item.highlights.list.list3}</li>
                                <li>{item.highlights.list.list4}</li>
                                <li>{item.highlights.list.list1}</li>
                                <li>{item.highlights.list.list2}</li>
                                <li>{item.highlights.list.list3}</li>
                                <li>{item.highlights.list.list4}</li>
                                <li>{item.highlights.list.list1}</li>
                                <li>{item.highlights.list.list2}</li>
                                <li>{item.highlights.list.list3}</li>
                                <li>{item.highlights.list.list4}</li>
                                <li>{item.highlights.list.list1}</li>
                                <li>{item.highlights.list.list2}</li>
                                <li>{item.highlights.list.list3}</li>
                                <li>{item.highlights.list.list4}</li>
                                <li>{item.highlights.list.list1}</li>
                                <li>{item.highlights.list.list2}</li>
                                <li>{item.highlights.list.list3}</li>
                                <li>{item.highlights.list.list4}</li>
                              </ul>
                            </div>
                          </Col>
                        </Row>
                      </div>
                    </div>
                </div>
              ))}
              </Container>
            </div>
          </div>
        </div>
      </PageLayout>
    </div>
  );
};

export default InstituteDetails;
