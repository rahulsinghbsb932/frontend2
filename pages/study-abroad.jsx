import React, { useState } from "react";
import Card from "react-bootstrap/Card";
import { Container, Row, Col } from "react-bootstrap";
import Head from "next/head";
import Image from "next/image";
import PageLayout from "@components/PageLayout";

const StudyAbroad = () => {
  return (
    <div>
      <Head>
        <title>Certified Corporate | Business Verification Services</title>
        <meta
          id="meta-description"
          name="description"
          content=" This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
        <meta
          property="og:title"
          content="Certified Corporate | Business Verification Services"
        />
        <meta
          property="og:description"
          content="This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
        <meta
          property="twitter:title"
          content="Certified Academy | Academy Verification | VerifyKiya"
        />
        <meta
          property="twitter:description"
          content="This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
      </Head>
      <PageLayout>
        <div className="resources">
          <div className="HeroSectionWrapper">
            <div className="frame1 d-xl-block d-none"></div>
            <div className="frame2 d-xl-block d-none"></div>
            <Container fluid>
              <Row>
                <Col md={12}>
                  <div className="headingWhileLabel text-center mb-5">
                    <h1>Study Abroad</h1>
                    <p>
                      Finding the ideal course is simple with our user-friendly
                      search tools. A substantial database of courses from
                      colleges all over the world is offered by VerifyKiya. To
                      locate the ideal course for you, use our course search and
                      get in touch with the provider directly.
                    </p>
                  </div>
                </Col>
              </Row>
            </Container>

            <Container fluid>
              <Row>
                <Col lg={4}>
                  <Card>
                    <Image
                      src="/images/StydyAbroad/1.svg"
                      height={293}
                      width={263}
                    />
                    <Card.Body>
                      <Card.Title>Why Study Abroad</Card.Title>
                      <Card.Text>
                        Let our team set up your new credentials & identity
                        ecosystem. modular architecture & unique consensus
                        components.
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </Col>
                <Col lg={4}>
                  <Card>
                    <Image
                      src="/images/StydyAbroad/2.svg"
                      height={293}
                      width={263}
                    />
                    <Card.Body>
                      <Card.Title>When and Where</Card.Title>
                      <Card.Text>
                        Though there is no perfect time to study abroad, you’ll
                        find certain school years and semesters to be most
                        common for students.
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </Col>
                <Col lg={4}>
                  <Card>
                    <Image
                      src="/images/StydyAbroad/3.svg"
                      height={293}
                      width={263}
                    />
                    <Card.Body>
                      <Card.Title>Ways to Study Abroad</Card.Title>
                      <Card.Text>
                        The two most popular ways to study abroad include direct
                        enrollment and using a third-party provider.
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </Col>
              </Row>
            </Container>
            <div className="frame3"></div>
            <div className="frame4"></div>
          </div>

          <div className="wrapper">
            <Container fluid>
              <Row>
                <Col xl={4} lg={12}>
                  <div className="img-mask">
                    <div className="img-Study">
                      <Image
                        src="/images/StydyAbroad/MaskGroup.svg"
                        height={600}
                        width={500}
                      />
                    </div>
                    <div className="img-Study1">
                      {" "}
                      <Image
                        src="/images/StydyAbroad/abs-img1.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                  </div>
                </Col>
                <Col xl={8} lg={12}>
                  <h5 className="heading-first">Study Abroad in the </h5>
                  <h2 className="heading-second">World Top Universities</h2>
                  <p className="para my-3">
                    According to the Economic Times, since 2005, the total
                    number of international students opting to study abroad has
                    globally risen from 28,00,000 to over 50,00,000. The desire
                    to travel and study in the top universities of the world has
                    only grown over the years.
                  </p>
                  <Row className="mt-5">
                    <Col lg={2}>
                      <div className="center-img">
                        <div className="img1-Study">
                          <Image
                            src="/images/StydyAbroad/Education.svg"
                            height={125}
                            width={125}
                          />
                        </div>
                      </div>
                    </Col>
                    <Col lg={10}>
                      <h5 className="sub-head">Globally-Renowned Education</h5>
                      <p className="sub-para">
                        The most significant reason for international students
                        to study abroad is the first-class quality of education.
                        No matter which field students decide to pursue,
                        studying in a foreign country offers them a large .
                      </p>
                    </Col>
                  </Row>
                  <Row>
                    <Col lg={2}>
                      <div className="center-img">
                        <div className="img1-Study">
                          <Image
                            src="/images/StydyAbroad/oppurtunity.svg"
                            height={125}
                            width={125}
                          />
                        </div>
                      </div>
                    </Col>
                    <Col lg={10}>
                      <h5 className="sub-head">Great Career Opportunities</h5>
                      <p className="sub-para">
                        The most significant reason for international students
                        to study abroad is the first-class quality of education.
                        No matter which field students decide to pursue,
                        studying in a foreign country offers them a large .
                      </p>
                    </Col>
                  </Row>
                  <Row>
                    <Col lg={2}>
                      <div className="center-img">
                        <div className="img1-Study">
                          <Image
                            src="/images/StydyAbroad/cultural.svg"
                            height={125}
                            width={125}
                          />
                        </div>
                      </div>
                    </Col>
                    <Col lg={10}>
                      <h5 className="sub-head">Cultural Exposure</h5>
                      <p className="sub-para">
                        Studying overseas offers international students a
                        lifetime opportunity to live in an entirely new country
                        and familiarise themselves with its culture, traditions,
                        people, lifestyles, etc.
                      </p>
                    </Col>
                  </Row>
                  <Row>
                    <Col lg={2}>
                      <div className="center-img">
                        <div className="img1-Study">
                          <Image
                            src="/images/StydyAbroad/discover.svg"
                            height={125}
                            width={125}
                          />
                        </div>
                      </div>
                    </Col>
                    <Col lg={10}>
                      <h5 className="sub-head">Discover Yourself</h5>
                      <p className="sub-para">
                        For many students, studying abroad might be the first
                        time when they will be living on their own. This could
                        be their only chance to re-discover their interests and
                        passions.
                      </p>
                    </Col>
                  </Row>
                  <Row>
                    <Col lg={2}>
                      <div className="center-img">
                        <div className="img1-Study">
                          <Image
                            src="/images/StydyAbroad/Linguistics.svg"
                            height={125}
                            width={125}
                          />
                        </div>
                      </div>
                    </Col>
                    <Col lg={10}>
                      <h5 className="sub-head">Linguistics</h5>
                      <p className="sub-para">
                        Students studying in foreign countries like the USA, UK,
                        France, Sweden, and Germany need to be fluent in their
                        native languages. Knowing a foreign language is always
                        an added benefit in resumes.
                      </p>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Container>
          </div>
          <div className="second-wrapper">
            <Container fluid className="bg">
              <div className="p-5 pb-5">
                <Row className="align-items-end position">
                  <Col xl={4} lg={12}>
                    <div className="position-relative">
                      <div className="second-wrapper-child-img">
                        <Image
                          src="/images/StydyAbroad/img.png"
                          height={600}
                          width={450}
                          className="img-fluid"
                        />
                      </div>
                      <div className="icons">
                        <Image
                          src="/images/StydyAbroad/icon.png"
                          height={300}
                          width={300}
                          className="img-fluid"
                        />
                      </div>
                      <div className="photo-frame"></div>
                    </div>
                  </Col>
                  <Col xl={8} lg={12}>
                    <div className="pl">
                      <h1 className="second-wrapper-heading-second">
                        How VerifyKiya
                      </h1>
                      <h2 className="second-wrapper-heading">
                        Can Help Students
                      </h2>

                      <p className="second-wrapper-para">
                        From institutions and recruiters to teachers and
                        students, VerifyKiya empowers everyone who is a part of
                        thInternational students can decide to study abroad at
                        any level of their education, from a few weeks long
                        summer programs to full-fledged degree courses. Some of
                        the most commonly-opted academic programs are:-e
                        educational ecosystem.
                      </p>
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col lg={6}>
                    <div className="box">
                      <div className="icon"></div>
                      <div className="content">
                        <h6>Foundation and Pathways Programs</h6>
                        <p>
                          These are the post-secondary academic programs
                          students choose to fill the gap between the current
                          level of their education and knowledge & skills
                          required to pursue a Bachelor’s or Master’s degree at
                          an international university.
                        </p>
                        <div className="more">
                          <div className="circle opacity-50"></div>
                          <div className="circle opacity-75"></div>
                          <div className="circle"></div>
                          <div className="arrow"></div>
                          <div>
                            <strong>Know more</strong>
                          </div>
                        </div>
                      </div>
                    </div>
                  </Col>
                  <Col lg={6}>
                    <div className="box">
                      <div className="icon"></div>
                      <div className="content">
                        <h6>Foundation and Pathways Programs</h6>
                        <p>
                          These are the post-secondary academic programs
                          students choose to fill the gap between the current
                          level of their education and knowledge & skills
                          required to pursue a Bachelor’s or Master’s degree at
                          an international university.
                        </p>
                        <div className="more">
                          <div className="circle opacity-50"></div>
                          <div className="circle opacity-75"></div>
                          <div className="circle"></div>
                          <div className="arrow"></div>
                          <div>
                            <strong>Know more</strong>
                          </div>
                        </div>
                      </div>
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col lg={6}>
                    <div className="box">
                      <div className="icon"></div>
                      <div className="content">
                        <h6>Foundation and Pathways Programs</h6>
                        <p>
                          These are the post-secondary academic programs
                          students choose to fill the gap between the current
                          level of their education and knowledge & skills
                          required to pursue a Bachelor’s or Master’s degree at
                          an international university.
                        </p>
                        <div className="more">
                          <div className="circle opacity-50"></div>
                          <div className="circle opacity-75"></div>
                          <div className="circle"></div>
                          <div className="arrow"></div>
                          <div>
                            <strong>Know more</strong>
                          </div>
                        </div>
                      </div>
                    </div>
                  </Col>
                  <Col lg={6}>
                    <div className="box">
                      <div className="icon"></div>
                      <div className="content">
                        <h6>Foundation and Pathways Programs</h6>
                        <p>
                          These are the post-secondary academic programs
                          students choose to fill the gap between the current
                          level of their education and knowledge & skills
                          required to pursue a Bachelor’s or Master’s degree at
                          an international university.
                        </p>
                        <div className="more">
                          <div className="circle opacity-50"></div>
                          <div className="circle opacity-75"></div>
                          <div className="circle"></div>
                          <div className="arrow"></div>
                          <div>
                            <strong>Know more</strong>
                          </div>
                        </div>
                      </div>
                    </div>
                  </Col>
                </Row>
              </div>
            </Container>
          </div>
          <section className="study">
            <Container fluid>
              <Row>
                <Col xl={8} lg={12} className="order-2 order-xl-1">
                  <h3>Study Abroad</h3>
                  <p className="pe-0 pe-lg-5 ">
                    Though there is no perfect time to study abroad, you’ll find
                    certain school years and semesters to be most common for
                    students seeking an international academic experience.
                  </p>
                  <div className="box mt-5">
                    <div className="icons">
                      <Image
                        src="/images/StydyAbroad/icon-1.png"
                        height={400}
                        width={300}
                        className="img-fluid"
                      />
                    </div>
                    <div className="content">
                      <h6>Admission to a university abroad directly</h6>
                      <p>
                        After deciding the study, the next step is to figure out
                        where. Traditionally, the USA dominates this chart with
                        over one lakh Indian students studying in US
                        universities.
                      </p>
                    </div>
                  </div>
                  <div className="box ">
                    <div className="icons">
                      <Image
                        src="/images/StydyAbroad/icon-5.png"
                        height={300}
                        width={300}
                        className="img-fluid"
                      />
                    </div>
                    <div className="content">
                      <h6>Admission to a university abroad directly</h6>
                      <p>
                        After deciding the study, the next step is to figure out
                        where. Traditionally, the USA dominates this chart with
                        over one lakh Indian students studying in US
                        universities.
                      </p>
                    </div>
                  </div>
                  <div className="box ">
                    <div className="icons">
                      <Image
                        src="/images/StydyAbroad/icon-3.png"
                        height={300}
                        width={300}
                        className="img-fluid"
                      />
                    </div>
                    <div className="content">
                      <h6>Admission to a university abroad directly</h6>
                      <p>
                        After deciding the study, the next step is to figure out
                        where. Traditionally, the USA dominates this chart with
                        over one lakh Indian students studying in US
                        universities.
                      </p>
                    </div>
                  </div>
                  <div className="box ">
                    <div className="icons">
                      <Image
                        src="/images/StydyAbroad/icon-4.png"
                        height={300}
                        width={300}
                        className="img-fluid"
                      />
                    </div>
                    <div className="content">
                      <h6>Admission to a university abroad directly</h6>
                      <p>
                        After deciding the study, the next step is to figure out
                        where. Traditionally, the USA dominates this chart with
                        over one lakh Indian students studying in US
                        universities.
                      </p>
                    </div>
                  </div>
                  <div className="ms-5">
                    <div className="more">
                      <div className="circle opacity-50"></div>
                      <div className="circle opacity-75"></div>
                      <div className="circle"></div>
                      <div className="arrow"></div>
                      <div>
                        <strong>Know more</strong>
                      </div>
                    </div>
                  </div>
                </Col>
                <Col xl={4} lg={12} className="order-1 order-xl-2">
                  <div className="frame">
                    <div className="img">
                      <Image
                        src="/images/StydyAbroad/img-2.png"
                        height={500}
                        width={400}
                        className="img-fluid"
                      />
                    </div>
                    <div className="icons">
                      <Image
                        src="/images/StydyAbroad/icon-2.png"
                        height={300}
                        width={300}
                        className="img-fluid"
                      />
                    </div>
                    <div className="frame-1 d-xl-block d-none"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </section>
          <section className="top-countery">
            <Container fluid className="bg">
              <div className="frame-1 d-xl-block d-none"></div>
              <Row>
                <Col lg={12}>
                  <div className="heading">
                    <div className="icons">
                      <Image
                        src="/images/StydyAbroad/globe.png"
                        height={300}
                        width={300}
                        className="img-fluid"
                      />
                    </div>
                    <h2 className="mb-3">
                      Top Countries Rankings to <span>Study Abroad</span>
                    </h2>
                    <p>
                      Though there is no perfect time to study abroad, you’ll
                      find certain school years and semesters to be most common
                      for students seeking an international academic experience.
                    </p>
                    <p>
                      While it isn’t unusual to hear of high school graduates
                      going overseas before their freshman year in college,
                      sophomore and junior years are often popular for study
                      abroad. By this time students have realized why everyone
                      should study abroad, and are more likely to have narrowed
                      down the direction of their major.
                    </p>
                  </div>
                </Col>
              </Row>
              <div className="p-xl-5 p-2 pb-5">
                {
                  <Row>
                    <Col lg={3}>
                      <div className="icon-box">
                        <div className="icons">
                          <Image
                            src="/images/StydyAbroad/flag.png"
                            height={300}
                            width={300}
                            className="img-fluid"
                          />
                        </div>
                        <div className="content">
                          <h6>United State</h6>
                        </div>
                      </div>
                    </Col>
                  </Row>
                }
                <Row>
                  <Col lg={3}>
                    <div className="icon-box">
                      <div className="icons">
                        <Image
                          src="/images/StydyAbroad/flag.png"
                          height={300}
                          width={300}
                          className="img-fluid"
                        />
                      </div>
                      <div className="content">
                        <h6>United State</h6>
                      </div>
                    </div>
                  </Col>
                  <Col lg={3}>
                    <div className="icon-box">
                      <div className="icons">
                        <Image
                          src="/images/StydyAbroad/sec-2.png"
                          height={300}
                          width={300}
                          className="img-fluid"
                        />
                      </div>
                      <div className="content">
                        <h6>United State</h6>
                      </div>
                    </div>
                  </Col>
                  <Col lg={3}>
                    <div className="icon-box">
                      <div className="icons">
                        <Image
                          src="/images/StydyAbroad/sec-3.png"
                          height={300}
                          width={300}
                          className="img-fluid"
                        />
                      </div>
                      <div className="content">
                        <h6>United State</h6>
                      </div>
                    </div>
                  </Col>
                  <Col lg={3}>
                    <div className="icon-box">
                      <div className="icons">
                        <Image
                          src="/images/StydyAbroad/sec-4.png"
                          height={300}
                          width={300}
                          className="img-fluid"
                        />
                      </div>
                      <div className="content">
                        <h6>United State</h6>
                      </div>
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col lg={3}>
                    <div className="icon-box">
                      <div className="icons">
                        <Image
                          src="/images/StydyAbroad/flag-1.png"
                          height={300}
                          width={300}
                          className="img-fluid"
                        />
                      </div>
                      <div className="content">
                        <h6>United State</h6>
                      </div>
                    </div>
                  </Col>
                  <Col lg={3}>
                    <div className="icon-box">
                      <div className="icons">
                        <Image
                          src="/images/StydyAbroad/flag-2.png"
                          height={300}
                          width={300}
                          className="img-fluid"
                        />
                      </div>
                      <div className="content">
                        <h6>United State</h6>
                      </div>
                    </div>
                  </Col>
                  <Col lg={3}>
                    <div className="icon-box">
                      <div className="icons">
                        <Image
                          src="/images/StydyAbroad/flag-3.png"
                          height={300}
                          width={300}
                          className="img-fluid"
                        />
                      </div>
                      <div className="content">
                        <h6>United State</h6>
                      </div>
                    </div>
                  </Col>
                  <Col lg={3}>
                    <div className="icon-box">
                      <div className="icons">
                        <Image
                          src="/images/StydyAbroad/flag-4.png"
                          height={300}
                          width={300}
                          className="img-fluid"
                        />
                      </div>
                      <div className="content">
                        <h6>United State</h6>
                      </div>
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col lg={3}>
                    <div className="icon-box">
                      <div className="icons">
                        <Image
                          src="/images/StydyAbroad/flag-5.png"
                          height={300}
                          width={300}
                          className="img-fluid"
                        />
                      </div>
                      <div className="content">
                        <h6>United State</h6>
                      </div>
                    </div>
                  </Col>
                  <Col lg={3}>
                    <div className="icon-box">
                      <div className="icons">
                        <Image
                          src="/images/StydyAbroad/flag-6.png"
                          height={300}
                          width={300}
                          className="img-fluid"
                        />
                      </div>
                      <div className="content">
                        <h6>United State</h6>
                      </div>
                    </div>
                  </Col>
                  <Col lg={3}>
                    <div className="icon-box">
                      <div className="icons">
                        <Image
                          src="/images/StydyAbroad/flag-7.png"
                          height={300}
                          width={300}
                          className="img-fluid"
                        />
                      </div>
                      <div className="content">
                        <h6>United State</h6>
                      </div>
                    </div>
                  </Col>
                  <Col lg={3}>
                    <div className="icon-box">
                      <div className="icons">
                        <Image
                          src="/images/StydyAbroad/flag-8.png"
                          height={300}
                          width={300}
                          className="img-fluid"
                        />
                      </div>
                      <div className="content">
                        <h6>United State</h6>
                      </div>
                    </div>
                  </Col>
                </Row>
                <div className="more mt-5">
                  <div className="circle opacity-50"></div>
                  <div className="circle opacity-75"></div>
                  <div className="circle"></div>
                  <div className="arrow"></div>
                  <div>
                    <strong>Know more</strong>
                  </div>
                </div>
              </div>
              <div className="frame d-xl-block d-none"></div>
              <div className="frame-3 d-xl-block d-none"></div>
            </Container>
          </section>
          <section className="top-countery top-university">
            <Container fluid className="bg">
              <div className="frame-1 d-xl-block d-none"></div>
              <Row>
                <Col lg={12}>
                  <div className="heading">
                    <div className="icons">
                      <Image
                        src="/images/StydyAbroad/uni.png"
                        height={300}
                        width={300}
                        className="img-fluid"
                      />
                    </div>
                    <h2 className="my-3">
                      Top Universities <span>Study Abroad</span>
                    </h2>
                    <p>
                      Though there is no perfect time to study abroad, you’ll
                      find certain school years and semesters to be most common
                      for students seeking an international academic experience.
                    </p>
                    <p>
                      While it isn’t unusual to hear of high school graduates
                      going overseas before their freshman year in college,
                      sophomore and junior years are often popular for study
                      abroad. By this time students have realized why everyone
                      should study abroad, and are more likely to have narrowed
                      down the direction of their major.
                    </p>
                  </div>
                </Col>
              </Row>
              <div className="p-xl-5 p-2 pb-5">
                <Row>
                  <Col lg={6}>
                    <div className="icon-box">
                      <div className="icons">
                        <Image
                          src="/images/StydyAbroad/inve.png"
                          height={300}
                          width={300}
                          className="img-fluid"
                        />
                      </div>
                      <div className="content">
                        <h6>Stanford Graduate School of Business</h6>
                        <p>Stanford (CA), United States</p>
                      </div>
                    </div>
                  </Col>
                  <Col lg={6}>
                    <div className="icon-box">
                      <div className="icons">
                        <Image
                          src="/images/StydyAbroad/Harvard University.png"
                          height={300}
                          width={300}
                          className="img-fluid"
                        />
                      </div>
                      <div className="content">
                        <h6>Harvard Business School</h6>
                        <p>Boston (MA), United States</p>
                      </div>
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col lg={6}>
                    <div className="icon-box">
                      <div className="icons">
                        <Image
                          src="/images/StydyAbroad/Wharton-logo.png"
                          height={300}
                          width={300}
                          className="img-fluid"
                        />
                      </div>
                      <div className="content">
                        <h6>Penn (Wharton)</h6>
                        <p>Philadelphia (PA), United States</p>
                      </div>
                    </div>
                  </Col>
                  <Col lg={6}>
                    <div className="icon-box">
                      <div className="icons">
                        <Image
                          src="/images/StydyAbroad/HEC_Paris.png"
                          height={300}
                          width={300}
                          className="img-fluid"
                        />
                      </div>
                      <div className="content">
                        <h6>HEC Paris</h6>
                        <p>Jouy en Josas, France</p>
                      </div>
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col lg={6}>
                    <div className="icon-box">
                      <div className="icons">
                        <Image
                          src="/images/StydyAbroad/mit.png"
                          height={300}
                          width={300}
                          className="img-fluid"
                        />
                      </div>
                      <div className="content">
                        <h6>MIT (Sloan)</h6>
                        <p>Cambridge (MA), United States</p>
                      </div>
                    </div>
                  </Col>
                  <Col lg={6}>
                    <div className="icon-box">
                      <div className="icons">
                        <Image
                          src="/images/StydyAbroad/mit.png"
                          height={300}
                          width={300}
                          className="img-fluid"
                        />
                      </div>
                      <div className="content">
                        <h6>London Business School</h6>
                        <p>Boston (MA), United States</p>
                      </div>
                    </div>
                  </Col>
                </Row>
                <div className="more mt-5">
                  <div className="circle opacity-50"></div>
                  <div className="circle opacity-75"></div>
                  <div className="circle"></div>
                  <div className="arrow"></div>
                  <div>
                    <strong>Know more</strong>
                  </div>
                </div>
              </div>
              <div className="frame d-xl-block d-none"></div>
              <div className="frame-3 d-xl-block d-none"></div>
            </Container>
          </section>
          <section className="top-countery popular mb-5">
            <Container fluid className="bg">
              <div className="frame-1 d-xl-block d-none"></div>
              <Row>
                <Col lg={12}>
                  <div className="heading">
                    <div className="icons">
                      <Image
                        src="/images/StydyAbroad/uni.png"
                        height={300}
                        width={300}
                        className="img-fluid"
                      />
                    </div>
                    <h2 className="mb-3">
                      Popular Courses <span>Study Abroad</span>
                    </h2>
                    <p>
                      Though there is no perfect time to study abroad, you’ll
                      find certain school years and semesters to be most common
                      for students seeking an international academic experience.
                    </p>
                    <p>
                      While it isn’t unusual to hear of high school graduates
                      going overseas before their freshman year in college,
                      sophomore and junior years are often popular for study
                      abroad. By this time students have realized why everyone
                      should study abroad, and are more likely to have narrowed
                      down the direction of their major.
                    </p>
                  </div>
                </Col>
              </Row>
              <div className="p-5 mt-5">
                <Row>
                  <Col lg={6}>
                    <div className="icon-box">
                      <div className="check"></div>
                      <div className="content">
                        <h6>Bachelor/Undergradute</h6>
                        <p>Arts, Humanities and Social Science</p>
                      </div>
                    </div>
                  </Col>
                  <Col lg={6}>
                    <div className="icon-box">
                      <div className="check"></div>
                      <div className="content">
                        <h6>Master with Pre-Master Programme</h6>
                        <p>Medicine, Pharmacy , Health and Life Sciences</p>
                      </div>
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col lg={6}>
                    <div className="icon-box">
                      <div className="check"></div>
                      <div className="content">
                        <h6>Bachelor with Foundation Year</h6>
                        <p>Computer Science, Data Sciene & IT</p>
                      </div>
                    </div>
                  </Col>
                  <Col lg={6}>
                    <div className="icon-box">
                      <div className="check"></div>
                      <div className="content">
                        <h6>PhD / Doctorate / Master</h6>
                        <p>Science</p>
                      </div>
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col lg={6}>
                    <div className="icon-box">
                      <div className="check"></div>
                      <div className="content">
                        <h6>Master of Business Administration</h6>
                        <p>Medicine, Pharmacy , Health & Life Sciences</p>
                      </div>
                    </div>
                  </Col>
                  <Col lg={6}>
                    <div className="icon-box">
                      <div className="check"></div>
                      <div className="content">
                        <h6>Associate Degree</h6>
                        <p>Sports, Exercise And Nutrition</p>
                      </div>
                    </div>
                  </Col>
                </Row>
                <div className="more mt-5">
                  <div className="circle opacity-50"></div>
                  <div className="circle opacity-75"></div>
                  <div className="circle"></div>
                  <div className="arrow"></div>
                  <div>
                    <strong>Know more</strong>
                  </div>
                </div>
              </div>
              <div className="frame d-xxl-block d-none"></div>
              <div className="frame-3 d-xxl-block d-none"></div>
            </Container>
          </section>
        </div>
      </PageLayout>
    </div>
  );
};

export default StudyAbroad;
