import React, { useEffect } from "react";
import Head from "next/head";
import PageFooter from "@components/includes/Footer";
import PageHeader from "@components/includes/Header";
import Image from "next/image";

export default function Index() {
  return (
    <>
      <Head>
        <title>Verifykiya | Certificate Verification | Fast and Secure</title>
        <meta
          id="meta-description"
          name="description"
          content="Verifykiya provides fast and secure certificate verification services. Verify your certificates online easily with Verifykiya"
        />
        <meta
          property="og:title"
          content="Verifykiya | Certificate Verification | Fast and Secure"
        />
        <meta
          property="og:description"
          content="Verifykiya provides fast and secure certificate verification services. Verify your certificates online easily with Verifykiya"
        />
        <meta
          property="twitter:title"
          content="Verifykiya | Certificate Verification | Fast and Secure"
        />
        <meta
          property="twitter:description"
          content="Verifykiya provides fast and secure certificate verification services. Verify your certificates online easily with Verifykiya"
        />
      </Head>

      <PageHeader />
      <div className="home">
        <div className="banner">
          <div className="bg">
            <div className="list-box">
              <div className="box">Verified seal</div>
              <div className="box">Scholarships</div>
              <div className="box">Resume template</div>
              <div className="box">Blockchain based wallet</div>
              <div className="box">Job Marketplace</div>
            </div>
          </div>
        </div>
        <div className="banner auto">
          <div className="container-fluid">
            <div className="row">
              <div className="col-xl-8 col-lg-12">
                <div className="padding">
                  <h1>World’s only Blockchain-Based NFT enabled</h1>
                  <h2>
                    Solution <span>for </span>Certification !
                  </h2>

                  <ul>
                    <li>Digital Certificates</li>
                    <li>Access to verified service providers</li>
                    <li>Min. Administrative Workload</li>
                    <li>Background Authentication</li>
                    <li>Automated Resume Builder</li>
                    <li>Huge Student and Talent Pool</li>
                    <li>White Label Solution</li>
                    <li>AI Curated Resumes</li>
                  </ul>
                </div>
              </div>
              <div className="col-lg-4"></div>
            </div>
          </div>
        </div>
        <section>
          <div className="ins">
            <div className="container-fluid">
              <div className="row">
                <div className="col-xl-4 col-lg-12 col-md-12">
                  <div className="relative">
                    <div className="bg-ins"></div>
                  </div>
                </div>
                <div className="col-xl-8 col-lg-12">
                  <div className="">
                    <h2>
                      VerifyKiya's Solutions for
                      <span>
                        <br />
                        Institutions & Corporates
                      </span>
                    </h2>
                    <p>
                      Our engaging and precisely curated feature set perfectly
                      coalesce to formulate a tailor-made solution for your
                      brand that promises progress, relevancy, and quality
                    </p>
                    <div className="mainBoxWrapper">
                      <div className="InstitutionBoxWraper">
                        <div className="wrapperBox1">
                          <div className="InstitutionoptionIcon">
                            <Image
                              src="/images/HomeIcon/Blockchain-based_digital_credentials_1_.svg"
                              height={60}
                              width={60}
                              alt="Blockchain-based digital credentials"
                            />
                          </div>
                          <div className="contentBox Montserrat">
                            <h2>Blockchain-based digital credentials</h2>
                            <p>
                              Get rid of fraudulent verifications with
                              blockchain-based digital certificates
                            </p>
                          </div>
                        </div>
                        <div className="wrapperBox1">
                          <div className="InstitutionoptionIcon">
                            <Image
                              src="/images/HomeIcon/Brand_Building_1_.svg"
                              height={60}
                              width={60}
                            />
                          </div>
                          <div className="contentBox Montserrat">
                            <h2>Brand Building</h2>
                            <p>
                              Prevent any loss of reputation with VerifyKiya's
                              blockchain-based certificate
                            </p>
                          </div>
                        </div>
                        <div className="wrapperBox1">
                          <div className="InstitutionoptionIcon">
                            <Image
                              src="/images/HomeIcon/Background_Checks_1_.svg"
                              height={60}
                              width={60}
                            />
                          </div>
                          <div className="contentBox Montserrat">
                            <h2>Background Checks</h2>
                            <p>
                              Check the veracity of the provided information
                              with quick background authentication
                            </p>
                          </div>
                        </div>
                        <div className="wrapperBox1">
                          <div className="InstitutionoptionIcon">
                            <Image
                              src="/images/HomeIcon/Access_to_Talent_Pool__x26__Student_Database_1_.svg"
                              height={60}
                              width={60}
                            />
                          </div>
                          <div className="contentBox Montserrat">
                            <h2>Access to Talent Pool & Student Database</h2>
                            <p>
                              Get easy access to a massive talent pool of
                              candidates who can fit into your job vacancies
                            </p>
                          </div>
                        </div>
                      </div>
                      <div className="InstitutionBoxWraper">
                        <div className="wrapperBox1">
                          <div className="InstitutionoptionIcon">
                            <Image
                              src="/images/HomeIcon/AI_Curated_and_verified_Profiles_1_.svg"
                              height={60}
                              width={60}
                            />
                          </div>
                          <div className="contentBox Montserrat">
                            <h2>AI Curated and verified Profiles</h2>
                            <p>
                              Weed out any fake or fabricated profiles to ensure
                              a transparent mechanism
                            </p>
                          </div>
                        </div>
                        <div className="wrapperBox1">
                          <div className="InstitutionoptionIcon">
                            <Image
                              src="/images/HomeIcon/Redundant_Administrative_Workload_1_.svg"
                              height={60}
                              width={60}
                            />
                          </div>
                          <div className="contentBox Montserrat">
                            <h2>Redundant Administrative Workload</h2>
                            <p>
                              Bid adieu to administrative wranglings with our
                              blockchain-powered platform
                            </p>
                          </div>
                        </div>
                        <div className="wrapperBox1">
                          <div className="InstitutionoptionIcon">
                            <Image
                              src="/images/HomeIcon/Verified_Service_Providers_1_.svg"
                              height={60}
                              width={60}
                            />
                          </div>
                          <div className="contentBox Montserrat">
                            <h2>Verified Service Providers</h2>
                            <p>
                              Get a list of verified service providers like
                              training institutes, event planners,providers, etc
                            </p>
                          </div>
                        </div>

                        <div className="wrapperBox1">
                          <div className="InstitutionoptionIcon">
                            <Image
                              src="/images/HomeIcon/Templates_Database_and_White_Label_Solution_1_ (1).svg"
                              height={60}
                              width={60}
                            />
                          </div>
                          <div className="contentBox Montserrat">
                            <h2>Templates Database and White Label Solution</h2>
                            <p>
                              Propel your brand toward success with VerifyKiya’s
                              template database and unique white-label solutions
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section>
          <div className="complete">
            <div className="container-fluid">
              <div className="row">
                <div className="col-xl-7 col-lg-12 order-xl-1 order-lg-2 order-2">
                  <div>
                    <h3 className="headingSolutionblack">
                      VerifyKiya's Solutions for
                    </h3>
                    <h1 className="headingSolutionWhite">
                      Students & Employees
                    </h1>

                    <p className="subpera">
                      VerifyKiya can benefit students and employees in a number
                      of ways with its real-time notifications and huge
                      partnerships across various stakeholders
                    </p>
                    <div className="solutionOptionOnebox">
                      <div className="optionSolitionIconone">
                        <Image
                          src="/images/HomeIcon/Digital_Wallet_2_.svg"
                          height={50}
                          width={50}
                        />
                      </div>
                      <div className="solutionContentBox Montserrat">
                        <h1>Digital Wallet</h1>
                        <p>
                          Store all the documents right from your earliest
                          student life days with the utmost ease in VerifyKiya's
                          Digilocker-integrated blockchain wallet.
                        </p>
                      </div>
                    </div>

                    <div className="solutionOptionOnebox">
                      <div className="optionSolitionIconone">
                        <Image
                          src="/images/HomeIcon/Easy_Access_to_Scholarships__x26__Job_Vacancies_1_ (1).svg"
                          height={50}
                          width={50}
                        />
                      </div>
                      <div className="solutionContentBox Montserrat">
                        <h1>Easy Access to Scholarships & Job Vacancies</h1>
                        <p>
                          Apply for scholarships and job vacancies that are best
                          suited to your skills with VerifyKiya's real-time
                          scholarship and job notifications.
                        </p>
                      </div>
                    </div>

                    <div className="solutionOptionOnebox">
                      <div className="optionSolitionIconone">
                        <Image
                          src="/images/HomeIcon/Automated_Resume_Builder_1_.svg"
                          height={50}
                          width={50}
                        />
                      </div>
                      <div className="solutionContentBox Montserrat">
                        <h1>Automated Resume Builder</h1>
                        <p>
                          Add every last one of your achievements with
                          VerifyKiya's automated resume builder and get picked
                          by companies looking for well-rounded, multi-faceted
                          individuals.
                        </p>
                      </div>
                    </div>
                    <div className="solutionOptionOnebox">
                      <div className="optionSolitionIconone">
                        <Image
                          src="/images/HomeIcon/Templates_Database_1_.svg"
                          height={50}
                          width={50}
                        />
                      </div>
                      <div className="solutionContentBox Montserrat">
                        <h1>Templates Database</h1>
                        <p>
                          Get access to a ready-to-use database containing a
                          huge collection of resumes, certificates, badges,
                          forms, etc. to kickstart your digital journey.
                        </p>
                      </div>
                    </div>
                    <div className="solutionOptionOnebox">
                      <div className="optionSolitionIconone">
                        <Image
                          src="/images/HomeIcon/Study_Abroad_With_Assured_Scholarships_1_.svg"
                          height={50}
                          width={50}
                        />
                      </div>
                      <div className="solutionContentBox Montserrat">
                        <h1>Study Abroad With Assured Scholarships</h1>
                        <p>
                          Get real-time notifications on both national and
                          international scholarships and pursue your dream of
                          studying abroad at the best universities.
                        </p>
                      </div>
                    </div>
                    <div className="solutionOptionOnebox">
                      <div className="optionSolitionIconone">
                        <Image
                          src="/images/HomeIcon/Single-Click_Access_to_All_Forms_1_.svg"
                          height={50}
                          width={50}
                        />
                      </div>
                      <div className="solutionContentBox Montserrat">
                        <h1>Single-Click Access to All Forms</h1>
                        <p>
                          Get your hands on all scholarship and admission forms
                          with a single click, and ensure that you’re always
                          ahead of the final date for form submission.
                        </p>
                      </div>
                    </div>
                    <div className="solutionOptionOnebox">
                      <div className="optionSolitionIconone">
                        <Image
                          src="/images/HomeIcon/Real-Time_Notifications_1_.svg"
                          height={50}
                          width={50}
                        />
                      </div>
                      <div className="solutionContentBox Montserrat">
                        <h1>Real-Time Notifications</h1>
                        <p>
                          Get real-time updates on all admissions, scholarships,
                          and examinations, and always stay ahead of your
                          friends and peers.
                        </p>
                      </div>
                    </div>
                    <div className="solutionOptionOnebox">
                      <div className="optionSolitionIconone">
                        <Image
                          src="/images/HomeIcon/Enrollment_In_National__x26__Global_Competitions_1_.svg"
                          height={50}
                          width={50}
                        />
                      </div>
                      <div className="solutionContentBox Montserrat">
                        <h1>Enrollment In National & Global Competitions</h1>
                        <p>
                          Enroll in all national and global competitions by
                          receiving timely updates on the commencement of all
                          such contests.
                        </p>
                      </div>
                    </div>

                    {/* <button className="readMoreBtn">
                    <span className="CircleOne"></span>
                    <span className="Circletwo"></span>
                    <span className="Circlethree"></span>
                    <span>
                      {" "}
                      <img src={arrow} className="arrow" />
                    </span>
                    <span>Know More</span>
                  </button> */}
                  </div>
                </div>
                <div className="col-xl-5 col-lg-12 order-xl-2 order-lg-1 order-1">
                  <div className="bg-image">
                    <div className="girl"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="d-lg-block">
          <div className="under">
            <div className="container-fluid">
              <div className="row">
                <div className="col col-lg-12 col-xl-6 col-12">
                  <div className="product"></div>
                </div>
                <div className="col col-lg-12 col-xl-6 col-12">
                  <div className="BenifitsWrapper">
                    <div className="benifitsHeadingblack Montserrat">
                      <h1 className="BenifitsHeadingOne">
                        The Advantages of using
                      </h1>
                      <h2 className="BenifitsheadingTwo">VerifyKiya</h2>
                      <p className="BanifitsPera">
                        VerifyKiya has got a world of benefits for everyone and
                        can pave the way to the future with its advanced
                        blockchain-based features.
                      </p>
                    </div>

                    <div className="solutionOptionOnebox">
                      <div className="optionSolitionIconone">
                        <Image
                          src="/images/Brand_icon.png"
                          height={50}
                          width={50}
                        />
                      </div>
                      <div className="solutionContentBox Montserrat">
                        <h1>Brand Building</h1>
                        <p>
                          With VerifyKiya’s blockchain-based certificate, you
                          can prevent scamsters from using your brand and help
                          it attain the heights that it was always supposed to.
                        </p>
                      </div>
                    </div>

                    <div className="solutionOptionOnebox">
                      <div className="optionSolitionIconone">
                        <Image
                          src="/images/SavesTime.png"
                          height={50}
                          width={50}
                        />
                      </div>
                      <div className="solutionContentBox Montserrat">
                        <h1>Time Saving</h1>
                        <p>
                          Get rid of unnecessary administrative workload and
                          expedite the entire process of background verification
                          with VerifyKiya’s blockchain-backed digital
                          certificate.
                        </p>
                      </div>
                    </div>

                    <div className="solutionOptionOnebox">
                      <div className="optionSolitionIconone">
                        <Image
                          src="/images/Immutable.png"
                          height={50}
                          width={50}
                        />
                      </div>
                      <div className="solutionContentBox Montserrat">
                        <h1>Immutable</h1>
                        <p>
                          With the entire process powered by blockchain
                          technology, VerifyKiya’s blockchain-based digital
                          certificates are immutable and can never be tampered
                          with.
                        </p>
                      </div>
                    </div>

                    <div className="solutionOptionOnebox">
                      <div className="optionSolitionIconone">
                        <Image
                          src="/images/Immutable.png"
                          height={50}
                          width={50}
                        />
                      </div>
                      <div className="solutionContentBox Montserrat">
                        <h1>Trustless</h1>
                        <p>
                          With VerifyKiya's trustless network, you can always
                          rely on our blockchain-based certificate and no longer
                          have to place your trust in third parties.
                        </p>
                      </div>
                    </div>

                    <div className="solutionOptionOnebox">
                      <div className="optionSolitionIconone">
                        <Image
                          src="/images/Footprint.png"
                          height={50}
                          width={50}
                        />
                      </div>
                      <div className="solutionContentBox Montserrat">
                        <h1>Reduces Carbon Footprint</h1>
                        <p>
                          Thanks to VerifyKiya's digital certificate, you can
                          bring your dependence on paper to a bare minimum and
                          play your part in reducing the overall carbon
                          footprint.
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
      <PageFooter />
    </>
  );
}
