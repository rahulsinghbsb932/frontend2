import React, { useState } from "react";
import {
  Container,
  Row,
  Col,
  Table,
  Button,
  Form,
  InputGroup,
} from "react-bootstrap";
import Head from "next/head";
import Link from "next/link";
import PageLayout from "@components/PageLayout";
import ArrowForwardIosIcon from "@mui/icons-material/ArrowForwardIos";
import { useEffect } from "react";
import Signup from "./form/Signup";
import RegisterRecieve from "./form/RegisterRecieve";
import Subscribe from "./form/Subscribe";

const CompetitiveExam = (props) => {
  const [items, setItems] = useState([]);
  const fetchData = async () => {
    const response = await fetch("https://retoolapi.dev/jGutXu/data");
    const data = await response.json();
    return setItems(data);
  };

  useEffect(() => {
    fetchData();
  }, []);
  return (
    <div>
      <Head>
        <title>Competitive Exam | Business Verification Services</title>
        <meta
          id="meta-description"
          name="description"
          content=" This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
        <meta
          property="og:title"
          content="Competitive Exam | Business Verification Services"
        />
        <meta
          property="og:description"
          content="This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
        <meta
          property="twitter:title"
          content="Certified Academy | Academy Verification | VerifyKiya"
        />
        <meta
          property="twitter:description"
          content="This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
      </Head>
      <PageLayout>
        <div className="resources">
          <div className="HeroSectionWrapper">
            <div className="frame1 d-xl-block d-none"></div>
            <Container fluid>
              <Row>
                <Col md={12}>
                  <div className="headingWhileLabel mb-5">
                    <div className="text-center">
                      <h1>Competitive Exam</h1>
                    </div>
                    <p>
                      It is common in India to prepare for a competitive exam.
                      You will find that One out of two students are doing
                      government job preparation or any other competitive exam
                      preparation because competitive exams provide you good
                      career options.
                    </p>
                    <div className="center">
                      <ul>
                        <li>CBSE</li>
                        <li>CISCE</li>
                        <li>NIOS</li>
                        <li>Andhra Pradesh</li>
                        <li>Assam</li>
                        <li>Bihar</li>
                        <li>Chhattisgarh</li>
                        <li>Goa</li>
                        <li>Gujarat</li>
                        <li>Haryana</li>
                        <li>Himachal Pradesh</li>
                        <li>Jammu & Kashmir</li>
                        <li>Jharkhand</li>
                        <li>Karnataka</li>
                      </ul>
                    </div>
                  </div>
                </Col>
              </Row>
            </Container>
            <div className="frame4"></div>
          </div>
          <Container fluid>
            <div className="d-flex flex-wrap flex-lg-nowrap ">
              <div className="mx-auto">
                <div className="frame-width">
                  <iframe
                    src="/images/1.png"
                    scrolling="no"
                    width="100%"
                    height="100%"
                    class="responsive-iframe"
                  ></iframe>
                </div>
              </div>
            </div>
          </Container>
          <div className="wrapper contact">
            <div className="contact-box pb-5">
              <Container fluid>
                <div className="">
                  <div className="z-index">
                    <Row>
                      <Col lg={9}>
                        {items.map((item, index) => (
                          <div key={index}>
                            <div className="price competive">
                              <div className="d-flex justify-content-between align-items-center header flex-wrap">
                                <div className="text d-flex flex-wrap align-items-center gap-2">
                                  <div className="ico">
                                    <div className="sm-icons">
                                      <img
                                        src={item.img}
                                        alt="img"
                                        className="img-fluid"
                                      />
                                    </div>
                                  </div>
                                  <strong>{item.title} </strong>- {item.online}
                                </div>
                                <div className="btns">
                                  <Link href="/CompetitiveExamDetails">
                                    <Button className="btn btn-success">
                                      Apply Now
                                    </Button>
                                  </Link>
                                </div>
                              </div>
                              <div className="px-4">
                                <Table responsive borderless>
                                  <tbody>
                                    <tr>
                                      <td colSpan={3}>{item.title}</td>
                                    </tr>
                                    <tr>
                                      <td>
                                        <strong>Exam Date</strong>
                                      </td>
                                      <td>
                                        <strong>Application Form</strong>
                                      </td>
                                      <td>
                                        <strong>Result Announce</strong>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td>{item.exam}</td>
                                      <td>{item.form}</td>
                                      <td>{item.result}</td>
                                    </tr>
                                    <tr>
                                      <td>
                                        <Link href="">
                                          <span>
                                            Application Process
                                            <ArrowForwardIosIcon
                                              sx={{ fontSize: "10px" }}
                                            />
                                          </span>
                                        </Link>
                                      </td>
                                      <td>
                                        <Link href="">
                                          <span>
                                            Exam Pattern
                                            <ArrowForwardIosIcon
                                              sx={{ fontSize: "10px" }}
                                            />
                                          </span>
                                        </Link>
                                      </td>
                                      <td>
                                        <Link href="">
                                          <span>
                                            Previous Year Paper
                                            <ArrowForwardIosIcon
                                              sx={{ fontSize: "10px" }}
                                            />
                                          </span>
                                        </Link>
                                      </td>
                                    </tr>
                                  </tbody>
                                </Table>
                              </div>
                            </div>
                          </div>
                        ))}
                      </Col>
                      <Col lg={3}>
                        <iframe
                          src="/images/host.png"
                          scrolling="no"
                          width="300"
                          height="250"
                          class="responsive-iframe"
                        ></iframe>
                        <div className="w-300">
                          <Signup />
                          <RegisterRecieve />
                          <Subscribe />
                          <iframe
                            src="/images/ad.png"
                            scrolling="no"
                            width="300"
                            height="600"
                          ></iframe>
                        </div>
                      </Col>
                    </Row>
                  </div>
                </div>
              </Container>
              <Container fluid>
                <div className="d-flex flex-wrap flex-lg-nowrap ">
                  <div className="mx-auto">
                    <div className="frame-width">
                      <iframe
                        src="/images/1.png"
                        scrolling="no"
                        width="100%"
                        height="100%"
                        class="responsive-iframe"
                      ></iframe>
                    </div>
                  </div>
                </div>
              </Container>
            </div>
          </div>
        </div>
      </PageLayout>
    </div>
  );
};

export default CompetitiveExam;
