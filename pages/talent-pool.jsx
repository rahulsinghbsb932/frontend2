import React from "react";

import Head from "next/head";
import Image from "next/image";
import PageLayout from "@components/PageLayout";
import { Container, Row, Col } from "react-bootstrap";
import Card from "react-bootstrap/Card";

const talentPool = () => {
  return (
    <div>
      <Head>
        <title>Create CV Online for Free | Degree</title>
        <meta
          id="meta-description"
          name="description"
          content="Build your perfect CV with our Degree. Create CVs online for free in minutes. Get started now and land your dream job."
        />
        <meta
          property="og:title"
          content="Create CV Online for Free | Degree"
        />
        <meta
          property="og:description"
          content="Build your perfect CV with our Degree. Create CVs online for free in minutes. Get started now and land your dream job."
        />
        <meta
          property="twitter:title"
          content="Create CV Online for Free | Degree"
        />
        <meta
          property="twitter:description"
          content="Build your perfect CV with our Degree. Create CVs online for free in minutes. Get started now and land your dream job."
        />
      </Head>

      <PageLayout>
        <div className="HeroSectionWrapper">
          <Container fluid>
            <Row>
              <Col md={12}>
                <div className="headingWhileLabel Montserrat">
                  <h1>Talent Pool & Student Database</h1>
                  <p>
                    Build A Professional Resume Within Minutes With the Best
                    Resume Templates!
                  </p>
                </div>
              </Col>
            </Row>
          </Container>

          <Container fluid className="mb-5">
            <Row>
              <Col md={3} className="rowCenter mt-5">
                <a className="cardanchor" href="#Verified">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Talent_Pool/Verified Resumes.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Talent_Pool/Verified Resumes.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Verified Resumes
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={3} className="rowCenter mt-5">
                <a className="cardanchor" href="#Unified">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Talent_Pool/Unified Platform.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Talent_Pool/Unified Platform.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Unified platform for learner Acquisition, Retention and
                        Upsell
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={3} className="rowCenter mt-5">
                <a className="cardanchor" href="#Skills">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Talent_Pool/Skills in Demand Analysis.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Talent_Pool/Skills in Demand Analysis.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Skills in Demand Analysis
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={3} className="rowCenter mt-5">
                <a className="cardanchor" href="#curated">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Talent_Pool/AI curated Resumes.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Talent_Pool/AI curated Resumes.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        AI curated Resumes
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
            </Row>
          </Container>
        </div>

        <section id="Verified">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Talent Pool & Student Database/Verified Resumes.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Talent Pool & Student Database/Verified Resumes.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Talent_Pool/Verified Resumes.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Talent_Pool/Verified Resumes.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Verified Resumes
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      VerifyKiya helps validate the authenticity and accuracy of
                      all the information provided by the applicant. We run
                      background verifications and check the credentials so that
                      you can focus on the skills, eligibility and personality,
                      without worrying about the authenticity of the information
                      on their resumes.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section id="Unified">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Talent_Pool/Unified Platform.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Talent_Pool/Unified Platform.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Unified platform for learner Acquisition, Retention and
                        Upsell
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      VerifyKiya is a comprehensive platform that hosts all
                      features required for a successful and efficient student
                      life. From targeted techniques of student acquisition to
                      their higher education, everything is taken care of in the
                      best possible manner. VerifyKiya takes care of all
                      students' needs, leaving the institution to focus on its
                      core competencies and not use the precious time of its
                      teachers for redundant administrative work. Verifykiya’s
                      unified platform allows the institution to track the
                      development of its students and build a robust alumni
                      network.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
                <Col xl={3} lg={12} className="order-xl-2 order-1">
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Talent Pool & Student Database/Unified platform.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid "
                    /> */}

                    <Image
                      src="/images/Services/images/Talent Pool & Student Database/Unified platform.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section id="Skills">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Talent Pool & Student Database/Skills in Demand Analysis.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}
                    <Image
                      src="/images/Services/images/Talent Pool & Student Database/Skills in Demand Analysis.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Talent_Pool/Skills in Demand Analysis.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Talent_Pool/Skills in Demand Analysis.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Skills in Demand Analysis
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      VerifyKiya conducts skills in demand analysis in order to
                      benefit institutions, students and employees. We help by
                      analyzing the trends and demand of skills and courses
                      being taken, matching them with the skills required for
                      the jobs, and suggest the institutes and corporates the
                      best courses and skill training they can offer the
                      candidates to help them grow in their careers.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section id="curated" className="mb-5">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Talent_Pool/AI curated Resumes.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Talent_Pool/AI curated Resumes.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        AI curated Resumes
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      VerifyKiya analyzes and curates resumes using artificial
                      intelligence technologies to speed up the screening and
                      selection process. VerifyKiya assists in reducing
                      unconscious bias and improving the fairness of the hiring
                      process, as well as allowing the management to focus on
                      more vital responsibilities. Verifykiya curates the
                      resumes on the basis of required job description thereby
                      saving time and effort of the recruiter in initial
                      screening of the resumes, we also provide a compatibility
                      score on the basis of the skills required in job
                      description enabling the recruiter to select the most
                      suitable candidates.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Talent Pool & Student Database/AI curated Resumes.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Talent Pool & Student Database/AI curated Resumes.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
      </PageLayout>
    </div>
  );
  // merge again
};

export default talentPool;
