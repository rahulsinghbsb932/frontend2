import React, { useState, useEffect } from "react";
import {
  Container,
  Row,
  Col,
  Card,
  Table,
  FormLabel,
  Button,
  Form,
} from "react-bootstrap";
import Head from "next/head";
import Link from "next/link";
import Image from "next/image";
import PageLayout from "@components/PageLayout";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import ListItemText from "@mui/material/ListItemText";
import Select from "@mui/material/Select";
import Checkbox from "@mui/material/Checkbox";
import Signup from "./form/Signup";
import RegisterRecieve from "./form/RegisterRecieve";
import Subscribe from "./form/Subscribe";

const Sholarships = () => {
  // multiple select in mui
  const ITEM_HEIGHT = 48;
  const ITEM_PADDING_TOP = 4;
  const MenuProps = {
    PaperProps: {
      style: {
        maxHeight: ITEM_HEIGHT * 3.5 + ITEM_PADDING_TOP,
        maxWidth: "100%",
        backgroundColor: "#B1B2FF",
        borderColor: "#B1B2FF",
        borderRadius: 1,
      },
    },
  };

  const names = [
    "Boston (6)",
    "Chicago (6)",
    "Hawaii (6)",
    "Los Angeles (6)",
    "Miami (5)",
    "New York (14)",
    "San Diego (1)",
    "San Francisco (9)",
    "Washington D.C. (5)",
  ];
  const courses = [
    "Masters Degree",
    "Bachelors Degree",
    "PG Diploma",
    "UG Diploma",
    "Short program",
    "PhD",
    "Diploma",
    "MBA",
    "Short Term",
  ];
  const tution = [
    "No Budget Constraints",
    "Upto 5 Lakh INR",
    "Upto 10 Lakh INR",
    "Upto 15 Lakh INR",
    "Upto 25 Lakh INR",
    "Upto 35 Lakh INR",
    "Upto 45 Lakh INR",
    "Upto 55 Lakh INR",
    "More than 55Lakh INR",
  ];
  const [personName, setPersonName] = React.useState([]);
  const [coursesName, setCoursesName] = React.useState([]);
  const [tutionName, setTutionName] = React.useState([]);

  const handleChange = (event) => {
    const {
      target: { value },
    } = event;
    setPersonName(
      // On autofill we get a stringified value.
      typeof value === "string" ? value.split(",") : value
    );
  };
  const coursesHandleChange = (event) => {
    const {
      target: { value },
    } = event;
    setCoursesName(
      // On autofill we get a stringified value.
      typeof value === "string" ? value.split(",") : value
    );
  };
  const tutionHandleChange = (event) => {
    const {
      target: { value },
    } = event;
    setTutionName(
      // On autofill we get a stringified value.
      typeof value === "string" ? value.split(",") : value
    );
  };
  // multiple select in mui
  const [items, setItems] = useState([]);
  const fetchData = async () => {
    const response = await fetch("https://retoolapi.dev/jGutXu/data");
    const data = await response.json();
    return setItems(data);
  };
  useEffect(() => {
    fetchData();
  }, []);
  return (
    <div>
      <Head>
        <title>Scholarships | Business Verification Services</title>
        <meta
          id="meta-description"
          name="description"
          content=" This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
        <meta
          property="og:title"
          content="Scholarships | Business Verification Services"
        />
        <meta
          property="og:description"
          content="This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
        <meta
          property="twitter:title"
          content="Certified Academy | Academy Verification | VerifyKiya"
        />
        <meta
          property="twitter:description"
          content="This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
      </Head>
      <PageLayout>
        <div className="resources ">
          <div className="HeroSectionWrapper">
            <Container fluid>
              <Row>
                <Col md={12}>
                  <div className="headingWhileLabel mb-5">
                    <div className="text-center">
                      <h1>Scholarships</h1>
                      <h4>Harvard College, Cambridge United States</h4>
                    </div>
                    <p>
                      An institute is an organizational body created for a
                      certain purpose. They are often research organizations
                      (research institutes) created to do research on specific
                      topics, or can also be a professional body. In some
                      countries, institutes can be part of a university or other
                      institutions of higher education, either as a group of
                      departments or an autonomous educational institution
                      without a traditional university status such as a
                      "university institute" (see Institute of Technology).
                    </p>
                  </div>
                  <div className="text-center">

                  <h4><strong>Tips to Secure Scholarships for International Students</strong></h4>
                  <p>As a study abroad applicant looking for study abroad scholarships, you can follow some tips below:</p>
                  </div>
                  <div className="list-center">
                    <ul>
                      <li>
                        Coaching -<strong> UPSC, PSC</strong>
                      </li>
                      <li>
                        Duration - <strong> 4 to 12 Months</strong>
                      </li>
                      <li>
                        Fees - <strong>₹ 7,000 - ₹ 1,60,000</strong>
                      </li>
                      <li>
                        Classes - <strong>6 days/week</strong>
                      </li>
                      <li>
                        Course Type -
                        <strong>Regular, Online, Test Series</strong>
                      </li>
                      <li>
                        Scholarships - <strong>Provided</strong>
                      </li>
                      <li>
                        Entrance exam - <strong>Conducted</strong>
                      </li>
                      <li>
                        Mock exams - <strong>Conducted</strong>
                      </li>
                      <li>
                        Test series - <strong>Conducted</strong>
                      </li>
                    </ul>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
          <div className="wrapper contact scholar">
            <div className="contact-box pb-5">
              <Container fluid>
                <Row>
                  <Col lg={9}>
                    <div className="bg ">
                      <div className="z-index">
                        <div className="align-items-end d-flex flex-wrap ">
                          <div className="me-3 mb-lg-0 mb-2">
                            <div className="icons">
                              <Image
                                src="/images/institute/icon.svg"
                                height={100}
                                width={100}
                              />
                            </div>
                          </div>
                          <div>
                            <div className="end">
                              <div>
                                <h1>
                                  Harvard College, Cambridge United States
                                </h1>
                                <p className="m-0">
                                  Select the stream for which you want to use
                                  College Predictor
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                        <Row className="mt-3">
                          <Col xl={4} lg={12}>
                            <FormLabel className="text-white">
                              <strong>Select Countries</strong>
                            </FormLabel>
                            <FormControl
                              sx={{
                                m: 0,
                                width: "100%",
                                backgroundColor: "#B1B2FF",
                                borderRadius: 1,
                              }}
                              size="small"
                            >
                              {/* <InputLabel id="demo-multiple-checkbox-label">
                          Search all locations
                        </InputLabel> */}
                              <Select
                                labelId="demo-multiple-checkbox-label"
                                id="demo-multiple-checkbox"
                                multiple
                                value={personName}
                                onChange={handleChange}
                                input={<OutlinedInput />}
                                renderValue={(selected) => selected.join(", ")}
                                MenuProps={MenuProps}
                              >
                                {names.map((name) => (
                                  <MenuItem key={name} value={name}>
                                    <Checkbox
                                      checked={personName.indexOf(name) > -1}
                                    />
                                    <ListItemText primary={name} />
                                  </MenuItem>
                                ))}
                              </Select>
                            </FormControl>
                          </Col>
                          <Col xl={4} lg={12}>
                            <FormLabel className="text-white">
                              <strong>Select Subject</strong>
                            </FormLabel>
                            <FormControl
                              sx={{
                                m: 0,
                                width: "100%",
                                backgroundColor: "#B1B2FF",
                                borderRadius: 1,
                              }}
                              size="small"
                            >
                              {/* <InputLabel id="demo-multiple-checkbox-label">
                          Browse by Courses
                        </InputLabel> */}
                              <Select
                                labelId="demo-multiple-checkbox-label"
                                id="demo-multiple-checkbox"
                                multiple
                                value={coursesName}
                                onChange={coursesHandleChange}
                                input={<OutlinedInput />}
                                renderValue={(selected) => selected.join(", ")}
                                MenuProps={MenuProps}
                              >
                                {courses.map((courses) => (
                                  <MenuItem key={courses} value={courses}>
                                    <Checkbox
                                      checked={
                                        coursesName.indexOf(courses) > -1
                                      }
                                    />
                                    <ListItemText primary={courses} />
                                  </MenuItem>
                                ))}
                              </Select>
                            </FormControl>
                          </Col>
                          <Col xl={3} lg={12}>
                            <FormLabel className="text-white">
                              <strong>Nationality</strong>
                            </FormLabel>
                            <FormControl
                              sx={{
                                m: 0,
                                width: "100%",
                                backgroundColor: "#B1B2FF",
                                borderRadius: 1,
                              }}
                              size="small"
                            >
                              {/* <InputLabel id="demo-multiple-checkbox-label">
                          Tuition Fees
                        </InputLabel> */}
                              <Select
                                labelId="demo-multiple-checkbox-label"
                                id="demo-multiple-checkbox"
                                multiple
                                value={tutionName}
                                onChange={tutionHandleChange}
                                input={<OutlinedInput />}
                                renderValue={(selected) => selected.join(", ")}
                                MenuProps={MenuProps}
                              >
                                {tution.map((tution) => (
                                  <MenuItem key={tution} value={tution}>
                                    <Checkbox
                                      checked={tutionName.indexOf(tution) > -1}
                                    />
                                    <ListItemText primary={tution} />
                                  </MenuItem>
                                ))}
                              </Select>
                            </FormControl>
                          </Col>
                          <Col
                            lg={1}
                            className="d-flex align-items-end justify-content-end-xl"
                          >
                            <div>
                              <button className="btn btn-success">
                                Search
                              </button>
                            </div>
                          </Col>
                        </Row>
                        <Row>
                          <Col lg={12}>
                            {items.map((item, index) => (
                              <div key={index}>
                                <div className="price my-5">
                                  <div className="d-flex justify-content-between align-items-center header flex-wrap gap-5">
                                    <div className="text d-flex flex-wrap align-items-center gap-2">
                                      <div className="ico">
                                        <div className="sm-icons">
                                          <img
                                            src={item.img}
                                            alt="img"
                                            className="img-fluid"
                                          />
                                        </div>
                                      </div>
                                      <strong>{item.title}</strong>
                                    </div>
                                    <div className="btns">
                                      <Link href="/">
                                        <Button className="btn btn-primary">
                                          Get Alert
                                        </Button>
                                      </Link>
                                      <Link href="">
                                        <Button className="btn btn-success" >
                                          <Link href='/ScholarshipDetails'><span>View Scholarship</span></Link>
                                        </Button>
                                      </Link>
                                    </div>
                                  </div>
                                  <div className="p-4">
                                    <ul>
                                      <li>
                                        <strong>
                                          International Student Eligible:
                                        </strong>
                                      </li>
                                      <li>
                                        <strong>Amount:</strong>
                                      </li>
                                      <li>
                                        <strong>Type:</strong>
                                      </li>
                                      <li>
                                        <strong>Level Of Study:</strong>
                                      </li>
                                      <li>
                                        <strong>Number Of Scholarships:</strong>
                                      </li>
                                    </ul>
                                  </div>
                                </div>
                              </div>
                            ))}
                          </Col>
                        </Row>
                      </div>
                    </div>
                  </Col>
                  <Col lg={3}>
                    <Signup />
                    <RegisterRecieve />
                    <Subscribe />
                  </Col>
                </Row>
              </Container>
            </div>
          </div>
        </div>
      </PageLayout>
    </div>
  );
};

export default Sholarships;
