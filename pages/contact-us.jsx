import React from "react";
import { Col, Row, Container } from "react-bootstrap";
import Card from "react-bootstrap/Card";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import Button from "react-bootstrap/Button";
import Image from "next/image";
import PageLayout from "@components/PageLayout";
import Head from "next/head";
function Contact() {
  return (
    <React.Fragment>
      <Head>
        <title>Contact VerifyKiya - Get in Touch with Us Today</title>
        <meta
          id="meta-description"
          name="description"
          content="Contact VerifyKiya for more information about our services and solutions. Fill out our contact form or email us to learn more."
        />
        <meta
          property="og:title"
          content="Contact VerifyKiya - Get in Touch with Us Today"
        />
        <meta
          property="og:description"
          content="Contact VerifyKiya for more information about our services and solutions. Fill out our contact form or email us to learn more."
        />
        <meta
          property="twitter:title"
          content="Contact VerifyKiya - Get in Touch with Us Today"
        />
        <meta
          property="twitter:description"
          content="Contact VerifyKiya for more information about our services and solutions. Fill out our contact form or email us to learn more."
        />
      </Head>
      <PageLayout>
        <div className="contact">
          <div className="HeroSectionWrapper">
            <div className="headingAboutUs Montserrat">
              <h1>Contact Us</h1>
              <div className="aboutuspera Montserrat">
                <p>
                  VerifyKiya’s wide gamut of degree verification, job &
                  scholarship notification, and listing services have helped
                  hundreds and thousands of students and educational institutes
                  alike and enabled them to ride the blockchain wave. Would you
                  like to be a part of that journey? Reach out to us; we’ll get
                  back to you shortly:)
                </p>
              </div>
            </div>
          </div>

          <Container fluid className="mb-5">
            <Row>
              <Col lg={4}>
                <Card>
                  <Image
                    src="/images/contact/help.svg"
                    height={263}
                    width={293}
                  />
                  <Card.Body>
                    <Card.Title>Help and Support</Card.Title>
                    <Card.Text>
                      Let our team set up your new credentials & identity
                      ecosystem. modular architecture & unique consensus
                      components.
                    </Card.Text>
                  </Card.Body>
                </Card>
              </Col>
              <Col lg={4}>
                <Card>
                  <Image
                    src="/images/contact/location.svg"
                    height={263}
                    width={293}
                  />
                  <Card.Body>
                    <Card.Title>Location</Card.Title>
                    <Card.Text>
                      Send Digital Certificate Start creating student
                      identities, credentials and detailed academic achievements
                      instantly.
                    </Card.Text>
                  </Card.Body>
                </Card>
              </Col>
              <Col lg={4}>
                <Card>
                  <Image
                    src="/images/contact/live.svg"
                    height={263}
                    width={293}
                  />
                  <Card.Body>
                    <Card.Title>Live Support</Card.Title>
                    <Card.Text>
                      Issue secured and blockchain encrypted credentials within
                      minutes unique consensus components.
                    </Card.Text>
                  </Card.Body>
                </Card>
              </Col>
            </Row>
          </Container>
          <div className="contact-box mb-5 d-none">
            <Container fluid>
              <Row className="bg ">
                <Col lg={12}>
                  <div className="box"></div>
                  <div className="icon"></div>
                  <h1>Contact Our Friendly</h1>
                  <h2>Support Team</h2>
                  <p>
                    e-Validated uses Hyperledger Fabric & Hyperledger Indy to
                    create the credential ecosystem and retain complete
                    confidentiality we have created a solution that’s truly
                    one-of-a-kind and native for Indian education’s
                    infrastructure.
                  </p>
                </Col>
                <Col lg={12}>
                  <Form>
                    <Row>
                      <Col lg={6}>
                        <Form>
                          <InputGroup size="sm" className="mb-3">
                            <InputGroup.Text id="inputGroup-sizing-sm">
                              <div>
                                <Image
                                  src="/images/contact/name.svg"
                                  width={293}
                                  height={263}
                                  className="img-fluid me-2"
                                />
                              </div>
                              <div className="text">Your Name:</div>
                            </InputGroup.Text>
                            <Form.Control
                              aria-label="Small"
                              aria-describedby="inputGroup-sizing-sm"
                            />
                          </InputGroup>
                        </Form>
                      </Col>
                      <Col lg={6}>
                        <Form>
                          <InputGroup size="sm" className="mb-3">
                            <InputGroup.Text id="inputGroup-sizing-sm">
                              <div>
                                <Image
                                  src="/images/contact/email.svg"
                                  width={293}
                                  height={263}
                                  className="img-fluid me-2"
                                />
                              </div>
                              <div className="text">Your Email:</div>
                            </InputGroup.Text>
                            <Form.Control
                              aria-label="Small"
                              aria-describedby="inputGroup-sizing-sm"
                            />
                          </InputGroup>
                        </Form>
                      </Col>
                    </Row>
                    <Row>
                      <Col lg={6}>
                        <Form>
                          <InputGroup size="sm" className="mb-3">
                            <InputGroup.Text id="inputGroup-sizing-sm">
                              <div>
                                <Image
                                  src="/images/contact/phone.svg"
                                  width={293}
                                  height={263}
                                  className="img-fluid me-2"
                                />
                              </div>
                              <div className="text">Phone:</div>
                            </InputGroup.Text>
                            <Form.Control
                              aria-label="Small"
                              aria-describedby="inputGroup-sizing-sm"
                            />
                          </InputGroup>
                        </Form>
                      </Col>
                      <Col lg={6}>
                        <Form>
                          <InputGroup size="sm" className="mb-3">
                            <InputGroup.Text id="inputGroup-sizing-sm">
                              <div>
                                <Image
                                  src="/images/contact/subject.svg"
                                  width={266}
                                  height={266}
                                  className="img-fluid me-2"
                                />
                              </div>
                              <div className="text">Subject: </div>
                            </InputGroup.Text>
                            <Form.Control
                              aria-label="Small"
                              aria-describedby="inputGroup-sizing-sm"
                            />
                          </InputGroup>
                        </Form>
                      </Col>
                    </Row>
                    <Row>
                      <Col lg={12}>
                        <Form
                          className="mb-3"
                          controlId="exampleForm.ControlTextarea1"
                        >
                          <Form.Control as="textarea" rows={3} />
                        </Form>
                      </Col>
                    </Row>
                    <div className="text-end">
                      <Button variant="success" size="sm">
                        Send
                      </Button>
                    </div>
                  </Form>
                  <div className="frame"></div>
                </Col>
              </Row>
            </Container>
          </div>
          <div className="contact-box map ">
            <Container fluid>
              <Row className="bg ">
                <Col lg={12}>
                  <div className="box"></div>
                  <div className="icon"></div>
                  <h1>You Reach to Me through </h1>
                  <h2>Google Maps</h2>
                  <p>
                    e-Validated uses Hyperledger Fabric & Hyperledger Indy to
                    create the credential ecosystem and retain complete
                    confidentiality we have created a solution that’s truly
                    one-of-a-kind and native for Indian education’s
                    infrastructure.
                  </p>
                </Col>
                <Col lg={12}>
                  <Form>
                    <Row>
                      <Col lg={6} className="d-none">
                        <Form>
                          <InputGroup size="sm" className="mb-3">
                            <InputGroup.Text id="inputGroup-sizing-sm">
                              <div>
                                <Image
                                  src="/images/contact/name.svg"
                                  width={266}
                                  height={266}
                                  className="img-fluid me-2"
                                />
                              </div>
                              <div className="text">From:</div>
                            </InputGroup.Text>
                            <Form.Control
                              aria-label="Small"
                              aria-describedby="inputGroup-sizing-sm"
                            />
                          </InputGroup>
                        </Form>
                      </Col>
                      <Col lg={6} className="d-none">
                        <Form>
                          <InputGroup size="sm" className="mb-3">
                            <InputGroup.Text id="inputGroup-sizing-sm">
                              <div>
                                <Image
                                  src="/images/contact/email.svg"
                                  width={266}
                                  height={266}
                                  className="img-fluid me-2"
                                />
                              </div>
                              <div className="text">To:</div>
                            </InputGroup.Text>
                            <Form.Control
                              aria-label="Small"
                              aria-describedby="inputGroup-sizing-sm"
                            />
                          </InputGroup>
                        </Form>
                      </Col>
                      <div className="map-image">
                        <div class="mapouter">
                          <div class="gmap_canvas">
                            <iframe
                              width="100%"
                              height="510"
                              id="gmap_canvas"
                              src="https://maps.google.com/maps?q=UTC Urbtech sector 132 Greater Noida&t=&z=10&ie=UTF8&iwloc=&output=embed"
                              frameborder="0"
                            ></iframe>
                          </div>
                        </div>
                      </div>
                    </Row>
                  </Form>
                  <div className="frame"></div>
                </Col>
              </Row>
            </Container>
          </div>
        </div>
      </PageLayout>
    </React.Fragment>
  );
}

export default Contact;
