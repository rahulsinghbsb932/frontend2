import React from "react";
import Head from "next/head";
import Image from "next/image";
import PageLayout from "@components/PageLayout";
import { Container, Row, Col } from "react-bootstrap";
import Card from "react-bootstrap/Card";

const jobMarketplace = () => {
  return (
    <div>
      <Head>
        <title>Job Marketplace | Career Platform | Hiring Marketplace</title>
        <meta
          id="meta-description"
          name="description"
          content="Discover new job opportunities on our career platform. Join our hiring marketplace and connect with top employers today. Find your dream job now."
        />
        <meta
          property="og:title"
          content=" Job Marketplace | Career Platform | Hiring Marketplace"
        />
        <meta
          property="og:description"
          content="Discover new job opportunities on our career platform. Join our hiring marketplace and connect with top employers today. Find your dream job now."
        />
        <meta
          property="twitter:title"
          content=" Job Marketplace | Career Platform | Hiring Marketplace"
        />
        <meta
          property="twitter:description"
          content="Discover new job opportunities on our career platform. Join our hiring marketplace and connect with top employers today. Find your dream job now."
        />
      </Head>

      <PageLayout>
        <div className="HeroSectionWrapper">
          <Container fluid>
            <Row>
              <Col md={12}>
                <div className="headingWhileLabel Montserrat">
                  <h1>Job MarketPlace</h1>
                  <p>
                    Document management is becoming increasingly important as
                    the idea of a paperless office becomes a commonplace
                    reality. Hold up your documents on a single platform
                    effortlessly. Verifykiya enables you to store documents in a
                    blockchain protected Digital Wallet which provides
                    protection against vulnerable situations and hackers.
                  </p>
                </div>
              </Col>
            </Row>
          </Container>

          <Container fluid className="mb-5">
            <Row>
              <Col md={3} className="rowCenter mt-5">
                <a className="cardanchor" href="#Skill-Based-Analysis">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Job_Marketplace/Skill Based Analysis.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Job_Marketplace/Skill Based Analysis.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Skill Based Analysis
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={3} className="rowCenter mt-5">
                <a className="cardanchor" href="#Background-Verified-Resumes">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Job_Marketplace/Background Verified Resumes.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Job_Marketplace/Background Verified Resumes.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Background Verified Resumes
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={3} className="rowCenter mt-5">
                <a className="cardanchor" href="#AI-Curated-Resumes">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Job_Marketplace/AI Curated Resumes.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Job_Marketplace/AI Curated Resumes.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        AI Curated Resumes
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={3} className="rowCenter mt-5">
                <a className="cardanchor" href="#Single-Click-Apply">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Job_Marketplace/Single Click Apply.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Job_Marketplace/Single Click Apply.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Single Click Apply
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={3} className="rowCenter mt-5">
                {" "}
                <a className="cardanchor" href="#Track-Status">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Job_Marketplace/Track Status.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Job_Marketplace/Track Status.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Track Status
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={3} className="rowCenter mt-5">
                {" "}
                <a className="cardanchor" href="#Real-Time-Alerts">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Job_Marketplace/Real Time Alerts.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Job_Marketplace/Real Time Alerts.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Real Time Alerts
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={3} className="rowCenter mt-5">
                {" "}
                <a
                  className="cardanchor"
                  href="#Accelerate-Hiring-With-ATS-Plug-ins"
                >
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Job_Marketplace/Accelerate Hiring With ATS Plug-ins.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Job_Marketplace/Accelerate Hiring With ATS Plug-ins.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Accelerate Hiring With ATS Plug-ins
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>

              <Col md={3} className="rowCenter mt-5"></Col>
            </Row>
          </Container>
        </div>

        <section id="Skill-Based-Analysis">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Job_Marketplace/Skill Based Analysis.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Job_Marketplace/Skill Based Analysis.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Job_Marketplace/Skill Based Analysis.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Job_Marketplace/Skill Based Analysis.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Skill Based Analysis
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      VerifyKiya assess and evaluate your skills, abilities, and
                      proficiency in relation to a specific job. We analyze your
                      skills, match them with the skills required for the
                      particular job, and suggest the best suitable job for you.
                      VerifyKiya assists in reducing unconscious bias and
                      improving the fairness of the hiring process, as well as
                      allowing the management to focus on more vital
                      responsibilities. Verifykiya’s curated resumes on the
                      basis of required job description reduces time and effort
                      of the recruiter in initial screening of the resume.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section id="Background-Verified-Resumes">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Job_Marketplace/Background Verified Resumes.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Job_Marketplace/Background Verified Resumes.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Background Verified Resumes
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      VerifyKiya helps validate the authenticity and accuracy of
                      all the information provided by the candidates applying
                      for jobs. We take care of the background screening and let
                      you focus on hiring the best suited employees for the jobs
                      without worrying about the authenticity of the information
                      on candidates resumes.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
                <Col xl={3} lg={12} className="order-xl-2 order-1">
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Job_Marketplace/Background Verified Resumes.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid "
                    /> */}
                    <Image
                      src="/images/Services/images/Job_Marketplace/Background Verified Resumes.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section id="AI-Curated-Resumes">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Job_Marketplace/AI Curated Resumes.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Job_Marketplace/AI Curated Resumes.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Job_Marketplace/AI Curated Resumes.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}
                      <Image
                        src="/images/Services/icon/Job_Marketplace/AI Curated Resumes.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        AI Curated Resumes
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      VerifyKiya analyzes and curates resumes using artificial
                      intelligence technologies to speed up the screening and
                      selection process, reducing the workload of the HR
                      department. VerifyKiya assists in reducing unconscious
                      bias and improving the fairness of the hiring process by
                      letting the AI select the resumes and avoiding personal
                      preferences, as well as providing compatibility score with
                      resume based on job description enabling the management to
                      hire the best possible candidate.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section id="Single-Click-Apply">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Job_Marketplace/Single Click Apply.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Job_Marketplace/Single Click Apply.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Job_Marketplace/Single Click Apply.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Job_Marketplace/Single Click Apply.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Single Click Apply
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Making a professional looking resume, updating the resumes
                      with time, and applying for a job is a tedious task, but
                      with Verifykiya, it is just a click away from you, whether
                      you are a student or an employee. We provide you with a
                      platform to apply for a job with a single click based on
                      your skills, qualifications, interests, and profile.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section id="Track-Status">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Job_Marketplace/Track Status.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}
                      <Image
                        src="/images/Services/icon/Job_Marketplace/Track Status.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">Track Status</h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Verifykiya gives you a feature to keep track of all the
                      jobs you have applied for and monitor the status of each
                      job effortlessly. Using this feature on Verifykiya lets
                      you communicate faster with the recruiter and also
                      additional details can be submitted faster to recruiter if
                      required. This helps you maintain a healthy work-life
                      balance and plan your future better with all the saved
                      time.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
                <Col xl={3} lg={12} className="order-xl-2 order-1">
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Job_Marketplace/Track Status.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}
                    <Image
                      src="/images/Services/images/Job_Marketplace/Track Status.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section id="Real-Time-Alerts">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Job_Marketplace/Real Time Alerts.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Job_Marketplace/Real Time Alerts.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Job_Marketplace/Real Time Alerts.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}
                      <Image
                        src="/images/Services/icon/Job_Marketplace/Real Time Alerts.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Real Time Alerts
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Verifykiya enables you to receive notifications in
                      real-time updates on your Job applications or new job
                      alerts so that you can take immediate action . With
                      real-time alerts, you can quickly respond to any requests
                      for additional information or follow-up. You can stay
                      organized and on top of your job application process,
                      increasing your chances of success in securing the best
                      job.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section className="mb-5" id="Accelerate-Hiring-With-ATS-Plug-ins">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Job_Marketplace/Real Time Alerts.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Job_Marketplace/Real Time Alerts.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Accelerate Hiring With ATS Plug-ins
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      VerifyKiya helps companies and institutions to avoid
                      aggravating risks in a candidate’s recruitment, checking
                      employment records, checking educational records, and
                      ensuring the ethical behavior of individuals. The
                      Automated Trust Score (ATS) plug-ins will helps the
                      organization by recruiting employees with high trust score
                      which safeguards the employer from potential cheats and
                      frauds.
                    </p>
                  </div>
                </Col>
                <Col xl={3} lg={12} className="order-xl-2 order-1">
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Job_Marketplace/Accelerate Hiring With ATS Plug-ins.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Job_Marketplace/Accelerate Hiring With ATS Plug-ins.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
      </PageLayout>
    </div>
  );
};

export default jobMarketplace;
