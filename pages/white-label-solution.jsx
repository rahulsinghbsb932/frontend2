import React from "react";
import Head from "next/head";
import Image from "next/image";
import PageLayout from "@components/PageLayout";
import { Container, Row, Col } from "react-bootstrap";
import Card from "react-bootstrap/Card";

const whiteLabelSolution = () => {
  return (
    <div>
      <Head>
        <title>White Label Solution</title>
        <meta
          id="meta-description"
          name="description"
          content="Securely store and verify your documents with our blockchain-based credentials wallet. Get started with document verification today."
        />
        <meta
          property="og:title"
          content="  Blockchain Based Wallets For Documents storage | Credentials Wallet"
        />
        <meta
          property="og:description"
          content="Securely store and verify your documents with our blockchain-based credentials wallet. Get started with document verification today."
        />
        <meta
          property="twitter:title"
          content="  Blockchain Based Wallets For Documents storage | Credentials Wallet"
        />
        <meta
          property="twitter:description"
          content="Securely store and verify your documents with our blockchain-based credentials wallet. Get started with document verification today."
        />
      </Head>

      <PageLayout>
        <div className="HeroSectionWrapper">
          <Container fluid>
            <Row>
              <Col md={12}>
                <div className="headingWhileLabel Montserrat">
                  <h1>White Label Solution</h1>
                  <p>
                    Document management is becoming increasingly important as
                    the idea of a paperless office becomes a commonplace
                    reality. Hold up your documents on a single platform
                    effortlessly. Verifykiya enables you to store documents in a
                    blockchain protected Digital Wallet which provides
                    protection against vulnerable situations and hackers.
                  </p>
                </div>
              </Col>
            </Row>
          </Container>

          <Container fluid className="mb-5">
            <Row>
              <Col md={4} className="rowCenter mt-5">
                <a className="cardanchor" href="#White-Label">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="img-fluid"
                      src={
                        require("../../assets/img/Services/icon/White_Label/White Label.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/White_Label/White Label.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">White Label</Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={4} className="rowCenter mt-5">
                <a className="cardanchor" href="#Option-to-customise">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/White_Label/Option to customise.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/White_Label/Option to customise.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Option to customise
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={4} className="rowCenter mt-5">
                <a className="cardanchor" href="#Brand-Building">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/White_Label/Brand Building.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/White_Label/Brand Building.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Brand Building
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
            </Row>
            <Row>
              <Col md={4} className="rowCenter mt-5">
                <a className="cardanchor" href="#Seamless-Integration">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/White_Label/Seamless Integration.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/White_Label/Seamless Integration.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Seamless Integration
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={4} className="rowCenter mt-5">
                {" "}
                <a className="cardanchor" href="#Trustless-Immutable">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/White_Label/Trustless__x26__Immutable_2_.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/White_Label/Trustless__x26__Immutable_2_.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Trustless & Immutable
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={4} className="rowCenter mt-5">
                {" "}
                <a className="cardanchor" href="#Data-Protection">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/White_Label/Data Protection.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/White_Label/Data Protection.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Data Protection
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
            </Row>
          </Container>
        </div>

        <section id="White-Label">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/White Label Solution/White Label.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/White Label Solution/White Label.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/White_Label/White Label.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/White_Label/White Label.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">White Label</h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      With VerifyKiya’s robust white labeling solution, you can
                      offer our services under your own brand while maintaining
                      originality and authenticity of all your certifications.
                      We understand the importance of branding which is why we
                      offer a white label solution where you can offer our
                      services under your own name without having to develop a
                      solution from scratch.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section id="Option-to-customise">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/White_Label/Option to customise.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/White_Label/Option to customise.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Option to customise
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      VerifyKiya offers a wide range of industry-approved and
                      attractive templates to choose from so that your work gets
                      easier and much quicker. Along with this, we also have a
                      customization feature that allows you to request for a
                      unique template personalized to your perfection.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
                <Col xl={3} lg={12} className="order-xl-2 order-1">
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/White Label Solution/Option to customise.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid "
                    /> */}

                    <Image
                      src="/images/Services/images/White Label Solution/Option to customise.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section id="Brand-Building">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/White Label Solution/Brand Building.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/White Label Solution/Brand Building.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/White_Label/Brand Building.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/White_Label/Brand Building.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">Brand Building</h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Brand building for institutions is a continuous and
                      painful exercise. Any unauthorized or spurious credential
                      adversely affects the brand of the institution and has a
                      direct effect on its business. Usage of blockchain and NFT
                      for credentials makes your institution stand apart from
                      the rest and also increases its credibility. It increases
                      your brand visibility not only within the student
                      community but also within the corporate sector.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section id="Seamless-Integration">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/White_Label/Seamless Integration.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/White_Label/Seamless Integration.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Seamless Integration
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      VerifyKiya's white label solution can be easily integrated
                      into any company without much disruption to their normal
                      processes. Companies can focus on their normal processes
                      without having to develop software like ours from scratch.
                      We make tailor-made solutions so that all your needs and
                      wants can be incorporated.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/White Label Solution/Seamless Integration.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/White Label Solution/Seamless Integration.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section id="Trustless-Immutable">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/White Label Solution/Trustless & Immutable.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/White Label Solution/Trustless & Immutable.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/White_Label/Trustless__x26__Immutable_2_.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/White_Label/Trustless__x26__Immutable_2_.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Trustless & Immutable
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      With the entire process powered by blockchain technology,
                      VerifyKiya’s blockchain-based digital certificates are
                      immutable and can never be tampered with. Also, with our
                      trustless network, you can always rely on our
                      blockchain-based certificate and no longer have to place
                      your trust in third parties.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section id="Data-Protection" className="mb-5">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/White_Label/Data Protection.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/White_Label/Data Protection.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">Data Protection</h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Storing all the information on VerifyKiya’s blockchain
                      powered solution you’ll never have to worry about
                      protecting your data against any third-party from misusing
                      or stealing it. The data stored on blockchain stays
                      protected as it cannot be tampered with, changed, or
                      deleted. And only you’ll have the access to the data using
                      your private key
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/White Label Solution/Data Protection.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/White Label Solution/Data Protection.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
      </PageLayout>
    </div>
  );
};

export default whiteLabelSolution;
