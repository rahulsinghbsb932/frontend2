import React from "react";
import Card from "react-bootstrap/Card";
import { Container, Row, Col } from "react-bootstrap";
import Head from "next/head";
import Image from "next/image";
import PageLayout from "@components/PageLayout";

const Solution = () => {
  return (
    <div>
      <Head>
        <title>
          Event Participation Certificate| Verified Certification | Buy Tickets
        </title>
        <meta
          id="meta-description"
          name="description"
          content="'Event Participation Certifications' validate individuals' knowledge, skills, and expertise in planning, organizing, and executing events of various types and scales."
        />
        <meta
          property="og:title"
          content="Event Participation Certificate| Verified Certification | Buy Tickets"
        />
        <meta
          property="og:description"
          content="'Event Participation Certifications' validate individuals' knowledge, skills, and expertise in planning, organizing, and executing events of various types and scales."
        />
        <meta
          property="twitter:title"
          content="Event Participation Certificate| Verified Certification | Buy Tickets"
        />
        <meta
          property="twitter:description"
          content="'Event Participation Certifications' validate individuals' knowledge, skills, and expertise in planning, organizing, and executing events of various types and scales."
        />
      </Head>
      <PageLayout>
        <div>
          <div className="HeroSectionWrapper">
            <Container fluid>
              <Row>
                <Col md={12}>
                  <div className="headingWhileLabel Montserrat">
                    <h1>Events & Competitions</h1>
                    <p>
                      Verifykiya let’s the event organisers reward their
                      participants with state of the art blockchain based NFT
                      enabled event/ certification and verification of
                      credentials, badges, invites, entry pass etc. Verifykiya
                      autoamates and helps reduce the workload of the organisers
                      by providing seamless software for all their certification
                      needs with pool of templates to choose from and a event/
                      wallet for the participants to store these credentials. 
                    </p>
                    <p>
                      Participants can also share their issued accolades
                      directly to their social media handles directly from our
                      platform making them your social ambassadors and in turn
                      providing you with visibility and brand building
                    </p>
                  </div>
                </Col>
              </Row>
            </Container>

            <Container fluid>
              <Row>
                <Col lg={3} md={6} className="rowCenter mt-5">
                  <a href="/" className="cardanchor">
                    <Card className="CardSol">
                      <Image
                        src="/images/event/saminar.svg"
                        height={125}
                        width={125}
                      />

                      <Card.Body className="CardBody">
                        <Card.Title className="CardTitle">Seminars</Card.Title>
                        <Card.Text>
                          A Private is an educational institution designed to
                          provide learning spaces and learning.
                        </Card.Text>
                      </Card.Body>
                    </Card>
                  </a>
                </Col>
                <Col lg={3} md={6} className="rowCenter mt-5">
                  <a href="/" className="cardanchor">
                    <Card className="CardSol">
                      <Image
                        src="/images/event/trade.svg"
                        height={125}
                        width={125}
                      />

                      <Card.Body className="CardBody">
                        <Card.Title className="CardTitle">
                          Trade Shows
                        </Card.Title>
                        <Card.Text>
                          A Government is an educational institution designed to
                          provide learning spaces and learning.
                        </Card.Text>
                      </Card.Body>
                    </Card>
                  </a>
                </Col>
                <Col lg={3} md={6} className="rowCenter mt-5">
                  <a href="/" className="cardanchor">
                    <Card className="CardSol">
                      <Image
                        src="/images/event/confrences.svg"
                        height={125}
                        width={125}
                      />

                      <Card.Body className="CardBody">
                        <Card.Title className="CardTitle">
                          Conferences
                        </Card.Title>
                        <Card.Text>
                          A Multinational is an educational institution designed
                          to provide learning spaces and learning.
                        </Card.Text>
                      </Card.Body>
                    </Card>
                  </a>
                </Col>
                <Col lg={3} md={6} className="rowCenter mt-5">
                  <a href="/" className="cardanchor">
                    <Card className="CardSol">
                      <Image
                        src="/images/event/competitions.svg"
                        height={125}
                        width={125}
                      />
                      <Card.Body className="CardBody">
                        <Card.Title className="CardTitle">
                          Competitions
                        </Card.Title>
                        <Card.Text>
                          A NGO l is an educational institution designed to
                          provide learning spaces and learning.
                        </Card.Text>
                      </Card.Body>
                    </Card>
                  </a>
                </Col>
              </Row>
            </Container>
          </div>

          <div className="sectionRelative m-5">
            <Container fluid>
              <Col md={12}>
                <div class="ourSolutionWrapper">
                  <h1 class="ourSolutionSectionheading">
                    <span>How does </span>
                    Verifykiya Help You?
                  </h1>
                  <p className="ourSolutionPera">
                    Companies have a lot on their plate. To ensure that you
                    focus on your fundamental and key areas of your
                    organization, Verifykiya extends its assistance. A myriad of
                    tasks become a single-click phenomenon with Verifykiya’s
                    cutting edge platform.
                  </p>
                </div>
              </Col>
            </Container>
          </div>

          <section>
            <div className="solution">
              <Container fluid>
                <Row className="bg">
                  <Col xl={3} lg={12}>
                    <div className="img">
                      <Image
                        src="/images/event/event1.svg"
                        height={480}
                        width={300}
                      />
                    </div>
                  </Col>
                  <Col xl={9} lg={12} className="pt-3 p-5">
                    <div className="d-flex position">
                      <div className="icon">
                        <div className="event1-icon"></div>
                      </div>
                      <div className="solutioseciconTop ">
                        <h1 class="ourSolutionSectionheading">
                          Digital Certification, Badges, Invites & Entry Pass
                        </h1>
                      </div>
                    </div>

                    <div className="solutionSeciconText">
                      <p>
                        With our blockchain-based and NFT-enabled certification,
                        creation and verification of genuine credentials like
                        badges, certificates etc. will become seamless. The
                        trustless algorithm and the immutable nature of NFTs
                        make it impossible for any unauthorized changes.
                      </p>
                      <div className="space"></div>
                    </div>
                  </Col>
                </Row>
              </Container>
            </div>
          </section>

          <section>
            <div className="solution">
              <Container fluid>
                <Row className="bg">
                  <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                    <div className="d-flex position">
                      <div className="icon">
                        <div className="event2-icon"></div>
                      </div>
                      <div className="solutioseciconTop ">
                        <h1 class="ourSolutionSectionheading">
                          Integration With Existing System
                        </h1>
                      </div>
                    </div>

                    <div className="solutionSeciconText">
                      <p>
                        Our platform can be easily integrated in any company
                        without much disruption. There is no requirement of
                        changing your process, our software can integrate with
                        your existing SAP or other software solutionsl. We make
                        tailor-made solutions so that all your needs and wants
                        can be incorporated.
                      </p>
                      <div className="space"></div>
                    </div>
                  </Col>
                  <Col xl={3} lg={12} className="order-xl-2 order-1">
                    <div className="img">
                      <Image
                        src="/images/event/event2.svg"
                        height={480}
                        width={300}
                      />
                    </div>
                  </Col>
                </Row>
              </Container>
            </div>
          </section>

          <section>
            <div className="solution">
              <Container fluid>
                <Row className="bg">
                  <Col xl={3} lg={12}>
                    <div className="img">
                      <Image
                        src="/images/event/event3.svg"
                        height={480}
                        width={300}
                      />
                    </div>
                  </Col>
                  <Col xl={9} lg={12} className="pt-3 p-5">
                    <div className="d-flex position">
                      <div className="icon">
                        <div className="event3-icon"></div>
                      </div>
                      <div className="solutioseciconTop ">
                        <h1 class="ourSolutionSectionheading">
                          Access to Wide Consumer Base
                        </h1>
                      </div>
                    </div>

                    <div className="solutionSeciconText">
                      <p>
                        Our diverse and verifiable consumer base  will be
                        available to you with just a click! This pool will have
                        people with different qualifications, cohorts etc. so
                        all possible needs can be taken care of! This talent
                        pool will save time and money as candidates through this
                        will already have verified credentials.
                      </p>
                      <div className="space"></div>
                    </div>
                  </Col>
                </Row>
              </Container>
            </div>
          </section>

          <section>
            <div className="solution">
              <Container fluid>
                <Row className="bg">
                  <Col xl={3} lg={12}>
                    <div className="img">
                      <Image
                        src="/images/event/event4.svg"
                        height={480}
                        width={300}
                      />
                    </div>
                  </Col>
                  <Col xl={9} lg={12} className="pt-3 p-5 ">
                    <div className="d-flex position">
                      <div className="icon">
                        <div className="event4-icon"></div>
                      </div>
                      <div className="solutioseciconTop ">
                        <h1 class="ourSolutionSectionheading">
                          Reduced Workload and Time Saving
                        </h1>
                      </div>
                    </div>

                    <div className="solutionSeciconText">
                      <p>
                        Verikykiya extends complete support for any data
                        maintenance or automation needs you may have. Our robust
                        platform reduces administrative burden from your
                        shoulders. We even step in and automate admission,
                        credential verification, fee handling, placement etc to
                        streamline everything. 
                      </p>
                      <div className="space"></div>
                    </div>
                  </Col>
                </Row>
              </Container>
            </div>
          </section>
          <section>
            <div className="solution">
              <Container fluid>
                <Row className="bg">
                  <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                    <div className="d-flex position">
                      <div className="icon">
                        <div className="event5-icon"></div>
                      </div>
                      <div className="solutioseciconTop ">
                        <h1 class="ourSolutionSectionheading">
                          Social Ambassadors
                        </h1>
                      </div>
                    </div>

                    <div className="solutionSeciconText">
                      <p>
                        Through Verifykiya, Participants can even share their
                        issued accolades like certificates, badges etc. which
                        can boost your public image and put faces to your
                        success stories. They can emerge as ambassadors and
                        represent your company. This will boost morale and even
                        attract new ones. 
                      </p>
                    </div>
                  </Col>
                  <Col xl={3} lg={12} className="order-xl-2 order-1">
                    <div className="img">
                      <Image
                        src="/images/event/event5.svg"
                        height={480}
                        width={300}
                      />
                    </div>
                  </Col>
                </Row>
              </Container>
            </div>
          </section>

          <section className="mb-5">
            <div className="solution">
              <Container fluid>
                <Row className="bg">
                  <Col xl={3} lg={12}>
                    <div className="img">
                      <Image
                        src="/images/event/event6.svg"
                        height={480}
                        width={300}
                      />
                    </div>
                  </Col>
                  <Col xl={9} lg={12} className="pt-3 p-5">
                    <div className="d-flex position">
                      <div className="icon">
                        <div className="event6-icon"></div>
                      </div>
                      <div className="solutioseciconTop ">
                        <h1 class="ourSolutionSectionheading">
                          Option for Customization and White Labeling
                        </h1>
                      </div>
                    </div>

                    <div className="solutionSeciconText">
                      <p>
                        We offer a wide range of industry-approved and
                        attractive templates to choose from so that your work
                        gets easier and much quicker. Along with this, we also
                        have a customization feature which allows you to request
                        for a unique template personalized to your perfection.
                        We also give an option to institutions to  utilise our
                        platform as a white label solution as well. 
                      </p>
                      <div className="space"></div>
                    </div>
                  </Col>
                </Row>
              </Container>
            </div>
          </section>
        </div>
      </PageLayout>
    </div>
  );
};

export default Solution;
