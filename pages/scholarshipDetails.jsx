import React, { useState, useEffect } from "react";
import {
  Container,
  Row,
  Col,
  Table,
} from "react-bootstrap";
import Head from "next/head";
import PageLayout from "@components/PageLayout";
import Signup from "./form/Signup";
import RegisterRecieve from "./form/RegisterRecieve";
import Subscribe from "./form/Subscribe";

const ScholarshipDetail = () => {
  const [items, setItems] = useState([
    {
      title: "UPSC Course - Regular",
      para: "Central Delhi Classroom Programs",
      content:
        "UPSC is one of the toughest examinations. The institute helps the students in making the examination preparation a little easier by giving the right guidance to the students. The institute also conducts test series and provide study material that is made by the best and experienced faculty of the institute. The institute conducts classes for 3 to 4 hour long on weekdays and 7 to 8 hours long on weekends.",
      course: "Based",
      payment: "Government bodies",
      courses: "Ontario Government      ",
      classes: "6 days",
      coursetype: "Regular, Online, Test Series",
      duration: "N/A      ",
      batch: "75      ",
      total: "$28,961      ",
      coaching: "One Time Payment",
      fees: "Yes",
      entrance: "N/A",
      scholarships: "Provided",
      doubt: "Conducted",
      Mock: "Conducted",
      test: "Conducted",
      Eligibility: {
        heading: "Eligibility Criteria",
        para: "The deadline to apply for the Ontario Trillium Scholarship varies according to the university and program starting as early as September. Suitable candidates are nominated in the winter session for getting admitted to the following fall, spring or winter terms.",
        list: {
          item1:
            "Be an international student with a valid Canadian student visa.",
        },
      },
      application: {
        title: "Application Process",
        para: "Each eligible university’s unit of graduate studies oversees the application and selection processes for the Ontario Trillium Scholarship. Every institution has its own application process for the OTS program.",
        list: {
          item1:
            "Be an international student with a valid Canadian student visa.",
        },
      },
      selection: {
        title: "Selection Process",
        para: "Each eligible university’s unit of graduate studies oversees the application and selection processes for the Ontario Trillium Scholarship. Every institution has its own application process for the OTS program.",
        list: {
          item1:
            "Be an international student with a valid Canadian student visa.",
        },
      },
    },
  ]);
  return (
    <div>
      <Head>
        <title>Scholarship Detail | Business Verification Services</title>
        <meta
          id="meta-description"
          name="description"
          content=" This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
        <meta
          property="og:title"
          content="Scholarship Detail | Business Verification Services"
        />
        <meta
          property="og:description"
          content="This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
        <meta
          property="twitter:title"
          content="Certified Academy | Academy Verification | VerifyKiya"
        />
        <meta
          property="twitter:description"
          content="This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
      </Head>
      <PageLayout>
        <div className="resources scholar">
          <div className="HeroSectionWrapper">
            <Container fluid>
              <Row>
                <Col md={12}>
                  <div className="headingWhileLabel Montserrat text-center mb-5">
                    <h1>IDFC FIRST Bank</h1>
                    <h3>Education Loan Programme</h3>
                  </div>
                </Col>
              </Row>
              <Row>
                <Col md={12}>
                  <div className="headingWhileLabel Montserrat text-center mb-5">
                    <p>
                      IDFC FIRST Bank Education Loan Programme is a joint
                      initiative of verifykiya and IDFC FIRST Bank to provide
                      financial backing to aspiring students who have secured
                      admission into higher educational institutions in India or
                      abroad.
                    </p>
                  </div>
                  <div className="list-center">
                    <ul>
                      <li>
                        An amount of 40,000 CAD is awarded every year to the
                        best doctoral students from across the globe, - Can be
                        renewed for up to 4 years.
                      </li>
                      <li>
                        Provides scholars with partial funding to pursue a Ph.D.
                        in any discipline at Ontario’s top universities.
                      </li>
                      <li>
                        Funding is shared in a government-institution ratio of
                        2:1 (CAD 26,666 from the government and CAD13,333 from
                        the institution)
                      </li>
                    </ul>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
          <div className="d-flex">
            <iframe
              src="/images/1.png"
              scrolling="no"
              width="100%"
              height="100%"
              class="responsive-iframe"
            ></iframe>
            <iframe
              src="/images/2.png"
              scrolling="no"
              width="100%"
              height="100%"
              class="responsive-iframe"
            ></iframe>
          </div>
          <div className="contact wrapper">
            <div className="contact-box">
              <Container fluid>
                <Row>
                  <Col lg={9}>
                    {items.map((item, index) => (
                      <div key={index}>
                        <div className="bg mb-5">
                          <div className="h-frame d-xl-block d-none"></div>
                          <div className="z-index">
                            <Row className="my-5">
                              <Col lg={12}>
                                <div className="price">
                                  <div className="d-flex justify-content-between align-items-center header flex-wrap">
                                    <div className="text d-flex flex-wrap align-items-center gap-2">
                                      <div className="ico">
                                        <div className="sm-icons">
                                          <img
                                            src="/images/FacultyCheckicon.png"
                                            alt=""
                                            className="img-fluid"
                                          />
                                        </div>
                                      </div>
                                      <strong>Highlights</strong>
                                    </div>
                                  </div>
                                  <div className="p-3">
                                    <Table responsive>
                                      <tbody>
                                        <tr>
                                          <td>Scholarship Type	Merit</td>
                                          <td>:</td>
                                          <td>{item.course}</td>
                                        </tr>
                                        <tr>
                                          <td>Offered by	</td>
                                          <td>:</td>
                                          <td>{item.duration}</td>
                                        </tr>
                                        <tr>
                                          <td>Organization</td>
                                          <td>:</td>
                                          <td>{item.fees}</td>
                                        </tr>
                                        <tr>
                                          <td>Application Deadline	</td>
                                          <td>:</td>
                                          <td>{item.classes}</td>
                                        </tr>
                                        <tr>
                                          <td>No. of Scholarships	</td>
                                          <td>:</td>
                                          <td>{item.entrance}</td>
                                        </tr>
                                        <tr>
                                          <td>Amount	</td>
                                          <td>:</td>
                                          <td>{item.scholarships}</td>
                                        </tr>
                                        <tr>
                                          <td>Renewability	One Time Payment</td>
                                          <td>:</td>
                                          <td>{item.doubt}</td>
                                        </tr>
                                        <tr>
                                          <td>International Students Eligible	Yes</td>
                                          <td>:</td>
                                          <td>{item.Mock}</td>
                                        </tr>
                                        <tr>
                                          <td>Scholarship Website Link	N/A</td>
                                          <td>:</td>
                                          <td>{item.test}</td>
                                        </tr>
                                      </tbody>
                                    </Table>
                                  </div>
                                </div>

                                <div className="price my-5">
                                  <div className="d-flex justify-content-between align-items-center header flex-wrap">
                                    <div className="text d-flex flex-wrap align-items-center gap-2">
                                      <div className="ico">
                                        <div className="sm-icons">
                                          <img
                                            src="/images/FacultyCheckicon.png"
                                            alt=""
                                            className="img-fluid"
                                          />
                                        </div>
                                      </div>
                                      <strong>
                                        {item.Eligibility.heading}
                                      </strong>
                                    </div>
                                  </div>
                                  <div className="content">
                                    <div className="p-4">
                                      <p>{item.Eligibility.para}</p>
                                      <div>
                                        <strong>
                                          To be eligible for receiving OTS, an
                                          applicant must:
                                        </strong>
                                      </div>
                                      <ul>
                                        <li>{item.Eligibility.list.item1}</li>
                                      </ul>
                                    </div>
                                  </div>
                                </div>

                                <div className="price my-5">
                                  <div className="d-flex justify-content-between align-items-center header flex-wrap">
                                    <div className="text d-flex flex-wrap align-items-center gap-2">
                                      <div className="ico">
                                        <div className="sm-icons">
                                          <img
                                            src="/images/FacultyCheckicon.png"
                                            alt=""
                                            className="img-fluid"
                                          />
                                        </div>
                                      </div>
                                      <strong>{item.application.title}</strong>
                                    </div>
                                  </div>
                                  <div className="content">
                                    <div className="p-4">
                                      <p>{item.application.para}</p>
                                      <div>
                                        <strong>
                                          To be eligible for receiving OTS, an
                                          applicant must:
                                        </strong>
                                      </div>
                                      <ul>
                                        <li>{item.application.list.item1}</li>
                                      </ul>
                                    </div>
                                  </div>
                                </div>

                                <div className="price my-5">
                                  <div className="d-flex justify-content-between align-items-center header flex-wrap">
                                    <div className="text d-flex flex-wrap align-items-center gap-2">
                                      <div className="ico">
                                        <div className="sm-icons">
                                          <img
                                            src="/images/FacultyCheckicon.png"
                                            alt=""
                                            className="img-fluid"
                                          />
                                        </div>
                                      </div>
                                      <strong>{item.selection.title}</strong>
                                    </div>
                                  </div>
                                  <div className="content">
                                    <div className="p-4">
                                      <p>{item.selection.para}</p>
                                      <div>
                                        <strong>
                                          To be eligible for receiving OTS, an
                                          applicant must:
                                        </strong>
                                      </div>
                                      <ul>
                                        <li>{item.selection.list.item1}</li>
                                      </ul>
                                    </div>
                                  </div>
                                </div>
                              </Col>
                            </Row>
                          </div>
                        </div>
                      </div>
                    ))}
                  </Col>
                  <Col lg={3}>
                    <div className="w-300">
                      <div className="btns mb-3 ">
                        <button className="btn btn-success w-100">
                          Apply Now
                        </button>
                        <button className="btn btn-white w-100 my-3">
                          Follow
                        </button>
                        <button className="btn btn-white w-100">Share</button>
                      </div>
                      <Signup/>
                      <RegisterRecieve/>
                      <Subscribe/>
                    </div>
                  </Col>
                </Row>
              </Container>
            </div>
          </div>
        </div>
      </PageLayout>
    </div>
  );
};

export default ScholarshipDetail;
