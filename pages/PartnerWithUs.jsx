import React, { useState } from "react";
import { Col, Container, Row } from "react-bootstrap";
import Image from "next/image";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import Button from "react-bootstrap/Button";
import Signup from "./form/Signup";
import RegisterRecieve from "./form/RegisterRecieve";
import Subscribe from "./form/Subscribe";

const PartnerWithUs = () => {
  const [validated, setValidated] = useState(false);

  const handleSubmit = (event) => {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }

    setValidated(true);
  };
  return (
    <>
      <div className="parent-component">
        <Container fluid>
          <Row>
            <Col lg={12}>
              <div className="child-component">
                <h1 className="child-component-heading">Partner with Us</h1>
                <p className="child-component-para">
                  Become a MyClassboard Channel Partner. We are the leading
                  school ERP management software in India. We enable schools to
                  automate everyday operations with our innovative solutions.
                </p>
              </div>

              <Row>
                <Col xl={4}>
                  <center>
                    <div className="parterner-imgg">
                      <Image
                        src="/images/partner/p1.svg"
                        height={500}
                        width={250}
                        className="child-component-img1"
                      />
                      <div className="align-img">
                        <img
                          alt="   "
                          src="/images/partner/immg.svg"
                          className="partImgg"
                        />
                      </div>

                      <div className="align-img-img">
                        <img
                          alt="   "
                          src="/images/partner/imgIcn.svg"
                          className="partImgg"
                        />
                      </div>
                    </div>
                  </center>
                </Col>
                <Col xl={8}>
                  <h2 className="child-component-sub-heading">
                    Why partner with MyClassboard?
                  </h2>
                  <p className="child-component-sub-para">
                    It’s a quick 4 step process, which is hassle free and quick
                  </p>

                  <Row>
                    <Col lg={2}>
                      {/* <img
                        src={img1}
                        alt=""
                        className="child-component-imgg1"
                      /> */}
                      <center>
                        <div className="child-component-imgg1">
                          <Image
                            src="/images/partner/img1.svg"
                            height={100}
                            width={100}
                          />
                        </div>
                      </center>
                    </Col>
                    <Col lg={4}>
                      <h3 className="sub-child-component-sub-heading">
                        Attractive commission
                      </h3>
                      <p className="sub-child-component-sub-para">
                        Partner with us to boost your career as you align your
                        success with MyClassboard and earn a handsome
                        commission.
                      </p>
                    </Col>
                    <Col lg={2}>
                      {/* <img
                        src={img2}
                        alt=""
                        className="child-component-imgg1"
                      /> */}
                      <center>
                        <div className="child-component-imgg1">
                          <Image
                            src="/images/partner/img2.svg"
                            height={100}
                            width={100}
                          />
                        </div>
                      </center>
                    </Col>
                    <Col lg={4}>
                      <h3 className="sub-child-component-sub-heading">
                        Partner agreement
                      </h3>
                      <p className="sub-child-component-sub-para">
                        Receive a formal partnership agreement to verify your
                        partnership and work with authority.
                      </p>
                    </Col>
                  </Row>

                  <Row>
                    <Col lg={2}>
                      {/* <img
                        src={img3}
                        alt=""
                        className="child-component-imgg1"
                      /> */}

                      <center>
                        <div className="child-component-imgg1">
                          <Image
                            src="/images/partner/img3.svg"
                            height={100}
                            width={100}
                          />
                        </div>
                      </center>
                    </Col>
                    <Col lg={4}>
                      <h3 className="sub-child-component-sub-heading">
                        Assistance in follow-ups & closures
                      </h3>
                      <p className="sub-child-component-sub-para">
                        Our in-house team of product experts is happy to assist
                        you in achieving lead closures.
                      </p>
                    </Col>
                    <Col lg={2}>
                      {/* <img
                        src={img4}
                        alt=""
                        className="child-component-imgg1"
                      /> */}

                      <center>
                        <div className="child-component-imgg1">
                          <Image
                            src="/images/partner/img3.svg"
                            height={100}
                            width={100}
                          />
                        </div>
                      </center>
                    </Col>
                    <Col lg={4}>
                      <h3 className="sub-child-component-sub-heading">
                        Expert training & presentation materials
                      </h3>
                      <p className="sub-child-component-sub-para">
                        Bring fast and quality closures of leads using our
                        training & presentation materials.
                      </p>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
          </Row>
        </Container>
      </div>

      <div className="align-div-parent">
        <Container fluid>
          <Row>
            <Col xl={9} lg={12}>
              <div className="second-parent-main">
                <Row className="align-items-end alignment">
                  <Col lg={2}>
                    <center className="cen-pad-right">
                      <Image
                        src="/images/partner/refer.svg"
                        width={200}
                        height={200}
                        className="second-parent-main-img"
                      />
                    </center>
                  </Col>
                  <Col lg={10}>
                    <h3 className="second-parent-main-heading">
                      Refer & Earn with MyClassboard
                    </h3>
                    <p className="second-parent-main-para">
                      Interested in partnering with MyClassboard?
                    </p>
                  </Col>
                </Row>
                <Row className="mt-five">
                  <Col lg={12}>
                    <p className="second-parent-main-para-head">
                      Whether you’re a Teacher, educationalist, Homemaker,
                      Retired Employee, Self Employed Individual, College
                      pass-outs Consultants and freelancers in the education
                      industry, or anyone with good contacts, we are excited to
                      get you on board to take our vision forward for providing
                      digital solutions to schools.
                    </p>
                  </Col>
                </Row>
                <Row>
                  <Col lg={12}>
                    <Form
                      noValidate
                      validated={validated}
                      onSubmit={handleSubmit}
                      class="z-index position-relative"
                    >
                      <Row className="mb-3">
                        <Form.Group
                          as={Col}
                          md="6"
                          controlId="validationCustomUsername"
                        >
                          {/* <Form.Label>Name</Form.Label> */}
                          <InputGroup
                            hasValidation
                            className="input-form-group-m"
                          >
                            <InputGroup.Text
                              id="inputGroupPrepend"
                              className="input-form-group"
                            >
                              <Image
                                src="/images/partner/referIcon.svg"
                                width={30}
                                height={30}
                              />
                            </InputGroup.Text>
                            <Form.Control
                              type="text"
                              placeholder="Name"
                              aria-describedby="inputGroupPrepend"
                              className="input-form-group"
                              required
                            />
                            <Form.Control.Feedback type="invalid">
                              Please enter a Name.
                            </Form.Control.Feedback>
                          </InputGroup>
                        </Form.Group>

                        <Form.Group
                          as={Col}
                          md="6"
                          controlId="validationCustomUsername"
                        >
                          {/* <Form.Label>Email</Form.Label> */}
                          <InputGroup hasValidation>
                            <InputGroup.Text
                              id="inputGroupPrepend"
                              className="input-form-group"
                            >
                              <Image
                                src="/images/partner/referIcon.svg"
                                width={30}
                                height={30}
                              />
                            </InputGroup.Text>
                            <Form.Control
                              type="email"
                              placeholder="Email"
                              aria-describedby="inputGroupPrepend"
                              className="input-form-group"
                              required
                            />
                            <Form.Control.Feedback type="invalid">
                              Please enter a Email.
                            </Form.Control.Feedback>
                          </InputGroup>
                        </Form.Group>
                      </Row>
                      <Row className="mb-3">
                        <Form.Group
                          as={Col}
                          md="6"
                          controlId="validationCustomUsername"
                        >
                          {/* <Form.Label>Name</Form.Label> */}
                          <InputGroup
                            hasValidation
                            className="input-form-group-m"
                          >
                            <InputGroup.Text
                              id="inputGroupPrepend"
                              className="input-form-group"
                            >
                              <Image
                                src="/images/partner/referIcon.svg"
                                width={30}
                                height={30}
                              />
                            </InputGroup.Text>
                            <Form.Control
                              type="text"
                              placeholder="Company / Institute"
                              aria-describedby="inputGroupPrepend"
                              className="input-form-group"
                              required
                            />
                            <Form.Control.Feedback type="invalid">
                              Please enter a Company / Institute.
                            </Form.Control.Feedback>
                          </InputGroup>
                        </Form.Group>

                        <Form.Group
                          as={Col}
                          md="6"
                          controlId="validationCustomUsername"
                        >
                          {/* <Form.Label>Email</Form.Label> */}
                          <InputGroup hasValidation>
                            <InputGroup.Text
                              id="inputGroupPrepend"
                              className="input-form-group"
                            >
                              <Image
                                src="/images/partner/referIcon.svg"
                                width={30}
                                height={30}
                              />
                            </InputGroup.Text>
                            <Form.Control
                              type="number"
                              placeholder="Mobile Number"
                              aria-describedby="inputGroupPrepend"
                              className="input-form-group"
                              required
                            />
                            <Form.Control.Feedback type="invalid">
                              Please enter a Mobile Number.
                            </Form.Control.Feedback>
                          </InputGroup>
                        </Form.Group>
                      </Row>

                      <Row className="mb-3">
                        <Form.Group
                          as={Col}
                          md="6"
                          controlId="validationCustomUsername"
                        >
                          {/* <Form.Label>Name</Form.Label> */}
                          <InputGroup
                            hasValidation
                            className="input-form-group-m"
                          >
                            <InputGroup.Text
                              id="inputGroupPrepend"
                              className="input-form-group"
                            >
                              <Image
                                src="/images/partner/referIcon.svg"
                                width={30}
                                height={30}
                              />
                            </InputGroup.Text>
                            <Form.Control
                              type="text"
                              placeholder="Address"
                              aria-describedby="inputGroupPrepend"
                              className="input-form-group"
                              required
                            />
                            <Form.Control.Feedback type="invalid">
                              Please enter a Address.
                            </Form.Control.Feedback>
                          </InputGroup>
                        </Form.Group>

                        <Form.Group
                          as={Col}
                          md="6"
                          controlId="validationCustomUsername"
                        >
                          {/* <Form.Label>Email</Form.Label> */}
                          <InputGroup hasValidation>
                            <InputGroup.Text
                              id="inputGroupPrepend"
                              className="input-form-group"
                            >
                              <Image
                                src="/images/partner/referIcon.svg"
                                width={30}
                                height={30}
                              />
                            </InputGroup.Text>
                            <Form.Control
                              type="text"
                              placeholder="Address"
                              aria-describedby="inputGroupPrepend"
                              className="input-form-group"
                              required
                            />
                            <Form.Control.Feedback type="invalid">
                              Please enter a Address.
                            </Form.Control.Feedback>
                          </InputGroup>
                        </Form.Group>
                      </Row>

                      <Row className="mb-3">
                        <Form.Group
                          as={Col}
                          md="6"
                          controlId="validationCustomUsername"
                        >
                          {/* <Form.Label>Name</Form.Label> */}
                          <InputGroup
                            hasValidation
                            className="input-form-group-m"
                          >
                            <InputGroup.Text
                              id="inputGroupPrepend"
                              className="input-form-group"
                            >
                              <Image
                                src="/images/partner/referIcon.svg"
                                width={30}
                                height={30}
                              />
                            </InputGroup.Text>
                            <Form.Control
                              type="text"
                              placeholder="State"
                              aria-describedby="inputGroupPrepend"
                              className="input-form-group"
                              required
                            />
                            <Form.Control.Feedback type="invalid">
                              Please enter a State.
                            </Form.Control.Feedback>
                          </InputGroup>
                        </Form.Group>

                        <Form.Group
                          as={Col}
                          md="6"
                          controlId="validationCustomUsername"
                        >
                          {/* <Form.Label>Email</Form.Label> */}
                          <InputGroup hasValidation>
                            <InputGroup.Text
                              id="inputGroupPrepend"
                              className="input-form-group"
                            >
                              <Image
                                src="/images/partner/referIcon.svg"
                                width={30}
                                height={30}
                              />
                            </InputGroup.Text>
                            <Form.Control
                              type="number"
                              placeholder="Pin Code"
                              aria-describedby="inputGroupPrepend"
                              className="input-form-group"
                              required
                            />
                            <Form.Control.Feedback type="invalid">
                              Please enter a Pin Code.
                            </Form.Control.Feedback>
                          </InputGroup>
                        </Form.Group>
                      </Row>

                      <Row className="mb-3">
                        <Form.Group
                          as={Col}
                          md="12"
                          controlId="validationCustomUsername"
                        >
                          {/* <Form.Label>Name</Form.Label> */}
                          <InputGroup hasValidation>
                            <InputGroup.Text
                              id="inputGroupPrepend"
                              className="input-form-group"
                            >
                              <Image
                                src="/images/partner/referIcon.svg"
                                width={30}
                                height={30}
                              />
                            </InputGroup.Text>
                            <Form.Control
                              // type="textarea"
                              as="textarea"
                              placeholder="About You"
                              aria-describedby="inputGroupPrepend"
                              style={{ height: "100px" }}
                              className="input-form-group"
                              required
                            />
                            <Form.Control.Feedback type="invalid">
                              Please enter a State.
                            </Form.Control.Feedback>
                          </InputGroup>
                        </Form.Group>
                      </Row>
                      <center>
                        <Button type="submit" className="submit-bttn">
                          Request Callback
                        </Button>
                      </center>
                    </Form>
                  </Col>
                </Row>
              </div>
              <br /> <br />
              <center>
                <h3 className="third-parent-heading">
                  Ready to Begin Your Journey with
                </h3>
                <h2 className="third-parent-heading-main">
                  with MyClassboard?
                </h2>
                <p className="third-parent-para">
                  Share your details and our team will take you through the next
                  steps of partnering with us.
                </p>
              </center>
              <Row>
                <Col lg={4}>
                  <center>
                    <Image
                      src="/images/partner/phone.svg"
                      width={250}
                      height={250}
                      className="third-parent-img"
                    />
                    <h5 className="third-parent-img-text">Phone</h5>
                    <p className="third-parent-img-num">+91 987 654 3310</p>
                  </center>
                </Col>
                <Col lg={4}>
                  <center>
                    <Image
                      src="/images/partner/email.svg"
                      width={250}
                      height={250}
                      className="third-parent-img"
                    />
                    <h5 className="third-parent-img-text">Email</h5>
                    <p className="third-parent-img-num">info@verifykiya.com</p>
                  </center>
                </Col>
                <Col lg={4}>
                  <center>
                    <Image
                      src="/images/partner/website.svg"
                      width={250}
                      height={250}
                      className="third-parent-img"
                    />
                    <h5 className="third-parent-img-text">Website</h5>
                    <p className="third-parent-img-num">www.verifykiya.com</p>
                  </center>
                </Col>
              </Row>
              <br />
              <br />
              <Row>
                <Col xl={12} xxl={2}>
                  <div className="position-relative right">
                    <div className="partnerImg">
                      <img
                        alt="   "
                        src="/images/partner/partnerImg.svg"
                        className="partImg"
                      />
                    </div>

                    <div className="fourth-parent-immg">
                      <img
                        alt="   "
                        src="/images/partner/immg.svg"
                        className="partImgg"
                      />
                    </div>
                    <div className="fourth-parent-immg-second">
                      <img
                        alt="   "
                        src="/images/partner/imgh.svg"
                        className="partImggg"
                      />
                    </div>
                  </div>
                </Col>
                <Col xl={12} xxl={10}>
                  <Row>
                    <Col lg={12}>
                      <div className="fourth-parent-sub">
                        <h3 className="fourth-parent-heading">Our Partners</h3>
                        <p className="fourth-parent-para">
                          We offer a diverse range of career opportunities, from
                          supporting students studying for a University of
                          London degree around the world, supporting the growth
                          ...
                        </p>

                        {/* <Image
                          src="/images/partner/all.svg"
                          width={800}
                          height={500}
                        /> */}

                        <Row>
                          <Col md={4}>
                            <div className="partnerImg-center">
                              <Image
                                src="/images/partner/allImg.png"
                                width={200}
                                height={100}
                              />
                            </div>
                          </Col>
                          <Col md={4}>
                            <div className="partnerImg-center">
                              <Image
                                src="/images/partner/allImg.png"
                                width={200}
                                height={100}
                              />
                            </div>
                          </Col>
                          <Col md={4}>
                            <div className="partnerImg-center">
                              <Image
                                src="/images/partner/allImg.png"
                                width={200}
                                height={100}
                              />
                            </div>
                          </Col>
                        </Row>

                        <Row>
                          <Col md={4}>
                            <div className="partnerImg-center">
                              <Image
                                src="/images/partner/allImg.png"
                                width={200}
                                height={100}
                              />
                            </div>
                          </Col>
                          <Col md={4}>
                            <div className="partnerImg-center">
                              <Image
                                src="/images/partner/allImg.png"
                                width={200}
                                height={100}
                              />
                            </div>
                          </Col>
                          <Col md={4}>
                            <div className="partnerImg-center">
                              <Image
                                src="/images/partner/allImg.png"
                                width={200}
                                height={100}
                              />
                            </div>
                          </Col>
                        </Row>
                        <Row>
                          <Col md={4}>
                            <div className="partnerImg-center">
                              <Image
                                src="/images/partner/allImg.png"
                                width={200}
                                height={100}
                              />
                            </div>
                          </Col>
                          <Col md={4}>
                            <div className="partnerImg-center">
                              <Image
                                src="/images/partner/allImg.png"
                                width={200}
                                height={100}
                              />
                            </div>
                          </Col>
                          <Col md={4}>
                            <div className="partnerImg-center">
                              <Image
                                src="/images/partner/allImg.png"
                                width={200}
                                height={100}
                              />
                            </div>
                          </Col>
                        </Row>
                      </div>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>

            <Col xl={3} lg={12}>
              <div className="resources">
                <div className="w-300">
                  <iframe
                    src="/images/host.png"
                    scrolling="no"
                    width="400"
                    height="250"
                    class="responsive-iframe"
                  ></iframe>
                  <Signup />
                  <RegisterRecieve />
                  <Subscribe />
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
};

export default PartnerWithUs;
