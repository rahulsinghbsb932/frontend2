import React from "react";
import { Container, Row, Col } from "react-bootstrap";

const jobsDetails = () => {
  return (
    <>
      <div className="hero">
        <Container fluid>
          <Row>
            <Col lg={12}>
              <div className="hero-detail">
                <div className="hero-upper">
                  <h1 className="hero-heading">UI/UX Designer</h1>
                  <h2 className="hero-sub-heading">
                    ClinicMind, Okhla, New Delhi, Delhi 110025
                  </h2>
                  <p className="hero-para">
                    ClinicMind is a leading company providing Healthcare IT and
                    RCM services, including EHR software and medical billing in
                    the US. We're hiring a UI/UX Designer to help us keep
                    growing. If you're excited to be part of a winning team,
                    ClinicMind is a perfect place to get ahead.
                  </p>
                </div>
                <div className="hero-lower">
                  <div className="lower-width">
                    <Row>
                      <Col lg={3}>
                        <div className="lower-img">
                          <img
                            src="/images/jobs-details/img1.svg"
                            alt=""
                            className="img-fluid"
                          />
                        </div>
                      </Col>
                      <Col lg={9}>
                        <div className="lower-text">
                          <h1 className="lower-heading">Job Role </h1>
                          <h1 className="lower-para">Web & App Designer</h1>
                        </div>
                      </Col>
                    </Row>
                  </div>

                  <div className="lower-width">
                    <Row>
                      <Col lg={3}>
                        <div className="lower-img">
                          <img
                            src="/images/jobs-details/img2.svg"
                            alt=""
                            className="img-fluid"
                          />
                        </div>
                      </Col>
                      <Col lg={9}>
                        <div className="lower-text">
                          <h1 className="lower-heading">Job Type </h1>
                          <h1 className="lower-para">Full-Time</h1>
                        </div>
                      </Col>
                    </Row>
                  </div>

                  <div className="lower-width">
                    <Row>
                      <Col lg={3}>
                        <div className="lower-img">
                          <img
                            src="/images/jobs-details/img3.svg"
                            alt=""
                            className="img-fluid"
                          />
                        </div>
                      </Col>
                      <Col lg={9}>
                        <div className="lower-text">
                          <h1 className="lower-heading">Salary </h1>
                          <h1 className="lower-para">
                            ₹25,000 - ₹55,000 a month
                          </h1>
                        </div>
                      </Col>
                    </Row>
                  </div>

                  <div className="lower-width">
                    <Row>
                      <Col lg={3}>
                        <div className="lower-img">
                          <img
                            src="/images/jobs-details/img4.svg"
                            alt=""
                            className="img-fluid"
                          />
                        </div>
                      </Col>
                      <Col lg={9}>
                        <div className="lower-text">
                          <h1 className="lower-heading">Experience </h1>
                          <h1 className="lower-para">2 - 5 Years</h1>
                        </div>
                      </Col>
                    </Row>
                  </div>

                  <div className="lower-width">
                    <Row>
                      <Col lg={3}>
                        <div className="lower-img">
                          <img
                            src="/images/jobs-details/img5.svg"
                            alt=""
                            className="img-fluid"
                          />
                        </div>
                      </Col>
                      <Col lg={9}>
                        <div className="lower-text">
                          <h1 className="lower-heading">Qualification </h1>
                          <h1 className="lower-para">Certification</h1>
                        </div>
                      </Col>
                    </Row>
                  </div>

                  <div className="lower-width">
                    <Row>
                      <Col lg={3}>
                        <div className="lower-img">
                          <img
                            src="/images/jobs-details/img6.svg"
                            alt=""
                            className="img-fluid"
                          />
                        </div>
                      </Col>
                      <Col lg={9}>
                        <div className="lower-text">
                          <h1 className="lower-heading">Shift and schedule </h1>
                          <h1 className="lower-para">
                            Day Shift - Monday to Friday
                          </h1>
                        </div>
                      </Col>
                    </Row>
                  </div>

                  <div className="lower-width">
                    <Row>
                      <Col lg={3}>
                        <div className="lower-img">
                          <img
                            src="/images/jobs-details/img7.svg"
                            alt=""
                            className="img-fluid"
                          />
                        </div>
                      </Col>
                      <Col lg={9}>
                        <div className="lower-text">
                          <h1 className="lower-heading">Quantity </h1>
                          <h1 className="lower-para">6 Person</h1>
                        </div>
                      </Col>
                    </Row>
                  </div>
                  <div className="lower-width">
                    <Row>
                      <Col lg={3}>
                        <div className="lower-img">
                          <img
                            src="/images/jobs-details/img8.svg"
                            alt=""
                            className="img-fluid"
                          />
                        </div>
                      </Col>
                      <Col lg={9}>
                        <div className="lower-text">
                          <h1 className="lower-heading">Gender </h1>
                          <h1 className="lower-para">Not Required</h1>
                        </div>
                      </Col>
                    </Row>
                  </div>
                  <div className="lower-width">
                    <Row>
                      <Col lg={3}>
                        <div className="lower-img">
                          <img
                            src="/images/jobs-details/img9.svg"
                            alt=""
                            className="img-fluid"
                          />
                        </div>
                      </Col>
                      <Col lg={9}>
                        <div className="lower-text">
                          <h1 className="lower-heading">Date posted </h1>
                          <h1 className="lower-para">Not Required</h1>
                        </div>
                      </Col>
                    </Row>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </div>

      {/* <iframe src="" frameborder="0"></iframe> */}

      <div className="wrapper-first">
        <Container fluid>
          <Row>
            <Col lg={9}>
              <div className="wrapper-box">
                <div className="top-wrapper">
                  <div className="top-head">
                    <div className="top-flex">
                      <div className="head-img">
                        <img
                          src="/images/jobs-details/boxImg.svg"
                          alt=""
                          className="img-fluid"
                        />
                      </div>
                      <div className="text">
                        <h1 className="wrap-head">Full Job Description</h1>
                        <p className="wrap-para">
                          Know your worth and find the job that qualify your
                          life
                        </p>
                      </div>
                    </div>
                    <div className="wrap-bttn">
                      <span className="apply-now">Apply Now</span>
                    </div>
                  </div>
                </div>
                <div className="head-img-jali">
                  <img
                    src="/images/jobs-details/jali.svg"
                    alt=""
                    className="img-fluid"
                  />
                </div>
                <div className="job-description">
                  <div className="description">
                    <div className="desc-img">
                      <img
                        src="/images/jobs-details/intro.svg"
                        alt=""
                        className="img-fluid"
                      />
                    </div>
                    <h1 className="desceiption-heading">
                      Company Introduction
                    </h1>
                  </div>
                  <p className="description-para">
                    LifeVitae is an edtech start-up based out of Singapore. Our
                    AI-powered platform provides that bridge for students to
                    make informed career choices based on their lived
                    experiences and soft skills along with their academic
                    grades.
                  </p>

                  <div className="description">
                    <div className="desc-img">
                      <img
                        src="/images/jobs-details/intro.svg"
                        alt=""
                        className="img-fluid"
                      />
                    </div>
                    <h1 className="desceiption-heading">Job Overview:</h1>
                  </div>
                  <p className="description-para">
                    LifeVitae is seeking a dynamic UI/UX Designer to join our
                    growing team. You will work closely with our product team to
                    create user-friendly interfaces with an easily navigable
                    design and translates design requirements into necessary
                    interaction flows and applications.
                  </p>

                  <div className="description">
                    <div className="desc-img">
                      <img
                        src="/images/jobs-details/intro.svg"
                        alt=""
                        className="img-fluid"
                      />
                    </div>
                    <h1 className="desceiption-heading">Responsibilities:</h1>
                  </div>

                  <ul>
                    <li className="description-para">
                      Collaborate with cross-functional teams to understand
                      business and user requirements and translate them into
                      high-quality designs
                    </li>
                    <li className="description-para">
                      Develop wireframes, prototypes, and high-fidelity designs
                      that effectively communicate design concepts and
                      functionality
                    </li>
                    <li className="description-para">
                      Create intuitive and engaging user interfaces that align
                      with our brand and user needs
                    </li>
                    <li className="description-para">
                      Conduct user research and usability testing to gather
                      feedback and iterate designs based on user feedback
                    </li>
                    <li className="description-para">
                      Work closely with developers to ensure designs are
                      implemented accurately and to design specifications
                    </li>
                    <li className="description-para">
                      Stay up-to-date with design trends, emerging technologies,
                      and industry best practices
                    </li>
                  </ul>

                  <div className="description">
                    <div className="desc-img">
                      <img
                        src="/images/jobs-details/intro.svg"
                        alt=""
                        className="img-fluid"
                      />
                    </div>
                    <h1 className="desceiption-heading">Requirements:</h1>
                  </div>

                  <ul>
                    <li className="description-para">
                      Bachelor's or Master's degree in a design-related field
                    </li>
                    <li className="description-para">
                      Proven experience as a UI/UX Designer with a portfolio
                      showcasing your design process and results
                    </li>
                    <li className="description-para">
                      Strong understanding of design principles, including
                      typography, color theory, and composition
                    </li>
                    <li className="description-para">
                      Experience with design tools such as Sketch, Figma, or
                      Adobe Creative Suite
                    </li>
                    <li className="description-para">
                      Experience with user research and usability testing
                      methodologies
                    </li>
                    <li className="description-para">
                      Strong communication and collaboration skills to work
                      effectively in a cross-functional team environment
                    </li>
                    <li className="description-para">
                      Passion for design and eagerness to learn and apply new
                      skills and technologies
                    </li>
                  </ul>
                  <p className="description-para">
                    If you're passionate about creating designs that positively
                    impact students' lives and want to be part of a fast-growing
                    start-up, we encourage you to apply.
                  </p>
                </div>
              </div>
            </Col>
            <Col lg={3}>ads</Col>
          </Row>
        </Container>
      </div>
    </>
  );
};

export default jobsDetails;
