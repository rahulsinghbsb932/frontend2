import React from "react";
import Head from "next/head";
import Image from "next/image";
import PageLayout from "@components/PageLayout";
import { Container, Row, Col } from "react-bootstrap";
import Card from "react-bootstrap/Card";

const employeeDocumentCertification = () => {
  return (
    <div>
      <Head>
        <title>Employee Document Certification | VerifyKiya</title>
        <meta
          id="meta-description"
          name="description"
          content="With the use of blockchain technology and NFTs (Non-Fungible Tokens), the certification process can be streamlined, and the authenticity of employee credentials can be easily verified."
        />
        <meta
          property="og:title"
          content="Employee Document Certification | VerifyKiya"
        />
        <meta
          property="og:description"
          content="With the use of blockchain technology and NFTs (Non-Fungible Tokens), the certification process can be streamlined, and the authenticity of employee credentials can be easily verified."
        />
        <meta
          property="twitter:title"
          content="Employee Document Certification | VerifyKiya"
        />
        <meta
          property="twitter:description"
          content="With the use of blockchain technology and NFTs (Non-Fungible Tokens), the certification process can be streamlined, and the authenticity of employee credentials can be easily verified."
        />
      </Head>

      <PageLayout>
        <div className="HeroSectionWrapper">
          <Container fluid>
            <Row>
              <Col md={12}>
                <div className="headingWhileLabel Montserrat">
                  <h1>Employee</h1>
                  <p>
                    Employees have a lot on their plate! The pressure of
                    performing well in the workplace and aiming towards greener
                    pastures can often wear you down. This is where Verifiykiya
                    comes in! To support you in your endeavours, we have created
                    an incomparable integrated platform that solves all your
                    troubles! Maintain every crucial credential in a formidable
                    platform that ensures its life-long authenticity. Keep a
                    track of all upcoming deadlines and opportunities in real
                    time! You will get access to professional training,
                    analytical support and automated resume building with just a
                    click! A platform which values growth, innovation and
                    convenience, Verifykiya is truly a must-have for all
                    employees out there! So what are you waiting for? Join us
                    and kickstart your future!
                  </p>
                </div>
              </Col>
            </Row>
          </Container>

          <Container fluid>
            <Row>
              <Col lg={3} md={6} className="rowCenter mt-5">
                <a className="cardanchor" href="/">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="img-fluid"
                      src={
                        require("../../../assets/img/employee/private.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/employee/private.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">Private</Card.Title>
                      <Card.Text>
                        A Private is an educational institution designed to
                        provide learning spaces and learning.
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col lg={3} md={6} className="rowCenter mt-5">
                <a className="cardanchor" href="/">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../../assets/img/employee/goverment.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/employee/goverment.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">Government</Card.Title>
                      <Card.Text>
                        A Government is an educational institution designed to
                        provide learning spaces and learning.
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col lg={3} md={6} className="rowCenter mt-5">
                <a className="cardanchor" href="/">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../../assets/img/employee/multinational.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/employee/multinational.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Multinational
                      </Card.Title>
                      <Card.Text>
                        A Multinational is an educational institution designed
                        to provide learning
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col lg={3} md={6} className="rowCenter mt-5">
                <a className="cardanchor" href="/">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../../assets/img/employee/training.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/employee/training.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Training institutes
                      </Card.Title>
                      <Card.Text>
                        Institutions are humanly devised structures of rules and
                        norms that shape & constrain.
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
            </Row>
            <Row>
              <Col lg={3} md={6} className="rowCenter mt-5">
                <a className="cardanchor" href="/">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="img-fluid"
                      src={
                        require("../../../assets/img/employee/saminar.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/employee/saminar.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">Seminars</Card.Title>
                      <Card.Text>
                        A Private is an educational institution designed to
                        provide learning spaces and learning.
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col lg={3} md={6} className="rowCenter mt-5">
                <a className="cardanchor" href="/">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../../assets/img/employee/trade.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/employee/trade.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">Trade Shows</Card.Title>
                      <Card.Text>
                        A Government is an educational institution designed to
                        provide learning spaces and learning.
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col lg={3} md={6} className="rowCenter mt-5">
                <a className="cardanchor" href="/">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../../assets/img/employee/confrences.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/employee/confrences.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">Conferences</Card.Title>
                      <Card.Text>
                        A Multinational is an educational institution designed
                        to provide learning
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col lg={3} md={6} className="rowCenter mt-5"></Col>
            </Row>
          </Container>
        </div>

        <div className="sectionRelative m-5">
          <Container fluid>
            <Col md={12}>
              <div class="ourSolutionWrapper">
                <h1 class="ourSolutionSectionheading">
                  <span style={{ fontWeight: 300 }}>Here comes </span>
                  Verifykiya!
                </h1>
                <p className="ourSolutionPera">
                  Understanding the gravity of the situation, we created this
                  platform to meet the needs of industry-wide institutions. You
                  can finally say goodbye to all the deceit and the irrelevant
                  workload. It’s time to take your brand to greater heights,
                  with Verifykiya!
                </p>
              </div>
            </Col>
          </Container>
        </div>

        <section>
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../../assets/img/employee/dw.svg").default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/employee/dw.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="dw-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Digital Wallet for Certificates, Badges, Degrees,
                        Marksheet, Experience Letter etc.
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Through our use of blockchain technology and NFTs, we've
                      streamlined the certification process, allowing for easy
                      creation and verification of authentic credentials like
                      badges and certificates. The trustless algorithm and
                      immutable nature of NFTs make it impossible for any
                      unauthorized changes to be made, ensuring maximum security
                      and authenticity for our clients.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section>
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="arb-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Automated Resume Builder
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Say goodbye to the hassle of creating a resume from
                      scratch with our automated resume builder. Our easy-to-use
                      platform guides you through the process and generates a
                      professional-looking resume tailored to your skills and
                      experience in just minutes. Stand out from the crowd and
                      land your dream job with ease.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
                <Col xl={3} lg={12} className="order-xl-2 order-1">
                  <div className="img">
                    {/* <img
                      src={
                        require("../../../assets/img/employee/arb.svg").default
                      }
                      alt=""
                      className="img-fluid "
                    /> */}

                    <Image
                      src="/images/employee/arb.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section>
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../../assets/img/employee/apsp.svg").default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/employee/apsp.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="event3-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Access to Placements, Skill and Personality Development
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Our all-encompassing program provides you with access to
                      scholarships, placements, and opportunities for skill and
                      personality development, unlocking your full potential. We
                      are dedicated to empowering you with the resources and
                      support you need to achieve your goals, including
                      financial assistance and career opportunities. Let us help
                      you succeed and thrive.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section>
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="rtjt-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Real Time Alerts Job, Training Programs & Skill
                        Development
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Stay ahead of the competition and never miss an
                      opportunity with our real-time alerts. Get notified
                      immediately of new job postings, training programs, and
                      skill development opportunities that match your interests
                      and qualifications. Our platform also provides alerts for
                      upcoming placements, so you can stay on top of your career
                      goals. Take advantage of our real-time alerts and make the
                      most of every opportunity.
                    </p>
                  </div>
                </Col>
                <Col xl={3} lg={12} className="order-xl-2 order-1">
                  <div className="img">
                    {/* <img
                      src={
                        require("../../../assets/img/employee/rtjt.svg").default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/employee/rtjt.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section>
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../../assets/img/employee/sfas.svg").default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/employee/sfas.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="sfas-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Single Form for Applying to all Services
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Simplify your life and save time with our single form for
                      applying to all services. With just one application, you
                      can apply for multiple services and programs, including
                      scholarships, training, job placements, and more. Our
                      user-friendly platform makes it easy to submit your
                      application and track its progress, so you can focus on
                      achieving your goals. Experience the convenience of a
                      single form for all your needs.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section>
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="mrt-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Multiple Resume Templates
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Stand out from the competition with our wide range of
                      multiple resume templates. Choose from a variety of
                      customizable options to create a professional-looking
                      resume that highlights your skills and experience. Whether
                      you're a recent graduate or an experienced professional,
                      our templates can help you make a lasting impression and
                      land your dream job.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
                <Col xl={3} lg={12} className="order-xl-2 order-1">
                  <div className="img">
                    {/* <img
                      src={
                        require("../../../assets/img/employee/mrt.svg").default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/employee/mrt.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section>
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../../assets/img/employee/vdti.svg").default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/employee/vdti.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="vdti-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Verified Database of Training Institutes
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Find the right training program for you with our verified
                      database of training institutes. Our platform ensures that
                      all listed institutes are authentic and offer quality
                      training programs, so you can choose with confidence. From
                      job-specific skills to personal development, we have the
                      resources you need to succeed in your career.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section>
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="enic-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Enroll in National & International Competitions and
                        Events
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Showcase your talents on a global stage with our option to
                      enroll in national and international competitions and
                      events. From sports to academic competitions, our platform
                      offers a range of opportunities to compete and excel. Gain
                      valuable experience, build your network, and boost your
                      resume with our competition and event options.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
                <Col xl={3} lg={12} className="order-xl-2 order-1">
                  <div className="img">
                    {/* <img
                      src={
                        require("../../../assets/img/employee/enic.svg").default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/employee/enic.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section>
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../../assets/img/employee/jobm.svg").default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/employee/jobm.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="jobm-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">Job Marketplace</h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Find your dream job with ease on our job marketplace. Our
                      platform connects job seekers with top employers across
                      various industries and locations. With a user-friendly
                      interface and advanced search filters, you can easily find
                      job postings that match your skills and experience. Get
                      started today and take the next step in your career.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section>
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="vsr-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Verified Seal on Resumes
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Stand out from the competition with our verified seal on
                      resumes. Our platform verifies the authenticity of your
                      credentials and adds a seal to your resume, showcasing
                      your qualifications to employers. With our verified seal,
                      you can increase your chances of getting hired and build
                      your professional reputation. Create your resume today and
                      get verified.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
                <Col xl={3} lg={12} className="order-xl-2 order-1">
                  <div className="img">
                    {/* <img
                      src={
                        require("../../../assets/img/employee/vsr.svg").default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/employee/vsr.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section>
          <div className="solution mb-5">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../../assets/img/employee/acjo.svg").default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}
                    <Image
                      src="/images/employee/acjo.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="jobm-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        AI Curated Job Opportunities
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Get personalized job recommendations with our AI-curated
                      job opportunities. Our platform uses advanced algorithms
                      to match your skills and experience with the right job
                      postings. With our AI-powered job search, you can save
                      time and focus on the opportunities that matter. Start
                      exploring our curated job opportunities and take the next
                      step in your career.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
      </PageLayout>
    </div>
  );
};

export default employeeDocumentCertification;
