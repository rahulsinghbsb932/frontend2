import React from "react";
import Head from "next/head";
import Image from "next/image";
import PageLayout from "@components/PageLayout";
import { Container, Row, Col } from "react-bootstrap";
import Card from "react-bootstrap/Card";

const digitalCertification = () => {
  return (
    <div>
      <Head>
        <title>
          Issue Digital Certificates | Secure and Reliable | Digital
          Certification
        </title>
        <meta
          id="meta-description"
          name="description"
          content="Get secure and reliable digital certification services. Issue digital certificates with ease and efficiency. Explore our solutions today."
        />
        <meta
          property="og:title"
          content=" Issue Digital Certificates | Secure and Reliable | Digital
          Certification"
        />
        <meta
          property="og:description"
          content="Get secure and reliable digital certification services. Issue digital certificates with ease and efficiency. Explore our solutions today."
        />
        <meta
          property="twitter:title"
          content=" Issue Digital Certificates | Secure and Reliable | Digital
          Certification"
        />
        <meta
          property="twitter:description"
          content="Get secure and reliable digital certification services. Issue digital certificates with ease and efficiency. Explore our solutions today."
        />
      </Head>

      <PageLayout>
        <div className="HeroSectionWrapper">
          <Container fluid>
            <Row>
              <Col md={12}>
                <div className="headingWhileLabel Montserrat">
                  <h1>Digital Certification</h1>
                  <p>
                    VerifyKiya's blockchain based NFT enabled digital
                    credentials, creation and verification of genuine
                    credentials will become seamless. The trustless algorithm
                    and the immutable nature of NFTs makes it impossible for any
                    unauthorised changes. you can bring your dependence on paper
                    to a bare minimum and play your part in reducing the overall
                    carbon footprint.
                  </p>
                </div>
              </Col>
            </Row>
          </Container>

          <Container fluid className="mb-5">
            <Row>
              <Col md={4} className="rowCenter mt-5">
                <a className="cardanchor" href="#Digital-Credentials">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Digital_Cerrificates/Digital Credentials.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Digital_Cerrificates/Digital Credentials.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Digital Credentials
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={4} className="rowCenter mt-5">
                <a className="cardanchor" href="#Credential-Verification">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Digital_Cerrificates/Credential Verification.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Digital_Cerrificates/Credential Verification.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Credential Verification
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={4} className="rowCenter mt-5">
                <a className="cardanchor" href="#Brand-Building">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Digital_Cerrificates/Brand Building.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Digital_Cerrificates/Brand Building.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Brand Building
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
            </Row>
            <Row>
              <Col md={4} className="rowCenter mt-5">
                <a className="cardanchor" href="#Trustless-and-Immutable">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Digital_Cerrificates/Trustless & Immutable.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Digital_Cerrificates/Trustless & Immutable.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Trustless & Immutable
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={4} className="rowCenter mt-5">
                <a className="cardanchor" href="#Data-Protection">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Digital_Cerrificates/Data Protection.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Digital_Cerrificates/Data Protection.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Data Protection
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={4} className="rowCenter mt-5">
                {" "}
                <a className="cardanchor" href="#Seamless-Integrations">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Digital_Cerrificates/Seamless Integrations.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Digital_Cerrificates/Seamless Integrations.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Seamless Integrations
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={4} className="rowCenter mt-5">
                {" "}
                <a className="cardanchor" href="#Bulk-Issuance">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Digital_Cerrificates/Bulk Issuance.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Digital_Cerrificates/Bulk Issuance.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Bulk Issuance
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={4} className="rowCenter mt-5">
                {" "}
                <a className="cardanchor" href="#Social-Media-Dissemination">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Digital_Cerrificates/Social Media Dissemination.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Digital_Cerrificates/Social Media Dissemination.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Social Media Dissemination
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={4} className="rowCenter mt-5">
                {" "}
                <a className="cardanchor" href="#Template-Database">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Digital_Cerrificates/Template Database.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Digital_Cerrificates/Template Database.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Template Database
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
            </Row>
          </Container>
        </div>

        <section id="Digital-Credentials">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Digital_Certification/Digital Credentials.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Digital_Certification/Digital Credentials.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Digital_Cerrificates/Digital Credentials.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Digital_Cerrificates/Digital Credentials.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Digital Credentials
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      OWith VerifyKiya's blockchain based NFT enabled digital
                      credentials, creation and verification of genuine
                      credentials will become seamless. The trustless algorithm
                      and the immutable nature of NFTs makes it impossible for
                      any unauthorised changes. you can bring your dependence on
                      paper to a bare minimum and play your part in reducing the
                      overall carbon footprint. Issue as many digital
                      credentials as you need with just a click without worrying
                      about environmental degradation. Additionally, get rid of
                      the burden of maintaining the records for every credential
                      issued, saving both time and paper.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section id="Credential-Verification">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Digital_Cerrificates/Credential Verification.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Digital_Cerrificates/Credential Verification.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Credential Verification
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      VerifyKiya helps institutions automate their credential
                      verification process. This allows them to prevent revenue
                      loss through unauthorized or faulty verifications,
                      Unauthorized or faulty verification also leads to fraud
                      and the issuance of fake credentials which leads to a loss
                      of reputation and brand for the Institutionuniversity.
                      VerifyKiya’s blockchain based NFT enabled credentials can
                      be given to candidates. These verifiable credentials go a
                      long way in easy authentication, credibility and avoidance
                      of disputes.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
                <Col xl={3} lg={12} className="order-xl-2 order-1">
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Digital_Certification/Credential Verification.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid "
                    /> */}

                    <Image
                      src="/images/Services/images/Digital_Certification/Credential Verification.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section id="Brand-Building<">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Digital_Certification/Brand Building.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Digital_Certification/Brand Building.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Digital_Cerrificates/Brand Building.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Digital_Cerrificates/Brand Building.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">Brand Building</h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Brand building for any institutions is a continuous and
                      painful exercise. Any unauthorized or spurious credential
                      adversely affects the brand of the institution and has a
                      direct effect on its business. Verifykiya’s promise of
                      legitimacy and security is an instant image booster for
                      institutions. It increases brand visibility not only
                      within the student communities but also within the
                      corporate sector. The use of blockchain and NFT for
                      credentials makes your institution stand apart from the
                      rest and also increases its credibility.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section id="Trustless">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Digital_Certification/Trustless & Immutable.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Digital_Certification/Trustless & Immutable.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Digital_Cerrificates/Trustless & Immutable.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Digital_Cerrificates/Trustless & Immutable.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Trustless & Immutable
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      With the entire process powered by blockchain technology,
                      VerifyKiya’s blockchain-based digital certificates are
                      immutable and can never be tampered with. Also, with our
                      trustless network, you can always rely on our
                      blockchain-based certificate and no longer have to place
                      your trust in third parties.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section id="Data-Protection">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Digital_Cerrificates/Data Protection.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Digital_Cerrificates/Data Protection.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">Data Protection</h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Storing all the information on VerifyKiya’s blockchain
                      powered solution you’ll never have to worry about
                      protecting your data against any third-party from misusing
                      or stealing it. The data stored on blockchain stays
                      protected as it cannot be tampered with, changed, or
                      deleted. And only you’ll have the access to the data using
                      your private key.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
                <Col xl={3} lg={12} className="order-xl-2 order-1">
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Digital_Certification/Data Protection.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Digital_Certification/Data Protection.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section id="Seamless-Integrations">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Digital_Certification/Seamless Integrations.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Digital_Certification/Seamless Integrations.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Digital_Cerrificates/Seamless Integrations.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}
                      <Image
                        src="/images/Services/icon/Digital_Cerrificates/Seamless Integrations.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Seamless Integrations
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      VerifyKiya's solution can be easily integrated with your
                      SAP, LMS or any other software without much disruption to
                      your normal processes. Companies can focus on their normal
                      processes without having to develop software like ours
                      from scratch.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section id="Bulk Issuance">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Digital_Cerrificates/Bulk Issuance.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Digital_Cerrificates/Bulk Issuance.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">Bulk Issuance</h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Issuing credentials in bulk is a painfull and time
                      consuming process but with Verifykiya’s platform issuing
                      credentials in bulk is a single click away. We provide
                      Multiple integration and upload options to minimize
                      workload and save time of your workforce. We have created
                      a robust system which allows you to create credentials in
                      bulk without compromising on the security and immutability
                      of each and every credential.
                    </p>
                  </div>
                </Col>
                <Col xl={3} lg={12} className="order-xl-2 order-1">
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Digital_Certification/Bulk Issuance.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Digital_Certification/Bulk Issuance.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section id="Social-Media-Dissemination">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Digital_Certification/Social Media Dissemination.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Digital_Certification/Social Media Dissemination.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Digital_Cerrificates/Social Media Dissemination.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Digital_Cerrificates/Social Media Dissemination.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Social Media Dissemination
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Through Verifykiya, receipient can even share their issued
                      accolades like certificates, badges etc. which can boost
                      your public image and put faces to your success stories.
                      They can emerge as ambassadors and represent your
                      Instutions. This will boost morale and even attract new
                      ones.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section className="mb-5" id="Template-Database">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Digital_Cerrificates/Template Database.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Digital_Cerrificates/Template Database.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Template Database
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      I We offer a wide range of industry-approved and
                      attractive templates to choose from so that your work gets
                      easier and much quicker. These templates are set by top
                      experts in the respected fields to match the industry
                      standard but even if that doesn’t satisfy the requirements
                      we also have a customization feature which allows you to
                      request for a unique template personalized to your
                      perfection. We also give an option to institutions to
                      utilise our platform as a white label solution as well.
                    </p>
                  </div>
                </Col>
                <Col xl={3} lg={12} className="order-xl-2 order-1">
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Digital_Certification/Template Database.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Digital_Certification/Template Database.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
      </PageLayout>
    </div>
  );
};

export default digitalCertification;
