import React from "react";
import { Col, Container, Row } from "react-bootstrap";

import Badge from "@mui/material/Badge";
import { styled } from "@mui/material/styles";
import IconButton from "@mui/material/IconButton";

const StyledBadge = styled(Badge)(({ theme }) => ({
  "& .MuiBadge-badge": {
    right: 14,
    top: 79,
    border: `1px solid #A6A6A6`,
    padding: " .8rem",

    background:
      "linear-gradient(180deg, #05FF00 0%, #B5FFC1 4.17%, #5CDA5A 50.52%, #4DBE4A 52.6%, #05FF00 100%)",
    // border: 1px solid #A6A6A6;
    boxShadow:
      "4px 4px 20px rgba(0, 0, 0, 0.25), inset 0px 0px 10px rgba(0, 0, 0, 0.25)",
    borderRadius: "5px",

    // fontStyle: "normal",
    fontWeight: "700",
    fontSize: "18px",
    lineHeight: "40px",

    textAlign: "center",

    color: "#0E1133",
  },
}));

const pricing = () => {
  return (
    <>
      <div className="pricing-hero">
        <Container fluid>
          <Row>
            <Col lg={12}>
              <div className="details">
                <div className="details-upper">
                  <h1 className="details-heading">Get the Ideal Price</h1>
                  <p className="details-para">
                    Get the ideal price plan for your projects, Access the best
                    content — all with worry-free licensing. Pricing is a
                    process of fixing the value that a manufacturer will receive
                    in the exchange of services and goods. Pricing method is
                    exercised to adjust the cost of the producer's offerings
                    suitable to both the manufacturer and the customer.
                  </p>
                </div>
                <div className="details-lower">
                  <div className="employee-card">
                    <IconButton aria-label="cart">
                      <StyledBadge badgeContent="Free" color="secondary">
                        <div className="ec-img">
                          <img
                            src="/images/pricing/ecImg1.svg"
                            alt=""
                            className="img-fluid"
                          />
                        </div>
                      </StyledBadge>
                    </IconButton>

                    <h5 className="ec-heading">Students</h5>
                    <p className="ec-para">
                      For business starting content management system
                    </p>
                  </div>

                  <div className="employee-card">
                    <IconButton aria-label="cart">
                      <StyledBadge badgeContent="Free" color="secondary">
                        <div className="ec-img">
                          <img
                            src="/images/pricing/ecImg2.svg"
                            alt=""
                            className="img-fluid"
                          />
                        </div>
                      </StyledBadge>
                    </IconButton>
                    <h5 className="ec-heading">Employee</h5>
                    <p className="ec-para">
                      For growing and established teams elevating their contents
                    </p>
                  </div>
                  <div className="employee-card">
                    <IconButton aria-label="cart">
                      <StyledBadge badgeContent="Free" color="secondary">
                        <div className="ec-img">
                          <img
                            src="/images/pricing/ecImg3.svg"
                            alt=""
                            className="img-fluid"
                          />
                        </div>
                      </StyledBadge>
                    </IconButton>
                    <h5 className="ec-heading">Tutoress</h5>
                    <p className="ec-para">
                      Built with scalability security and enterprise grade
                      feature set in mind
                    </p>
                  </div>
                  <div className="employee-card">
                    <IconButton aria-label="cart">
                      <StyledBadge badgeContent="Free" color="secondary">
                        <div className="ec-img">
                          <img
                            src="/images/pricing/ecImg4.svg"
                            alt=""
                            className="img-fluid"
                          />
                        </div>
                      </StyledBadge>
                    </IconButton>
                    <h5 className="ec-heading">Others</h5>
                    <p className="ec-para">
                      For business with highest profit & scalability standard
                    </p>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
      {/*  */}

      <div className="pricing-wrapper">
        <Container fluid>
          <Row>
            <Col lg={12}>
              <div className="pricing-main">
                <div className="wrapper-upper">
                  <div className="wp-img">
                    <img
                      src="/images/pricing/wpImg.svg"
                      alt=""
                      className="img-fluid"
                    />
                  </div>
                  <div className="wp-text">
                    <h1 className="wp-heading">Pricing for Students</h1>
                    <p className="wp-para">
                      Pricing method is exercised to adjust the cost of the
                      producer's offerings suitable to both the manufacturer.
                    </p>
                  </div>
                </div>

                <div className="wrapper-lower">
                  <Row>
                    <Col lg={3} md={6}>
                      <div className="type-of-prize">
                        <h4 className="name">Bronze</h4>
                        <div className="icn">
                          <img
                            src="/images/pricing/bronze.svg"
                            alt=""
                            className="img-fluid"
                          />
                        </div>
                        <h4 className="cost">Free</h4>

                        <div className="feature">
                          <ul>
                            <hr />
                            <li className="feature-name">Issued By us</li>
                            <hr />
                            <li className="feature-name">Manual</li>
                            <hr />
                            <li className="feature-name">3 Resume Templates</li>
                            <hr />
                            <li className="feature-name">Alerts</li>
                            <hr />
                            <li className="feature-name">
                              Institution Comparison
                            </li>
                            <hr />
                            <li className="feature-name">
                              Form / Admission Alerts
                            </li>
                            <hr />
                            <li className="feature-name">Eligibilty Check</li>
                            <hr />
                            <li className="feature-name">
                              Single form for All
                            </li>
                            <hr />
                            <li className="feature-name">
                              {" "}
                              Student Loan Facility
                            </li>
                            <hr />
                            <li className="feature-name"> Extra Curricualar</li>
                            <hr />
                          </ul>
                        </div>

                        <div className="bttn">
                          <span className="btn-w">Buy Now</span>
                        </div>
                      </div>
                    </Col>
                    <Col lg={3} md={6}>
                      <div className="type-of-prize-s">
                        <h4 className="name">Silver</h4>
                        <div className="icn">
                          <img
                            src="/images/pricing/silver.svg"
                            alt=""
                            className="img-fluid"
                          />
                        </div>
                        <h4 className="cost-s">199/-</h4>
                        <h5 className="cost-f">per annum</h5>

                        <div className="feature">
                          <ul>
                            <hr />
                            <li className="feature-name-s">
                              All Bronze Features
                            </li>
                            <hr />
                            <li className="feature-name">Manual</li>
                            <hr />
                            <li className="feature-name">3 Resume Templates</li>
                            <hr />
                            <li className="feature-name">Alerts</li>
                            <hr />
                            <li className="feature-name">
                              Institution Comparison
                            </li>
                            <hr />
                            <li className="feature-name">
                              Form / Admission Alerts
                            </li>
                            <hr />
                            <li className="feature-name">Eligibilty Check</li>
                            <hr />
                            <li className="feature-name">
                              Single form for All
                            </li>
                            <hr />
                            <li className="feature-name">
                              {" "}
                              Student Loan Facility
                            </li>
                            <hr />
                            <li className="feature-name"> Extra Curricualar</li>
                            <hr />
                          </ul>
                        </div>

                        <div className="bttn-s">
                          <span className="btn-w">Buy Now</span>
                        </div>
                      </div>
                    </Col>
                    <Col lg={3} md={6}>
                      <div className="type-of-prize-g">
                        <h4 className="name">Gold</h4>
                        <div className="icn">
                          <img
                            src="/images/pricing/gold.svg"
                            alt=""
                            className="img-fluid"
                          />
                        </div>
                        <h4 className="cost-s">349/-</h4>
                        <h5 className="cost-f">per annum</h5>

                        <div className="feature">
                          <ul>
                            <hr />
                            <li className="feature-name-s">
                              All Silver Features
                            </li>
                            <hr />
                            <li className="feature-name">Manual</li>
                            <hr />
                            <li className="feature-name">3 Resume Templates</li>
                            <hr />
                            <li className="feature-name">Alerts</li>
                            <hr />
                            <li className="feature-name">
                              Institution Comparison
                            </li>
                            <hr />
                            <li className="feature-name">
                              Form / Admission Alerts
                            </li>
                            <hr />
                            <li className="feature-name">Eligibilty Check</li>
                            <hr />
                            <li className="feature-name">
                              Single form for All
                            </li>
                            <hr />
                            <li className="feature-name">
                              {" "}
                              Student Loan Facility
                            </li>
                            <hr />
                            <li className="feature-name"> Extra Curricualar</li>
                            <hr />
                          </ul>
                        </div>

                        <div className="bttn-g">
                          <span className="btn-w">Buy Now</span>
                        </div>
                      </div>
                    </Col>
                    <Col lg={3} md={6}>
                      <div className="type-of-prize-p">
                        <h4 className="name">Platinum</h4>
                        <div className="icn">
                          <img
                            src="/images/pricing/platinum.svg"
                            alt=""
                            className="img-fluid"
                          />
                        </div>
                        <h4 className="cost-s">499/-</h4>
                        <h5 className="cost-f">per annum</h5>

                        <div className="feature">
                          <ul>
                            <hr />
                            <li className="feature-name-s">
                              All Gold Features
                            </li>
                            <hr />
                            <li className="feature-name">Manual</li>
                            <hr />
                            <li className="feature-name">3 Resume Templates</li>
                            <hr />
                            <li className="feature-name">Alerts</li>
                            <hr />
                            <li className="feature-name">
                              Institution Comparison
                            </li>
                            <hr />
                            <li className="feature-name">
                              Form / Admission Alerts
                            </li>
                            <hr />
                            <li className="feature-name">Eligibilty Check</li>
                            <hr />
                            <li className="feature-name">
                              Single form for All
                            </li>
                            <hr />
                            <li className="feature-name">
                              {" "}
                              Student Loan Facility
                            </li>
                            <hr />
                            <li className="feature-name"> Extra Curricualar</li>
                            <hr />
                          </ul>
                        </div>

                        <div className="bttn-p">
                          <span className="btn-w">Buy Now</span>
                        </div>
                      </div>
                    </Col>
                  </Row>
                </div>
              </div>
            </Col>
            <Col lg={12}>
              <div className="pricing-main">
                <div className="wrapper-upper">
                  <div className="wp-img">
                    <img
                      src="/images/pricing/wpImg.svg"
                      alt=""
                      className="img-fluid"
                    />
                  </div>
                  <div className="wp-text">
                    <h1 className="wp-heading">Pricing for Employee</h1>
                    <p className="wp-para">
                      Pricing method is exercised to adjust the cost of the
                      producer's offerings suitable to both the manufacturer.
                    </p>
                  </div>
                </div>

                <div className="wrapper-lower">
                  <Row>
                    <Col lg={3} md={6}>
                      <div className="type-of-prize">
                        <h4 className="name">Bronze</h4>
                        <div className="icn">
                          <img
                            src="/images/pricing/bronze.svg"
                            alt=""
                            className="img-fluid"
                          />
                        </div>
                        <h4 className="cost">Free</h4>

                        <div className="feature">
                          <ul>
                            <hr />
                            <li className="feature-name">Issued By us</li>
                            <hr />
                            <li className="feature-name">Manual</li>
                            <hr />
                            <li className="feature-name">3 Resume Templates</li>
                            <hr />
                            <li className="feature-name">Alerts</li>
                            <hr />
                            <li className="feature-name">
                              Institution Comparison
                            </li>
                            <hr />
                            <li className="feature-name">
                              Form / Admission Alerts
                            </li>
                            <hr />
                            <li className="feature-name">Eligibilty Check</li>
                            <hr />
                            <li className="feature-name">
                              Single form for All
                            </li>
                            <hr />
                            <li className="feature-name">
                              {" "}
                              Student Loan Facility
                            </li>
                            <hr />
                            <li className="feature-name"> Extra Curricualar</li>
                            <hr />
                          </ul>
                        </div>

                        <div className="bttn">
                          <span className="btn-w">Buy Now</span>
                        </div>
                      </div>
                    </Col>
                    <Col lg={3} md={6}>
                      <div className="type-of-prize-s">
                        <h4 className="name">Silver</h4>
                        <div className="icn">
                          <img
                            src="/images/pricing/silver.svg"
                            alt=""
                            className="img-fluid"
                          />
                        </div>
                        <h4 className="cost-s">199/-</h4>
                        <h5 className="cost-f">per annum</h5>

                        <div className="feature">
                          <ul>
                            <hr />
                            <li className="feature-name-s">
                              All Bronze Features
                            </li>
                            <hr />
                            <li className="feature-name">Manual</li>
                            <hr />
                            <li className="feature-name">3 Resume Templates</li>
                            <hr />
                            <li className="feature-name">Alerts</li>
                            <hr />
                            <li className="feature-name">
                              Institution Comparison
                            </li>
                            <hr />
                            <li className="feature-name">
                              Form / Admission Alerts
                            </li>
                            <hr />
                            <li className="feature-name">Eligibilty Check</li>
                            <hr />
                            <li className="feature-name">
                              Single form for All
                            </li>
                            <hr />
                            <li className="feature-name">
                              {" "}
                              Student Loan Facility
                            </li>
                            <hr />
                            <li className="feature-name"> Extra Curricualar</li>
                            <hr />
                          </ul>
                        </div>

                        <div className="bttn-s">
                          <span className="btn-w">Buy Now</span>
                        </div>
                      </div>
                    </Col>
                    <Col lg={3} md={6}>
                      <div className="type-of-prize-g">
                        <h4 className="name">Gold</h4>
                        <div className="icn">
                          <img
                            src="/images/pricing/gold.svg"
                            alt=""
                            className="img-fluid"
                          />
                        </div>
                        <h4 className="cost-s">349/-</h4>
                        <h5 className="cost-f">per annum</h5>

                        <div className="feature">
                          <ul>
                            <hr />
                            <li className="feature-name-s">
                              All Silver Features
                            </li>
                            <hr />
                            <li className="feature-name">Manual</li>
                            <hr />
                            <li className="feature-name">3 Resume Templates</li>
                            <hr />
                            <li className="feature-name">Alerts</li>
                            <hr />
                            <li className="feature-name">
                              Institution Comparison
                            </li>
                            <hr />
                            <li className="feature-name">
                              Form / Admission Alerts
                            </li>
                            <hr />
                            <li className="feature-name">Eligibilty Check</li>
                            <hr />
                            <li className="feature-name">
                              Single form for All
                            </li>
                            <hr />
                            <li className="feature-name">
                              {" "}
                              Student Loan Facility
                            </li>
                            <hr />
                            <li className="feature-name"> Extra Curricualar</li>
                            <hr />
                          </ul>
                        </div>

                        <div className="bttn-g">
                          <span className="btn-w">Buy Now</span>
                        </div>
                      </div>
                    </Col>
                    <Col lg={3} md={6}>
                      <div className="type-of-prize-p">
                        <h4 className="name">Platinum</h4>
                        <div className="icn">
                          <img
                            src="/images/pricing/platinum.svg"
                            alt=""
                            className="img-fluid"
                          />
                        </div>
                        <h4 className="cost-s">499/-</h4>
                        <h5 className="cost-f">per annum</h5>

                        <div className="feature">
                          <ul>
                            <hr />
                            <li className="feature-name-s">
                              All Gold Features
                            </li>
                            <hr />
                            <li className="feature-name">Manual</li>
                            <hr />
                            <li className="feature-name">3 Resume Templates</li>
                            <hr />
                            <li className="feature-name">Alerts</li>
                            <hr />
                            <li className="feature-name">
                              Institution Comparison
                            </li>
                            <hr />
                            <li className="feature-name">
                              Form / Admission Alerts
                            </li>
                            <hr />
                            <li className="feature-name">Eligibilty Check</li>
                            <hr />
                            <li className="feature-name">
                              Single form for All
                            </li>
                            <hr />
                            <li className="feature-name">
                              {" "}
                              Student Loan Facility
                            </li>
                            <hr />
                            <li className="feature-name"> Extra Curricualar</li>
                            <hr />
                          </ul>
                        </div>

                        <div className="bttn-p">
                          <span className="btn-w">Buy Now</span>
                        </div>
                      </div>
                    </Col>
                  </Row>
                </div>
              </div>
            </Col>

            <Col lg={12}>
              <div className="pricing-main">
                <div className="wrapper-upper">
                  <div className="wp-img">
                    <img
                      src="/images/pricing/wpImg.svg"
                      alt=""
                      className="img-fluid"
                    />
                  </div>
                  <div className="wp-text">
                    <h1 className="wp-heading">Pricing for Tutoress</h1>
                    <p className="wp-para">
                      Pricing method is exercised to adjust the cost of the
                      producer's offerings suitable to both the manufacturer.
                    </p>
                  </div>
                </div>

                <div className="wrapper-lower">
                  <Row>
                    <Col lg={3} md={6}>
                      <div className="type-of-prize">
                        <h4 className="name">Bronze</h4>
                        <div className="icn">
                          <img
                            src="/images/pricing/bronze.svg"
                            alt=""
                            className="img-fluid"
                          />
                        </div>
                        <h4 className="cost">Free</h4>

                        <div className="feature">
                          <ul>
                            <hr />
                            <li className="feature-name">Issued By us</li>
                            <hr />
                            <li className="feature-name">Manual</li>
                            <hr />
                            <li className="feature-name">3 Resume Templates</li>
                            <hr />
                            <li className="feature-name">Alerts</li>
                            <hr />
                            <li className="feature-name">
                              Institution Comparison
                            </li>
                            <hr />
                            <li className="feature-name">
                              Form / Admission Alerts
                            </li>
                            <hr />
                            <li className="feature-name">Eligibilty Check</li>
                            <hr />
                            <li className="feature-name">
                              Single form for All
                            </li>
                            <hr />
                            <li className="feature-name">
                              {" "}
                              Student Loan Facility
                            </li>
                            <hr />
                            <li className="feature-name"> Extra Curricualar</li>
                            <hr />
                          </ul>
                        </div>

                        <div className="bttn">
                          <span className="btn-w">Buy Now</span>
                        </div>
                      </div>
                    </Col>
                    <Col lg={3} md={6}>
                      <div className="type-of-prize-s">
                        <h4 className="name">Silver</h4>
                        <div className="icn">
                          <img
                            src="/images/pricing/silver.svg"
                            alt=""
                            className="img-fluid"
                          />
                        </div>
                        <h4 className="cost-s">199/-</h4>
                        <h5 className="cost-f">per annum</h5>

                        <div className="feature">
                          <ul>
                            <hr />
                            <li className="feature-name-s">
                              All Bronze Features
                            </li>
                            <hr />
                            <li className="feature-name">Manual</li>
                            <hr />
                            <li className="feature-name">3 Resume Templates</li>
                            <hr />
                            <li className="feature-name">Alerts</li>
                            <hr />
                            <li className="feature-name">
                              Institution Comparison
                            </li>
                            <hr />
                            <li className="feature-name">
                              Form / Admission Alerts
                            </li>
                            <hr />
                            <li className="feature-name">Eligibilty Check</li>
                            <hr />
                            <li className="feature-name">
                              Single form for All
                            </li>
                            <hr />
                            <li className="feature-name">
                              {" "}
                              Student Loan Facility
                            </li>
                            <hr />
                            <li className="feature-name"> Extra Curricualar</li>
                            <hr />
                          </ul>
                        </div>

                        <div className="bttn-s">
                          <span className="btn-w">Buy Now</span>
                        </div>
                      </div>
                    </Col>
                    <Col lg={3} md={6}>
                      <div className="type-of-prize-g">
                        <h4 className="name">Gold</h4>
                        <div className="icn">
                          <img
                            src="/images/pricing/gold.svg"
                            alt=""
                            className="img-fluid"
                          />
                        </div>
                        <h4 className="cost-s">349/-</h4>
                        <h5 className="cost-f">per annum</h5>

                        <div className="feature">
                          <ul>
                            <hr />
                            <li className="feature-name-s">
                              All Silver Features
                            </li>
                            <hr />
                            <li className="feature-name">Manual</li>
                            <hr />
                            <li className="feature-name">3 Resume Templates</li>
                            <hr />
                            <li className="feature-name">Alerts</li>
                            <hr />
                            <li className="feature-name">
                              Institution Comparison
                            </li>
                            <hr />
                            <li className="feature-name">
                              Form / Admission Alerts
                            </li>
                            <hr />
                            <li className="feature-name">Eligibilty Check</li>
                            <hr />
                            <li className="feature-name">
                              Single form for All
                            </li>
                            <hr />
                            <li className="feature-name">
                              {" "}
                              Student Loan Facility
                            </li>
                            <hr />
                            <li className="feature-name"> Extra Curricualar</li>
                            <hr />
                          </ul>
                        </div>

                        <div className="bttn-g">
                          <span className="btn-w">Buy Now</span>
                        </div>
                      </div>
                    </Col>
                    <Col lg={3} md={6}>
                      <div className="type-of-prize-p">
                        <h4 className="name">Platinum</h4>
                        <div className="icn">
                          <img
                            src="/images/pricing/platinum.svg"
                            alt=""
                            className="img-fluid"
                          />
                        </div>
                        <h4 className="cost-s">499/-</h4>
                        <h5 className="cost-f">per annum</h5>

                        <div className="feature">
                          <ul>
                            <hr />
                            <li className="feature-name-s">
                              All Gold Features
                            </li>
                            <hr />
                            <li className="feature-name">Manual</li>
                            <hr />
                            <li className="feature-name">3 Resume Templates</li>
                            <hr />
                            <li className="feature-name">Alerts</li>
                            <hr />
                            <li className="feature-name">
                              Institution Comparison
                            </li>
                            <hr />
                            <li className="feature-name">
                              Form / Admission Alerts
                            </li>
                            <hr />
                            <li className="feature-name">Eligibilty Check</li>
                            <hr />
                            <li className="feature-name">
                              Single form for All
                            </li>
                            <hr />
                            <li className="feature-name">
                              {" "}
                              Student Loan Facility
                            </li>
                            <hr />
                            <li className="feature-name"> Extra Curricualar</li>
                            <hr />
                          </ul>
                        </div>

                        <div className="bttn-p">
                          <span className="btn-w">Buy Now</span>
                        </div>
                      </div>
                    </Col>
                  </Row>
                </div>
              </div>
            </Col>

            <Col lg={12}>
              <div className="pricing-main">
                <div className="wrapper-upper">
                  <div className="wp-img">
                    <img
                      src="/images/pricing/wpImg.svg"
                      alt=""
                      className="img-fluid"
                    />
                  </div>
                  <div className="wp-text">
                    <h1 className="wp-heading">Pricing for Others</h1>
                    <p className="wp-para">
                      Pricing method is exercised to adjust the cost of the
                      producer's offerings suitable to both the manufacturer.
                    </p>
                  </div>
                </div>

                <div className="wrapper-lower">
                  <Row>
                    <Col lg={3} md={6}>
                      <div className="type-of-prize">
                        <h4 className="name">Bronze</h4>
                        <div className="icn">
                          <img
                            src="/images/pricing/bronze.svg"
                            alt=""
                            className="img-fluid"
                          />
                        </div>
                        <h4 className="cost">Free</h4>

                        <div className="feature">
                          <ul>
                            <hr />
                            <li className="feature-name">Issued By us</li>
                            <hr />
                            <li className="feature-name">Manual</li>
                            <hr />
                            <li className="feature-name">3 Resume Templates</li>
                            <hr />
                            <li className="feature-name">Alerts</li>
                            <hr />
                            <li className="feature-name">
                              Institution Comparison
                            </li>
                            <hr />
                            <li className="feature-name">
                              Form / Admission Alerts
                            </li>
                            <hr />
                            <li className="feature-name">Eligibilty Check</li>
                            <hr />
                            <li className="feature-name">
                              Single form for All
                            </li>
                            <hr />
                            <li className="feature-name">
                              {" "}
                              Student Loan Facility
                            </li>
                            <hr />
                            <li className="feature-name"> Extra Curricualar</li>
                            <hr />
                          </ul>
                        </div>

                        <div className="bttn">
                          <span className="btn-w">Buy Now</span>
                        </div>
                      </div>
                    </Col>
                    <Col lg={3} md={6}>
                      <div className="type-of-prize-s">
                        <h4 className="name">Silver</h4>
                        <div className="icn">
                          <img
                            src="/images/pricing/silver.svg"
                            alt=""
                            className="img-fluid"
                          />
                        </div>
                        <h4 className="cost-s">199/-</h4>
                        <h5 className="cost-f">per annum</h5>

                        <div className="feature">
                          <ul>
                            <hr />
                            <li className="feature-name-s">
                              All Bronze Features
                            </li>
                            <hr />
                            <li className="feature-name">Manual</li>
                            <hr />
                            <li className="feature-name">3 Resume Templates</li>
                            <hr />
                            <li className="feature-name">Alerts</li>
                            <hr />
                            <li className="feature-name">
                              Institution Comparison
                            </li>
                            <hr />
                            <li className="feature-name">
                              Form / Admission Alerts
                            </li>
                            <hr />
                            <li className="feature-name">Eligibilty Check</li>
                            <hr />
                            <li className="feature-name">
                              Single form for All
                            </li>
                            <hr />
                            <li className="feature-name">
                              {" "}
                              Student Loan Facility
                            </li>
                            <hr />
                            <li className="feature-name"> Extra Curricualar</li>
                            <hr />
                          </ul>
                        </div>

                        <div className="bttn-s">
                          <span className="btn-w">Buy Now</span>
                        </div>
                      </div>
                    </Col>
                    <Col lg={3} md={6}>
                      <div className="type-of-prize-g">
                        <h4 className="name">Gold</h4>
                        <div className="icn">
                          <img
                            src="/images/pricing/gold.svg"
                            alt=""
                            className="img-fluid"
                          />
                        </div>
                        <h4 className="cost-s">349/-</h4>
                        <h5 className="cost-f">per annum</h5>

                        <div className="feature">
                          <ul>
                            <hr />
                            <li className="feature-name-s">
                              All Silver Features
                            </li>
                            <hr />
                            <li className="feature-name">Manual</li>
                            <hr />
                            <li className="feature-name">3 Resume Templates</li>
                            <hr />
                            <li className="feature-name">Alerts</li>
                            <hr />
                            <li className="feature-name">
                              Institution Comparison
                            </li>
                            <hr />
                            <li className="feature-name">
                              Form / Admission Alerts
                            </li>
                            <hr />
                            <li className="feature-name">Eligibilty Check</li>
                            <hr />
                            <li className="feature-name">
                              Single form for All
                            </li>
                            <hr />
                            <li className="feature-name">
                              {" "}
                              Student Loan Facility
                            </li>
                            <hr />
                            <li className="feature-name"> Extra Curricualar</li>
                            <hr />
                          </ul>
                        </div>

                        <div className="bttn-g">
                          <span className="btn-w">Buy Now</span>
                        </div>
                      </div>
                    </Col>
                    <Col lg={3} md={6}>
                      <div className="type-of-prize-p">
                        <h4 className="name">Platinum</h4>
                        <div className="icn">
                          <img
                            src="/images/pricing/platinum.svg"
                            alt=""
                            className="img-fluid"
                          />
                        </div>
                        <h4 className="cost-s">499/-</h4>
                        <h5 className="cost-f">per annum</h5>

                        <div className="feature">
                          <ul>
                            <hr />
                            <li className="feature-name-s">
                              All Gold Features
                            </li>
                            <hr />
                            <li className="feature-name">Manual</li>
                            <hr />
                            <li className="feature-name">3 Resume Templates</li>
                            <hr />
                            <li className="feature-name">Alerts</li>
                            <hr />
                            <li className="feature-name">
                              Institution Comparison
                            </li>
                            <hr />
                            <li className="feature-name">
                              Form / Admission Alerts
                            </li>
                            <hr />
                            <li className="feature-name">Eligibilty Check</li>
                            <hr />
                            <li className="feature-name">
                              Single form for All
                            </li>
                            <hr />
                            <li className="feature-name">
                              {" "}
                              Student Loan Facility
                            </li>
                            <hr />
                            <li className="feature-name"> Extra Curricualar</li>
                            <hr />
                          </ul>
                        </div>

                        <div className="bttn-p">
                          <span className="btn-w">Buy Now</span>
                        </div>
                      </div>
                    </Col>
                  </Row>
                </div>
              </div>
            </Col>

            <Col lg={12}>
              <div className="pricing-main">
                <div className="contact-text">
                  <div className="img-contact">
                    <img
                      src="/images/pricing/mail.svg"
                      alt=""
                      className="img-fluid"
                    />
                  </div>
                  <h2 className="head1">Contact Our Friendly</h2>
                  <h1 className="head2">Support Team</h1>
                  <p className="para">
                    e-Validated uses Hyperledger Fabric & Hyperledger Indy to
                    create the credential ecosystem and retain complete
                    confidentiality we have created a solution that’s truly
                    one-of-a-kind and native for Indian education’s
                    infrastructure.
                  </p>
                </div>
                <div className="contact-form">
                  <form class="row g-3 needs-validation" novalidate>
                    <div class="col-md-6">
                      <div class="input-group has-validation">
                        <span class="input-group-text" id="inputGroupPrepend">
                          <img
                            src="/images/pricing/nameI.svg"
                            alt=""
                            className="formIcon"
                          />
                        </span>
                        <input
                          type="text"
                          class="form-control"
                          id="validationCustomUsername"
                          aria-describedby="inputGroupPrepend"
                          placeholder="Your Name: "
                          required
                        />
                        <div class="invalid-feedback">
                          Please choose a username.
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="input-group has-validation">
                        <span class="input-group-text" id="inputGroupPrepend">
                          <img
                            src="/images/pricing/emailI.svg"
                            alt=""
                            className="formIcon"
                          />
                        </span>
                        <input
                          type="text"
                          class="form-control"
                          id="validationCustomUsername"
                          aria-describedby="inputGroupPrepend"
                          placeholder="Your Email: "
                          required
                        />
                        <div class="invalid-feedback">
                          Please choose a username.
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="input-group has-validation">
                        <span class="input-group-text" id="inputGroupPrepend">
                          <img
                            src="/images/pricing/phoneI.svg"
                            alt=""
                            className="formIcon"
                          />
                        </span>
                        <input
                          type="text"
                          class="form-control"
                          id="validationCustomUsername"
                          aria-describedby="inputGroupPrepend"
                          placeholder="Phone: "
                          required
                        />
                        <div class="invalid-feedback">
                          Please choose a username.
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="input-group has-validation">
                        <span class="input-group-text" id="inputGroupPrepend">
                          <img
                            src="/images/pricing/subI.svg"
                            alt=""
                            className="formIcon"
                          />
                        </span>
                        <input
                          type="text"
                          class="form-control"
                          id="validationCustomUsername"
                          aria-describedby="inputGroupPrepend"
                          placeholder="Subject:   "
                          required
                        />
                        <div class="invalid-feedback">
                          Please choose a username.
                        </div>
                      </div>
                    </div>

                    <div class="col-md-12">
                      <div class="input-group has-validation">
                        <span class="input-group-text" id="inputGroupPrepend">
                          <img
                            src="/images/pricing/smsI.svg"
                            alt=""
                            className="formIcon"
                          />
                        </span>

                        <textarea
                          class="form-control"
                          aria-label="With textarea"
                          placeholder="Message:"
                          required
                          id="exampleFormControlTextarea1"
                          rows="5"
                        ></textarea>

                        <div class="invalid-feedback">
                          Please choose a username.
                        </div>
                      </div>
                    </div>
                    <div class="col-12 bttn-b">
                      <button class="btn btn-primary" type="submit">
                        Send
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
};

export default pricing;
