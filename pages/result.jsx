import React, { useState } from "react";
import { Container, Row, Col, Accordion } from "react-bootstrap";
import Head from "next/head";
import Image from "next/image";
import PageLayout from "@components/PageLayout";
import Dashboard from "public/images/admin-menu-icon/dashboard.svg";
import Link from "next/link";
import Signup from "./form/Signup";
import RegisterRecieve from "./form/RegisterRecieve";
import Subscribe from "./form/Subscribe";

const Result = () => {
  const [items, setItems] = useState([
    {
      title: "Board of Intermediate Education, Andhra Pradesh",
      img: Dashboard,
      date: "13 April 2023",
      content: {
        heading: "SSC Exam Result 2023 (Regular & Private)",
        register: "Register",
      },
    },
    {
      title: "Central Board of Secondary Education",
      date: "13 April 2023",
      img: Dashboard,
      content: {
        heading: "SSC Exam Result 2023 (Regular & Private)",
        register: "Register",
      },
    },
    {
      title: "Joint Entrance Examination (JEE)",
      date: "13 April 2023",
      img: Dashboard,
      content: {
        heading: "SSC Exam Result 2023 (Regular & Private)",
        register: "Register",
      },
    },
    {
      title: "Krishna University, Andhra Pradesh",
      date: "13 April 2023",
      img: Dashboard,
      content: {
        heading: "SSC Exam Result 2023 (Regular & Private)",
        register: "Register",
      },
    },
  ]);
  return (
    <div>
      <Head>
        <title>Result | Business Verification Services</title>
        <meta
          id="meta-description"
          name="description"
          content=" This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
        <meta
          property="og:title"
          content="Result | Business Verification Services"
        />
        <meta
          property="og:description"
          content="This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
        <meta
          property="twitter:title"
          content="Certified Academy | Academy Verification | VerifyKiya"
        />
        <meta
          property="twitter:description"
          content="This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
      </Head>
      <PageLayout>
        <div className="resources">
          <div className="HeroSectionWrapper">
            <Container fluid>
              <Row>
                <Col md={12}>
                  <div className="headingWhileLabel text-center">
                    <h1>Exam Results in India</h1>
                  </div>
                </Col>
              </Row>
              <Row>
                <Col md={12}>
                  <div className="headingWhileLabel Montserrat ">
                    <h4 className="text-center">
                      Boards of Education in India
                    </h4>
                    <p className="text-center">
                      Check out the latest board class 10th and 12th exam
                      results updates, entrance exams such as management,
                      engineering and all other
                    </p>
                    <div className="center">
                      <ul>
                        <li>CBSE</li>
                        <li>CISCE</li>
                        <li>NIOS</li>
                        <li>Andhra Pradesh</li>
                        <li>Assam</li>
                        <li>Bihar</li>
                        <li>Chhattisgarh</li>
                        <li>Goa</li>
                        <li>Gujarat</li>
                        <li>Haryana</li>
                        <li>Himachal Pradesh</li>
                        <li>Jammu & Kashmir</li>
                        <li>Jharkhand</li>
                        <li>Karnataka</li>
                        <li>Kerala</li>
                        <li>Madhya Pradesh</li>
                        <li>Maharashtra</li>
                        <li>Manipur</li>
                        <li>Meghalaya</li>
                        <li>Mizoram</li>
                        <li>Nagaland</li>
                        <li>Odisha</li>
                        <li>Punjab</li>
                        <li>Rajasthan</li>
                        <li>Tamil Nadu</li>
                        <li>Telangana</li>
                        <li>Tripura</li>
                        <li>Uttarakhand</li>
                        <li>Uttar Pradesh</li>
                        <li>West Bengal</li>
                      </ul>
                    </div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
          <div className="ranking wrapper">
            <Container fluid>
              <Row>
                <Col xl={9} lg={12}>
                  <h2>Latest Announcements</h2>
                  <p className="pb-5 text-white">
                    The most recent information on entrance examinations,
                    academic tests for high school, college, and government jobs
                    in India. Get the latest news updates on entrance exams,
                    school & university exams & competitive exams.
                  </p>
                  <div className="mt-5">
                    {items.map((item, index) => (
                      <div key={index}>
                        <Accordion defaultActiveKey="0">
                          <Accordion.Item eventKey="0">
                            <Accordion.Header>
                              <div className="scholar">
                                <div className="price">
                                  <div className="ico">
                                    <div className="sm-icons">
                                      <Image
                                        src={item.img}
                                        width={60}
                                        height={60}
                                        alt="img"
                                        className="img-fluid"
                                      />
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div className="d-flex justify-content-between w-100 flex-wrap">
                                <div>{item.title}</div>
                                <div>{item.date}</div>
                              </div>
                            </Accordion.Header>
                            <Accordion.Body>
                              <ul>
                                <li>
                                  <div className="flex">
                                    <Link href="/">{item.content.heading}</Link>
                                    <span>{item.content.register}</span>
                                  </div>
                                </li>
                              </ul>
                            </Accordion.Body>
                          </Accordion.Item>
                        </Accordion>
                      </div>
                    ))}
                  </div>
                </Col>
                <Col xl={3} lg={12}>
                  <div className="w-300">
                    <iframe
                      src="/images/host.png"
                      scrolling="no"
                      width="400"
                      height="250"
                      class="responsive-iframe"
                    ></iframe>
                    <Signup />
                    <RegisterRecieve />
                    <Subscribe />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </div>
      </PageLayout>
    </div>
  );
};

export default Result;
