import React, { useState } from "react";
import { Container, Row, Col, Table } from "react-bootstrap";
import Head from "next/head";
import Link from "next/link";
import PageLayout from "@components/PageLayout";
import Grid from '@mui/material/Unstable_Grid2';

const Ranking = () => {
  const [items, setItems] = useState([
    {
      title: "UPSC Course - Regular",
      para: "Central Delhi Classroom Programs",
      content:
        "UPSC is one of the toughest examinations. The institute helps the students in making the examination preparation a little easier by giving the right guidance to the students. The institute also conducts test series and provide study material that is made by the best and experienced faculty of the institute. The institute conducts classes for 3 to 4 hour long on weekdays and 7 to 8 hours long on weekends.",
      course: "Work Visa",
      payment: "Part Payment Avialable",
      courses: "Masters Degree",
      classes: "12 Months",
      coursetype: "Jun 2023",
      fees: {
        list: {
          item1: "Tuition - 32,100 USD",
          item2: "Living - 10,000 USD/Year",
          item3: " Insurance - 450 USD",
        },
      },
      exam: {
        list: {
          item1: "IELTS - 6.5",
          item2: "TOEFL IBT - 79-93",
          item3: "TOEFL CBT - 213-233",
          item4: "PTE - 58-65",
        },
      },
      scholarship: {
        title: "Nutanix Heart Women in Technology Scholarships 2023",
        eligiblity: "Full-time Undergraduate or Graduate students",
        award: "Up to $2,000 towards tuition and fees (one-time)",
      },
    },
  ]);

  return (
    <div>
      <Head>
        <title>
          Study Abroad-University-Course | Business Verification Services
        </title>
        <meta
          id="meta-description"
          name="description"
          content=" This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
        <meta
          property="og:title"
          content="Study Abroad-University-Course | Business Verification Services"
        />
        <meta
          property="og:description"
          content="This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
        <meta
          property="twitter:title"
          content="Certified Academy | Academy Verification | VerifyKiya"
        />
        <meta
          property="twitter:description"
          content="This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
      </Head>
      <PageLayout>
        <div className="resources">
          <div className="HeroSectionWrapper">
            <div className="frame1 d-xl-block d-none"></div>
            <div className="frame2 d-xl-block d-none"></div>
            <Container fluid>
              <Row>
                <Col md={12}>
                  <div className="headingWhileLabel mb-5">
                    <div className="text-center">
                      <h1>Master of Business Administration</h1>
                      <h4 className="mb-4">
                        Harvard University, Cambridge United States
                      </h4>
                    </div>
                    <p>
                      This graduate degree is intended for those early in their
                      career or those looking to add business knowledge to their
                      undergraduate degree. This is a 10-month, cohort-based
                      MBA. Students with a background in accounting earn credits
                      toward the 150-unit CPA licensing requirements along with
                      their MBA. Management students acquire communication and
                      leadership skills valuable to employers and have the
                      opportunity to participate in an experiential class where
                      they consult for real clients.
                    </p>
                  </div>
                  <div className="content">
                    <h5>Entry Requirement: </h5>
                    <p>
                      The minimum academic requirement for full entry and
                      enrolment is a Bachelor degree or international equivalent
                      with a minimum GPA of 5/7.
                    </p>
                  </div>
                  <div className="content">
                    <h5>
                      <strong>English Requirement:</strong> An overall score of
                      6.5 with a minimum of 6.0 in each component of the test.
                    </h5>
                  </div>
                  <div className="content ">
                    <h5>
                      <strong>Entry Requirement:</strong> The minimum academic
                      requirement for full entry and enrolment is a Bachelor
                      degree or international equivalent with a minimum GPA of
                      5/7.
                    </h5>
                    <ul className="list1">
                      <li>
                        <strong>Annual Fees: </strong>AUD 49,330
                      </li>
                      <li>
                        <strong>Intake:</strong>Febuary
                      </li>
                      <li>
                        <strong>Insurance:</strong>AUD $478 to $529
                      </li>
                      <li>
                        <strong>Time: </strong>2 Years
                      </li>
                    </ul>
                    <ul>
                      <strong>
                        In pursuit of these goals we commit to the following:
                      </strong>
                      <li>
                        foster a sense of community and promote diversity among
                        our students, staff, faculty, and alumni,
                      </li>
                      <li>
                        offer a rigorous, current, AACSB-accredited curriculum,
                      </li>
                      <li>
                        integrate advanced technologies into the curriculum,
                      </li>
                      <li>
                        support and encourage our faculty in their creative and
                        personalized delivery of our curriculum and their
                        counseling of our students,
                      </li>
                      <li>
                        maintain and nurture an ongoing faculty commitment to a
                        broad array of intellectual contributions that includes
                        pedagogical research, contributions to practice, and
                        discipline-based scholarship.
                      </li>
                    </ul>
                  </div>
                </Col>
              </Row>
            </Container>
            <div className="frame3"></div>
            <div className="frame4"></div>
          </div>
          <div className="wrapper contact">
            <div className="contact-box pb-5">
              <Container fluid>
            
              {items.map((item, index) => (
                <div key={index}>
                  < >
                    <div className="bg">
                      <div className="z-index">
                      <Row className="align-items-end">
                        <Col xl={2} lg={12}>
                          <div className="icons">
                            <img
                              src="images/Immutable.png"
                              className="img-fluid"
                            />
                          </div>
                        </Col>
                        <Col xl={10} lg={12}>
                          <div className="end">
                            <div>
                              <h1>Master of Business Administration</h1>
                              <h2 className="text-black">
                                Harvard University, Cambridge United States
                              </h2>
                            </div>
                            <div></div>
                          </div>
                        </Col>
                      </Row>

                      <Row className="my-5">
                        <Col lg={12}>
                          <div className="price">
                            <div className="d-flex justify-content-between align-items-center header flex-wrap">
                              <div className="text d-flex flex-wrap align-items-center gap-2">
                                <strong>{item.course}</strong>
                              </div>
                              <div className="btns">
                                <Link href="/institutesDetails">
                                  <span className="btn btn-primary">Apply Now</span>
                                </Link>
                                <Link href="">
                                  <span className="btn btn-apply">Email Brochure</span>
                                </Link>
                              </div>
                            </div>
                            <Table responsive borderless>
                              <tbody>
                                <tr>
                                  <td>Course Level</td>
                                  <td>Duration</td>
                                  <td>Intake Session</td>
                                  <td>Fees & Expenses</td>
                                </tr>
                                <tr>
                                  <td>
                                    <strong>{item.courses}</strong>
                                  </td>
                                  <td>
                                    <strong>{item.classes}</strong>
                                  </td>
                                  <td>
                                    <strong>{item.coursetype}</strong>
                                  </td>
                                  <td>
                                    <ul>
                                      <li>
                                        <strong>{item.fees.list.item1}</strong>
                                      </li>
                                      <li>
                                        <strong>{item.fees.list.item2}</strong>
                                      </li>
                                      <li>
                                        <strong>{item.fees.list.item3}</strong>
                                      </li>
                                    </ul>
                                  </td>
                                </tr>
                              </tbody>
                            </Table>
                          </div>
                        </Col>
                      </Row>
                      <Row className="my-5">
                        <Col lg={12}>
                          <div className="price">
                            <div className="d-flex justify-content-between align-items-center header flex-wrap">
                              <div className="text d-flex flex-wrap align-items-center gap-2">
                                <strong>Eligibility Requirements</strong>
                              </div>
                              <div className="btns">
                                <Link href="/institutesDetails">
                                  <span className="btn btn-primary">Apply Now</span>
                                </Link>
                              </div>
                            </div>
                            <Table responsive borderless>
                              <tbody>
                                <tr>
                                  <td>Exam Accepted</td>
                                  <td>{item.course}</td>
                                </tr>
                                <tr>
                                  <td>
                                    <ul>
                                      <li>
                                        <strong>{item.exam.list.item1}</strong>
                                      </li>
                                      <li>
                                        <strong>{item.exam.list.item2}</strong>
                                      </li>
                                      <li>
                                        <strong>{item.exam.list.item3}</strong>
                                      </li>
                                      <li>
                                        <strong>{item.exam.list.item4}</strong>
                                      </li>
                                    </ul>
                                  </td>
                                  <td>...</td>
                                </tr>
                              </tbody>
                            </Table>
                          </div>
                        </Col>
                      </Row>
                      <Row className="py-5">
                        <Col lg={12}>
                          <div className="price">
                            <div className="d-flex justify-content-between align-items-center header flex-wrap">
                              <div className="text d-flex flex-wrap align-items-center gap-2">
                                <strong>Scholarships 2023</strong>
                              </div>
                            </div>
                          </div>
                          <Row>
                            <Col lg={6}>
                              <div className="price">
                                <div className="p-3">
                                  <div>
                                    <h6>
                                      <strong>{item.scholarship.title}</strong>
                                    </h6>
                                    <p>
                                      Eligiblity:{item.scholarship.eligiblity}
                                    </p>
                                    <p>Award:{item.scholarship.award}</p>
                                  </div>

                                  <div className="btns d-flex justify-content-between">
                                    <Link href="/institutesDetails">
                                      <span className="btn btn-primary">
                                        View Details
                                      </span>
                                    </Link>
                                    <Link href="">
                                      <span className="btn btn-apply">
                                        Last Date to apply 15 May, 23
                                      </span>
                                    </Link>
                                  </div>
                                </div>
                              </div>
                            </Col>
                            <Col lg={6}>
                            <div className="price">
                                <div className="p-3">
                                  <div>
                                    <h6>
                                      <strong>{item.scholarship.title}</strong>
                                    </h6>
                                    <p>
                                      Eligiblity:{item.scholarship.eligiblity}
                                    </p>
                                    <p>Award:{item.scholarship.award}</p>
                                  </div>

                                  <div className="btns d-flex justify-content-between">
                                    <Link href="/institutesDetails">
                                      <span className="btn btn-primary">
                                        View Details
                                      </span>
                                    </Link>
                                    <Link href="">
                                      <span className="btn btn-apply">
                                        Last Date to apply 15 May, 23
                                      </span>
                                    </Link>
                                  </div>
                                </div>
                              </div>
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                      </div>
                    </div>
                  </>
                </div>
              ))}
              </Container>
            </div>
          </div>
        </div>
      </PageLayout>
    </div>
  );
};

export default Ranking;
