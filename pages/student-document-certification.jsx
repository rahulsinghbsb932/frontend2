import React from "react";
import Head from "next/head";
import Image from "next/image";
import PageLayout from "@components/PageLayout";
import { Container, Row, Col } from "react-bootstrap";
import Card from "react-bootstrap/Card";

const studentDocumentCertification = () => {
  return (
    <div>
      <Head>
        <title>
          Student Document Certification | Verification | VerifyKiya
        </title>
        <meta
          id="meta-description"
          name="description"
          content="Student Document Certification is a service provided to students to authenticate and verify the validity and accuracy of their educational documents, such as transcripts, diplomas, certificates, and other academic records."
        />
        <meta
          property="og:title"
          content=" Student Document Certification | Verification | VerifyKiya"
        />
        <meta
          property="og:description"
          content="Student Document Certification is a service provided to students to authenticate and verify the validity and accuracy of their educational documents, such as transcripts, diplomas, certificates, and other academic records."
        />
        <meta
          property="twitter:title"
          content=" Student Document Certification | Verification | VerifyKiya"
        />
        <meta
          property="twitter:description"
          content="Student Document Certification is a service provided to students to authenticate and verify the validity and accuracy of their educational documents, such as transcripts, diplomas, certificates, and other academic records."
        />
      </Head>

      <PageLayout>
        <div className="HeroSectionWrapper">
          <Container fluid>
            <Row>
              <Col md={12}>
                <div className="headingWhileLabel Montserrat">
                  <h1>Student </h1>
                  <p>
                    An average student's life is already jam-packed with
                    studies, deadlines, planning etc. With all the mayhem,
                    Verifykiya strives to be the one-stop destination for every
                    student's needs in their academic and professional journey.
                    By leveraging the benefits of our platform, students will
                    have much more time and mental space to focus on their core
                    competencies. Our automated resume builder makes it easier
                    to draft industry-appropriate resumes within a fraction of
                    the time. Our robust technological base also ensures that
                    these credentials will have foolproof validity. Verifykiya
                    helps students make informed decisions by offering research
                    and analytical support So, join Verikykiya and take control
                    of your future!
                  </p>
                </div>
              </Col>
            </Row>
          </Container>

          <Container fluid>
            <Row>
              <Col lg={3} md={6} className="rowCenter mt-5">
                <a className="cardanchor" href="/">
                  <Card className="CardSol">
                    <Image
                      src="/images/student/school.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">Play School</Card.Title>
                      <Card.Text>
                        A school is an educational institution designed to
                        provide learning spaces and learning.
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col lg={3} md={6} className="rowCenter mt-5">
                <a className="cardanchor" href="/">
                  <Card className="CardSol">
                    <Image
                      src="/images/student/tution.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">Schools</Card.Title>
                      <Card.Text>
                        A school is an educational institution designed to
                        provide learning spaces and learning.
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col lg={3} md={6} className="rowCenter mt-5">
                <a className="cardanchor" href="/">
                  <Card className="CardSol">
                    <Image
                      src="/images/student/schools.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">College</Card.Title>
                      <Card.Text>
                        A school is an educational institution designed to
                        provide learning spaces and learning.
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col lg={3} md={6} className="rowCenter mt-5">
                <a className="cardanchor" href="/">
                  <Card className="CardSol">
                    <Image
                      src="/images/student/training.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Universities
                      </Card.Title>
                      <Card.Text>
                        A university is an institution of higher education and
                        research which awards academic.
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
            </Row>
            <Row>
              <Col lg={3} md={6} className="rowCenter mt-5">
                <a className="cardanchor" href="/">
                  <Card className="CardSol">
                    <Image
                      src="/images/student/colledge.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Training institutes
                      </Card.Title>
                      <Card.Text>
                        Institutions are humanly devised structures of rules and
                        norms that shape & constrain.
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col lg={3} md={6} className="rowCenter mt-5">
                <a className="cardanchor" href="/">
                  <Card className="CardSol">
                    <Image
                      src="/images/student/sports.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Coaching Center
                      </Card.Title>
                      <Card.Text>
                        A school is an educational institution designed to
                        provide learning spaces and learning.
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col lg={3} md={6} className="rowCenter mt-5">
                <a className="cardanchor" href="/">
                  <Card className="CardSol">
                    <Image
                      src="/images/student/coaching.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Tuition Center
                      </Card.Title>
                      <Card.Text>
                        A university is an institution of higher education and
                        research which awards academic.
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col lg={3} md={6} className="rowCenter mt-5">
                <a className="cardanchor" href="/">
                  <Card className="CardSol">
                    <Image
                      src="/images/student/university.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">Sports</Card.Title>
                      <Card.Text>
                        Institutions are humanly devised structures of rules and
                        norms that shape & constrain.
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
            </Row>
          </Container>
        </div>

        <div className="sectionRelative m-5">
          <Container fluid>
            <Col md={12}>
              <div class="ourSolutionWrapper">
                <h1 class="ourSolutionSectionheading">
                  <span style={{ fontWeight: 300 }}>Here comes </span>
                  Verifykiya!
                </h1>
                <p className="ourSolutionPera">
                  Understanding the gravity of the situation, we created this
                  platform to meet the needs of industry-wide institutions. You
                  can finally say goodbye to all the deceit and the irrelevant
                  workload. It’s time to take your brand to greater heights,
                  with Verifykiya!
                </p>
              </div>
            </Col>
          </Container>
        </div>

        <section>
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../../assets/img/student/degree.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/student/degree.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="degree-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Digital Wallet for Certificates, Badges, Degrees,
                        Marksheet etc.
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Accumulating and maintaining all the credentials one gets
                      in their life along with their proofs is a tedious task.
                      Not only can documents get misplaced but also there is a
                      chance their authenticity might be called into question.
                      But with Verifykiya, a blockchain based digital wallet is
                      created for each student to record their life's work on.
                      This wallet works on permission and no information will be
                      accessible to anyone without proper authorization. The
                      information stored and verified by this wallet can't be
                      copied and therefore maintains its integrity. 
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section>
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="ar-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Automated and Resume Builder
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Sometimes even the best candidates miss out on amazing
                      opportunities because they are clueless about how to build
                      an appropriate resume. Writing and designing your resume
                      is an art and one of the most important steps in your
                      journey as a student. To make your life simpler and to
                      save your precious time, Verifykiya has come up with an
                      automated resume builder. All your key credentials will be
                      automatically placed on a template of your choice - this
                      can even be further customized.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
                <Col xl={3} lg={12} className="order-xl-2 order-1">
                  <div className="img">
                    {/* <img
                      src={
                        require("../../../assets/img/student/ar.svg").default
                      }
                      alt=""
                      className="img-fluid "
                    /> */}

                    <Image
                      src="/images/student/ar.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section>
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../../assets/img/student/as.svg").default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/student/as.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="as-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Access to Scholarships, Placements, Skill and
                        Personality Development
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      In this day and age, students need something much more
                      than their course material. They are expected to go above
                      and beyond. And if they have the means to support in this
                      journey, that is what sets apart the best students. With
                      Verifykiya, students will get exclusive and real-time
                      updates about all scholarships, placements etc that they
                      may be eligible for and will even offer registration
                      support. To make students more well-rounded, our platform
                      even features great gamified techniques to boost and hone
                      their soft and hard skills.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section>
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="sa-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Study Abroad with Assured Scholarships
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Verifykiya offers a complete guide for students who wish
                      to study abroad, Our Program is designed to educate
                      students and guide them in exploring best possible
                      university options.  Using Verifykiya platform student
                      just needs to fill up a single application for applying to
                      multiple universities and also get a assured assured
                      scholarships and attractive discounts
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
                <Col xl={3} lg={12} className="order-xl-2 order-1">
                  <div className="img">
                    {/* <img
                      src={
                        require("../../../assets/img/student/sa.svg").default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/student/sa.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section>
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../../assets/img/student/rt.svg").default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/student/rt.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="rt-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Real Time Alerts on Admission, Exams, Results etc.
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      A student's life is a constant chase from deadline to
                      deadline. In this chaos, we aim to lend you a helping hand
                      with real time updates on a multitude of topics. From
                      admission deadlines, attendance trackers, reminders,exam
                      dates to their results, all the little updates will be
                      known to you as and when they come. This will help you
                      organize your work and set up an accurate priority list of
                      tasks. 
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section>
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="sa-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Single Form for Applying to all
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Does it feel like sometimes your day starts and ends with
                      just filling different forms. And it's often the same
                      information again and again. Not only is this time
                      consuming but let's be real, it is also frustrating.
                      Verifykiya facilitates the automated filling of
                      information in all the forms. Standardized questions in
                      this way can be quickly filled out, and you can move on to
                      the more imperative essay type questions.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
                <Col xl={3} lg={12} className="order-xl-2 order-1">
                  <div className="img">
                    <Image
                      src="/images/student/sf.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section>
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../../assets/img/student/mr.svg").default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/student/mr.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="mr-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Multiple Resume Templates
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      We host a collection of industry approved and attractive
                      resume templates. Students have the option of browsing
                      through them and selecting the one of their choice. They
                      are even further segregated according to the industry to
                      quicken your exploration and give you better and more
                      suitable results. 
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section>
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="am-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Access to Mock Test and Practice Questions
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      A majority of the students are preparing for some
                      competitive exams or the other. To help you excel, we also
                      offer a set of mock tests , practice questions and past
                      year papers for numerous exams like DU JAT, GMAT, CAT etc.
                      This comes with a feature of evaluating your performance
                      like time taken, difficulty level etc. You can even
                      compare and measure your performance with other students
                      attempting that same paper.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
                <Col xl={3} lg={12} className="order-xl-2 order-1">
                  <div className="img">
                    {/* <img
                      src={
                        require("../../../assets/img/student/am.svg").default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/student/am.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section>
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../../assets/img/student/makes.svg").default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/student/makes.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="makes-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Verified Database of Coaching Institutes, Tuition
                        Centers and Independent Tutors etc
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Recognising credible and good coaching and or tuition
                      centers is a struggle. Committing to such tutors is a risk
                      both financially and study-wise. Verifykiya curates a list
                      of such institutions or independent tutors who have a
                      spotless track record and ensure quality of training
                      provided.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section>
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="da-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Option to Enroll in National and International
                        Competitions
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Verifykiya curates and partners with a host of credible
                      and challenging competition organisers, both national and
                      international. This facilitates easy tracking of
                      interesting competitions that will not only enhance
                      students’ experiences but also contribute to their
                      development. Verifykiya provides real time alerts so that
                      students do not miss out on any competition for which they
                      are eligible. Participation in these competitions acts as
                      a image booster for students.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
                <Col xl={3} lg={12} className="order-xl-2 order-1">
                  <div className="img">
                    {/* <img
                      src={
                        require("../../../assets/img/student/database.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/student/database.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section>
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../../assets/img/student/job.svg").default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/student/job.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="job-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Job and Internship Marketplace
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Verifykiya connects employers to potential employees,
                      recruiters to students. This common marketplace makes it
                      easy for students to find their dream job and for
                      recruiters to employ the perfect candidate. This
                      marketplace makes things simpler and much more seamless.
                      The verified resumes of all students are present on the
                      portal. Once a student expresses interest in a particular
                      job opening or recruiter, they can send their resumes
                      right away. The recruiter can also do away with the
                      background check as the resumes are pre-verified.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section>
          <div className="solution mb-5">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                  <div className="d-flex position">
                    <div className="icon">
                      <div className="veri-icon"></div>
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Option to Enroll in National and International
                        Competitions
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Verifykiya curates and partners with a host of credible
                      and challenging competition organisers, both national and
                      international. This facilitates easy tracking of
                      interesting competitions that will not only enhance
                      students’ experiences but also contribute to their
                      development. Verifykiya provides real time alerts so that
                      students do not miss out on any competition for which they
                      are eligible. Participation in these competitions acts as
                      a image booster for students.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
                <Col xl={3} lg={12} className="order-xl-2 order-1">
                  <div className="img">
                    {/* <img
                      src={
                        require("../../../assets/img/student/verify.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/student/verify.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
      </PageLayout>
    </div>
  );
};

export default studentDocumentCertification;
