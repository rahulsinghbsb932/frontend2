import *  as React from 'react';
import { useState } from 'react';
import { Col, Container, Row } from "react-bootstrap";

import OutlinedInput from "@mui/material/OutlinedInput";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import ListItemText from "@mui/material/ListItemText";
import Select from "@mui/material/Select";
import Checkbox from "@mui/material/Checkbox";
import PageLayout from "@components/PageLayout";
import Head from "next/head";
import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import Signup from "./form/Signup";
import RegisterRecieve from "./form/RegisterRecieve";
import Subscribe from "./form/Subscribe";
import { DatePicker, Space } from 'antd';
const { RangePicker } = DatePicker;



const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 100,
      background: "linear-gradient(180deg, #9DA2DF 0%, #CCD0FF 7.29%)",
      borderColor: "#fff",
      border: 0,
    },
  },
};

const AcademicSession = ["2022 - 2023", "2023 - 2024"];
const SelectClass = ["LKG", "UKG"];
const AdmissionStatus = [
  "Ongoing",
  "Yet to Begin",
  "Tentatively Know",
  "Not Announced Yet",
];

const TypeOfSchool = [
  "All Schools",
  "Play ",
  "Primary ",
  "High ",
  "Boarding ",
  "Day ",
  "Day Boarding ",
  "Residential Boarding",
];

const AdmissionTracker = () => {
  const [personName, setPersonName] = React.useState([]);
  const [selectClass, setSelectClass] = React.useState([]);
  const [admissionStatus, setAdmissionStatus] = React.useState([]);
  const [typeOfSchool, setTypeOfSchool] = React.useState([]);

  //date
  const [state, setState] = useState([
    {
      startDate: new Date(),
      endDate: null,
      key: 'selection'
    }
  ]);

  const handleChange = (event) => {
    const {
      target: { value },
    } = event;
    setPersonName(
      // On autofill we get a stringified value.
      typeof value === "string" ? value.split(",") : value
    );
  };

  const handleChangeSelectClass = (event) => {
    const {
      target: { value },
    } = event;
    setSelectClass(
      // On autofill we get a stringified value.
      typeof value === "string" ? value.split(",") : value
    );
  };

  const handleChangeAdmissionStatus = (event) => {
    const {
      target: { value },
    } = event;
    setAdmissionStatus(
      // On autofill we get a stringified value.
      typeof value === "string" ? value.split(",") : value
    );
  };

  const handleChangeTypeOfSchool = (event) => {
    const {
      target: { value },
    } = event;
    setTypeOfSchool(
      // On autofill we get a stringified value.
      typeof value === "string" ? value.split(",") : value
    );
  };
  return (
    <>
      <Head>
        <title>Admission Tracker| Business Verification Services</title>
        <meta
          id="meta-description"
          name="description"
          content=" This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
        <meta
          property="og:title"
          content="Admission Tracker | Business Verification Services"
        />
        <meta
          property="og:description"
          content="This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
        <meta
          property="twitter:title"
          content="Certified Academy | Academy Verification | VerifyKiya"
        />
        <meta
          property="twitter:description"
          content="This certification process involves a thorough evaluation and verification of corporate practices, policies, and performance against established standards, guidelines, and industry best practices."
        />
      </Head>
      <PageLayout>
        <div className="resources">
          <div className="light-blue"></div>
          <div className="z-index">
            <div className="admission-wrapper">
              <div className="mt-12">
                <Container fluid>
                  <Row>
                    <Col lg={9}>
                      <div className="box">
                        <div className="box-upper">
                          <div className="img">
                            <img
                              src="/images/AdmissionTracker/icn.svg"
                              alt=""
                              className="img-fluid"
                            />
                          </div>
                          <div className="text">
                            <h1 className="head1">Real Time </h1>
                            <h1 className="head2">Admission Tracker</h1>
                            <p className="para">
                              Give you information about the school and helps
                              you keep track of these universities.
                            </p>
                          </div>
                        </div>
                        <div className="box-lower">
                          <h4 className="head">
                            Total Search result found (9683)
                          </h4>
                          <>
                            <Row>
                              <Col className="px-1 m-0">
                                <FormControl
                                  sx={{
                                    m: 1,
                                    width: "100%",
                                    border: 0,
                                    borderRadius: 1,
                                    backgroundColor: "#FFFFFF",
                                    color: "#0E1133",
                                    fontWeight: 700,
                                  }}
                                  size="small"
                                >
                                  {/* <InputLabel id="demo-multiple-checkbox-label">
                              Academic Session
                            </InputLabel> */}
                                  <Select
                                    multiple
                                    displayEmpty
                                    value={personName}
                                    onChange={handleChange}
                                    input={<OutlinedInput />}
                                    renderValue={(selected) => {
                                      if (selected.length === 0) {
                                        return <>Academic Session</>;
                                      }

                                      return selected.join(", ");
                                    }}
                                    MenuProps={MenuProps}
                                    inputProps={{
                                      "aria-label": "Without label",
                                    }}
                                  >
                                    {AcademicSession.map((name) => (
                                      <MenuItem key={name} value={name}>
                                        <Checkbox
                                          checked={
                                            personName.indexOf(name) > -1
                                          }
                                        />
                                        <ListItemText primary={name} />
                                      </MenuItem>
                                    ))}
                                  </Select>
                                </FormControl>
                              </Col>

                              <Col className="px-1 m-0">
                                <FormControl
                                  sx={{
                                    m: 1,
                                    width: "100%",
                                    border: 0,
                                    borderRadius: 1,
                                    backgroundColor: "#FFFFFF",
                                    color: "#0E1133",
                                    fontWeight: 700,
                                  }}
                                  size="small"
                                >
                                  {/* <InputLabel id="demo-multiple-checkbox-label">
                              Select Class
                            </InputLabel> */}
                                  <Select
                                    multiple
                                    displayEmpty
                                    value={selectClass}
                                    onChange={handleChangeSelectClass}
                                    input={<OutlinedInput />}
                                    renderValue={(selected) => {
                                      if (selected.length === 0) {
                                        return <>Select Class</>;
                                      }

                                      return selected.join(", ");
                                    }}
                                    MenuProps={MenuProps}
                                    inputProps={{
                                      "aria-label": "Without label",
                                    }}
                                  >
                                    {SelectClass.map((name) => (
                                      <MenuItem key={name} value={name}>
                                        <Checkbox
                                          checked={
                                            selectClass.indexOf(name) > -1
                                          }
                                        />
                                        <ListItemText primary={name} />
                                      </MenuItem>
                                    ))}
                                  </Select>
                                </FormControl>
                              </Col>

                              <Col className="px-1 m-0">
                                <FormControl
                                  sx={{
                                    m: 1,
                                    width: "100%",
                                    border: 0,
                                    borderRadius: 1,
                                    backgroundColor: "#FFFFFF",
                                    color: "#0E1133",
                                    fontWeight: 700,
                                  }}
                                  size="small"
                                >
                                  {/* <InputLabel id="demo-multiple-checkbox-label">
                              Admission Status
                            </InputLabel> */}
                                  <Select
                                    multiple
                                    displayEmpty
                                    value={selectClass}
                                    onChange={handleChangeAdmissionStatus}
                                    input={<OutlinedInput />}
                                    renderValue={(selected) => {
                                      if (selected.length === 0) {
                                        return <>Admission Status</>;
                                      }

                                      return selected.join(", ");
                                    }}
                                    MenuProps={MenuProps}
                                    inputProps={{
                                      "aria-label": "Without label",
                                    }}
                                  >
                                    {AdmissionStatus.map((name) => (
                                      <MenuItem key={name} value={name}>
                                        <Checkbox
                                          checked={
                                            admissionStatus.indexOf(name) > -1
                                          }
                                        />
                                        <ListItemText primary={name} />
                                      </MenuItem>
                                    ))}
                                  </Select>
                                </FormControl>
                              </Col>

                              <Col className="px-1 m-0">
                                <FormControl
                                  sx={{
                                    m: 1,
                                    width: "100%",
                                    border: 0,
                                    borderRadius: 1,
                                    backgroundColor: "#FFFFFF",
                                    color: "#0E1133",
                                    fontWeight: 700,
                                  }}
                                  size="small"
                                >
                                  {/* <InputLabel id="demo-multiple-checkbox-label">
                              Type of School
                            </InputLabel> */}
                                  <Select
                                    multiple
                                    displayEmpty
                                    value={typeOfSchool}
                                    onChange={handleChangeTypeOfSchool}
                                    input={<OutlinedInput />}
                                    renderValue={(selected) => {
                                      if (selected.length === 0) {
                                        return <>Type of School</>;
                                      }

                                      return selected.join(", ");
                                    }}
                                    MenuProps={MenuProps}
                                    inputProps={{
                                      "aria-label": "Without label",
                                    }}
                                  >
                                    {TypeOfSchool.map((name) => (
                                      <MenuItem key={name} value={name}>
                                        <Checkbox
                                          checked={
                                            typeOfSchool.indexOf(name) > -1
                                          }
                                        />
                                        <ListItemText primary={name} />
                                      </MenuItem>
                                    ))}
                                  </Select>
                                </FormControl>
                              </Col>
                              <Col className="px-1 m-0">
                              <Space direction="vertical" >
    <RangePicker />
  </Space>
                              </Col>
                            </Row>
                          </>

                          <div className="compareSchool">
                            <div className="schoolCard">
                              <div className="imgg">
                                <img
                                  src="/images/AdmissionTracker/s1.svg"
                                  alt=""
                                  className="img-fluid"
                                />
                              </div>
                              <div className="upper">
                                <p className="school">
                                  {" "}
                                  <span className="bold">
                                    Apeejay International School
                                  </span>{" "}
                                  - Surajpur-Kasna Road, Greater Noida
                                </p>
                                <FormGroup>
                                  <FormControlLabel
                                    control={<Checkbox defaultChecked />}
                                    label="COMPARE"
                                    className="check"
                                  />
                                </FormGroup>
                              </div>

                              <div className="middle">
                                <div className="wrap">
                                  <h4 className="head1">Class</h4>
                                  <h4 className="head2">Nursery</h4>
                                </div>
                                <div className="wrap">
                                  <h4 className="head1">Admission Status</h4>
                                  <h4 className="head2">Ongoing</h4>
                                </div>
                                <div className="wrap">
                                  <h4 className="head1">Type of School</h4>
                                  <h4 className="head2">CBSE</h4>
                                </div>
                                <div className="wrap">
                                  <h4 className="head1">Date: Start to End</h4>
                                  <h4 className="head2">31Jan23 - 31Aug23</h4>
                                </div>
                                <div className="wrap">
                                  <h4 className="head1">Ownership</h4>
                                  <h4 className="head2">Private</h4>
                                </div>
                              </div>

                              <div className="lower">
                                <div className="txt">
                                  <img
                                    src="/images/AdmissionTracker/tick.svg"
                                    alt=""
                                    className="img"
                                  />
                                  <span className="verify">verified</span>
                                  <span className="para">
                                    20+ parents applied in the last 15 days
                                  </span>
                                </div>
                                <div className="bttn">
                                  <span className="btn1">Notify Me</span>
                                  <span className="btn2">Apply Now</span>
                                </div>
                              </div>
                            </div>
                            <div className="schoolCard">
                              <div className="imgg">
                                <img
                                  src="/images/AdmissionTracker/s2.svg"
                                  alt=""
                                  className="img-fluid"
                                />
                              </div>
                              <div className="upper">
                                <p className="school">
                                  {" "}
                                  <span className="bold">
                                    Ramagya World School
                                  </span>{" "}
                                  - Delta 2, Greater Noida
                                </p>
                                <FormGroup>
                                  <FormControlLabel
                                    control={<Checkbox />}
                                    label="COMPARE"
                                    className="check"
                                  />
                                </FormGroup>
                              </div>

                              <div className="middle">
                                <div className="wrap">
                                  <h4 className="head1">Class</h4>
                                  <h4 className="head2">
                                    Pre-School (Nursery)
                                  </h4>
                                </div>
                                <div className="wrap">
                                  <h4 className="head1">Admission Status</h4>
                                  <h4 className="head2">Ongoing</h4>
                                </div>
                                <div className="wrap">
                                  <h4 className="head1">Type of School</h4>
                                  <h4 className="head2">CBSE</h4>
                                </div>
                                <div className="wrap">
                                  <h4 className="head1">Date: Start to End</h4>
                                  <h4 className="head2">31Jan23 - 31Aug23</h4>
                                </div>
                                <div className="wrap">
                                  <h4 className="head1">Ownership</h4>
                                  <h4 className="head2">Private</h4>
                                </div>
                              </div>

                              <div className="lower">
                                <div className="txt">
                                  <img
                                    src="/images/AdmissionTracker/tick.svg"
                                    alt=""
                                    className="img"
                                  />
                                  <span className="verify">verified</span>
                                  <span className="para">
                                    20+ parents applied in the last 15 days
                                  </span>
                                </div>
                                <div className="bttn">
                                  <span className="btn1">Notify Me</span>
                                  <span className="btn2">Apply Now</span>
                                </div>
                              </div>
                            </div>

                            <div className="schoolCard">
                              <div className="imgg">
                                <img
                                  src="/images/AdmissionTracker/s3.svg"
                                  alt=""
                                  className="img-fluid"
                                />
                              </div>
                              <div className="upper">
                                <p className="school">
                                  {" "}
                                  <span className="bold">
                                    Delhi World Public School{" "}
                                  </span>{" "}
                                  - Knowledge Park 3, Greater Noida
                                </p>
                                <FormGroup>
                                  <FormControlLabel
                                    control={<Checkbox />}
                                    label="COMPARE"
                                    className="check"
                                  />
                                </FormGroup>
                              </div>

                              <div className="middle">
                                <div className="wrap">
                                  <h4 className="head1">Class</h4>
                                  <h4 className="head2">Nursery</h4>
                                </div>
                                <div className="wrap">
                                  <h4 className="head1">Admission Status</h4>
                                  <h4 className="head2">Ongoing</h4>
                                </div>
                                <div className="wrap">
                                  <h4 className="head1">Type of School</h4>
                                  <h4 className="head2">CBSE</h4>
                                </div>
                                <div className="wrap">
                                  <h4 className="head1">Date: Start to End</h4>
                                  <h4 className="head2">31Jan23 - 31Aug23</h4>
                                </div>
                                <div className="wrap">
                                  <h4 className="head1">Ownership</h4>
                                  <h4 className="head2">Private</h4>
                                </div>
                              </div>

                              <div className="lower">
                                <div className="txt">
                                  <img
                                    src="/images/AdmissionTracker/tick.svg"
                                    alt=""
                                    className="img"
                                  />
                                  <span className="verify">verified</span>
                                  <span className="para">
                                    20+ parents applied in the last 15 days
                                  </span>
                                </div>
                                <div className="bttn">
                                  <span className="btn1">Notify Me</span>
                                  <span className="btn2">Apply Now</span>
                                </div>
                              </div>
                            </div>

                            <div className="schoolCard">
                              <div className="imgg">
                                <img
                                  src="/images/AdmissionTracker/s4.svg"
                                  alt=""
                                  className="img-fluid"
                                />
                              </div>
                              <div className="upper">
                                <p className="school">
                                  {" "}
                                  <span className="bold">
                                    Dharam Public School{" "}
                                  </span>{" "}
                                  - Knowledge Park 1, Greater Noida
                                </p>
                                <FormGroup>
                                  <FormControlLabel
                                    control={<Checkbox />}
                                    label="COMPARE"
                                    className="check"
                                  />
                                </FormGroup>
                              </div>

                              <div className="middle">
                                <div className="wrap">
                                  <h4 className="head1">Class</h4>
                                  <h4 className="head2">Nursery</h4>
                                </div>
                                <div className="wrap">
                                  <h4 className="head1">Admission Status</h4>
                                  <h4 className="head2">Ongoing</h4>
                                </div>
                                <div className="wrap">
                                  <h4 className="head1">Type of School</h4>
                                  <h4 className="head2">CBSE</h4>
                                </div>
                                <div className="wrap">
                                  <h4 className="head1">Date: Start to End</h4>
                                  <h4 className="head2">31Jan23 - 31Aug23</h4>
                                </div>
                                <div className="wrap">
                                  <h4 className="head1">Ownership</h4>
                                  <h4 className="head2">Private</h4>
                                </div>
                              </div>

                              <div className="lower">
                                <div className="txt">
                                  <img
                                    src="/images/AdmissionTracker/tick.svg"
                                    alt=""
                                    className="img"
                                  />
                                  <span className="verify">verified</span>
                                  <span className="para">
                                    20+ parents applied in the last 15 days
                                  </span>
                                </div>
                                <div className="bttn">
                                  <span className="btn1">Notify Me</span>
                                  <span className="btn2">Apply Now</span>
                                </div>
                              </div>
                            </div>

                            <div className="schoolCard">
                              <div className="imgg">
                                <img
                                  src="/images/AdmissionTracker/s4.svg"
                                  alt=""
                                  className="img-fluid"
                                />
                              </div>
                              <div className="upper">
                                <p className="school">
                                  {" "}
                                  <span className="bold">
                                    Dharam Public School{" "}
                                  </span>{" "}
                                  - Knowledge Park 1, Greater Noida
                                </p>
                                <FormGroup>
                                  <FormControlLabel
                                    control={<Checkbox />}
                                    label="COMPARE"
                                    className="check"
                                  />
                                </FormGroup>
                              </div>

                              <div className="middle">
                                <div className="wrap">
                                  <h4 className="head1">Class</h4>
                                  <h4 className="head2">Nursery</h4>
                                </div>
                                <div className="wrap">
                                  <h4 className="head1">Admission Status</h4>
                                  <h4 className="head2">Ongoing</h4>
                                </div>
                                <div className="wrap">
                                  <h4 className="head1">Type of School</h4>
                                  <h4 className="head2">CBSE</h4>
                                </div>
                                <div className="wrap">
                                  <h4 className="head1">Date: Start to End</h4>
                                  <h4 className="head2">31Jan23 - 31Aug23</h4>
                                </div>
                                <div className="wrap">
                                  <h4 className="head1">Ownership</h4>
                                  <h4 className="head2">Private</h4>
                                </div>
                              </div>

                              <div className="lower">
                                <div className="txt">
                                  <img
                                    src="/images/AdmissionTracker/tick.svg"
                                    alt=""
                                    className="img"
                                  />
                                  <span className="verify">verified</span>
                                  <span className="para">
                                    20+ parents applied in the last 15 days
                                  </span>
                                </div>
                                <div className="bttn">
                                  <span className="btn1">Notify Me</span>
                                  <span className="btn2">Apply Now</span>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </Col>
                    <Col lg={3}>
                      <Signup />
                      <RegisterRecieve />
                      <Subscribe />
                    </Col>
                  </Row>
                </Container>
              </div>
            </div>
          </div>
        </div>
      </PageLayout>
    </>
  );
};

export default AdmissionTracker;
