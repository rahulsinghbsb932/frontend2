import React from "react";

import Head from "next/head";
import Image from "next/image";
import PageLayout from "@components/PageLayout";
import { Container, Row, Col } from "react-bootstrap";
import Card from "react-bootstrap/Card";

const scholarship = () => {
  return (
    <div>
      <Head>
        <title>
          Study Abroad Scholarships | Graduate Scholarships | College
          Scholarships
        </title>
        <meta
          id="meta-description"
          name="description"
          content="Explore a variety of scholarship opportunities, including post matric scholarships, to help fund your education. Apply for study abroad scholarships, graduate scholarships, and college scholarships today."
        />
        <meta
          property="og:title"
          content=" Study Abroad Scholarships | Graduate Scholarships | College Scholarships"
        />
        <meta
          property="og:description"
          content="Explore a variety of scholarship opportunities, including post matric scholarships, to help fund your education. Apply for study abroad scholarships, graduate scholarships, and college scholarships today."
        />
        <meta
          property="twitter:title"
          content=" Study Abroad Scholarships | Graduate Scholarships | College Scholarships"
        />
        <meta
          property="twitter:description"
          content="Explore a variety of scholarship opportunities, including post matric scholarships, to help fund your education. Apply for study abroad scholarships, graduate scholarships, and college scholarships today."
        />
      </Head>

      <PageLayout>
        <div className="HeroSectionWrapper">
          <Container fluid>
            <Row>
              <Col md={12}>
                <div className="headingWhileLabel Montserrat">
                  <h1>Scholarship</h1>
                  <p>
                    Large and  aggregated scholarship websites can be helpful.
                    But a more targeted approach may produce better and more
                    useful results. With Verifykiya the process of searching for
                    scholarships can be less overwhelming. Verifykiya curates
                    and provides you with the information on scholarships based
                    on your profile, qualification and conditions.
                  </p>
                </div>
              </Col>
            </Row>
          </Container>

          <Container fluid className="mb-5">
            <Row>
              <Col md={3} className="rowCenter mt-5">
                <a className="cardanchor" href="#Curated-List">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Scholarship/Curated List.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Scholarship/Curated List.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Curated List
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={3} className="rowCenter mt-5">
                <a className="cardanchor" href="#Single-Click-Apply">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Scholarship/Single Click Apply.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Scholarship/Single Click Apply.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Single Click Apply
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={3} className="rowCenter mt-5">
                <a className="cardanchor" href="#Track-Status">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Scholarship/Track Status.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Scholarship/Track Status.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Track Status
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
              <Col md={3} className="rowCenter mt-5">
                <a className="cardanchor" href="#Real-Time-Alerts">
                  <Card className="CardSol">
                    {/* <Card.Img
                      className="CardImage img-fluid"
                      src={
                        require("../../assets/img/Services/icon/Scholarship/Real Time Alerts.svg")
                          .default
                      }
                      alt=""
                    /> */}

                    <Image
                      src="/images/Services/icon/Scholarship/Real Time Alerts.svg"
                      height={125}
                      width={125}
                    />
                    <Card.Body className="CardBody">
                      <Card.Title className="CardTitle">
                        Real Time Alerts
                      </Card.Title>
                    </Card.Body>
                  </Card>
                </a>
              </Col>
            </Row>
          </Container>
        </div>

        <section id="Curated-List">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Scholarship/Curated List.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Scholarship/Curated List.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Scholarship/Curated List.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Scholarship/Curated List.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">Curated List</h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Large and aggregated scholarship websites can be helpful.
                      But a more targeted approach may produce better and more
                      useful results. With Verifykiya the process of searching
                      for scholarships can be less overwhelming. Verifykiya
                      curates and provides you with the information on
                      scholarships based on your profile, qualification and
                      conditions. Find the relevant scholarship amongst our
                      curated list as per your profile thus increasing the
                      chances of success.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section id="Single-Click-Apply">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={9} lg={12} className="pt-3 p-5 order-xl-1 order-2">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Scholarship/Single Click Apply.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Scholarship/Single Click Apply.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Single Click Apply
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Receiving a scholarship is an honor. It's a great way to
                      pay for some or even all of your educational expenses.
                      Applying to scholarships at our platform Verifykiya is
                      worth the effort and can benefit you during your student
                      years and beyond. Verifykiya provides you platform from
                      wherein you can apply for multiple scholarships by a
                      single click only thereby saving time and effort.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
                <Col xl={3} lg={12} className="order-xl-2 order-1">
                  <div className="img">
                   

                    <Image
                      src="/images/Services/images/Scholarship/Single Click Apply.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>

        <section id="Track-Status">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Scholarship/Track Status.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}

                    <Image
                      src="/images/Services/images/Scholarship/Track Status.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Scholarship/Track Status.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}

                      <Image
                        src="/images/Services/icon/Scholarship/Track Status.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">Track Status</h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Tracking status of your various application is a tedious
                      task and requires a lot of effort and time. Verifykiya
                      gives you a feature to keep a track of all the
                      scholarships you have applied for and monitor the status
                      of each application effortlessly. Verifykiya tracks the
                      various stages of the application and provides you updates
                      on your dashboard and via notifications if opted soi that
                      you do not lose out on any information and timely submit
                      any additional details if required by the provider.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section id="Real-Time-Alerts" className="mb-5">
          <div className="solution">
            <Container fluid>
              <Row className="bg">
                <Col xl={3} lg={12}>
                  <div className="img">
                    {/* <img
                      src={
                        require("../../assets/img/Services/images/Scholarship/Real Time Alerts.svg")
                          .default
                      }
                      alt=""
                      className="img-fluid w-80"
                    /> */}
                    <Image
                      src="/images/Services/images/Scholarship/Real Time Alerts.svg"
                      height={480}
                      width={300}
                    />
                  </div>
                </Col>
                <Col xl={9} lg={12} className="pt-3 p-5">
                  <div className="d-flex position">
                    <div className="icon">
                      {/* <img
                        src={
                          require("../../assets/img/Services/icon/Scholarship/Real Time Alerts.svg")
                            .default
                        }
                        alt=""
                        className="img-fluid"
                      /> */}
                      <Image
                        src="/images/Services/icon/Scholarship/Real Time Alerts.svg"
                        height={125}
                        width={125}
                      />
                    </div>
                    <div className="solutioseciconTop ">
                      <h1 class="ourSolutionSectionheading">
                        Real Time Alerts
                      </h1>
                    </div>
                  </div>

                  <div className="solutionSeciconText">
                    <p>
                      Verifykiya enables you to receive notifications in
                      real-time updates on your scholarship applications so you
                      can take immediate action . With real-time alerts, you can
                      quickly respond to any requests for additional information
                      or follow-up. Stay organized and on top of your
                      scholarship application process increasing your chances of
                      success in securing scholarships.
                    </p>
                    <div className="space"></div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
      </PageLayout>
    </div>
  );
};

export default scholarship;
