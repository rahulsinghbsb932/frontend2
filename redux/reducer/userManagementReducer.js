import { getUserData } from "@components/TokenServices";

const initialState = {
  userData: {},
};

export const ADD_USER_DATA = "ADD_USER_DATA";

const userManagementReducer = (state = initialState, action) => {
  let user = getUserData();
  switch (action.type) {
    case ADD_USER_DATA:
      let data = action.item;
      console.log(action, "========action");
      return { ...state, userData: { ...data } };

    default:
      return { ...state };
  }
};

export default userManagementReducer;
