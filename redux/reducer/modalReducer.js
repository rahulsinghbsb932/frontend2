const initialState = {
  isRegisterModal: false,
};

export const REGISTER_MODAL = "REGISTER_MODAL";

const modalReducer = (state = initialState, action) => {
  switch (action.type) {
    case REGISTER_MODAL:
      return { ...state, isRegisterModal: action.status };

    default:
      return { ...state };
  }
};

export default modalReducer;
