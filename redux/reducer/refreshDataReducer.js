import { getUserData } from "@components/TokenServices";

const initialState = {
  userData: {},
  companyProfile: {},
};

export const ADD_USER_DATA = "ADD_USER_DATA";
export const COMPANY_DATA = "COMPANY_DATA";

const refreshDataReducer = (state = initialState, action) => {
  let user = getUserData();
  switch (action.type) {
    case ADD_USER_DATA:
      let data = action.item;
      return { ...state, userData: { ...data } };

    case COMPANY_DATA:
      let data1 = action.item;
      return { ...state, companyProfile: { ...data1 } };

    default:
      return { ...state };
  }
};

export default refreshDataReducer;
