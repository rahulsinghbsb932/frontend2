import { getUserData } from "@components/TokenServices";

const initialState = {
  userDetail: {},
};

export const USER_DETAIL = "USER_DETAIL";

const userReducer = (state = initialState, action) => {
  let user = getUserData();
  switch (action.type) {
    case USER_DETAIL:
      return { ...state, ...user };

    default:
      return { ...state };
  }
};

export default userReducer;
