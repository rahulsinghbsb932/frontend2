import { combineReducers } from "redux";
import refreshDataReducer from "./reducer/refreshDataReducer";
import userReducer from "./reducer/userReducer";
import modalReducer from "./reducer/modalReducer";

const rootReducer = combineReducers({
  userData: userReducer,
  refreshDataReducer: refreshDataReducer,
  modalReducer: modalReducer,
});

export default rootReducer;
